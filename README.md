# Gighouse

This application was generated using JHipster 6.10.4, you can find documentation and help at [https://www.jhipster.tech/documentation-archive/v6.10.4](https://www.jhipster.tech/documentation-archive/v6.10.4).

The GUI app was generated with angular-cli, but some functionality was taken from JHipster Angular App.
##Login

See lastpass

## Deployment

1. Install Heroku CLI: https://devcenter.heroku.com/articles/heroku-cli

2. Build and deploy:
```
./mvnw -Pprod clean verify

heroku deploy:jar target/*.jar -a gighouse-web-dev
```
test
```
heroku deploy:jar target/*.jar -a gighouse-web-test
```
prod
```
heroku deploy:jar target/*.jar -a gighouse-web
```
## Environments

- dev: https://gighouse-web-dev.herokuapp.com/
- test: https://gighouse-web-test.herokuapp.com/
- prod: https://opdrachtgevers.gighouse.be


## Development

Before you can build this project, you must install and configure the following dependencies on your machine:

1. [Node.js][]: We use Node to run a development web server and build the project.
   Depending on your system, you can install Node either from source or as a pre-packaged bundle.

After installing Node, you should be able to run the following command to install development tools.
You will only need to run this command when dependencies change in [package.json](package.json).

```
npm install
```

We use npm scripts and [Webpack][] as our build system.

Run the following commands in two separate terminals to create a blissful development experience where your browser
auto-refreshes when files change on your hard drive.

The current setup has the test enviroment of Gighouse linked to the application-dev.yml
These values have been hardcoded and are not introduced through parameters. To run with this setup use the -no-liquibase command

```
mvn spring-boot:run -Dspring.profiles.active=dev,no-liquibase

./mvnw

npm start
```

Npm is also used to manage CSS and JavaScript dependencies used in this application. You can upgrade dependencies by
specifying a newer version in [package.json](package.json). You can also run `npm update` and `npm install` to manage dependencies.
Add the `help` flag on any command to see how you can use it. For example, `npm help update`.

The `npm run` command will list all of the scripts available to run for this project.

## OAuth 2.0 / OpenID Connect

Congratulations! You've selected an excellent way to secure your JHipster application. If you're not sure what OAuth and OpenID Connect (OIDC) are, please see [What the Heck is OAuth?](https://developer.okta.com/blog/2017/06/21/what-the-heck-is-oauth)

To log in to your app, you'll need to have [Keycloak](https://keycloak.org) up and running. The JHipster Team has created a Docker container for you that has the default users and roles. Start Keycloak using the following command.

```
docker-compose -f src/main/docker/keycloak.yml up
```

The security settings in `src/main/resources/config/application.yml` are configured for this image.

```yaml
spring:
  ...
  security:
    oauth2:
      client:
        provider:
          oidc:
            issuer-uri: http://localhost:9080/auth/realms/jhipster
        registration:
          oidc:
            client-id: web_app
            client-secret: web_app
```

### Okta

If you'd like to use Okta instead of Keycloak, you'll need to change a few things. First, you'll need to create a free developer account at <https://developer.okta.com/signup/>. After doing so, you'll get your own Okta domain, that has a name like `https://dev-123456.okta.com`.

Modify `src/main/resources/config/application.yml` to use your Okta settings.

```yaml
spring:
  ...
  security:
    oauth2:
      client:
        provider:
          oidc:
            issuer-uri: https://{yourOktaDomain}/oauth2/default
        registration:
          oidc:
            client-id: {clientId}
            client-secret: {clientSecret}
security:
```

Create an OIDC App in Okta to get a `{clientId}` and `{clientSecret}`. To do this, log in to your Okta Developer account and navigate to **Applications** > **Add Application**. Click **Web** and click the **Next** button. Give the app a name you’ll remember, specify `http://localhost:8080` as a Base URI, and `http://localhost:8080dp/login/oauth2/code/oidc` as a Login Redirect URI. Click **Done**, then Edit and add `http://localhost:8080` as a Logout redirect URI. Copy and paste the client ID and secret into your `application.yml` file.

Create a `ROLE_ADMIN` and `ROLE_USER` group and add users into them. Modify e2e tests to use this account when running integration tests. You'll need to change credentials in `src/test/javascript/e2e/account/account.spec.ts` and `src/test/javascript/e2e/admin/administration.spec.ts`.

Navigate to **API** > **Authorization Servers**, click the **Authorization Servers** tab and edit the default one. Click the **Claims** tab and **Add Claim**. Name it "groups", and include it in the ID Token. Set the value type to "Groups" and set the filter to be a Regex of `.*`.

After making these changes, you should be good to go! If you have any issues, please post them to [Stack Overflow](https://stackoverflow.com/questions/tagged/jhipster). Make sure to tag your question with "jhipster" and "okta".

### Managing dependencies

For example, to add [Leaflet][] library as a runtime dependency of your application, you would run following command:

```
npm install --save --save-exact leaflet
```

To benefit from TypeScript type definitions from [DefinitelyTyped][] repository in development, you would run following command:

```
npm install --save-dev --save-exact @types/leaflet
```

Then you would import the JS and CSS files specified in library's installation instructions so that [Webpack][] knows about them:
Edit [src/main/webapp/app/vendor.ts](src/main/webapp/app/vendor.ts) file:

```
import 'leaflet/dist/leaflet.js';
```

Edit [src/main/webapp/content/scss/vendor.scss](src/main/webapp/content/scss/vendor.scss) file:

```
@import '~leaflet/dist/leaflet.css';
```

Note: There are still a few other things remaining to do for Leaflet that we won't detail here.

For further instructions on how to develop with JHipster, have a look at [Using JHipster in development][].

### Using Angular CLI

You can also use [Angular CLI][] to generate some custom client code.

For example, the following command:

```
ng generate component my-component
```

will generate few files:

```
create src/main/webapp/app/my-component/my-component.component.html
create src/main/webapp/app/my-component/my-component.component.ts
update src/main/webapp/app/app.module.ts
```

### Handling i18n files in Angular project

In Angular app i18n json files are located in ng-app/src/assets/i18n in directories named based on 
language key, e.g. ng-app/src/assets/i18n/en/home.json | admin.json | global.json.

During a compilation a custom Webpack task merges those files into a single file per language,
e.g. en-merged.json. When using ng dev server the generated file is kept in-memory and when building
the app the generated file is outputed in the "output" directory.

Merge task is defined in ng-app/custom-webpack.config.js and that file is referenced in angular.json
to make Angular CLI aware of it.
To be able to run custom webpack config we use custom angular builders:

```
"build": {
    "builder": "@angular-builders/custom-webpack:browser"
}

"serve": {
    "builder": "@angular-builders/custom-webpack:dev-server"
}
```

For loading correct files in GUI we have configured TranslateHttpLoader in such a way that it loads
files from path: assets/i18n/[language]-merged.json.

### Custom SVG Icons in Angular project

#### Generate SVG Icons:
- Download an SVG icon to ```src/assets/icons/svg``` in Angular project
- Rename the file to something simple and meaningful. Use lower kebab case ("kebab-case"). E.g. arrow-left.svg
- Make sure that svg element has width, height and viewBox attributes (important for scaling in app with font-size)
- If there is extra styling inside svg defined with ```<defs><style>...</style></defs>``` and svg elements contain a class like ```class="a"```, replace those with a more appropriate alternative:
	- If svg element contains a style definition like:

		```<defs><style>.a{fill:#201545;stroke:#201545;stroke-width:0.3px;}</style></defs>```

	  remove it and apply relevant styles from there to the <svg> element like this:
	  
	  ```<svg width="14.64" height="16.478" viewBox="0 0 14.64 16.478" fill="currentColor" stroke="currentColor" stroke-width="0.3px">```

	  where ```fill="currentColor"``` and ```stroke="currentColor"``` are important because the icon will use the color of the parent (or text next to the icon) and
	  ```stroke-width="0.3px"``` is important because the icon depends on that setting for thickness.
	  Sometimes original style definition only has fill color or only stroke color so we also only apply that one attribute to the <svg> element to keep the original style.

	- If svg elements (path, line, etc.) contain a class like class="a", class="b", remove those to avoid strange side-effects

- Open a terminal at a directory with package.json and run ```"npm run generate-icons"```
- Now your icons are generated and ready for use in Typescript!
- Import icons to array in ```core/icons/svg-icons.ts``` to make them available in Angular through the angular-svg-icon library
- In HTML use the icon as ```<svg-icon icon="...name of the icon..."></svg-icon>``` 
  E.g. ```<svg-icon icon="arrow-left"></svg-icon>```
  - You can also reference an icon like this ```[icon]="'arrow-left'"```

#### Tools used to generate and use the icons:
- svg-to-ts for generating a .ts file with icons from svg files
	- For example, from ```assets/icons/svg/arrow-left.svg | bell.svg``` generates ```assets/icons/app-icon.model.ts``` which contains tree-shakeable icons as svg, type and model for usage in Typescript.
	- Config for the svg-to-ts is defined in package.json in ```"svg-to-ts": {...}```

- AngularSvgIconModule to be able to use icons in templates as ```<svg-icon icon="...name of the icon..."></svg-icon>```
	- Also provides a registry (SvgIconRegistryService) for loading particular icons that will be used. Icons not added to registry will be ignored during ts compilation by tree shaking.

## Building for production

### Packaging as jar

To build the final jar and optimize the gighouse application for production, run:

```

./mvnw -Pprod clean verify


```

This will concatenate and minify the client CSS and JavaScript files. It will also modify `index.html` so it references these new files.
To ensure everything worked, run:

```

java -jar target/*.jar


```

Then navigate to [http://localhost:8080](http://localhost:8080) in your browser.

Refer to [Using JHipster in production][] for more details.

### Packaging as war

To package your application as a war in order to deploy it to an application server, run:

```

./mvnw -Pprod,war clean verify


```

## Testing

To launch your application's tests, run:

```
./mvnw verify
```

### Client tests

Unit tests are run by [Jest][] and written with [Jasmine][]. They're located in [src/test/javascript/](src/test/javascript/) and can be run with:

```
npm test
```

For more information, refer to the [Running tests page][].

### Code quality

Sonar is used to analyse code quality. You can start a local Sonar server (accessible on http://localhost:9001) with:

```
docker-compose -f src/main/docker/sonar.yml up -d
```

You can run a Sonar analysis with using the [sonar-scanner](https://docs.sonarqube.org/display/SCAN/Analyzing+with+SonarQube+Scanner) or by using the maven plugin.

Then, run a Sonar analysis:

```
./mvnw -Pprod clean verify sonar:sonar
```

If you need to re-run the Sonar phase, please be sure to specify at least the `initialize` phase since Sonar properties are loaded from the sonar-project.properties file.

```
./mvnw initialize sonar:sonar
```

For more information, refer to the [Code quality page][].

## Using Docker to simplify development (optional)

You can use Docker to improve your JHipster development experience. A number of docker-compose configuration are available in the [src/main/docker](src/main/docker) folder to launch required third party services.

For example, to start a postgresql database in a docker container, run:

```
docker-compose -f src/main/docker/postgresql.yml up -d
```

To stop it and remove the container, run:

```
docker-compose -f src/main/docker/postgresql.yml down
```

You can also fully dockerize your application and all the services that it depends on.
To achieve this, first build a docker image of your app by running:

```
./mvnw -Pprod verify jib:dockerBuild
```

Then run:

```
docker-compose -f src/main/docker/app.yml up -d
```

For more information refer to [Using Docker and Docker-Compose][], this page also contains information on the docker-compose sub-generator (`jhipster docker-compose`), which is able to generate docker configurations for one or several JHipster applications.

## Continuous Integration (optional)

To configure CI for your project, run the ci-cd sub-generator (`jhipster ci-cd`), this will let you generate configuration files for a number of Continuous Integration systems. Consult the [Setting up Continuous Integration][] page for more information.

[jhipster homepage and latest documentation]: https://www.jhipster.tech
[jhipster 6.10.4 archive]: https://www.jhipster.tech/documentation-archive/v6.10.4
[using jhipster in development]: https://www.jhipster.tech/documentation-archive/v6.10.4/development/
[using docker and docker-compose]: https://www.jhipster.tech/documentation-archive/v6.10.4/docker-compose
[using jhipster in production]: https://www.jhipster.tech/documentation-archive/v6.10.4/production/
[running tests page]: https://www.jhipster.tech/documentation-archive/v6.10.4/running-tests/
[code quality page]: https://www.jhipster.tech/documentation-archive/v6.10.4/code-quality/
[setting up continuous integration]: https://www.jhipster.tech/documentation-archive/v6.10.4/setting-up-ci/
[node.js]: https://nodejs.org/
[yarn]: https://yarnpkg.org/
[webpack]: https://webpack.github.io/
[angular cli]: https://cli.angular.io/
[browsersync]: https://www.browsersync.io/
[jest]: https://facebook.github.io/jest/
[jasmine]: https://jasmine.github.io/2.0/introduction.html
[protractor]: https://angular.github.io/protractor/
[leaflet]: https://leafletjs.com/
[definitelytyped]: https://definitelytyped.org/
