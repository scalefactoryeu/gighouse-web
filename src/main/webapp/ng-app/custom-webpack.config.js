// Custom Webpack config ran by Angular CLI while making a build or running a dev server.
// This file is referenced in angular.json to make Angular CLI aware of it. To be able to run custom webpack config we use
// custom angular builders in angular.json: "@angular-builders/custom-webpack:browser" and "@angular-builders/custom-webpack:dev-server"

const MergeJsonWebpackPlugin = require("merge-jsons-webpack-plugin");

module.exports = {
  plugins: [
    new MergeJsonWebpackPlugin({
      output: {
        groupBy: [
          // Merges i18n json files per language into one file (e.g. en-merged.json) and outputs generated files in directory defined in
          // "outputPath" in angular.json
          { pattern: "src/assets/i18n/en/*.json", fileName: "./assets/i18n/en-merged.json" },
          { pattern: "src/assets/i18n/nl/*.json", fileName: "./assets/i18n/nl-merged.json" },
          { pattern: "src/assets/i18n/fr/*.json", fileName: "./assets/i18n/fr-merged.json" }
        ]
      }
    })
  ]
};
