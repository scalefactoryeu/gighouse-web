import {Component, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {BehaviorSubject, Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {TimeSheetsService} from "../../features/administration/timesheets/timesheets.service";
import {ErrorHandlingService} from "../../core/error-handling/error-handling.service";
import {TranslateService} from "@ngx-translate/core";

export enum RejectFields {
  REJECT_MESSAGE = 'rejectMessageControl',
}


@Component({
  selector: 'app-modal-confirm-reject',
  templateUrl: './modal-confirm-reject.component.html'
})

export class ModalConfirmRejectComponent implements OnInit {

  form: FormGroup;

  title: string;
  body: string;
  confirmBtnText: string;
  rejectText: string;
  confirmMethod: () => Observable<any>;

  timeEntryIds: string[];

  actionRunning$: BehaviorSubject<boolean> = new BehaviorSubject(false);
  failedText$ = new BehaviorSubject<string>(null);

  constructor(public activeModal: NgbActiveModal,
              private formBuilder: FormBuilder,
              private timeSheetsService: TimeSheetsService,
              private errorHandlingService: ErrorHandlingService,
              private translate: TranslateService) {
  }

  ngOnInit() {

    const rejectMessageControl = this.formBuilder.control(
      this.rejectText ? this.rejectText : "",
      [Validators.required, Validators.maxLength(100)]
    );

    this.form = this.formBuilder.group({
      [RejectFields.REJECT_MESSAGE]: rejectMessageControl
    });
  }

  confirm() {
    console.log('confirm');
    if (this.form.valid) {
      this.rejectText = this.form.get('rejectMessageControl').value;

      this.timeSheetsService.reject(this.timeEntryIds, this.rejectText)
        .pipe(
          catchError((error) => {
            this.errorHandlingService.openErrorModal(this.translate.instant('timesheets.failedToApproveTimeSheet'));
            return throwError(error);
          })
        ).subscribe(() => {
        this.activeModal.close();
      });
    } else {
      console.log('not valid');
    }
    // if (this.confirmMethod) {
    //   this.confirmAsync();
    // } else {
    //   this.activeModal.close();
    // }
  }

  private confirmAsync() {


    this.actionRunning$.next(true);

    this.confirmMethod()
      .pipe(
        catchError((error) => {
          this.actionRunning$.next(false);
          this.failedText$.next(error);
          throw error;
        })
      )
      .subscribe(() => {
        this.activeModal.close();
      });
  }

}
