import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalConfirmComponent } from './modal-confirm/modal-confirm.component';
import { ModalConfirmRemoveComponent } from './modal-confirm-remove/modal-confirm-remove.component';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap/modal/modal-ref';
import { Observable } from 'rxjs';
import { ModalUserDetailsComponent } from './modal-user-details/modal-user-details.component';
import { ModalCompanyDetailsComponent } from './modal-company-details/modal-company-details.component';
import { CompanyProfile, UserProfile } from '../features/profile/profile.model';
import { CompanyFormat } from '../core/models/company-format';
import { State } from '../core/models/states';
import {ModalConfirmRejectComponent} from "./modal-confirm-reject/modal-confirm-reject.component";

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  constructor(private ngbModal: NgbModal) {

  }

  openConfirmModal(body: string, confirmMethod: () => Promise<void>, confirmBtnText?: string): ModalConfirmComponent {
    const modalRef = this.ngbModal.open(ModalConfirmComponent, { windowClass: 'confirmation-modal general-confirmation-modal', backdrop: 'static' });
    const modal = modalRef.componentInstance as ModalConfirmComponent;

    modal.body = body;
    modal.confirmBtnText = confirmBtnText;
    modal.confirmMethod = confirmMethod;

    return modal;
  }

  openCompanyDetailsModal(companyProfile: CompanyProfile, confirmMethod: () => Promise<void>, companyFormats: CompanyFormat[], states: State[], confirmBtnText?: string): ModalCompanyDetailsComponent{
    const modalRef = this.ngbModal.open(ModalCompanyDetailsComponent, { windowClass: 'user-details-modal', backdrop: 'static' });
    const modal = modalRef.componentInstance as ModalCompanyDetailsComponent;

    modal.companyProfile = companyProfile;
    modal.confirmBtnText = confirmBtnText;
    modal.companyFormats = companyFormats;
    modal.states = states;
    modal.confirmMethod = confirmMethod;

    return modal;
  }

  openUserDetailsModal(userProfile: UserProfile, confirmMethod: () => Promise<void>, states: State[], confirmBtnText?: string): ModalUserDetailsComponent {
    const modalRef = this.ngbModal.open(ModalUserDetailsComponent, { windowClass: 'user-details-modal', backdrop: 'static' });
    const modal = modalRef.componentInstance as ModalUserDetailsComponent;

    modal.userProfile = userProfile;
    modal.confirmBtnText = confirmBtnText;
    modal.states = states;
    modal.confirmMethod = confirmMethod;

    return modal;
  }

  openConfirmRemoveModal(
    title: string,
    body: string,
    confirmBtnText: string,
    confirmMethod?: () => Observable<any>): NgbModalRef {

    const modalRef = this.ngbModal.open(ModalConfirmRemoveComponent, {
      windowClass: 'confirmation-modal remove-confirmation-modal',
      backdrop: 'static'
    });

    const modal = modalRef.componentInstance as ModalConfirmRemoveComponent;

    modal.title = title;
    modal.body = body;
    modal.confirmBtnText = confirmBtnText;
    modal.confirmMethod = confirmMethod;

    return modalRef;
  }


  openConfirmRejectModal(
    title: string,
    body: string,
    timeEntryIds: string[],
    confirmBtnText: string,
    confirmMethod?: () => Observable<any>): NgbModalRef {

    const modalRef = this.ngbModal.open(ModalConfirmRejectComponent, {
      windowClass: 'confirmation-modal reject-confirmation-modal',
      backdrop: 'static'
    });

    const modal = modalRef.componentInstance as ModalConfirmRejectComponent;

    modal.title = title;
    modal.body = body;
    modal.confirmBtnText = confirmBtnText;
    modal.confirmMethod = confirmMethod;
    modal.timeEntryIds = timeEntryIds;

    return modalRef;
  }
}
