import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { BehaviorSubject, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Component({
  selector: 'app-modal-confirm-remove',
  templateUrl: './modal-confirm-remove.component.html'
})
export class ModalConfirmRemoveComponent implements OnInit {

  title: string;
  body: string;
  confirmBtnText: string;
  confirmMethod: () => Observable<any>;

  actionRunning$: BehaviorSubject<boolean> = new BehaviorSubject(false);
  failedText$ = new BehaviorSubject<string>(null);

  constructor(public activeModal: NgbActiveModal) {
  }

  ngOnInit() {
  }

  confirm() {
    if (this.confirmMethod) {
      this.confirmAsync();
    } else {
      this.activeModal.close();
    }
  }

  private confirmAsync() {
    this.actionRunning$.next(true);

    this.confirmMethod()
      .pipe(
        catchError((error) => {
          this.actionRunning$.next(false);
          this.failedText$.next(error);
          throw error;
        })
      )
      .subscribe(() => {
        this.activeModal.close();
      });
  }

}
