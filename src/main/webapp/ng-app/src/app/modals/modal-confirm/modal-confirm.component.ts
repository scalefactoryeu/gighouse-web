import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-modal-confirm',
  templateUrl: './modal-confirm.component.html'
})
export class ModalConfirmComponent implements OnInit {

  body: string;
  confirmBtnText: string;
  confirmMethod: () => Promise<void>;

  actionRunning$: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor(public activeModal: NgbActiveModal) {
  }

  ngOnInit() {
  }

  confirm() {
    this.actionRunning$.next(true);

    this.confirmMethod()
      .then(() => {
        this.activeModal.close();
      })
      .catch(() => {
        this.actionRunning$.next(false);
      });
  }

}
