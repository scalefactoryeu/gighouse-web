import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CanDeactivateProjectFormReason } from '../../features/project/project-form/project-form.model';

@Component({
  selector: 'app-modal-confirm-leave-page',
  templateUrl: './modal-confirm-leave-page.component.html'
})
export class ModalConfirmLeavePageComponent implements OnInit {

  reason: CanDeactivateProjectFormReason;

  canDeactivateProjectFormReasons = CanDeactivateProjectFormReason;

  constructor(public activeModal: NgbActiveModal) {
  }

  ngOnInit() {
  }

}
