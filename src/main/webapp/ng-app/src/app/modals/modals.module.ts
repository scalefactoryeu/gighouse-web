import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { ModalConfirmComponent } from './modal-confirm/modal-confirm.component';
import { ModalConfirmRemoveComponent } from './modal-confirm-remove/modal-confirm-remove.component';
import { ModalConfirmLeavePageComponent } from './modal-confirm-leave-page/modal-confirm-leave-page.component';
import { ModalUserDetailsComponent } from './modal-user-details/modal-user-details.component';
import { ModalCompanyDetailsComponent } from './modal-company-details/modal-company-details.component';
import {ModalConfirmRejectComponent} from "./modal-confirm-reject/modal-confirm-reject.component";


@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [
    ModalConfirmComponent,
    ModalConfirmRemoveComponent,
    ModalConfirmLeavePageComponent,
    ModalUserDetailsComponent,
    ModalCompanyDetailsComponent,
    ModalConfirmRejectComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ModalsModule {}
