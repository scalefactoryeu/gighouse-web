import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { BehaviorSubject, throwError } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { IUserProfileBackend, UserProfile } from 'src/app/features/profile/profile.model';
import { State } from 'src/app/core/models/states';
import { ProfileService } from 'src/app/features/profile/profile.service';
import { catchError } from 'rxjs/operators';
import { ErrorHandlingService } from 'src/app/core/error-handling/error-handling.service';
import { Router } from '@angular/router';
import {AppValidators} from '../../shared/util/validators';

export enum UserProfileFormFields {
  NAME = 'userProfileNameControl',
  SURNAME = 'userProfileSurnameControl',
  STREET = 'userProfileStreetControl',
  POSTAL_CODE = 'userProfilePostalCodeControl',
  CITY = 'userProfileCityControl',
  STATE = 'userProfileStateControl',
  EMAIL = 'userProfileEmailControl',
  PHONE = 'userProfilePhoneControl'
}

@Component({
  selector: 'app-modal-user-details',
  templateUrl: './modal-user-details.component.html'
})
export class ModalUserDetailsComponent implements OnInit {

  form: FormGroup;

  states: State[];

  userProfile: UserProfile;
  confirmBtnText: string;
  confirmMethod: () => Promise<void>;

  actionRunning$: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor(public activeModal: NgbActiveModal,
    private formBuilder: FormBuilder,
    private translate: TranslateService,
    private profileService: ProfileService,
    private errorHandlingService: ErrorHandlingService,
    private router: Router) {
  }

  ngOnInit() {

    const userProfileNameControl = this.formBuilder.control(
      this.userProfile ? this.userProfile.firstName : "",
      [Validators.required, Validators.maxLength(100)]
    );

    const userProfileSurnameControl = this.formBuilder.control(
      this.userProfile ? this.userProfile.lastName : "",
      [Validators.required, Validators.maxLength(100)]
    );

    const userProfileStreetControl = this.formBuilder.control(
      this.userProfile ? this.userProfile.street : "",
      [Validators.required, Validators.maxLength(100)]
    );

    const userProfilePostalCodeControl = this.formBuilder.control(
      this.userProfile ? this.userProfile.postalCode : "",
      [Validators.required, Validators.maxLength(7)]
    );

    const userProfileCityControl = this.formBuilder.control(
      this.userProfile ? this.userProfile.city : "",
      [Validators.required, Validators.maxLength(100)]
    );

    const userProfileStateControl = this.formBuilder.control(
      this.userProfile.state ? this.userProfile.state : null,
      [Validators.required]
    );

    const userProfileEmailControl = this.formBuilder.control(
      this.userProfile ? this.userProfile.email : "",
      [Validators.required, Validators.maxLength(100)]
    );

    const userProfilePhoneControl = this.formBuilder.control(
      this.userProfile ? this.userProfile.phone : "",
      [Validators.required, AppValidators.phoneNumberValidator(), Validators.maxLength(100)]
    );

    this.form = this.formBuilder.group({
      [UserProfileFormFields.NAME]: userProfileNameControl,
      [UserProfileFormFields.SURNAME]: userProfileSurnameControl,
      [UserProfileFormFields.STREET]: userProfileStreetControl,
      [UserProfileFormFields.POSTAL_CODE]: userProfilePostalCodeControl,
      [UserProfileFormFields.CITY]: userProfileCityControl,
      [UserProfileFormFields.STATE]: userProfileStateControl,
      [UserProfileFormFields.EMAIL]: userProfileEmailControl,
      [UserProfileFormFields.PHONE]: userProfilePhoneControl
    });

  }


  confirm() {
    if (this.form.valid) {
      this.userProfile.firstName = this.form.get('userProfileNameControl').value;
      this.userProfile.lastName = this.form.get('userProfileSurnameControl').value;
      this.userProfile.street = this.form.get('userProfileStreetControl').value;
      this.userProfile.postalCode = this.form.get('userProfilePostalCodeControl').value;
      this.userProfile.city = this.form.get('userProfileCityControl').value;
      this.userProfile.state = this.form.get('userProfileStateControl').value;
      this.userProfile.email = this.form.get('userProfileEmailControl').value;
      this.userProfile.phone = this.form.get('userProfilePhoneControl').value;

      this.profileService.updateUserProfile(this.userProfile)
        .pipe(
          catchError(error => {
            this.actionRunning$.next(false);
            this.errorHandlingService.openErrorModal(this.translate.instant('profiles.saveFailed'));
            return throwError(error);
          })
        )
        .subscribe(() => {
          //window.location.reload();
          this.activeModal.close();
          this.router.navigate(['/profiles'])
        });
    }
    else {
      this.validateAllFormFields(this.form);
    }
  }


  /* Control Getters */

  getUserProfileNameControl(): FormControl {
    return this.form.get(UserProfileFormFields.NAME) as FormControl;
  }

  getUserProfileSurNameControl(): FormControl {
    return this.form.get(UserProfileFormFields.SURNAME) as FormControl;
  }

  getUserProfileStreetControl(): FormControl {
    return this.form.get(UserProfileFormFields.STREET) as FormControl;
  }

  getUserProfilePostalCodeControl(): FormControl {
    return this.form.get(UserProfileFormFields.POSTAL_CODE) as FormControl;
  }

  getUserProfileCityControl(): FormControl {
    return this.form.get(UserProfileFormFields.CITY) as FormControl;
  }

  getUserProfileStateControl(): FormControl {
    return this.form.get(UserProfileFormFields.STATE) as FormControl;
  }

  getUserProfileEmailControl(): FormControl {
    return this.form.get(UserProfileFormFields.EMAIL) as FormControl;
  }

  getUserProfilePhoneControl(): FormControl {
    return this.form.get(UserProfileFormFields.PHONE) as FormControl;
  }

  /* Error Getters */

  getUserProfileNameErrorMsg(): string {
    const errors = this.getUserProfileNameControl().errors;

    if (errors) {
      if (errors.maxlength) {
        return this.translate.instant('profiles.user-profile-form-validation.userNameMaxLength', { maxLength: errors.maxlength.requiredLength });
      }
    }

    return null;
  }
  getUserProfileSurnameErrorMsg(): string {
    const errors = this.getUserProfileSurNameControl().errors;

    if (errors) {
      if (errors.maxlength) {
        return this.translate.instant('profiles.user-profile-form-validation.userSurnameMaxLength', { maxLength: errors.maxlength.requiredLength });
      }
    }

    return null;
  }
  getUserProfileStreetErrorMsg(): string {
    const errors = this.getUserProfileStreetControl().errors;

    if (errors) {
      if (errors.maxlength) {
        return this.translate.instant('profiles.user-profile-form-validation.userStreetMaxLength', { maxLength: errors.maxlength.requiredLength });
      }
    }

    return null;
  }
  getUserProfilePostalCodeErrorMsg(): string {
    const errors = this.getUserProfilePostalCodeControl().errors;

    if (errors) {
      if (errors.maxlength) {
        return this.translate.instant('profiles.user-profile-form-validation.userPostalCodeMaxLength', { maxLength: errors.maxlength.requiredLength });
      }
    }

    return null;
  }
  getUserProfileCityErrorMsg(): string {
    const errors = this.getUserProfileCityControl().errors;

    if (errors) {
      if (errors.maxlength) {
        return this.translate.instant('profiles.user-profile-form-validation.userCityMaxLength', { maxLength: errors.maxlength.requiredLength });
      }
    }

    return null;
  }
  getUserProfileStateErrorMsg(): string {
    const errors = this.getUserProfileStateControl().errors;

    if (errors) {
      if (errors.maxlength) {
        return this.translate.instant('profiles.user-profile-form-validation.userStateMaxLength', { maxLength: errors.maxlength.requiredLength });
      }
    }

    return null;
  }
  getUserProfileEmailErrorMsg(): string {
    const errors = this.getUserProfileEmailControl().errors;

    if (errors) {
      if (errors.maxlength) {
        return this.translate.instant('profiles.user-profile-form-validation.userEmailMaxLength', { maxLength: errors.maxlength.requiredLength });
      }
    }

    return null;
  }
  getUserProfilePhoneErrorMsg(): string {
    const errors = this.getUserProfilePhoneControl().errors;

    if (errors && !errors['required']) {
      if (errors.maxlength) {
        return this.translate.instant(
          'profiles.user-profile-form-validation.userPhoneMaxLength', { maxLength: errors.maxlength.requiredLength });
      } else if (errors.phoneNumber) {
        return this.translate.instant(
          'profiles.user-profile-form-validation.userPhonePattern');
      }
    }

    return null;
  }

  validateAllFormFields(formGroup: FormGroup) {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof FormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }

}
