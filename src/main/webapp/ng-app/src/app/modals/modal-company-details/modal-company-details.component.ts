import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { BehaviorSubject, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ErrorHandlingService } from 'src/app/core/error-handling/error-handling.service';
import { CompanyFormat } from 'src/app/core/models/company-format';
import { State } from 'src/app/core/models/states';
import { CompanyProfile } from 'src/app/features/profile/profile.model';
import { ProfileService } from 'src/app/features/profile/profile.service';

export enum CompanyProfileFormFields {
  NAME = 'companyProfileNameControl',
  FORMAT = 'companyProfileFormatControl',
  STREET = 'companyProfileStreetControl',
  POSTAL_CODE = 'companyProfilePostalCodeControl',
  CITY = 'companyProfileCityControl',
  STATE = 'companyProfileStateControl',
  EMAIL = 'companyProfileEmailControl',
  PHONE = 'companyProfilePhoneControl',
  VAT = 'companyProfileVatControl'
}

@Component({
  selector: 'app-modal-company-details',
  templateUrl: './modal-company-details.component.html'
})
export class ModalCompanyDetailsComponent implements OnInit {

  form: FormGroup;

  states: State[];
  companyFormats: CompanyFormat[];

  companyProfile: CompanyProfile;
  confirmBtnText: string;
  confirmMethod: () => Promise<void>;

  actionRunning$: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor(public activeModal: NgbActiveModal,
    private formBuilder: FormBuilder,
    private translate: TranslateService,
    private profileService: ProfileService,
    private errorHandlingService: ErrorHandlingService,
    private router: Router) {
  }

  ngOnInit() {

    const companyProfileNameControl = this.formBuilder.control(
      this.companyProfile ? this.companyProfile.name : "",
      [Validators.required, Validators.maxLength(100)]
    );

    const companyProfileFormatControl = this.formBuilder.control(
      this.companyProfile.companyFormat ? this.companyProfile.companyFormat : null,
      [Validators.required]
    );

    const companyProfileStreetControl = this.formBuilder.control(
      this.companyProfile ? this.companyProfile.street : "",
      [Validators.required, Validators.maxLength(100)]
    );

    const companyProfilePostalCodeControl = this.formBuilder.control(
      this.companyProfile ? this.companyProfile.postalCode : "",
      [Validators.required, Validators.maxLength(7)]
    );

    const companyProfileCityControl = this.formBuilder.control(
      this.companyProfile ? this.companyProfile.city : "",
      [Validators.required, Validators.maxLength(100)]
    );

    const companyProfileStateControl = this.formBuilder.control(
      this.companyProfile.state ? this.companyProfile.state : null,
      [Validators.required]
    );

    const companyProfileEmailControl = this.formBuilder.control(
      this.companyProfile ? this.companyProfile.invoiceEmail : "",
      [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]
    );

    const companyProfilePhoneControl = this.formBuilder.control(
      this.companyProfile ? this.companyProfile.phone : "",
      [Validators.required, Validators.maxLength(100)]
    );

    const companyProfileVatControl = this.formBuilder.control(
      this.companyProfile ? this.companyProfile.vatNumber : "",
      [Validators.required, Validators.maxLength(100)]
    );

    this.form = this.formBuilder.group({
      [CompanyProfileFormFields.NAME]: companyProfileNameControl,
      [CompanyProfileFormFields.FORMAT]: companyProfileFormatControl,
      [CompanyProfileFormFields.STREET]: companyProfileStreetControl,
      [CompanyProfileFormFields.POSTAL_CODE]: companyProfilePostalCodeControl,
      [CompanyProfileFormFields.CITY]: companyProfileCityControl,
      [CompanyProfileFormFields.STATE]: companyProfileStateControl,
      [CompanyProfileFormFields.EMAIL]: companyProfileEmailControl,
      [CompanyProfileFormFields.PHONE]: companyProfilePhoneControl,
      [CompanyProfileFormFields.VAT]: companyProfileVatControl
    });
  }

  confirm() {
    if (this.form.valid) {
      this.companyProfile.name = this.form.get('companyProfileNameControl').value;
      this.companyProfile.companyFormat = this.form.get('companyProfileFormatControl').value;
      this.companyProfile.street = this.form.get('companyProfileStreetControl').value;
      this.companyProfile.postalCode = this.form.get('companyProfilePostalCodeControl').value;
      this.companyProfile.city = this.form.get('companyProfileCityControl').value;
      this.companyProfile.state = this.form.get('companyProfileStateControl').value;
      this.companyProfile.invoiceEmail = this.form.get('companyProfileEmailControl').value;
      this.companyProfile.phone = this.form.get('companyProfilePhoneControl').value;
      this.companyProfile.vatNumber = this.form.get('companyProfileVatControl').value;

      this.profileService.updateCompanyProfile(this.companyProfile)
        .pipe(
          catchError(error => {
            this.actionRunning$.next(false);
            this.errorHandlingService.openErrorModal(this.translate.instant('profiles.saveFailed'));
            return throwError(error);
          })
        )
        .subscribe(() => {
          //window.location.reload();
          this.activeModal.close();
          this.router.navigate(['/profiles'])
        });
    }
    else {
      this.validateAllFormFields(this.form);
    }
  }

  /* Control Getters */

  getCompanyProfileNameControl(): FormControl {
    return this.form.get(CompanyProfileFormFields.NAME) as FormControl;
  }

  getCompanyProfileFormatControl(): FormControl {
    return this.form.get(CompanyProfileFormFields.FORMAT) as FormControl;
  }

  getCompanyProfileStreetControl(): FormControl {
    return this.form.get(CompanyProfileFormFields.STREET) as FormControl;
  }

  getCompanyProfilePostalCodeControl(): FormControl {
    return this.form.get(CompanyProfileFormFields.POSTAL_CODE) as FormControl;
  }

  getCompanyProfileCityControl(): FormControl {
    return this.form.get(CompanyProfileFormFields.CITY) as FormControl;
  }

  getCompanyProfileStateControl(): FormControl {
    return this.form.get(CompanyProfileFormFields.STATE) as FormControl;
  }

  getCompanyProfileEmailControl(): FormControl {
    return this.form.get(CompanyProfileFormFields.EMAIL) as FormControl;
  }

  getCompanyProfilePhoneControl(): FormControl {
    return this.form.get(CompanyProfileFormFields.PHONE) as FormControl;
  }

  getCompanyProfileVatControl(): FormControl {
    return this.form.get(CompanyProfileFormFields.VAT) as FormControl;
  }


  /* Error Getters */

  getCompanyProfileNameErrorMsg(): string {
    const errors = this.getCompanyProfileNameControl().errors;

    if (errors) {
      if (errors.maxlength) {
        return this.translate.instant('profiles.company-profile-form-validation.companyNameMaxLength', { maxLength: errors.maxlength.requiredLength });
      }
    }

    return null;
  }
  getCompanyProfileFormatErrorMsg(): string {
    const errors = this.getCompanyProfileFormatControl().errors;

    if (errors) {
      if (errors.maxlength) {
        return this.translate.instant('profiles.company-profile-form-validation.companyFormatMaxLength', { maxLength: errors.maxlength.requiredLength });
      }
    }

    return null;
  }
  getCompanyProfileStreetErrorMsg(): string {
    const errors = this.getCompanyProfileStreetControl().errors;

    if (errors) {
      if (errors.maxlength) {
        return this.translate.instant('profiles.company-profile-form-validation.companyStreetMaxLength', { maxLength: errors.maxlength.requiredLength });
      }
    }

    return null;
  }
  getCompanyProfilePostalCodeErrorMsg(): string {
    const errors = this.getCompanyProfilePostalCodeControl().errors;

    if (errors) {
      if (errors.maxlength) {
        return this.translate.instant('profiles.company-profile-form-validation.companyPostalCodeMaxLength', { maxLength: errors.maxlength.requiredLength });
      }
    }

    return null;
  }
  getCompanyProfileCityErrorMsg(): string {
    const errors = this.getCompanyProfileCityControl().errors;

    if (errors) {
      if (errors.maxlength) {
        return this.translate.instant('profiles.company-profile-form-validation.companyCityMaxLength', { maxLength: errors.maxlength.requiredLength });
      }
    }

    return null;
  }
  getCompanyProfileStateErrorMsg(): string {
    const errors = this.getCompanyProfileStateControl().errors;

    if (errors) {
      if (errors.maxlength) {
        return this.translate.instant('profiles.company-profile-form-validation.companyStateMaxLength', { maxLength: errors.maxlength.requiredLength });
      }
    }

    return null;
  }
  getCompanyProfileEmailErrorMsg(): string {
    const errors = this.getCompanyProfileEmailControl().errors;

    if (errors) {
      if (errors.maxlength) {
        return this.translate.instant('profiles.company-profile-form-validation.companyEmailMaxLength', { maxLength: errors.maxlength.requiredLength });
      }
    }

    return null;
  }
  getCompanyProfilePhoneErrorMsg(): string {
    const errors = this.getCompanyProfilePhoneControl().errors;

    if (errors) {
      if (errors.maxlength) {
        return this.translate.instant('profiles.company-profile-form-validation.companyPhoneMaxLength', { maxLength: errors.maxlength.requiredLength });
      }
    }

    return null;
  }
  getCompanyProfileVatErrorMsg(): string {
    const errors = this.getCompanyProfileVatControl().errors;

    if (errors) {
      if (errors.maxlength) {
        return this.translate.instant('profiles.company-profile-form-validation.companyVatMaxLength', { maxLength: errors.maxlength.requiredLength });
      }
    }

    return null;
  }


  validateAllFormFields(formGroup: FormGroup) {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof FormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }

}
