import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { GigHouseContact, IGigHouseContactBackend } from '../models/gighouse-contact';

@Injectable({
  providedIn: 'root'
})
export class GighouseContactService {

  constructor(private httpClient: HttpClient) { }

  getGigHouseContact(): Observable<GigHouseContact> {
    return this.httpClient.get<IGigHouseContactBackend>(`/api/profile/gighouse-contact`)
      .pipe(
        map((res: IGigHouseContactBackend) => {
          return new GigHouseContact(res);
        })
      );
  }



}
