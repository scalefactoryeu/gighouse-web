import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { distinct, map } from 'rxjs/operators';

import { PaginatedData } from '../models/model';
import { IStateBackend, State } from '../models/states';

@Injectable({
  providedIn: 'root'
})
export class StateService {

  constructor(private httpClient: HttpClient) {
  }

  getAllStates(): Observable<State[]> {
    return this.getStatesPaginated(0, 1000000)
      .pipe(
        map((skillsPaginated) => {
          return skillsPaginated.items;
        })
      );
  }

  getStatesPaginated(page: number, pageSize: number): Observable<PaginatedData<State>> {
    let params = new HttpParams();
    params = params.append('page', page.toString());
    params = params.append('size', pageSize.toString());

    return this.httpClient.get<IStateBackend[]>(`/api/lists/states`, { observe: 'response', params })
      .pipe(
        map((res: HttpResponse<IStateBackend[]>) => {
          const states = res.body.map(state => new State(state));
          return new PaginatedData<State>(res, states, page);
        })
      );
  }

}
