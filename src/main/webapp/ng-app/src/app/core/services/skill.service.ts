import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ISkillBackend, Skill } from '../models/skill.model';
import { PaginatedData } from '../models/model';

@Injectable({
  providedIn: 'root'
})
export class SkillService {

  constructor(private httpClient: HttpClient) {
  }

  getAllSkills(): Observable<Skill[]> {
    return this.getSkillsPaginated(0, 1000000)
      .pipe(
        map((skillsPaginated) => {
          return skillsPaginated.items;
        })
      );
  }

  getSkillsPaginated(page: number, pageSize: number): Observable<PaginatedData<Skill>> {
    let params = new HttpParams();
    params = params.append('page', page.toString());
    params = params.append('size', pageSize.toString());

    return this.httpClient.get<ISkillBackend[]>(`/api/skills/`, {observe: 'response', params})
      .pipe(
        map((res: HttpResponse<ISkillBackend[]>) => {
          const skills = res.body.map(skill => new Skill(skill));
          return new PaginatedData<Skill>(res, skills, page);
        })
      );
  }

}
