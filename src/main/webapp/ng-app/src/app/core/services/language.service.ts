import {Injectable} from '@angular/core';
import {HttpClient, HttpParams, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {PaginatedData} from '../models/model';
import {ILanguageBackend, Language} from "../models/language";
import {ILanguageLevelBackend, LanguageLevel} from "../models/language-level";

@Injectable({
  providedIn: 'root'
})
export class LanguageService {

  constructor(private httpClient: HttpClient) {
  }

  getAllLanguages(): Observable<Language[]> {
    return this.getLanguagesPaginated(0, 1000000)
      .pipe(
        map((languagesPaginated) => {
          return languagesPaginated.items;
        })
      );
  }

  getLanguagesPaginated(page: number, pageSize: number): Observable<PaginatedData<Language>> {
    let params = new HttpParams();
    params = params.append('page', page.toString());
    params = params.append('size', pageSize.toString());

    return this.httpClient.get<ILanguageBackend[]>(`/api/lists/project-languages`, {observe: 'response', params})
      .pipe(
        map((res: HttpResponse<ILanguageBackend[]>) => {
          const languages = res.body.map(language => new Language(language));
          return new PaginatedData<Language>(res, languages, page);
        })
      );
  }

  getAllLanguageLevels(): Observable<LanguageLevel[]> {
    return this.getLanguageLevelsPaginated(0, 1000000)
      .pipe(
        map((languageLevelsPaginated) => {
          return languageLevelsPaginated.items;
        })
      );
  }

  getLanguageLevelsPaginated(page: number, pageSize: number): Observable<PaginatedData<LanguageLevel>> {
    let params = new HttpParams();
    params = params.append('page', page.toString());
    params = params.append('size', pageSize.toString());

    return this.httpClient.get<ILanguageLevelBackend[]>(`/api/lists/project-language-levels`, {observe: 'response', params})
      .pipe(
        map((res: HttpResponse<ILanguageLevelBackend[]>) => {
          const languageLevels = res.body.map(language => new LanguageLevel(language));
          return new PaginatedData<LanguageLevel>(res, languageLevels, page);
        })
      );
  }

}
