import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { PaginatedData } from '../models/model';
import { CompanyFormat, ICompanyFormatBackend } from '../models/company-format';


@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  constructor(private httpClient: HttpClient) {
  }

  getAllCompanyFormats(): Observable<CompanyFormat[]> {
    return this.getCompanyFormatPaginated(0, 1000000)
      .pipe(
        map((companyFormatsPaginated) => {
          return companyFormatsPaginated.items;
        })
      );
  }

  getCompanyFormatPaginated(page: number, pageSize: number): Observable<PaginatedData<CompanyFormat>> {
    let params = new HttpParams();
    params = params.append('page', page.toString());
    params = params.append('size', pageSize.toString());

    return this.httpClient.get<ICompanyFormatBackend[]>(`/api/lists/company-formats`, {observe: 'response', params})
      .pipe(
        map((res: HttpResponse<ICompanyFormatBackend[]>) => {
          const companyFormats = res.body.map(companyFormat => new CompanyFormat(companyFormat));
          return new PaginatedData<CompanyFormat>(res, companyFormats, page);
        })
      );
  }

}
