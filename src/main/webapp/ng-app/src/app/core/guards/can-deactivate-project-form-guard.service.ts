import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { ComponentCanDeactivate } from '../../shared/components/component-can-deactivate.component';
import { TranslateService } from '@ngx-translate/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalConfirmLeavePageComponent } from '../../modals/modal-confirm-leave-page/modal-confirm-leave-page.component';

/**
 * Guard that prevents navigating to another route if the current component can't be deactivated (e.g. unsaved changes in form).
 * Implementation based on:
 * https://medium.com/better-programming/angular-how-keep-user-from-lost-his-data-by-accidentally-leaving-the-page-before-submit-4eeb74420f0d
 */
@Injectable({providedIn: 'root'})
export class CanDeactivateProjectFormGuard implements CanDeactivate<ComponentCanDeactivate> {

  constructor(private translate: TranslateService, private ngbModal: NgbModal) {
  }

  canDeactivate(component: ComponentCanDeactivate): Promise<boolean> {
    return new Promise((resolve, reject) => {
      const canDeactivateResult = component.canDeactivate();

      if (!canDeactivateResult.canDeactivate) {
        const modalRef = this.ngbModal.open(ModalConfirmLeavePageComponent, {
          windowClass: 'modal-confirm-leave-project-form',
          backdrop: 'static'
        });

        const modal = modalRef.componentInstance as ModalConfirmLeavePageComponent;

        modal.reason = canDeactivateResult.reason;

        modalRef.result
          .then(
            () => {resolve(true); },
            () => {resolve(false); }
          );
      } else {
        resolve(true);
      }
    });
  }
}
