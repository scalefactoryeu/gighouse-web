

export interface ICompanyFormatBackend {
    label: string;
    value: string;
  }

export class CompanyFormat implements ICompanyFormatBackend {
    label: string;
    value: string;

    constructor(companyFormatBackend: ICompanyFormatBackend){
        this.label = companyFormatBackend.label;
        this.value = companyFormatBackend.value;
    }
}