

export interface IGigHouseContactBackend {
    email: string;
    name: string;
    phone: string;
}

export class GigHouseContact implements IGigHouseContactBackend {
    email: string;
    name: string;
    phone: string;

    constructor(gighouseContactBackend: IGigHouseContactBackend) {
        this.email = gighouseContactBackend.email;
        this.name = gighouseContactBackend.name;
        this.phone = gighouseContactBackend.phone;
    }
}