import {assignIn} from 'lodash';

export interface IProjectLanguageBackend {
  id: string;
  language: string;
  level: string;
}

export class ProjectLanguage implements IProjectLanguageBackend {
  id: string;
  language: string;
  level: string;

  constructor(languageBackend: IProjectLanguageBackend) {
    assignIn(this, languageBackend);
  }
}
