import {assignIn} from 'lodash';

export enum SkillType {
  FUNCTION = 'Function',
  SPECIALIZATION = 'Specialization',
  TOOLS = 'Tools',
}

export enum SkillRequirement {
  REQUIRED = 'Required',
  OPTIONAL = 'Optional'
}

export interface ISkillBackend {
  id: string;
  name: string;
  type: string;
  parent: string;
}

export class Skill implements ISkillBackend {
  id: string;
  name: string;
  type: SkillType;
  parent: string;

  constructor(skillBackend: ISkillBackend) {
    assignIn(this, skillBackend);

    // if (skillBackend.parent) {
    //   this.parent = new Skill(skillBackend.parent);
    // }
  }
}
