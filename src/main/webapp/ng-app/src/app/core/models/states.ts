

export interface IStateBackend {
    label: string;
    value: string;
  }

export class State implements IStateBackend {
    label: string;
    value: string;

    constructor(stateBackend: IStateBackend){
        this.label = stateBackend.label;
        this.value = stateBackend.value;
    }
}