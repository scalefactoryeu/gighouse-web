export interface ILanguageLevelBackend {
  label: string;
  value: string;
}

export class LanguageLevel implements ILanguageLevelBackend {
  label: string;
  value: string;

  constructor(languageLevelBackend: ILanguageLevelBackend) {
    this.label = languageLevelBackend.label;
    this.value = languageLevelBackend.value;
  }

}
