export class InvoiceStatus {
    translateLabel: string;
    icon: string;
    statusCssClass: string;
    status: InvoiceStatuses;
}

export enum InvoiceStatuses {
    RELEASED = "Released",
    CREDITED = "Credited"
}

type InvoiceStatusesConfig = {
    [P in InvoiceStatuses]: InvoiceStatus;
};

export const INVOICE_STATUSES_CONFIG: InvoiceStatusesConfig = {
    [InvoiceStatuses.RELEASED]: {
        translateLabel: 'invoices.status.RELEASED',
        icon: 'check',
        statusCssClass: 'invoice-status-paid',
        status: InvoiceStatuses.RELEASED
    },

    [InvoiceStatuses.CREDITED]: {
        translateLabel: 'invoices.status.CREDITED',
        icon: 'close',
        statusCssClass: 'invoice-status-not-paid',
        status: InvoiceStatuses.CREDITED
    }
}
