import { HttpResponse } from '@angular/common/http';

export class PaginatedData<T> {

  readonly totalCount: number;
  readonly items: T[];
  readonly page: number;

  constructor(httpResponse: HttpResponse<any>, items: T[], page: number) {
    this.totalCount = Number(httpResponse.headers.get('X-Total-Count'));
    this.items = items;
    this.page = page;
  }
}
