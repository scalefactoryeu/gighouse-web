

export interface ILanguageBackend {
  label: string;
  value: string;
}

export class Language implements ILanguageBackend {
  label: string;
  value: string;

  constructor(languageBackend: ILanguageBackend){
    this.label = languageBackend.label;
    this.value = languageBackend.value;
  }
}
