import {Injectable} from '@angular/core';
import * as moment from 'moment';
import {TranslateService} from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class ChatDatesService {

  constructor(private translate: TranslateService) {
  }


  getChatDateFormatPerDay(date: Date): string {
    const momentDate = moment(date);

    const isToday = momentDate.isSame(moment(), 'day');
    if (isToday) {
      return this.translate.instant('global.relativeDates.today') + ', ' + momentDate.format('D MMMM');
    }

    const isYesterday = momentDate.isSame(moment().subtract(1, 'day'), 'day');
    if (isYesterday) {
      return this.translate.instant('global.relativeDates.yesterday')  + ', ' + momentDate.format('D MMMM');
    }

    return momentDate.format('D MMMM YYYY');
  }

  getChatMessageTime(date: Date): string {
    const momentDate = moment(date);
    return momentDate.format('HH:mm');
  }
}
