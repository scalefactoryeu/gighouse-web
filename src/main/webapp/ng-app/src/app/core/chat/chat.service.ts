import { Injectable } from '@angular/core';
import { TwilioService } from './twilio.service';
import { BehaviorSubject, combineLatest, Observable, Subject } from 'rxjs';
import { catchError, filter, map, switchMap, take, takeUntil } from 'rxjs/operators';
import { Channel } from 'twilio-chat/lib/channel';
import { Message } from 'twilio-chat/lib/message';
import {
  AccessTokenResponse,
  ChatMessage,
  SubscribedChannels,
  CreateConversationResponse,
  ChatMessagesByDay, ChatMessagesByAuthor
} from './chat.model';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, NavigationEnd, Router, RouterEvent } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  chatChannel: Channel;

  chatMessagesByDay$: Observable<ChatMessagesByDay[]>;
  subscribedChannels$: Observable<SubscribedChannels>;
  loadingClient$: Observable<boolean>;
  isConnected$: Observable<boolean>;
  loadingConversationMessages$: Observable<boolean>;

  isTyping$ = new BehaviorSubject<boolean>(false);
  // error while establishing a connection to twilio
  connectionError$: Observable<boolean>;
  // error while fetching data for a channel
  private chatServiceChannelError$ = new BehaviorSubject<boolean>(false);
  channelError$: Observable<boolean> = this.chatServiceChannelError$.asObservable();

  private chatUserIdentity: string;
  private chatServiceConnectionError$ = new BehaviorSubject<boolean>(false);
  // Cancel ongoing async actions (requests) for current channel to make sure
  // it's data won't leak into a new chat window when switching between channels
  private cancelChannelActions$ = new Subject();

  constructor(private twilio: TwilioService, private httpClient: HttpClient, private translate: TranslateService,
    private activatedRoute: ActivatedRoute, private router: Router) {

    this.loadingConversationMessages$ = this.twilio.loadingConversationMessages$;
    this.chatMessagesByDay$ = this.twilio.chatMessages$
      .pipe(map((chatMessages) => this.groupMessagesByDayAndAuthor(chatMessages)));
    this.loadingClient$ = this.twilio.loadingClient$;
    this.isConnected$ = this.twilio.isConnected$;
    this.subscribedChannels$ = this.twilio.subscribedChannels$;

    this.twilio.tokenAboutToExpire$.subscribe(() => this.refreshAccessToken());

    this.connectionError$ = combineLatest([this.twilio.twilioConnectionError$, this.chatServiceConnectionError$])
      .pipe(
        map(([twilioConnectionError, chatServiceConnectionError]) => {
          return twilioConnectionError || chatServiceConnectionError;
        })
      );
  }

  initChatService() {
    this.getAccessToken()
      .pipe(
        catchError((error) => {
          this.chatServiceConnectionError$.next(true);
          throw error;
        })
      )
      .subscribe((accessTokenResponse) => {
        this.chatUserIdentity = accessTokenResponse.identity;
        this.twilio.connect(accessTokenResponse.token);
      });
  }

  private refreshAccessToken() {
    this.getAccessToken()
      .pipe(
        catchError((error) => {
          this.chatServiceConnectionError$.next(true);
          throw error;
        })
      )
      .subscribe((accessTokenResponse) => {
        this.chatUserIdentity = accessTokenResponse.identity;
        this.twilio.updateToken(accessTokenResponse.token);
      });
  }

  /**
   * Gets an existing channel or creates a new one with twilio in the backend
   */
  selectChannel(projectId: string, freelancerId: string) {
    const body = { projectId, freelancerId };

    this.resetChannel();
    this.chatServiceChannelError$.next(false); // reset to false

    this.httpClient.post<CreateConversationResponse>('/api/chat/conversation', body)
      .pipe(
        catchError((error) => {
          this.chatServiceChannelError$.next(true);
          throw error;
        }),
        takeUntil(this.cancelChannelActions$)
      )
      .subscribe((response) => {
        this.openChannel(response.conversationSid);
      });
  }

  selectAdminChannel(projectId: string) {
    const body = { projectId };

    this.resetChannel();
    this.chatServiceChannelError$.next(false); // reset to false

    this.httpClient.post<CreateConversationResponse>('/api/chat/conversation/admin', body)
      .pipe(
        catchError((error) => {
          this.chatServiceChannelError$.next(true);
          throw error;
        }),
        takeUntil(this.cancelChannelActions$)
      )
      .subscribe((response) => {
        this.openChannel(response.conversationSid);
      });
  }

  resetChannel() {
    this.cancelChannelActions$.next();

    if (this.chatChannel) {
      this.removeChannelListeners();
      this.chatChannel = null;
    }

    this.twilio.resetChannel();
  }

  sendMessage(chatMessage: string | FormData): Promise<any> {
    return this.chatChannel.sendMessage(chatMessage);
  }

  typing() {
    this.chatChannel.typing();
  }

  private getAccessToken(): Observable<AccessTokenResponse> {
    // ToDo: This is a temporary hack to chat as a freelancer instead of the currently logged in user.
    // To chat as a freelancer open url /chat?chatAsFreelancer=0039E000010OaAfQAK
    // Keep in mind when using this method and token expires we won't be able to fetch access token to update the token in twilio because
    // here we depend on router events.
    return this.router.events
      .pipe(
        filter((event: RouterEvent) => event instanceof NavigationEnd),
        take(1),
        switchMap((event: RouterEvent) => {
          const identity = this.activatedRoute.snapshot.queryParams['chatAsFreelancer'];
          return this.httpClient.post<AccessTokenResponse>('/api/chat/access-token', { identity });
        })
      );

    // ToDo: Uncomment this. This should be a standard way of fetching access token for the currently logged in user.
    // return this.httpClient.post<AccessTokenResponse>('/api/chat/access-token', {});
  }

  private openChannel(sid: string) {
    // initialize chat channel
    this.twilio.getChannel(sid)
      .pipe(
        catchError((error) => {
          this.chatServiceChannelError$.next(true);
          throw error;
        }),
        takeUntil(this.cancelChannelActions$)
      )
      .subscribe((channel) => {
        this.chatChannel = channel;

        // get chat messages
        this.getChannelMessages(this.chatChannel);

        // setup event listeners
        this.channelEventsListeners(this.chatChannel);
      });
  }

  private getChannelMessages(channel: Channel) {
    this.twilio.getMessages(channel)
      .pipe(
        catchError((error) => {
          this.chatServiceChannelError$.next(true);
          throw error;
        }),
        takeUntil(this.cancelChannelActions$)
      )
      .subscribe(paginator => {
        paginator.items.forEach(async (message) => {
          const mappedMessage = this.mapMessage(message);
          this.twilio.addChatMessage(mappedMessage);
        });

        channel.setAllMessagesConsumed();
      });
  }

  private mapMessage(message: Message): ChatMessage {
    return {
      author: message.author,
      sid: message.sid,
      body: message.body,
      dateCreated: message.dateCreated,
      isCurrentUserAuthor: this.chatUserIdentity === message.author,
      index: message.index
    };
  }

  private channelEventsListeners(channel: Channel) {
    channel.on('typingStarted', () => {
      this.isTyping$.next(true);
    });
    channel.on('typingEnded', () => {
      this.isTyping$.next(false);
    });
    channel.on('messageAdded', this.messageAddedEvent.bind(this));
  }

  private removeChannelListeners() {
    this.chatChannel.removeAllListeners('memberUpdated');
    this.chatChannel.removeAllListeners('messageAdded');
    this.chatChannel.removeAllListeners('typingStarted');
    this.chatChannel.removeAllListeners('typingEnded');
  }

  private async messageAddedEvent(message) {
    this.chatChannel.updateLastConsumedMessageIndex(message.index);
    this.twilio.addChatMessage({ ...this.mapMessage(message) });
  }

  private groupMessagesByDayAndAuthor(chatMessages: ChatMessage[]): ChatMessagesByDay[] {
    const chatMessagesByDay: { day: string, chatMessages: ChatMessage[] }[] = [];

    /**
     * Generate array of days with messages belonging to that day
     */
    chatMessages.forEach((chatMessage) => {
      // day as string, e.g. 2021-03-06
      const messageDay = chatMessage.dateCreated.toISOString().split('T')[0];
      // check if last item in array has the same day string
      const dayFoundInArray = chatMessagesByDay.length && chatMessagesByDay[chatMessagesByDay.length - 1].day === messageDay;

      if (!dayFoundInArray) {
        chatMessagesByDay.push({ day: messageDay, chatMessages: [] });
      }

      const lastDayItem = chatMessagesByDay[chatMessagesByDay.length - 1];
      lastDayItem.chatMessages.push(chatMessage);
    });

    /**
     * For each day generate an array of message sequences by author. E.g.
     *   06/03/2021 -> [
     *      {author: '#john', chatMessages: ['msg-1', 'msg-2']},
     *      {author: '#jane', chatMessages: ['msg-3']},
     *      {author: '#john', chatMessages: ['msg-4']}
     *   ]
     */
    const chatMessagesByDayResult = chatMessagesByDay.map(chatMessagesByDayItem => {
      const chatMessagesByAuthor: ChatMessagesByAuthor[] = [];

      chatMessagesByDayItem.chatMessages.forEach(chatMessage => {
        // check if last item in array has the same author
        const lastItemHasSameAuthor = chatMessagesByAuthor.length
          && chatMessagesByAuthor[chatMessagesByAuthor.length - 1].author === chatMessage.author;

        if (!lastItemHasSameAuthor) {
          chatMessagesByAuthor.push(
            new ChatMessagesByAuthor(
              chatMessage.author,
              chatMessage.author === this.chatUserIdentity,
              []
            )
          );
        }

        const lastChatMessageByAuthorItem = chatMessagesByAuthor[chatMessagesByAuthor.length - 1];
        lastChatMessageByAuthorItem.chatMessages.push(chatMessage);
      });

      return new ChatMessagesByDay(new Date(chatMessagesByDayItem.day), chatMessagesByAuthor);
    });

    chatMessagesByDayResult.sort((a, b) => a.date.getTime() - b.date.getTime());
    return chatMessagesByDayResult;
  }

}
