import { Injectable } from '@angular/core';
import {ChatMessage, ConversationAttributes, SubscribedChannels} from './chat.model';
import {BehaviorSubject, from, Observable, Subject} from 'rxjs';
import TwilioClient from 'twilio-chat';
import { Channel } from 'twilio-chat/lib/channel';
import {catchError, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TwilioService {

  isConnected$ = new BehaviorSubject<boolean>(false);
  loadingClient$ = new BehaviorSubject<boolean>(true);
  tokenAboutToExpire$ = new Subject();

  private _twilioConnectionError$ =  new BehaviorSubject<boolean>(false);
  twilioConnectionError$ = this._twilioConnectionError$.asObservable();

  private _loadingConversationMessages$ = new BehaviorSubject<boolean>(false);
  loadingConversationMessages$ = this._loadingConversationMessages$.asObservable();

  private _chatMessages$ = new BehaviorSubject<ChatMessage[]>([]);
  chatMessages$ = this._chatMessages$.asObservable();

  private _subscribedChannels$ = new BehaviorSubject<SubscribedChannels>(null);
  subscribedChannels$ = this._subscribedChannels$.asObservable();

  private twilioClient: TwilioClient;

  constructor() {}

  connect(token: string) {
    TwilioClient.create(token)
      .then((client) => {
        this.twilioClient = client;
        this.setupClientEventListeners();
        this.isConnected$.next(true);
        this.loadingClient$.next(false);
        this.getSubscribedChannels();
      })
      .catch((err: any) => {
        this.isConnected$.next(false);
        this._twilioConnectionError$.next(true);
        throw err;
      })
      .finally(() => {
        this.loadingClient$.next(false);
      });
  }

  private getSubscribedChannels() {
    this.twilioClient.getSubscribedChannels()
      .then((value) => {
        value.items.map((channel) => {
          this.updateChannel(channel);
        });
      })
      .catch(() => {
        this._twilioConnectionError$.next(true);
      });
  }

  resetChannel() {
    this._chatMessages$.next([]);
    this._loadingConversationMessages$.next(true);
  }

  getChannel(sid: string): Observable<Channel> {
    const promise = this.twilioClient.getChannelBySid(sid);
    return from(promise);
  }

  getMessages(channel: Channel) {
    const promise = channel.getMessages();

    return from(promise)
      .pipe(
        // Set loading to false on success or error. Don't use 'finalize' pipe because it also triggers when stream is destroyed.
        tap(() => {
          this._loadingConversationMessages$.next(false);
        }),
        catchError((error) => {
          this._loadingConversationMessages$.next(false);
          throw error;
        })
      );
  }

  private updateChannel(channel: Channel) {
    const channelInvalid = !channel.attributes['projectId'] || !channel.attributes['participants'];
    if (channelInvalid) { return; }

    const unreadMessages = (channel.lastMessage?.index || 0) - channel.lastConsumedMessageIndex;

    this._subscribedChannels$.next({
      ...this._subscribedChannels$.value,
      [channel.sid]: {
        sid: channel.sid,
        friendlyName: channel.friendlyName,
        unreadMessages,
        attributes: channel.attributes as ConversationAttributes
      }
    });
  }

  private setupClientEventListeners() {
    this.twilioClient.on('tokenAboutToExpire', () => {
      this.tokenAboutToExpire$.next();
    });

    this.twilioClient.on('channelUpdated', (e) => {
      if (
        ['lastMessage', 'lastConsumedMessageIndex']
          .some((updateReason) => (e.updateReasons || []).includes(updateReason))
      ) {
        this.updateChannel(e.channel);
      }
    });
  }

  addChatMessage(message) {
    this._chatMessages$.next([...this._chatMessages$.value, message]);
  }

  updateToken(updatedToken: string) {
    this.twilioClient.updateToken(updatedToken)
      .catch(() => {
        this._twilioConnectionError$.next(true);
      });
  }

}
