export class AccessTokenResponse {
  token: string;
  identity: string;
}

export class CreateConversationResponse {
  conversationSid: string;
}

export interface ConversationAttributes {
  projectId: string;
  participants: string[];
}

export interface ChannelEntity {
  sid?: string;
  unreadMessages?: number;
  friendlyName?: string;
  attributes: ConversationAttributes;
}

export interface SubscribedChannels {
  [key: string]: ChannelEntity;
}

export interface ChatMessage {
  author: string;
  sid: string;
  body: string;
  dateCreated: Date;
  isCurrentUserAuthor: boolean;
  index?: number;
}

export class ChatMessagesByAuthor {
  author: string;
  isCurrentUserAuthor: boolean;
  chatMessages: ChatMessage[];

  constructor(author: string, isCurrentUserAuthor: boolean, chatMessages: ChatMessage[]) {
    this.author = author;
    this.isCurrentUserAuthor = isCurrentUserAuthor;
    this.chatMessages = chatMessages;
  }
}

export class ChatMessagesByDay {
  date: Date;
  chatMessagesByAuthor: ChatMessagesByAuthor[];

  constructor(date: Date, chatMessagesByAuthor: ChatMessagesByAuthor[]) {
    this.date = date;
    this.chatMessagesByAuthor = chatMessagesByAuthor;
  }
}
