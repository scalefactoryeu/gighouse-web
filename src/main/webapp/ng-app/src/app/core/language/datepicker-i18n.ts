import {Injectable} from '@angular/core';
import { NgbDatepickerI18n, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class CustomDatepickerI18n extends NgbDatepickerI18n {

  constructor(private translate: TranslateService) {
    super();
  }

  getWeekdayShortName(weekday: number): string {
    return this.translate.instant('global.short-weekdays.' + weekday);
  }

  getMonthShortName(month: number): string {
    return this.translate.instant('global.months.' + month);
  }

  getMonthFullName(month: number): string {
    return this.translate.instant('global.months.' + month);
  }

  getDayAriaLabel(date: NgbDateStruct): string {
    return `${date.day}/${date.month}/${date.year}`;
  }

}
