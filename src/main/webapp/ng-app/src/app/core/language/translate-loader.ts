import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HttpClient} from '@angular/common/http';

export function AppTranslateLoader(http: HttpClient): TranslateHttpLoader {
  // Loads a translation file from assets/i18n based on the selected language (e.g. assets/i18n/en-merged.json)
  // IMPORTANT NOTE: file "en-merged.json" is generated in-memory for dev server and in output directory for ng build by a Webpack task
  // defined in custom-webpack.config.js. The Webpack task merges all the files from assets/i18n/[language] directory and outputs it as
  // assets/i18n/[language]-merged.json
  return new TranslateHttpLoader(http, 'assets/i18n/', '-merged.json');
}
