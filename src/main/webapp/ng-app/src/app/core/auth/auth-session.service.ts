import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Logout } from '../login/logout.model';
import { environment } from '../../../environments/environment';

@Injectable({ providedIn: 'root' })
export class AuthServerProvider {
  constructor(private http: HttpClient) {}

  logout(): Observable<Logout> {
    return this.http.post<Logout>(environment.SERVER_API_URL + 'api/logout', {});
  }
}
