import { Injectable, isDevMode } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { StateStorageService } from './state-storage.service';
import { LoginService } from '../login/login.service';
import { AccountService } from './account.service';
import {Authority} from '../../shared/constants/authority.constants';

@Injectable({ providedIn: 'root' })
export class UserRouteAccessService implements CanActivate {
  constructor(
    private router: Router,
    private loginService: LoginService,
    private accountService: AccountService,
    private stateStorageService: StateStorageService
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    const authorities = route.data['authorities'];
    // We need to call the checkLogin / and so the accountService.identity() function, to ensure,
    // that the client has a principal too, if they already logged in by the server.
    // This could happen on a page refresh.
    return this.checkLogin(authorities, state.url);
  }

  checkLogin(authorities: string[], url: string): Observable<boolean> {
    return this.accountService.identity().pipe(
      map(account => {

        if (account) {

          // if (account.authorities.find(authority => authority === 'ROLE_GIG_ADMIN')) {
          //   this.router.navigate(['/chat']);
          //   return true;
          // }

          // After the user has his account and has logged in, he will need to finalise his details and accept terms before entering
          // the app. We will redirect any unregistered user to the register page (if the target url is not already for that page),
          // but we will also not allow a registered user to access that page.
          if (account.authorities.includes(Authority.ROLE_UNREGISTERED) && !url.includes('/profiles/register')) {
            this.router.navigate(['/profiles/register']);
            return false;
          }
          if (account.authorities.includes(Authority.PENDING_USER)) {
            this.router.navigate(['/pending']);
          }

          if (!authorities || authorities.length === 0) {
            return true;
          } else {
            const hasAnyAuthority = this.accountService.hasAnyAuthority(authorities);
            if (hasAnyAuthority) {
              return true;
            }
            if (isDevMode()) {
              console.error('User has not any of required authorities: ', authorities);
            }
            this.router.navigate(['accessdenied']);
            return false;
          }
        }

        this.stateStorageService.storeUrl(url);
        this.loginService.login();
        return false;
      })
    );
  }
}
