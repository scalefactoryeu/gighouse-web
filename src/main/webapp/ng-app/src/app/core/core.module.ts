import { LOCALE_ID, NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http';
import { NgxWebstorageModule } from 'ngx-webstorage';
import { JhiLanguageService, NgJhipsterModule } from 'ng-jhipster';
import { MissingTranslationHandler, TranslateLoader, TranslateModule } from '@ngx-translate/core';
import {Title } from '@angular/platform-browser';
import { NgbDateAdapter, NgbDateParserFormatter, NgbDatepickerI18n } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateMomentAdapter } from '../shared/util/datepicker-adapter';
import { DatePipe, registerLocaleData } from '@angular/common';
import { AuthExpiredInterceptor } from './interceptors/auth-expired.interceptor';
import { ErrorHandlerInterceptor } from './interceptors/errorhandler.interceptor';
import { NotificationInterceptor } from './interceptors/notification.interceptor';
import { FaIconLibrary } from '@fortawesome/angular-fontawesome';
import locale from '@angular/common/locales/en';
import { AppTranslateLoader } from './language/translate-loader';
import { AppMissingTranslationHandler } from './language/missing-translation-handler';
import { fontAwesomeIcons } from './icons/font-awesome-icons';
import { svgIcons } from './icons/svg-icons';
import { AngularSvgIconModule, SvgIconRegistryService } from 'angular-svg-icon';
import { CustomDatepickerI18n } from './language/datepicker-i18n';
import { CustomDateParserFormatter } from '../shared/util/datepicker-parser';
import { ErrorHandlingModule } from './error-handling/error-handling.module';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ModalsModule } from '../modals/modals.module';

@NgModule({
  imports: [
    BrowserAnimationsModule,
    HttpClientModule,
    NgxWebstorageModule.forRoot({ prefix: 'jhi', separator: '-' }),
    NgJhipsterModule.forRoot({
      // set below to true to make alerts look like toast
      alertAsToast: false,
      alertTimeout: 5000,
      i18nEnabled: true,
      defaultI18nLang: 'en',
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader, useFactory: AppTranslateLoader, deps: [HttpClient]
      },
      missingTranslationHandler: {
        provide: MissingTranslationHandler, useClass: AppMissingTranslationHandler
      }
    }),
    ToastrModule.forRoot(),
    AngularSvgIconModule.forRoot(),
    ErrorHandlingModule,
    ModalsModule
  ],
  providers: [
    Title,
    {
      provide: LOCALE_ID,
      useValue: 'en',
    },
    { provide: NgbDateAdapter, useClass: NgbDateMomentAdapter },
    { provide: NgbDateParserFormatter, useClass: CustomDateParserFormatter },
    { provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n },
    DatePipe,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthExpiredInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorHandlerInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: NotificationInterceptor,
      multi: true,
    },
  ],
})
export class CoreModule {

  constructor(faIconLibrary: FaIconLibrary, svgIconRegistry: SvgIconRegistryService, languageService: JhiLanguageService) {
    registerLocaleData(locale);
    languageService.init();

    faIconLibrary.addIcons(...fontAwesomeIcons);
    svgIcons.forEach(svgIcon => svgIconRegistry.addSvg(svgIcon.name, svgIcon.data));
  }

}
