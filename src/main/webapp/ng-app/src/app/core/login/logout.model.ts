export class Logout {
  constructor(public idToken: string, public logoutUrl: string, public identityProvider: string) {}
}
