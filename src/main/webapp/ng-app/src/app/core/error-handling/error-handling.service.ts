import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ErrorModalComponent } from './error-modal/error-modal.component';

@Injectable({
  providedIn: 'root'
})
export class ErrorHandlingService {

  constructor(private ngbModal: NgbModal) {
  }

  openErrorModal(errorBody: string): ErrorModalComponent {
    const modalRef = this.ngbModal.open(ErrorModalComponent, {windowClass: 'error-modal'});
    const modal = modalRef.componentInstance as ErrorModalComponent;

    modal.errorBody = errorBody;

    return modal;
  }
}
