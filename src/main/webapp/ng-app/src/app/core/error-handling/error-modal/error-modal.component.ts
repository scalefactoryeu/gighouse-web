import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-error-modal',
  templateUrl: './error-modal.component.html'
})
export class ErrorModalComponent implements OnInit {

  errorBody: string;

  constructor(public activeModal: NgbActiveModal) {
  }

  ngOnInit() {
  }

}
