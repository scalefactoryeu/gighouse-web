import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { ErrorModalComponent } from './error-modal/error-modal.component';


@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [
    ErrorModalComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ErrorHandlingModule {}
