import { IconProp } from '@fortawesome/fontawesome-svg-core';

export class BaseNavItem {
  path: string;
  label: string;
  svgIcon?: string;
  faIcon?: IconProp;
  routerLinkActiveExact?: boolean;
}

export class NavItem extends BaseNavItem {
  children?: BaseNavItem[];
}

export const appNavItems: NavItem[] = [
  {
    path: '/',
    label: 'global.menu.home',
    svgIcon: 'dashboard',
    routerLinkActiveExact: true
  },
  {
    path: '/projects',
    label: 'global.menu.projects',
    svgIcon: 'clipboard-plus',
    children: [
      {
        path: '/active',
        label: 'global.menu.activeProjects',
        svgIcon: 'dot-circle'
      },
      {
        path: '/archive',
        label: 'global.menu.archivedProjects',
        svgIcon: 'archive'
      },
      {
        path: '/new',
        label: 'global.menu.newProject',
        svgIcon: 'plus'
      }
    ]
  },
  {
    path: '/freelancers',
    label: 'global.menu.freelancers',
    svgIcon: 'hero-cape',
    children: [
      {
        path: '/matches',
        label: 'global.menu.matches',
        svgIcon: 'hug'
      },
      {
        path: '/accepted',
        label: 'global.menu.accepted',
        svgIcon: 'thumbs-up'
      },
      {
        path: '/favourites',
        label: 'global.menu.favourites',
        svgIcon: 'heart'
      },
      {
        path: '/rejected',
        label: 'global.menu.rejected',
        svgIcon: 'thumbs-down'
      },
      {
        path: 'proposed-never-reviewed',
        label : 'global.menu.proposed',
        svgIcon: 'archive'
      }
    ]
  },
  {
    path: '/administration',
    label: 'global.menu.administration',
    svgIcon: 'paperwork',
    children: [
      {
        path: '/timesheets',
        label: 'global.menu.timesheets',
        svgIcon: 'timesheet'
      },
      {
        path: '/invoices',
        label: 'global.menu.invoices',
        svgIcon: 'euro'
      }
      // ,
      // {
      //   path: '/contracts',
      //   label: 'global.menu.contracts',
      //   svgIcon: 'contract'
      // },
      // {
      //   path: '/domiciliering',
      //   label: 'global.menu.domiciliering',
      //   svgIcon: 'transfer'
      // }
    ]
  }
];


export const sysAdminNavItems: NavItem[] = [
  {
    path: '/sys-admin/metrics',
    label: 'global.menu.sys-admin.metrics',
    faIcon: ['fas', 'tachometer-alt']
  },
  {
    path: '/sys-admin/health',
    label: 'global.menu.sys-admin.health',
    faIcon: ['fas', 'heart']
  },
  {
    path: '/sys-admin/configuration',
    label: 'global.menu.sys-admin.configuration',
    faIcon: ['fas', 'list']
  },
  {
    path: '/sys-admin/audits',
    label: 'global.menu.sys-admin.audits',
    faIcon: ['fas', 'bell']
  },
  {
    path: '/sys-admin/logs',
    label: 'global.menu.sys-admin.logs',
    faIcon: ['fas', 'tasks']
  },
  {
    path: '/sys-admin/docs',
    label: 'global.menu.sys-admin.apidocs',
    faIcon: ['fas', 'book']
  }
];
