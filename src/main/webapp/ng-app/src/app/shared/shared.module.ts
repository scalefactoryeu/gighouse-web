import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AlertErrorComponent } from './alert/alert-error.component';
import { HasAnyAuthorityDirective } from './auth/has-any-authority.directive';
import { NgJhipsterModule } from 'ng-jhipster';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FindLanguageFromKeyPipe } from './language/find-language-from-key.pipe';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { SkillRatingComponent } from './components/skill-rating.component';
import { MomentToDatePipe } from './pipes/moment-to-date.pipe';
import { DatepickerComponent } from './components/datepicker/datepicker.component';
import { LoadingComponent } from './components/loading/loading.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import {AutosizeDirective} from './directives/auto-size.directive';

const IMPORT_EXPORT_MODULES = [
  CommonModule,
  FormsModule,
  ReactiveFormsModule,
  TranslateModule,
  NgbModule,
  NgJhipsterModule,
  FontAwesomeModule,
  PerfectScrollbarModule,
  InfiniteScrollModule,
  AngularSvgIconModule
];

const COMPONENTS = [
  AlertErrorComponent,
  SkillRatingComponent,
  DatepickerComponent,
  LoadingComponent
];

const DIRECTIVES = [
  HasAnyAuthorityDirective,
  AutosizeDirective
];

const PIPES = [
  FindLanguageFromKeyPipe,
  MomentToDatePipe
];

@NgModule({
  imports: [
    ...IMPORT_EXPORT_MODULES
  ],
  declarations: [
    ...COMPONENTS,
    ...DIRECTIVES,
    ...PIPES
  ],
  exports: [
    ...IMPORT_EXPORT_MODULES,
    ...COMPONENTS,
    ...DIRECTIVES,
    ...PIPES
  ],
})
export class SharedModule {}
