import {AbstractControl, ValidationErrors, ValidatorFn} from '@angular/forms';

export class AppValidators {

  static phoneNumberValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      // The Phone number must start with a '+' and further contain only numbers and spaces
      const phoneNumberPatter = /^[+]\s*\d+\s*(\d+\s*)*$/;
      const valid = phoneNumberPatter.test(control.value);
      return valid ? null : {phoneNumber: {value: control.value}};
    };
  }

  static vatNumberValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      let valid = true;
      if (control.value !== '') {
        const vatNumberPatter = /^BE\d{10}(?![\s\S])|DE\d{10}(?![\s\S])|FR[0-9A-Z]{2}[0-9]{9}(?![\s\S])|NL[0-9]{9}B[0-9]{2}(?![\s\S])$/;
        valid = vatNumberPatter.test(control.value);
      }

      return valid ? null : {vatNumber: {value: control.value}};
    };
  }

}
