import { Pipe, PipeTransform } from '@angular/core';
import { Moment } from 'moment';
import { GUI_DATE_FORMAT } from '../../core/models/constants';

@Pipe({ name: 'momentToDate' })
export class MomentToDatePipe implements PipeTransform {

  transform(date: Moment): string {
    return date.format(GUI_DATE_FORMAT);
  }

}
