import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-skill-rating',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './skill-rating.component.html'
})
export class SkillRatingComponent implements OnInit {

  @Input() rating: number;
  @Input() canSetRating = false;

  @Output() ratingChange = new EventEmitter<number>();

  mouseOverRating: number;

  private starsNum = 5;

  constructor() {
  }

  ngOnInit() {
  }

  getStarLevels(): number[] {
    return Array(this.starsNum).fill(null).map((item, index) => index + 1);
  }

  changeRating(rating: number) {
    this.ratingChange.next(rating);
  }

  onMouseOverRating(rating: number) {
    this.mouseOverRating = rating;
  }

  onMouseOutRating() {
    this.mouseOverRating = null;
  }

}
