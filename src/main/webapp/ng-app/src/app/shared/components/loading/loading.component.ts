import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-loading',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <fa-icon class="loading-spinner" icon="spinner" [spin]="true" *ngIf="show$|async"></fa-icon>
  `
})
export class LoadingComponent implements OnInit {

  @Input() withDelay = true;

  show$ = new BehaviorSubject<boolean>(false);

  private delayMs = 300;

  constructor() {
  }

  ngOnInit() {
    if (this.withDelay) {
      setTimeout(() => {
        this.show$.next(true);
      }, this.delayMs);
    } else {
      this.show$.next(true);
    }
  }

}
