import { Directive, HostListener } from '@angular/core';

export interface CanDeactivateResult<T> {
  canDeactivate: boolean;
  reason: any; // reason in case canDeactivate=false
}

/**
 * Base components that adds functionality for showing a browser dialog when refreshing or closing tab while the component cannot
 * be deactivated (e.g. unsaved changes in form).
 * Implementation based on:
 * https://medium.com/better-programming/angular-how-keep-user-from-lost-his-data-by-accidentally-leaving-the-page-before-submit-4eeb74420f0d
 */
@Directive()
// tslint:disable-next-line:directive-class-suffix
export abstract class ComponentCanDeactivate {

  abstract canDeactivate(): CanDeactivateResult<any>;

  @HostListener('window:beforeunload', ['$event'])
  unloadNotification($event: any) {
    if (!this.canDeactivate().canDeactivate) {
      $event.returnValue = true;
    }
  }
}
