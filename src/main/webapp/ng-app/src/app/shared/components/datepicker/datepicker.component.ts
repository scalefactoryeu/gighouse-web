import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { NgbCalendar, NgbDate, NgbDateAdapter, NgbDatepicker, NgbPopover } from '@ng-bootstrap/ng-bootstrap';
import { Moment } from 'moment';

@Component({
  selector: 'app-datepicker',
  templateUrl: './datepicker.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatepickerComponent implements OnInit {

  @ViewChild(NgbDatepicker, {static: true}) datepicker: NgbDatepicker;

  private aSelectedDate: Moment;
  private rangeFromDateNgb: NgbDate;
  private rangeToDateNgb: NgbDate | null = null;

  @Input()
  set selectedDate(date: Moment) {
    if (date) {
      this.aSelectedDate = date;
      this.navigateToDate(date);
    }
  }

  get selectedDate(): Moment {
    return this.aSelectedDate;
  }

  @Input()
  set rangeFromDate(date: Moment) {
    this.rangeFromDateNgb = NgbDate.from(this.ngbDateMomentAdapter.fromModel(date));
  }

  @Input()
  set rangeToDate(date: Moment) {
    this.rangeToDateNgb = NgbDate.from(this.ngbDateMomentAdapter.fromModel(date));
  }

  @Input() selectedDateNoHighlight = false; // If true, selected date is not highlighted
  @Input() popover: NgbPopover;

  @Output() selectDate = new EventEmitter<Moment>();

  hoveredDate: NgbDate | null = null;

  constructor(calendar: NgbCalendar, private ngbDateMomentAdapter: NgbDateAdapter<Moment>) {
  }

  ngOnInit(): void {
    // If datepicker is in popover, focus the selected day
    if (this.popover) {
      this.datepicker.focus();
    }
  }

  onDateSelection(date: NgbDate) {
    const momentDate = this.ngbDateMomentAdapter.toModel(date);
    this.selectDate.next(momentDate);

    // If datepicker is in popover, close it when date is selected
    if (this.popover) {
      this.popover.close();
    }
  }

  isHovered(date: NgbDate) {
    return this.rangeFromDateNgb && !this.rangeToDateNgb && this.hoveredDate && date.after(this.rangeFromDateNgb) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return this.rangeToDateNgb && date.after(this.rangeFromDateNgb) && date.before(this.rangeToDateNgb);
  }

  isRange(date: NgbDate) {
    return date.equals(this.rangeFromDateNgb) || (this.rangeToDateNgb && date.equals(this.rangeToDateNgb)) || this.isInside(date) || this.isHovered(date);
  }

  isRangeFrom(date: NgbDate): boolean {
    return date.equals(this.rangeFromDateNgb);
  }

  isRangeTo(date: NgbDate): boolean {
    return date.equals(this.rangeToDateNgb);
  }

  navigate(step: number) {
    const {state, calendar} = this.datepicker;
    this.datepicker.navigateTo(calendar.getNext(state.firstDate, 'm', step));
  }

  navigateToDate(momentDate: Moment) {
    const ngbDate = NgbDate.from(this.ngbDateMomentAdapter.fromModel(momentDate));
    this.datepicker.navigateTo(ngbDate);
  }

  isOutsideOfCurrentMonth(date: NgbDate): boolean {
    const {state} = this.datepicker;
    return state.firstDate.month !== date.month;
  }

}
