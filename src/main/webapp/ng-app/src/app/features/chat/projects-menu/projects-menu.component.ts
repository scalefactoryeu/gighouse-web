import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Project} from '../../project/project.model';

@Component({
  selector: 'app-chat-projects-menu',
  templateUrl: './projects-menu.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProjectsMenuComponent implements OnInit {

  @Input() projects: Project[];
  @Input() projectsLoading: boolean;
  @Input() projectsLoadingFailed: boolean;
  @Input() selectedProject: Project;

  @Output() selectProject = new EventEmitter<Project>();

  perfectScrollbarConfig = {
    suppressScrollX: true
  };

  constructor() {
  }

  ngOnInit() {

  }

}
