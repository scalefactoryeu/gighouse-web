import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from '@angular/core';
import {Project} from '../../../project/project.model';
import {Observable} from 'rxjs';
import {ChatService} from '../../../../core/chat/chat.service';
import {debounceTime, map} from 'rxjs/operators';

@Component({
  selector: 'app-chat-project-menu-item',
  templateUrl: './project-menu-item.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProjectMenuItemComponent {

  @Input() project: Project;
  @Input() selectedProject: Project;

  @Output() selectProject = new EventEmitter<Project>();

  unreadMessages$: Observable<boolean>;

  constructor(chatService: ChatService) {
    this.unreadMessages$ = chatService.subscribedChannels$
      .pipe(
        debounceTime(500), // use debounce so the unread indicator doesn't flash for user's own sent message
        map((channels) => {
          if (channels) {
            const channelsArray = Object.values(channels);

            const matchingChannels = channelsArray.filter(channel => {
              return channel.attributes.projectId === this.project.id;
            });

            if (matchingChannels.length) {
              return matchingChannels.some(channel => channel.unreadMessages > 0);
            }
          }

          return false;
        })
      );
  }


}
