import { Freelancers } from './../freelancer/freelancer.model';
import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { BehaviorSubject, merge, Observable, of, Subject } from 'rxjs';
import { ProjectService } from '../project/project.service';
import { Project } from '../project/project.model';
import { Freelancer } from '../freelancer/freelancer.model';
import { catchError, filter, first, map, mapTo, shareReplay, startWith, switchMap, takeUntil, tap } from 'rxjs/operators';
import { FreelancerService } from '../freelancer/freelancer.service';
import { SPECIAL_PARTICIPANT } from './chat.model';
import { ChatService } from '../../core/chat/chat.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChatComponent implements OnInit, OnDestroy {

  mainMenuExpanded = false;

  projects$: Observable<Project[]>;
  projectsLoading$: Observable<boolean>;
  projectsLoadingFailed$: Observable<boolean>;

  freelancers$: Observable<Freelancers[]>;
  freelancersLoading$: Observable<boolean>;
  freelancersLoadingFailed$: Observable<boolean>;

  selectedProject$: BehaviorSubject<Project> = new BehaviorSubject(null);
  selectedChatParticipant$: BehaviorSubject<Freelancers | SPECIAL_PARTICIPANT> = new BehaviorSubject(null);

  perfectScrollbarConfig = {
    suppressScrollX: true
  };

  private selectedProjectIndex: number; // ToDo: Remove. Only used temporarily to fake loading freelancers per project.
  private destroy$ = new Subject();

  constructor(public chatService: ChatService, private projectService: ProjectService, private freelancerService: FreelancerService) { }

  ngOnInit(): void {
    this.loadProjects();
    this.initLoadingFreelancers();
  }

  private loadProjects() {
    this.projects$ = this.projectService.getChatProjects()
      .pipe(
        catchError(() => {
          return of(null);
        }),
        takeUntil(this.destroy$),
        shareReplay()
      );

    this.projectsLoading$ = this.projects$
      .pipe(
        map(result => {
          return false;
        }),
        startWith(true)
      );

    this.projectsLoadingFailed$ = this.projects$
      .pipe(
        map(result => {
          return result === null;
        })
      );

    this.projects$
      .pipe(first())
      .subscribe((projects) => {
        if (projects && projects.length) {
          this.selectProject(projects[0], projects);
        }
      });
  }

  private initLoadingFreelancers() {
    const onSelectedProject$ = this.selectedProject$
      .pipe(
        filter(selectedProject => selectedProject !== null),
      );

    this.freelancers$ = onSelectedProject$
      .pipe(
        switchMap((project) => {
          return this.freelancerService.getChatFreelancersPerProject(project.id, this.selectedProjectIndex);
        }),
        catchError(() => {
          return of(null);
        }),
        shareReplay(),
        takeUntil(this.destroy$)
      );

    this.freelancersLoading$ = merge(
      onSelectedProject$.pipe(mapTo(true)),
      this.freelancers$.pipe(mapTo(false))
    );

    this.freelancersLoadingFailed$ = merge(
      onSelectedProject$.pipe(mapTo(false)),
      this.freelancers$.pipe(map(result => result === null))
    );

    this.freelancers$
      .subscribe((freelancers) => {
        if (freelancers && freelancers.length) {
          this.selectChatParticipant(freelancers[0]);
        } else {
          this.selectChatParticipant(SPECIAL_PARTICIPANT.GIGHOUSEADMIN);
        }
      });
  }

  toggleMenu() {
    this.mainMenuExpanded = !this.mainMenuExpanded;
  }

  selectProject(project: Project, allProjects: Project[]) {
    this.selectedProjectIndex = allProjects.findIndex(aProject => aProject.id === project.id);
    this.selectedProject$.next(project);

    // Unselect previous chat participant
    this.selectedChatParticipant$.next(null);
  }

  selectChatParticipant(chatParticipant: Freelancers | SPECIAL_PARTICIPANT) {
    this.selectedChatParticipant$.next(chatParticipant);
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
