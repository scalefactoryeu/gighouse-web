import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ChatComponent } from './chat.component';
import {SharedModule} from '../../shared/shared.module';
import {LayoutsModule} from '../../layouts/layouts.module';
import {ProjectsMenuComponent} from './projects-menu/projects-menu.component';
import {chatRoutes} from './chat-routes';
import {FreelancersMenuComponent} from './freelancers-menu/freelancers-menu.component';
import {FreelancerSharedModule} from '../freelancer/freelancer-shared/freelancer-shared.module';
import {ChatWindowComponent} from './chat-window/chat-window.component';
import {ChatWindowFooterComponent} from './chat-window/chat-window-footer/chat-window-footer.component';
import {ChatBlockComponent} from './chat-window/chat-block/chat-block.component';
import {FreelancerMenuItemComponent} from './freelancers-menu/freelancer-menu-item/freelancer-menu-item.component';
import {ProjectMenuItemComponent} from './projects-menu/project-menu-item/project-menu-item.component';
import {ChatDatePipe} from './pipes/chat-date.pipe';
import {ChatMessageTimePipe} from './pipes/chat-time.pipe';

@NgModule({
  imports: [
    SharedModule,
    LayoutsModule,
    RouterModule.forChild(chatRoutes),
    FreelancerSharedModule
  ],
  declarations: [
    ChatComponent,
    ProjectsMenuComponent,
    ProjectMenuItemComponent,
    FreelancersMenuComponent,
    FreelancerMenuItemComponent,
    ChatWindowComponent,
    ChatWindowFooterComponent,
    ChatBlockComponent,

    ChatDatePipe,
    ChatMessageTimePipe
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ChatModule {}
