import {Pipe, PipeTransform} from '@angular/core';
import {ChatDatesService} from '../../../core/chat/chat-dates.service';

@Pipe({
  name: 'chatMessageTimePipe'
})
export class ChatMessageTimePipe implements PipeTransform {

  constructor(private chatDatesService: ChatDatesService) {

  }

  transform(date: Date): string {
    return this.chatDatesService.getChatMessageTime(date);
  }
}
