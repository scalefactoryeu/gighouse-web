import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {ChatMessagesByAuthor} from '../../../../core/chat/chat.model';
import {Freelancer} from '../../../freelancer/freelancer.model';
import {SPECIAL_PARTICIPANT} from '../../chat.model';

@Component({
  selector: 'app-chat-block',
  templateUrl: './chat-block.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChatBlockComponent {

  @Input() messagesByAuthorItem: ChatMessagesByAuthor;
  @Input() participant: Freelancer | SPECIAL_PARTICIPANT;

  getAuthorDisplayName(): string {
    if (this.messagesByAuthorItem.isCurrentUserAuthor) {
      return 'You'; // todo: translate
    }


    // ToDo: Only working with the freelancer for now, in the future we will take into account GigAdmin (should return name 'GigHouse')
    const participant = this.participant as Freelancer;

    if (participant.name === SPECIAL_PARTICIPANT.GIGHOUSEADMIN) {
      return 'Gighouse Account Manager';
    }


    if (this.messagesByAuthorItem.author === participant.id) {
      return participant.name;
    }

    return this.messagesByAuthorItem.author;
  }

}
