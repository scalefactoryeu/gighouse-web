import {ChangeDetectionStrategy, Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormControl} from '@angular/forms';
import {ChatService} from '../../../../core/chat/chat.service';
import {ErrorHandlingService} from '../../../../core/error-handling/error-handling.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-chat-window-footer',
  templateUrl: './chat-window-footer.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChatWindowFooterComponent implements OnInit {

  @Output() typing = new EventEmitter<boolean>();

  chatForm = this.fb.group({
    message: ['', []],
  });

  get chatMessage() {
    return this.chatForm.get('message') as FormControl;
  }

  constructor(private fb: FormBuilder, private chatService: ChatService, private errorHandlingService: ErrorHandlingService,
              private translate: TranslateService) {
  }

  ngOnInit() {
    this.chatMessage.valueChanges.subscribe(() => this.typing.next());
  }

  /**
   * Prevent going to a new line when Enter is pressed. Note that Shift+Enter will still work to go to new line.
   */
  onKeyDownEnter(evt) {
    evt.preventDefault();
  }

  /**
   * When Enter key is pressed alone we will send a message. Note that Shift+Enter will not trigger this event.
   */
  onKeyUpEnter(evt) {
    evt.preventDefault();
    this.sendMessage();
  }

  sendMessage() {
    if (!this.chatMessage.value) {
      return;
    }

    this.chatService.sendMessage(this.chatMessage.value)
      .then(() => {
        this.chatMessage.setValue('');
      })
      .catch(() => {
        this.errorHandlingService.openErrorModal(this.translate.instant('chat.sendMessageError'));
      });
  }
}
