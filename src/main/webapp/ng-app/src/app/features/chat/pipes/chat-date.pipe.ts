import {Pipe, PipeTransform} from '@angular/core';
import {ChatDatesService} from '../../../core/chat/chat-dates.service';

@Pipe({
  name: 'chatDate'
})
export class ChatDatePipe implements PipeTransform {

  constructor(private chatDatesService: ChatDatesService) {

  }

  transform(date: Date): string {
    return this.chatDatesService.getChatDateFormatPerDay(date);
  }
}
