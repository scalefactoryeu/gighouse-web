import { Freelancers } from './../../freelancer/freelancer.model';
import { ChangeDetectionStrategy, Component, ElementRef, Input, OnChanges, OnDestroy, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { Freelancer } from '../../freelancer/freelancer.model';
import { SPECIAL_PARTICIPANT } from '../chat.model';
import { Project } from '../../project/project.model';
import { ChatService } from '../../../core/chat/chat.service';
import { takeUntil } from 'rxjs/operators';
import { Subject, merge } from 'rxjs';

@Component({
  selector: 'app-chat-window',
  templateUrl: './chat-window.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChatWindowComponent implements OnInit, OnDestroy, OnChanges {

  @Input() project: Project;
  @Input() participant: Freelancers | SPECIAL_PARTICIPANT;

  @ViewChild('chatWindow') private chatWindow: ElementRef;

  private destroy$ = new Subject();

  constructor(public chatService: ChatService) {

  }

  ngOnInit(): void {
    // ToDo:
    //  This can be further improved to only scroll down when:
    //   - current user sends a message (so he can see his own message)
    //   - message from freelancer is received AND scrollbar was already at the bottom of the window
    //   - typing event is received AND scrollbar was already at the bottom of the window
    //   When user went up in the window to read old messages we don't want to scroll down on new message by freelancer or 'typing' event.
    merge(
      this.chatService.chatMessagesByDay$,
      this.chatService.isTyping$
    )
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.scrollChatWindowToBottom();
      });

  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['participant'] && this.participant) {
      this.selectChannel();
    }
  }

  private selectChannel() {
    const participant = this.participant as Freelancers;
    if (participant.name === 'GIGHOUSEADMIN') {
      this.chatService.selectAdminChannel(this.project.id);
    }
    else {
      this.chatService.selectChannel(this.project.id, participant.id);
    }

  }

  typing() {
    this.chatService.typing();
  }

  private scrollChatWindowToBottom() {
    if (this.chatWindow) {
      setTimeout(() => {
        this.chatWindow.nativeElement.scrollTop = this.chatWindow.nativeElement.scrollHeight;
      });
    }
  }

  ngOnDestroy(): void {
    this.chatService.resetChannel();
    this.destroy$.next();
  }

}
