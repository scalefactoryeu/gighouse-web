import { Freelancers } from './../../../freelancer/freelancer.model';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { Freelancer } from '../../../freelancer/freelancer.model';
import { SPECIAL_PARTICIPANT } from '../../chat.model';
import { Project } from '../../../project/project.model';
import { Observable } from 'rxjs';
import { ChatService } from '../../../../core/chat/chat.service';
import { debounceTime, map } from 'rxjs/operators';

@Component({
  selector: 'app-chat-freelancer-menu-item',
  templateUrl: './freelancer-menu-item.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FreelancerMenuItemComponent {

  @Input() freelancer: Freelancers;
  @Input() selectedChatParticipant: Freelancers | SPECIAL_PARTICIPANT;
  @Input() selectedProject: Project;

  @Output() selectChatParticipant = new EventEmitter<Freelancers | SPECIAL_PARTICIPANT>();

  specialParticipant = SPECIAL_PARTICIPANT;

  unreadMessagesNum$: Observable<number>;

  constructor(chatService: ChatService) {
    this.unreadMessagesNum$ = chatService.subscribedChannels$
      .pipe(
        debounceTime(500), // use debounce so the unread indicator doesn't flash for user's own sent message
        map((channels) => {
          if (channels) {
            const channelsArray = Object.values(channels);

            const matchingChannel = channelsArray.find(channel => {
              return channel.attributes.projectId === this.selectedProject.id &&
                channel.attributes.participants.includes(this.freelancer.id);
            });

            if (matchingChannel) {
              return matchingChannel.unreadMessages;
            }
          }

          return 0;
        })
      );
  }


}
