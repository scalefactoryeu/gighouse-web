import { Freelancers } from './../../freelancer/freelancer.model';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Freelancer } from '../../freelancer/freelancer.model';
import { SPECIAL_PARTICIPANT } from '../chat.model';
import { Project } from '../../project/project.model';
import { identity } from 'lodash';

@Component({
  selector: 'app-chat-freelancers-menu',
  templateUrl: './freelancers-menu.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FreelancersMenuComponent implements OnInit {

  @Input() freelancers: Freelancers[];
  @Input() freelancersLoading: boolean;
  @Input() freelancersLoadingFailed: boolean;
  @Input() selectedChatParticipant: Freelancers | SPECIAL_PARTICIPANT;
  @Input() selectedProject: Project;

  @Output() selectChatParticipant = new EventEmitter<Freelancers | SPECIAL_PARTICIPANT>();

  specialParticipant = SPECIAL_PARTICIPANT;
  admin: Freelancers = new Freelancers({
    id: SPECIAL_PARTICIPANT.GIGHOUSEADMIN,
    name: SPECIAL_PARTICIPANT.GIGHOUSEADMIN,
    rating: '',
    type: ''
  })

  constructor() {
  }

  ngOnInit() {

  }

}
