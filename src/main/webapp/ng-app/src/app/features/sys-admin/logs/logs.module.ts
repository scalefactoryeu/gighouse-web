import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { LogsComponent } from './logs.component';

import { logsRoute } from './logs.route';
import { SharedModule } from '../../../shared/shared.module';

@NgModule({
  imports: [SharedModule, RouterModule.forChild([logsRoute])],
  declarations: [LogsComponent],
})
export class LogsModule {}
