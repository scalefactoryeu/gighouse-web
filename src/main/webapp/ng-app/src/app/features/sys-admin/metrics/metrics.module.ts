import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MetricsComponent } from './metrics.component';

import { metricsRoute } from './metrics.route';
import { SharedModule } from '../../../shared/shared.module';

@NgModule({
  imports: [SharedModule, RouterModule.forChild([metricsRoute])],
  declarations: [MetricsComponent],
})
export class MetricsModule {}
