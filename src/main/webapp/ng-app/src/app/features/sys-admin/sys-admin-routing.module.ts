import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SysAdminEntryComponent } from './sys-admin-entry.component';
import { LayoutsModule } from '../../layouts/layouts.module';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [
    SharedModule,
    LayoutsModule,
    RouterModule.forChild([
      {
        path: '',
        component: SysAdminEntryComponent,
        children: [
          {
            path: '',
            redirectTo: 'metrics'
          },
          {
            path: 'audits',
            loadChildren: () => import('./audits/audits.module').then(m => m.AuditsModule),
          },
          {
            path: 'configuration',
            loadChildren: () => import('./configuration/configuration.module').then(m => m.ConfigurationModule),
          },
          {
            path: 'docs',
            loadChildren: () => import('./docs/docs.module').then(m => m.DocsModule),
          },
          {
            path: 'health',
            loadChildren: () => import('./health/health.module').then(m => m.HealthModule),
          },
          {
            path: 'logs',
            loadChildren: () => import('./logs/logs.module').then(m => m.LogsModule),
          },
          {
            path: 'metrics',
            loadChildren: () => import('./metrics/metrics.module').then(m => m.MetricsModule),
          },
        ]
      }
    ]),
  ],
  declarations: [SysAdminEntryComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SysAdminRoutingModule {}
