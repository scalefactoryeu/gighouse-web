import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { LoggersResponse, Level } from './log.model';
import { environment } from '../../../../environments/environment';

@Injectable({ providedIn: 'root' })
export class LogsService {
  constructor(private http: HttpClient) {}

  changeLevel(name: string, configuredLevel: Level): Observable<{}> {
    return this.http.post(environment.SERVER_API_URL + 'management/loggers/' + name, { configuredLevel });
  }

  findAll(): Observable<LoggersResponse> {
    return this.http.get<LoggersResponse>(environment.SERVER_API_URL + 'management/loggers');
  }
}
