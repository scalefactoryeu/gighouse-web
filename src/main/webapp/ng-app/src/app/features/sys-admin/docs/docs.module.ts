import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { DocsComponent } from './docs.component';

import { docsRoute } from './docs.route';
import { SharedModule } from '../../../shared/shared.module';

@NgModule({
  imports: [SharedModule, RouterModule.forChild([docsRoute])],
  declarations: [DocsComponent],
})
export class DocsModule {}
