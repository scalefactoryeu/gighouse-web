import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { BehaviorSubject, Observable, of, Subject, throwError } from 'rxjs';
import { catchError, distinctUntilChanged, first, map, shareReplay, skip, startWith, takeUntil, tap } from 'rxjs/operators';
import { Project } from '../../project/project.model';
import { ProjectService } from '../../project/project.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-freelancer-matches',
  templateUrl: './freelancer-matches.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FreelancerMatchesComponent implements OnInit, OnDestroy {

  matchesProjects$: Observable<Project[]>;
  allProjects$: Observable<Project[]>;
  selectedProject$ = new BehaviorSubject<Project>(null);
  loadingProjects$: Observable<boolean>;
  loadingProjectsFailed$ = new BehaviorSubject<boolean>(false);

  perfectScrollbarConfig = {
    suppressScrollX: true
  };

  private destroy$ = new Subject();
  private readonly projectIdQueryParamKey = 'projectId';

  constructor(private projectService: ProjectService, private activatedRoute: ActivatedRoute, private router: Router) {}

  ngOnInit(): void {
    let queryParamProjectId: string;

    this.activatedRoute.queryParamMap
      .pipe(first())
      .subscribe(queryParamMap => {
        queryParamProjectId = queryParamMap.get(this.projectIdQueryParamKey);
      });


    this.allProjects$ = this.projectService.getDashboardProjects()
      .pipe(
        catchError((error) => {
          return of(null);
        }),
        shareReplay()
      );
    this.matchesProjects$ = this.projectService.getMatchesProjects()
      .pipe(
        shareReplay(),
        tap(projects => {
          if (projects && projects.length && !this.selectedProject$.getValue()) {
            this.selectInitialProject(projects, queryParamProjectId);
          }
        }),
        catchError((error) => {
          this.loadingProjectsFailed$.next(true);
          return throwError(error);
        })
      );

    this.loadingProjects$ = this.matchesProjects$
      .pipe(
        map(result => {
          return false;
        }),
        catchError((error) => {
          return of(false);
        }),
        startWith(true)
      );

    this.selectedProject$
      .pipe(
        skip(1), // skip 1 because initially selectedProject is null so we are not interested
        distinctUntilChanged((prev, current) => {
          return prev.id === current.id;
        }),
        takeUntil(this.destroy$)
      )
      .subscribe(selectedProject => {
        this.addSelectedProjectToQueryParams(selectedProject);
      });

  }

  selectProject(project: Project) {
    this.selectedProject$.next(project);
  }

  private addSelectedProjectToQueryParams(selectedProject: Project) {
    this.router.navigate(
      [],
      {
        relativeTo: this.activatedRoute,
        queryParams: { [this.projectIdQueryParamKey]: selectedProject.id },
        queryParamsHandling: 'merge'
      });
  }

  private selectInitialProject(projects: Project[], queryParamProjectId: string) {
    if (queryParamProjectId) {
      const initialSelectedProject = projects.find(project => project.id === queryParamProjectId);

      if (initialSelectedProject) {
        // select project requested from url query params
        this.selectedProject$.next(initialSelectedProject);
        return;
      }
    }

    // select the first project from list
    this.selectedProject$.next(projects[0]);
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
