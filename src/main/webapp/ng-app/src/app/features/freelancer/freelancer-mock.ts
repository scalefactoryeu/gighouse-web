import {IFreelancerBackend, IFreelancerLanguageBackend, IFreelancerSkillBackend, LanguageLevel} from './freelancer.model';

export const freelancerSkillsMock: IFreelancerSkillBackend[] = [
  /* Specialisation/Skill types */
  // {
  //   rating: 3,
  //   sfid: '#sfid',
  //   skill: {
  //     name: 'Information Security',
  //     type: 'Specialization',
  //     parent: {
  //       name: 'Cybersecurity',
  //       type: 'Specialization',
  //       parent: {
  //         name: 'IT',
  //         type: 'Function',
  //         parent: null,
  //         id: 'a0P9E000005DNGWUA4'
  //       },
  //       id: 'a0P9E000005DNGzUAO'
  //     },
  //     id: 'a0P9E000005DNPbUAO'
  //   }
  // },
  // {
  //   rating: 4,
  //   sfid: '#sfid',
  //   skill: {
  //     name: 'Cloud system management',
  //     type: 'Specialization',
  //     parent: {
  //       name: 'Cloud computing',
  //       type: 'Specialization',
  //       parent: {
  //         name: 'IT',
  //         type: 'Function',
  //         parent: null,
  //         id: 'a0P9E000005DNGWUA4'
  //       },
  //       id: 'a0P9E000005DNH9UAO'
  //     },
  //     id: 'a0P9E000005DNPdUAO'
  //   }
  // },
  // {
  //   rating: 4,
  //   sfid: '#sfid',
  //   skill: {
  //     name: 'Data mining',
  //     type: 'Specialization',
  //     parent: {
  //       name: 'Data analytics and data science',
  //       type: 'Specialization',
  //       parent: {
  //         name: 'IT',
  //         type: 'Function',
  //         parent: null,
  //         id: 'a0P9E000005DNGWUA4'
  //       },
  //       id: 'a0P9E000005DNHJUA4'
  //     },
  //     id: 'a0P9E000005DNPfUAO'
  //   }
  // },
  // {
  //   rating: 3,
  //   sfid: '#sfid',
  //   skill: {
  //     name: 'HTML5',
  //     type: 'Specialization',
  //     parent: {
  //       name: 'Programming',
  //       type: 'Specialization',
  //       parent: {
  //         name: 'IT',
  //         type: 'Function',
  //         parent: null,
  //         id: 'a0P9E000005DNGWUA4'
  //       },
  //       id: 'a0P9E000005DNHdUAO'
  //     },
  //     id: 'a0P9E000005DNPhUAO'
  //   }
  // },
  // {
  //   rating: 4,
  //   sfid: '#sfid',
  //   skill: {
  //     name: 'CSS',
  //     type: 'Specialization',
  //     parent: {
  //       name: 'Programming',
  //       type: 'Specialization',
  //       parent: {
  //         name: 'IT',
  //         type: 'Function',
  //         parent: null,
  //         id: 'a0P9E000005DNGWUA4'
  //       },
  //       id: 'a0P9E000005DNHdUAO'
  //     },
  //     id: 'a0P9E000005DNPiUAO'
  //   }
  // },
  // {
  //   rating: 4,
  //   sfid: '#sfid',
  //   skill: {
  //     name: 'Sass',
  //     type: 'Specialization',
  //     parent: {
  //       name: 'Programming',
  //       type: 'Specialization',
  //       parent: {
  //         name: 'IT',
  //         type: 'Function',
  //         parent: null,
  //         id: 'a0P9E000005DNGWUA4'
  //       },
  //       id: 'a0P9E000005DNHdUAO'
  //     },
  //     id: 'a0P9E000005DNPjUAO'
  //   }
  // },
  // /* Tool types */
  // {
  //   rating: 3,
  //   sfid: '#sfid',
  //   skill: {
  //     name: 'Wordpress',
  //     type: 'Tools',
  //     parent: {
  //       name: 'Programming',
  //       type: 'Tools',
  //       parent: {
  //         name: 'IT',
  //         type: 'Function',
  //         parent: null,
  //         id: 'a0P9E000005DNGWUA4'
  //       },
  //       id: 'a0P9E000005DNL0UAO'
  //     },
  //     id: 'a0P9E000005DNQcUAO'
  //   }
  // },
  // {
  //   rating: 1,
  //   sfid: '#sfid',
  //   skill: {
  //     name: 'Wix',
  //     type: 'Tools',
  //     parent: {
  //       name: 'Programming',
  //       type: 'Tools',
  //       parent: {
  //         name: 'IT',
  //         type: 'Function',
  //         parent: null,
  //         id: 'a0P9E000005DNGWUA4'
  //       },
  //       id: 'a0P9E000005DNL0UAO'
  //     },
  //     id: 'a0P9E000005DNQdUAO'
  //   }
  // },
  // {
  //   rating: 3,
  //   sfid: '#sfid',
  //   skill: {
  //     name: 'Joomla',
  //     type: 'Tools',
  //     parent: {
  //       name: 'Programming',
  //       type: 'Tools',
  //       parent: {
  //         name: 'IT',
  //         type: 'Function',
  //         parent: null,
  //         id: 'a0P9E000005DNGWUA4'
  //       },
  //       id: 'a0P9E000005DNL0UAO'
  //     },
  //     id: 'a0P9E000005DNQeUAO'

  //   }
  // },
  // {
  //   rating: 4,
  //   sfid: '#sfid',
  //   skill: {
  //     name: 'Nginx',
  //     type: 'Tools',
  //     parent: {
  //       name: 'Cloud computing',
  //       type: 'Tools',
  //       parent: {
  //         name: 'IT',
  //         type: 'Function',
  //         parent: null,
  //         id: 'a0P9E000005DNGWUA4'
  //       },
  //       id: 'a0P9E000005DNI6UAO'
  //     },
  //     id: 'a0P9E000005DNQUUA4'
  //   }
  // },
  // {
  //   rating: 4,
  //   sfid: '#sfid',
  //   skill: {
  //     name: 'AWS',
  //     type: 'Tools',
  //     parent: {
  //       name: 'Cloud computing',
  //       type: 'Tools',
  //       parent: {
  //         name: 'IT',
  //         type: 'Function',
  //         parent: null,
  //         id: 'a0P9E000005DNGWUA4'
  //       },
  //       id: 'a0P9E000005DNI6UAO'
  //     },
  //     id: 'a0P9E000005DNQWUA4'
  //   }
  // }
];


export const freelancerLanguagesMock: IFreelancerLanguageBackend[] = [
  {
    language: 'Dutch',
    level: LanguageLevel.NATIVE
  },
  {
    language: 'English',
    level: LanguageLevel.EXPERT
  },
  {
    language: 'French',
    level: LanguageLevel.EXPERT
  },
  {
    language: 'German',
    level: LanguageLevel.INTERMEDIATE
  },
  {
    language: 'Spanish',
    level: LanguageLevel.BASIC
  }
];

export const freelancersMock: IFreelancerBackend[] = [
  // {
  //   name: 'Joren Vos',
  //   type: FreelancerType.MATCH,
  //   description: 'Dit is lijn 1 Dit is lijn 2 En een derde erbij Nog een vierde Een vijfde, waarom niet!',
  //   rating: null,
  //   state: null,
  //   availabilityDate: null,
  //   availableDaysPerWeek: null,
  //   availableForTeleworking: false,
  //   doesEveningWork: true,
  //   doesWeekendWork: true,
  //   isFavourite: true,
  //   rate: 2,
  //   hasOwnCar: false,
  //   skills: [],
  //   languages: [],
  //   id: '0039E000010OaAfQAK',
  //   contactId: '0039E000010OaAfQAK',
  //   favouriteId: '0039E000010OaAfQAK',
  //   candidateId: '0039E000010OaAfQAK',
  //   profileName: 'Web developer',
  //   gighouseOpinion: 'Very good!',
  //   matchingProjects: [],
  // },
  // {
  //   name: 'Lieven Degrauwe',
  //   description: 'test opnieuw CR',
  //   rating: 'A',
  //   city: 'Moorslede',
  //   availabilityDate: '2020-01-01',
  //   availableDaysPerWeek: 2,
  //   willingToTravelAbroad: true,
  //   maximumTravelTime: '02:00:00',
  //   maximumTravelDistance: 90,
  //   teleworking: true,
  //   feeComment: null,
  //   dayFee: 600,
  //   fixedFee: false,
  //   hourlyFee: null,
  //   ownCar: true,
  //   otherAspiredProfile: null,
  //   consent: false,
  //   consentDate: null,
  //   friendly: false,
  //   account: {
  //     recordTypeId: '0121t000000H6TvAAK',
  //     contacts: null,
  //     id: '0011t00000DziIjAAJ'
  //   },
  //   skills: [],
  //   languages: [],
  //   id: '0031t00000AqHuUAAV',
  //   profileName: 'Development UX/UI'
  // }
];
