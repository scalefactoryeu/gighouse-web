import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {BehaviorSubject, Subject, throwError} from 'rxjs';
import {Freelancer} from '../../freelancer.model';
import {Project, ProjectDetails} from '../../../project/project.model';
import {catchError, takeUntil} from 'rxjs/operators';
import {FreelancerService} from '../../freelancer.service';
import {cloneDeep} from 'lodash';
import {FreelancerRejectModalComponent} from '../../freelancer-reject-modal/freelancer-reject-modal.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ErrorHandlingService} from '../../../../core/error-handling/error-handling.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-freelancer-matches-list',
  templateUrl: './freelancer-matches-list.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FreelancerMatchesListComponent implements OnInit, OnDestroy {

  project: Project | ProjectDetails;

  @Input()
  set selectedProject(selectedProject: Project | ProjectDetails) {
    this.onSelectedProjectChange(selectedProject);
    this.project = selectedProject;
  }

  @Input() freelancerCardContainerClass: string;

  @Output() loadedFreelancers = new EventEmitter<Freelancer[]>();

  @Output() loadedAcceptedFreelancers = new EventEmitter<Freelancer[]>();

  freelancers$ = new BehaviorSubject<Freelancer[]>([]);
  loadingFreelancers$ = new BehaviorSubject<boolean>(false);
  loadingFreelancersFailed$ = new BehaviorSubject<boolean>(false);

  acceptedFreelancers$ = new BehaviorSubject<Freelancer[]>([]);
  loadingAcceptedFreelancers$ = new BehaviorSubject<boolean>(false);
  loadingAcceptedFreelancersFailed$ = new BehaviorSubject<boolean>(false);

  private destroy$ = new Subject();
  private cancelLoadingFreelancers$ = new Subject();
  private cancelLoadingAcceptedFreelancers$ = new Subject();
  private showAllFreelancers = false;


  constructor(private freelancerService: FreelancerService, private ngbModal: NgbModal, private errorHandlingService: ErrorHandlingService,
              private translate: TranslateService) {
  }

  ngOnInit() {
  }

  onSelectedProjectChange(selectedProject: Project | ProjectDetails) {
    this.freelancers$.next([]); // make the list empty
    this.cancelLoadingFreelancers$.next(); // cancel previous request
    this.loadingFreelancers$.next(true); // show 'loading...'
    this.loadingFreelancersFailed$.next(false);

    this.acceptedFreelancers$.next([]); // make the list empty
    this.cancelLoadingAcceptedFreelancers$.next(); // cancel previous request
    this.loadingAcceptedFreelancers$.next(true); // show 'loading...'
    this.loadingAcceptedFreelancersFailed$.next(false);

    this.getAcceptedFreelancersPerProject(selectedProject);
    this.getFreelancersPerProject(selectedProject);
  }

  private getFreelancersPerProject(project: Project | ProjectDetails) {

    if (project instanceof ProjectDetails) {
      this.showAllFreelancers = true;
    }

    return this.freelancerService.getFreelancerMatchesPerProject(project.id, this.showAllFreelancers)
      .pipe(
        takeUntil(this.cancelLoadingFreelancers$),
        catchError((error) => {
          this.loadingFreelancersFailed$.next(true);
          this.loadingFreelancers$.next(false);
          return throwError(error);
        })
      )
      .subscribe(freelancers => {
        this.freelancers$.next(freelancers);
        this.loadedFreelancers.next(freelancers);
        this.loadingFreelancers$.next(false);
      });
  }

  private getAcceptedFreelancersPerProject(project: Project | ProjectDetails) {
    let allFreelancers = false;
    if (project instanceof ProjectDetails) {
      allFreelancers = true;
    }

    return this.freelancerService.getAcceptedFreelancerPerProject(project.id, allFreelancers)
      .pipe(
        takeUntil(this.cancelLoadingAcceptedFreelancers$),
        catchError((error) => {
          this.loadingAcceptedFreelancersFailed$.next(true);
          this.loadingAcceptedFreelancers$.next(false);
          return throwError(error);
        })
      )
      .subscribe(freelancers => {
        this.acceptedFreelancers$.next(freelancers);
        this.loadedAcceptedFreelancers.next(freelancers);
        this.loadingAcceptedFreelancers$.next(false);
      });
  }

  showNoFreelancersMessage(): boolean {
    return this.loadingFreelancers$.getValue() === false &&
      this.loadingFreelancersFailed$.getValue() === false &&
      this.freelancers$.getValue().length === 0 &&
      this.showAlreadyAFreelancerAcceptedMessage() === false;
  }

  showAlreadyAFreelancerAcceptedMessage(): boolean {
    return this.loadingAcceptedFreelancers$.getValue() === false &&
      this.showAllFreelancers === false &&
      this.loadingAcceptedFreelancersFailed$.getValue() === false &&
      this.acceptedFreelancers$.getValue().length > 0;
  }

  toggleFavourite(freelancer: Freelancer) {
    const newFavouriteValue = !freelancer.isFavourite;

    // Change state immediately on local data for UX. In case change fails on the backend, we will revert the state in GUI.
    this.toggleFavouriteOnLocalData(freelancer, true);

    this.freelancerService.setFreelancerFavourite(freelancer.contactId, newFavouriteValue)
      .pipe(
        catchError((error) => {
          let errorBody: string;
          if (newFavouriteValue) {
            errorBody = this.translate.instant('freelancers.setFavouriteFailure', {freelancerName: freelancer.name});
          } else {
            errorBody = this.translate.instant('freelancers.unsetFavouriteFailure', {freelancerName: freelancer.name});
          }

          this.errorHandlingService.openErrorModal(errorBody);
          this.toggleFavouriteOnLocalData(freelancer, false);

          return throwError(error);
        })
      )
      .subscribe(() => {
        this.toggleFavouriteOnLocalData(freelancer, false, false);
      });
  }

  private toggleFavouriteOnLocalData(freelancer: Freelancer, isFavouriteUpdating: boolean, toggleValue = true) {
    const currentFreelancers = cloneDeep(this.freelancers$.getValue());
    const mutatedFreelancer = currentFreelancers.find(cFreelancer => cFreelancer.id === freelancer.id);

    if (mutatedFreelancer) {
      mutatedFreelancer.isFavouriteUpdating = isFavouriteUpdating;

      if (toggleValue) {
        mutatedFreelancer.isFavourite = !mutatedFreelancer.isFavourite;
      }

      this.freelancers$.next(currentFreelancers);
    }
  }

  reject(freelancer: Freelancer) {
    if (freelancer.isRejected()) {
      return;
    }

    const modalRef = this.ngbModal.open(FreelancerRejectModalComponent, {windowClass: 'freelancer-reject-modal', backdrop: 'static'});
    const modal = modalRef.componentInstance as FreelancerRejectModalComponent;

    modal.freelancer = freelancer;
    modalRef.result
      .then(
        () => this.onRejected(freelancer),
        () => {
        }
      );
  }

  private onRejected(freelancer: Freelancer) {
    if (freelancer.isRejected()) {
      return;
    }

    freelancer.candidateStatus = 'Rejected';
    const currentFreelancers = cloneDeep(this.freelancers$.getValue());
    const mutatedFreelancer = currentFreelancers.find(cFreelancer => cFreelancer.id === freelancer.id);
    mutatedFreelancer.candidateStatus = 'Rejected';
    this.freelancers$.next(currentFreelancers);
  }

  meet(freelancer: Freelancer) {
    if (freelancer.isAccepted()) {
      return;
    }

    this.freelancerService.acceptFreelancer(freelancer)
      .pipe(
        catchError((error) => {
          this.errorHandlingService.openErrorModal(this.translate.instant('freelancers.errorApprovingCandidate'));
          return throwError(error);
        })
      ).subscribe();
  }

  trackByFn(index: number, freelancer: Freelancer): string {
    return freelancer.contactId;
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();

    this.cancelLoadingFreelancers$.next();
    this.cancelLoadingFreelancers$.complete();
  }
}
