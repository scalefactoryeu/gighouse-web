import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/shared.module';
import { LayoutsModule } from '../../../layouts/layouts.module';
import { FreelancerMatchesListComponent } from './freelancer-matches-list/freelancer-matches-list.component';
import { FreelancerRatingComponent } from './freelancer-rating.component';
import { FreelancerCardComponent } from './freelancer-card/freelancer-card.component';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    SharedModule,
    LayoutsModule,
    RouterModule
  ],
  declarations: [
    FreelancerMatchesListComponent,
    FreelancerCardComponent,
    FreelancerRatingComponent
  ],
  exports: [
    FreelancerMatchesListComponent,
    FreelancerCardComponent,
    FreelancerRatingComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class FreelancerSharedModule {}
