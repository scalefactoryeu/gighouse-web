import {FreelancerMatchesComponent} from './freelancer-matches/freelancer-matches.component';
import {FreelancerFavouritesComponent} from './freelancer-favourites/freelancer-favourites.component';
import {FreelancerRejectedComponent} from './freelancer-rejected/freelancer-rejected.component';
import {FreelancerProfileComponent} from './freelancer-profile/freelancer-profile.component';
import {Routes} from '@angular/router';
import {FreelancerAcceptedComponent} from "./freelancer-accepted/freelancer-accepted.component";
import {FreelancerProposedNeverReviewedComponent} from "./freelancer-proposed-never-reviewed/freelancer-proposed-never-reviewed.component";

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'matches'
  },
  {
    path: 'matches',
    component: FreelancerMatchesComponent,
    data: {
      authorities: [],
      pageTitle: 'freelancers.matchesTitle'
    }
  },
  {
    path: 'matches/details/:id',
    component: FreelancerProfileComponent,
    data: {
      authorities: [],
      pageTitle: 'freelancers.profileTitle'
    }
  },
  {
    path: 'accepted',
    component: FreelancerAcceptedComponent,
    data: {
      authorities: [],
      pageTitle: 'freelancers.acceptedTitle'
    }
  },
  {
    path: 'favourites',
    component: FreelancerFavouritesComponent,
    data: {
      authorities: [],
      pageTitle: 'freelancers.favouritesTitle'
    }
  },
  {
    path: 'favourites/details/:id',
    component: FreelancerProfileComponent,
    data: {
      authorities: [],
      pageTitle: 'freelancers.profileTitle'
    }
  },
  {
    path: 'rejected',
    component: FreelancerRejectedComponent,
    data: {
      authorities: [],
      pageTitle: 'freelancers.rejectedTitle'
    }
  },
  {
    path: 'profile/:id',
    component: FreelancerProfileComponent,
    data: {
      authorities: [],
      pageTitle: 'freelancers.profileTitle'
    },
  },
  {
    path : 'proposed-never-reviewed',
    component: FreelancerProposedNeverReviewedComponent,
    data : {
      authorities: [],
      pageTitle: 'freelancers.proposedTitle'
    }
  }
];
