import {Component, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Freelancer, RejectionSettings} from '../freelancer.model';
import {FormBuilder, Validators} from '@angular/forms';
import {FreelancerService} from '../freelancer.service';
import {BehaviorSubject, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';

@Component({
  selector: 'app-freelancer-reject-modal',
  templateUrl: './freelancer-reject-modal.component.html'
})
export class FreelancerRejectModalComponent implements OnInit {

  freelancer: Freelancer;

  form = this.formBuilder.group({
    reason: ['', [Validators.maxLength(2000)]],
    save: [true]
  });

  rejecting$ = new BehaviorSubject<boolean>(false);
  failed$ = new BehaviorSubject<boolean>(false);

  constructor(public activeModal: NgbActiveModal, private formBuilder: FormBuilder, private freelancerService: FreelancerService) {
  }

  ngOnInit() {
  }

  confirm() {
    const rejectionSettings: RejectionSettings = {
      reason: this.form.get('reason').value,
      saveForOtherProjects: this.form.get('save').value
    };

    this.rejecting$.next(true);
    this.failed$.next(false);
    this.form.disable();

    this.freelancerService.rejectFreelancer(this.freelancer, rejectionSettings.reason, rejectionSettings.saveForOtherProjects)
      .pipe(
        catchError((error) => {
          this.rejecting$.next(false);
          this.failed$.next(true);
          this.form.enable();
          return throwError(error);
        })
      )
      .subscribe(() => {
        this.activeModal.close();
      });
  }

}
