import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-freelancer-rating',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div class="freelancer-rating d-inline-block">
      <svg-icon *ngFor="let ratingLevel of getRatingLevels()"
                class="rating-icon"
                [name]="ratingLevel <= rating ? 'thumbs-up-full' : 'thumbs-up'">
      </svg-icon>
    </div>
  `
})
export class FreelancerRatingComponent implements OnInit {

  @Input() rating: number;

  private ratingNum = 3;

  constructor() {
  }

  ngOnInit() {
  }

  getRatingLevels(): number[] {
    return Array(this.ratingNum).fill(null).map((item, index) => index + 1);
  }

}
