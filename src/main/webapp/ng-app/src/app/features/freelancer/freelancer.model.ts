import {assignIn} from 'lodash';
import * as moment from 'moment';
import {ISkillBackend, Skill, SkillType} from '../../core/models/skill.model';

export enum LanguageLevel {
  BASIC = 'basic',
  INTERMEDIATE = 'intermediate',
  EXPERT = 'expert',
  NATIVE = 'native'
}

export enum FreelancerType {
  MATCH = 'MATCH',
  FAVORITE = 'FAVORITE',
  REJECTED = 'REJECTED',
}

export interface IFreelancerLanguageBackend {
  language: string;
  level: LanguageLevel;
}

export interface IFreelancerSkillBackend {
  id: string;
  rating: number;
  skill: ISkillBackend;
}

export interface IMatchingProjectBackend {
  id: string;
  name: string;
  candidateId: string;
}


export interface IPortfolio {
  id: string;
  name: string;
  description: string;
  linkToSource: string;
  type: string;
}


export interface IReferral {
  id: string;
  name: string;
  rating: string;
  quote: string;
  detail: string;
  profile: string;
}

export interface IAccountBackend {
  id: string;
  contacts: any[];
  recordTypeId: string;
}

export interface IFreelancersBackend {
  id: string;
  name: string;
  rating: string;
  type: string;
}

export interface IFreelancerBackend {
  id: string;
  favouriteId: string;
  contactId: string;
  candidateId: string;
  type: FreelancerType;
  name: string;
  profileName: string;
  description: string;
  rating: string;
  state: string;
  rateRequestedByFreelancer: number;
  rateProposedToCustomer: number;
  availabilityDate: string; // '2020-11-07'
  availableDaysPerWeek: number;
  hasOwnCar: boolean;
  availableForTeleworking: boolean;
  doesWeekendWork: boolean;
  doesEveningWork: boolean;
  isFavourite: boolean;
  gighouseOpinion: string;
  candidateStatus: string;
  candidateProjectName : string;

  skills: IFreelancerSkillBackend[];
  contactLanguages: IFreelancerLanguageBackend[];
  matchingProjects: IMatchingProjectBackend[];
  portfolios: IPortfolio[];
  referrals: IReferral[];
}

export class Account implements IAccountBackend {
  id: string;
  contacts: any[];
  recordTypeId: string;

  constructor(accountBackend: IAccountBackend) {
    assignIn(this, accountBackend);
  }
}

export class FreelancerSkill implements IFreelancerSkillBackend {
  rating: number;
  id: string;
  skill: Skill;

  constructor(freelancerSkillBackend?: IFreelancerSkillBackend) {
    if (freelancerSkillBackend) {
      assignIn(this, freelancerSkillBackend);
      this.skill = new Skill(freelancerSkillBackend.skill);
    }
  }
}

export class SkillCategory {
  freelancerSkill: FreelancerSkill;
  children: FreelancerSkill[];

  constructor(freelancerSkill: FreelancerSkill) {
    this.freelancerSkill = freelancerSkill;
  }
}

export class FreelancerLanguage implements IFreelancerLanguageBackend {
  language: string;
  level: LanguageLevel;

  constructor(freelancerLanguageBackend: IFreelancerLanguageBackend) {
    assignIn(this, freelancerLanguageBackend);
  }
}

export class MatchingProject implements IMatchingProjectBackend {
  id: string;
  name: string;
  candidateId: string;

  constructor(matchingProjectBackend: IMatchingProjectBackend) {
    assignIn(this, matchingProjectBackend)
  }
}

export class Freelancers implements IFreelancersBackend {
  id: string;
  name: string;
  rating: string;
  type: string;

  constructor(freelancersBackend: IFreelancersBackend) {
    assignIn(this, freelancersBackend);
  }


  getRatingInt(): number {
    switch (this.rating) {
      case 'A':
        return 3;
      case 'B':
        return 2;
      case 'C':
        return 1;
      default:
        return 0;
    }
  }
}

export class Freelancer implements IFreelancerBackend {
  id: string;
  favouriteId: string;
  contactId: string;
  candidateId: string;
  type: FreelancerType;
  name: string;
  profileName: string;
  description: string;
  rating: string;
  state: string;
  rateRequestedByFreelancer: number;
  rateProposedToCustomer: number;
  availabilityDate: string; // '2020-11-07'
  availableDaysPerWeek: number;
  hasOwnCar: boolean;
  availableForTeleworking: boolean;
  doesWeekendWork: boolean;
  doesEveningWork: boolean;
  isFavourite: boolean;
  gighouseOpinion: string;
  candidateStatus: string;
  candidateProjectName : string;
  skills: IFreelancerSkillBackend[];
  skillsCategorised: SkillCategory[];
  toolsCategorised: SkillCategory[];
  contactLanguages: FreelancerLanguage[];
  matchingProjects: MatchingProject[];
  portfolios: IPortfolio[];
  referrals: IReferral[];

  isFavouriteUpdating: boolean;

  constructor(freelancerBackend: IFreelancerBackend, allSkills: Skill[]) {
    assignIn(this, freelancerBackend);

    this.availabilityDate = this.availabilityDate ? moment(this.availabilityDate).format('DD/MM/YYYY') : null;

    if (freelancerBackend.contactLanguages) {
      this.contactLanguages = freelancerBackend.contactLanguages.map(lang => new FreelancerLanguage(lang));
    }

    if (freelancerBackend.skills && allSkills) {
      const skillsFiltered = freelancerBackend.skills.filter(freelancerSkill => {
        return freelancerSkill.skill && freelancerSkill.skill.type === SkillType.SPECIALIZATION;
      });
      this.skillsCategorised = this.createCategorisedSkills(skillsFiltered, allSkills);

      const toolsFiltered = freelancerBackend.skills.filter(freelancerSkill => {
        return freelancerSkill.skill && freelancerSkill.skill.type === SkillType.TOOLS;
      });
      this.toolsCategorised = this.createCategorisedSkills(toolsFiltered, allSkills);
    }
  }

  private createCategorisedSkills(freelancerSkills: IFreelancerSkillBackend[], allSkills: Skill[]): SkillCategory[] {
    const skillsCategorised: SkillCategory[] = [];

    freelancerSkills.forEach(lastLevelFreelancerSkill => {
      const freelancerSkill = new FreelancerSkill(lastLevelFreelancerSkill);
      if (!lastLevelFreelancerSkill.skill.parent) {
        // We are assuming that there are skills that don't have a parent so they are visualised standalone
        const skillCategory = new SkillCategory(freelancerSkill);
        skillsCategorised.push(skillCategory);
      } else {
        // Find a parent skill object if it exists or create a new one
        let skillCategory = skillsCategorised.find(_skillCategory => {
          return _skillCategory.freelancerSkill.skill.id === lastLevelFreelancerSkill.skill.parent;
        });

        if (!skillCategory) {
          let skill = allSkills.find(_skill => _skill.id === lastLevelFreelancerSkill.skill.parent);
          if (skill) {
            const freelancerSkill = new FreelancerSkill();
            freelancerSkill.skill = skill;
            skillCategory = new SkillCategory(freelancerSkill);
            skillCategory.children = [];
            skillsCategorised.push(skillCategory);
          }
        }
        // push the skill to the parent
        if (skillCategory) {
          skillCategory.children.push(freelancerSkill);
        }
      }
    });

    return skillsCategorised;
  }

  cardDescription() {
    const limit = 130;

    if (this.description && this.description.length > limit) {
      return this.description.slice(0, limit) + '...';
    }
    return this.description;
  }

  getRatingInt(): number {
    switch (this.rating) {
      case 'A':
        return 3;
      case 'B':
        return 2;
      case 'C':
        return 1;
      default:
        return 0;
    }
  }

  isRejected(): boolean {
    return this.candidateStatus != null && this.candidateStatus == 'Rejected';
  }

  isAccepted(): boolean {
    return this.candidateStatus != null
      && this.candidateStatus != 'Rejected'
      && this.candidateStatus != 'Proposed'
  }
}

export interface RejectionSettings {
  reason: string;
  saveForOtherProjects: boolean;
}
