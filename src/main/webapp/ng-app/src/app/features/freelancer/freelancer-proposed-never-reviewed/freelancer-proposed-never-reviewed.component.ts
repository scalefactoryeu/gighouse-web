import { Component, OnInit } from '@angular/core';
import {BehaviorSubject, merge, Observable, of, Subject, throwError} from "rxjs";
import {Freelancer} from "../freelancer.model";
import {FreelancerService} from "../freelancer.service";
import {TranslateService} from "@ngx-translate/core";
import {ErrorHandlingService} from "../../../core/error-handling/error-handling.service";
import {catchError, concatMap, first, map, mapTo, scan, shareReplay, takeUntil} from "rxjs/operators";
import {PaginatedData} from "../../../core/models/model";

@Component({
  selector: 'app-freelancer-proposed-never-reviewed',
  templateUrl: './freelancer-proposed-never-reviewed.component.html'
})
export class FreelancerProposedNeverReviewedComponent implements OnInit {
  freelancers$: Observable<Freelancer[]>;
  freelancersTotalCount$: Observable<number>;
  loading$: Observable<boolean>;
  loadingFailed$: Observable<boolean>;
  retryLoad$ = new Subject();

  private page$ = new BehaviorSubject<number>(0);
  // Keep page size relatively big because the UX is better when user immediately sees a scrollbar.
  // With a smaller number there is no scrollbar and we have to show a "Load more" button.
  private readonly pageSize = 20;
  private destroy$ = new Subject();

  constructor(private freelancerService: FreelancerService, private translate: TranslateService, private errorHandlingService: ErrorHandlingService) {}

  ngOnInit(): void {
    this.startLoadingFreelancers();
  }

  scrolled(freelancers: Freelancer[]) {
    this.freelancersTotalCount$
      .pipe(first())
      .subscribe((totalCount) => {
        if (freelancers.length < totalCount) {
          this.loadMoreFreelancers();
        }
      });
  }
  private startLoadingFreelancers() {
    let lastLoadedPage: number;

    // load freelancers when page changes during infinite scroll and when retry button is clicked
    const loadFreelancers$ = merge(this.page$, this.retryLoad$);

    const newPaginatedFreelancers$: Observable<PaginatedData<Freelancer>> = loadFreelancers$
      .pipe(
        concatMap(() => {
          return this.freelancerService.getProposedNeverReviewedFreelancers(this.page$.value, this.pageSize)
            .pipe(
              catchError(() => {
                // return null instead of throwing the error to keep the observable stream alive
                return of(null);
              })
            );
        }),
        takeUntil(this.destroy$),
        shareReplay()
      );
    this.freelancers$ = newPaginatedFreelancers$
      .pipe(
        scan((accumulatedFreelancers, currentData) => {
          if (currentData) {
            if (currentData.page <= lastLoadedPage) {
              // If currently loaded page is smaller than the last loaded page it means we are refreshing the data starting from a lower
              // page, meaning that we need to remove all the  existing items starting from the current page and then add newly
              // loaded items. This case happens when an item is removed from the list so we want to refresh data starting from the page
              // of that item.
              const firstItemIndexOnPage = currentData.page * this.pageSize;
              accumulatedFreelancers.splice(firstItemIndexOnPage);
            }

            // Add newly loaded projects to projects array.
            accumulatedFreelancers = accumulatedFreelancers.concat(currentData.items);
            lastLoadedPage = currentData.page;
          }
          return accumulatedFreelancers;
        }, [])
      );

    this.freelancersTotalCount$ = newPaginatedFreelancers$
      .pipe(
        map((paginatedFreelancers) => paginatedFreelancers ? paginatedFreelancers.totalCount : null)
      );

    const loadingCount$ = merge(
      loadFreelancers$.pipe(mapTo(+1)),
      newPaginatedFreelancers$.pipe(mapTo(-1)),
    )
      .pipe(
        scan((accumulated, currentValue) => {
          return accumulated + currentValue;
        }, 0)
      );

    this.loading$ = loadingCount$
      .pipe(
        map(loadingCount => loadingCount > 0)
      );

    this.loadingFailed$ = merge(
      loadFreelancers$.pipe(mapTo(false)),
      newPaginatedFreelancers$.pipe(
        // loading is failed when newFreelancers=null
        map((newPaginatedFreelancers) => !newPaginatedFreelancers)
      )
    );
  }

  loadMoreFreelancers() {
    const nextPage = this.page$.value + 1;
    this.page$.next(nextPage);
  }

  toggleFavourite(freelancer: Freelancer) {
    freelancer.isFavourite = !freelancer.isFavourite;

    this.freelancerService.setFreelancerFavourite(freelancer.contactId, freelancer.isFavourite)
      .pipe(
        catchError((error) => {
          let errorBody: string;
          if (freelancer.isFavourite) {
            errorBody = this.translate.instant('freelancers.setFavouriteFailure', {freelancerName: freelancer.name});
          } else {
            errorBody = this.translate.instant('freelancers.unsetFavouriteFailure', {freelancerName: freelancer.name});
          }

          this.errorHandlingService.openErrorModal(errorBody);

          return throwError(error);
        })
      ).subscribe();
  }

  ngOnDestroy() {
    this.destroy$.next();
  }

}
