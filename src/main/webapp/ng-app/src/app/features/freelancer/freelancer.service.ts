import {Injectable} from '@angular/core';
import {HttpClient, HttpParams, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Freelancer, Freelancers, IFreelancerBackend, IFreelancersBackend} from './freelancer.model';
import {map} from 'rxjs/operators';
import {PaginatedData} from '../../core/models/model';
import {createRequestOption, Project, ProjectMatchesRequest, SearchWithPagination} from '../../shared/util/request-util';
import {Skill} from "../../core/models/skill.model";

@Injectable({
  providedIn: 'root'
})
export class FreelancerService {

  constructor(private httpClient: HttpClient) {
  }

  getMatchProfile(candidateId: string, allSkills: Skill[]): Observable<Freelancer> {
    return this.httpClient.get<IFreelancerBackend>(`/api/freelancers/matches/${candidateId}/`)
      .pipe(
        map((freelancerBackend: IFreelancerBackend) => {
          return new Freelancer(freelancerBackend, allSkills);
        })
      );
  }

  getFavouriteProfile(favouriteId: string, allSkills: Skill[]): Observable<Freelancer> {
    return this.httpClient.get<IFreelancerBackend>(`/api/freelancers/favorites/${favouriteId}/`)
      .pipe(
        map((freelancerBackend: IFreelancerBackend) => {
          return new Freelancer(freelancerBackend, allSkills);
        })
      );
  }

  getFreelancerMatchesPerProject(projectId: string, all: boolean): Observable<Freelancer[]> {
    const req: ProjectMatchesRequest = { projectId: projectId, all: all }
    const params: HttpParams = createRequestOption(req);

    return this.httpClient.get<IFreelancerBackend[]>(`/api/freelancers/matches`, { params })
      .pipe(
        map((freelancersBackend: IFreelancerBackend[]) => {
          return freelancersBackend.map(freelancer => new Freelancer(freelancer, null));
        })
      );
  }

  getAcceptedFreelancerPerProject(projectId: string, all: boolean): Observable<Freelancer[]> {
    const req: ProjectMatchesRequest = { projectId: projectId, all: all }
    const params: HttpParams = createRequestOption(req);

    return this.httpClient.get<IFreelancerBackend[]>(`/api/freelancers/accepted-for-project`, { params })
      .pipe(
        map((freelancersBackend: IFreelancerBackend[]) => {
          return freelancersBackend.map(freelancer => new Freelancer(freelancer, null));
        })
      );
  }

  getChatFreelancersPerProject(projectId: string, projectIndex: number): Observable<Freelancers[]> {
    const req: Project = { projectId: projectId };
    const params: HttpParams = createRequestOption(req);

    return this.httpClient.get<IFreelancersBackend[]>(`/api/chat/participants`, { params })
      .pipe(
        map((freelancersBackend: IFreelancersBackend[]) => {
          return freelancersBackend.map(freelancer => new Freelancers(freelancer));
        })
      );
  }

  getFavouriteFreelancers(page: number, pageSize: number): Observable<PaginatedData<Freelancer>> {
    let params = new HttpParams();
    params = params.append('page', page.toString());
    params = params.append('size', pageSize.toString());

    return this.httpClient.get<IFreelancerBackend[]>(`/api/freelancers/favorites`, { observe: 'response', params })
      .pipe(
        map((res: HttpResponse<IFreelancerBackend[]>) => {
          const freelancers = res.body.map(freelancer => new Freelancer(freelancer, null));
          return new PaginatedData<Freelancer>(res, freelancers, page);
        })
      );
  }

  getAcceptedFreelancers(page: number, pageSize: number): Observable<PaginatedData<Freelancer>> {
    let params = new HttpParams();
    params = params.append('page', page.toString());
    params = params.append('size', pageSize.toString());

    return this.httpClient.get<IFreelancerBackend[]>(`/api/freelancers/accepted`, { observe: 'response', params })
      .pipe(
        map((res: HttpResponse<IFreelancerBackend[]>) => {
          const freelancers = res.body.map(freelancer => new Freelancer(freelancer, null));
          return new PaginatedData<Freelancer>(res, freelancers, page);
        })
      );
  }

  getProposedNeverReviewedFreelancers(page: number, pageSize: number): Observable<PaginatedData<Freelancer>> {
    let params = new HttpParams();
    params = params.append('page', page.toString());
    params = params.append('size', pageSize.toString());

    return this.httpClient.get<IFreelancerBackend[]>(`/api/freelancers/proposed-never-reviewed`, { observe: 'response', params })
      .pipe(
        map((res: HttpResponse<IFreelancerBackend[]>) => {
          const freelancers = res.body.map(freelancer => new Freelancer(freelancer, null));
          console.log(freelancers);
          return new PaginatedData<Freelancer>(res, freelancers, page);
        })
      );
  }

  getRejectedFreelancers(page: number, pageSize: number): Observable<PaginatedData<Freelancer>> {
    let params = new HttpParams();
    params = params.append('page', page.toString());
    params = params.append('size', pageSize.toString());

    return this.httpClient.get<IFreelancerBackend[]>(`/api/freelancers/rejected`, { observe: 'response', params })
      .pipe(
        map((res: HttpResponse<IFreelancerBackend[]>) => {
          const freelancers = res.body.map(freelancer => new Freelancer(freelancer, null));
          return new PaginatedData<Freelancer>(res, freelancers, page);
        })
      );
  }

  getContractedFreelancers(req: SearchWithPagination): Observable<PaginatedData<Freelancer>> {
    const params: HttpParams = createRequestOption(req);

    return this.httpClient.get<IFreelancerBackend[]>(`/api/freelancers/`, { observe: 'response', params })
      .pipe(
        map((res: HttpResponse<IFreelancerBackend[]>) => {
          const freelancers = res.body.map(freelancer => new Freelancer(freelancer, null));
          return new PaginatedData<Freelancer>(res, freelancers, req.page);
        })
      );
  }

  setFreelancerFavourite(contactId: string, favourite: boolean): Observable<any> {
    if (favourite) {
      return this.httpClient.post<any>(`/api/freelancers/favorites`, { contactId });
    } else {
      return this.httpClient.delete<any>(`/api/freelancers/favorites/${contactId}`);
    }
  }

  rejectFreelancer(freelancer: Freelancer, reason: string, saveForOtherProjects: boolean): Observable<any> {
    freelancer.candidateStatus = 'Rejected';
    return this.httpClient.post<any>(`/api/freelancers/matches/${freelancer.candidateId}/reject`, { reason, saveForOtherProjects });
  }

  acceptFreelancer(freelancer: Freelancer): Observable<any> {
    freelancer.candidateStatus = 'Considered by Customer';
    return this.httpClient.post<any>(`/api/freelancers/matches/${freelancer.candidateId}/accept`, {});
  }

}
