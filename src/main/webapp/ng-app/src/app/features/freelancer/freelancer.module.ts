import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {SharedModule} from '../../shared/shared.module';
import {LayoutsModule} from '../../layouts/layouts.module';
import {routes} from './freelancer-routes';
import {FreelancerFavouritesComponent} from './freelancer-favourites/freelancer-favourites.component';
import {FreelancerMatchesComponent} from './freelancer-matches/freelancer-matches.component';
import {FreelancerRejectedComponent} from './freelancer-rejected/freelancer-rejected.component';
import {FreelancerProfileComponent} from './freelancer-profile/freelancer-profile.component';
import {FreelancerRejectModalComponent} from './freelancer-reject-modal/freelancer-reject-modal.component';
import {FreelancerProfileSidebarComponent} from './freelancer-profile/freelancer-profile-sidebar/freelancer-profile-sidebar.component';
import {FreelancerSharedModule} from './freelancer-shared/freelancer-shared.module';
import {FreelancerAcceptedComponent} from "./freelancer-accepted/freelancer-accepted.component";
import {FreelancerProposedNeverReviewedComponent} from './freelancer-proposed-never-reviewed/freelancer-proposed-never-reviewed.component';
import {QuillModule} from "ngx-quill";


@NgModule({
  imports: [
    SharedModule,
    LayoutsModule,
    FreelancerSharedModule,
    RouterModule.forChild(routes),
    QuillModule.forRoot()
  ],
  declarations: [
    FreelancerFavouritesComponent,
    FreelancerMatchesComponent,
    FreelancerAcceptedComponent,
    FreelancerRejectedComponent,
    FreelancerProfileComponent,
    FreelancerProfileSidebarComponent,
    FreelancerRejectModalComponent,
    FreelancerProposedNeverReviewedComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class FreelancerModule {
}
