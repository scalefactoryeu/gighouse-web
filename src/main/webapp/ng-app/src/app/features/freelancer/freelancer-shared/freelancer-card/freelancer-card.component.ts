import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {Router} from '@angular/router';
import {Freelancer} from '../../freelancer.model';

@Component({
  selector: 'app-freelancer-card',
  templateUrl: './freelancer-card.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FreelancerCardComponent implements OnInit {

  @Input() freelancer: Freelancer;
  @Input() cardColorLight: boolean;
  @Input() projectMatchingCard: boolean;
  @Input() archive : boolean;

  @Output() toggleFavourite = new EventEmitter();
  @Output() reject = new EventEmitter();
  @Output() meet = new EventEmitter();
  @Output() meetOrGreet = new EventEmitter();

  skillMatches: {skill: string, matchPercentage: number}[] = [
    {
      skill: 'Google Adwords',
      matchPercentage: 86
    },
    {
      skill: 'Data Analytics',
      matchPercentage: 80
    },
    {
      skill: 'Wordpress',
      matchPercentage: 20
    }
  ];

  constructor(private translate: TranslateService, public router: Router) {

  }

  ngOnInit() {

  }

  availableDaysPerWeek() {
    if (this.freelancer.availableDaysPerWeek === 1) {
      return this.translate.instant('freelancers.availableDaysPerWeekSingular', {availableDaysPerWeek: this.freelancer.availableDaysPerWeek});
    }
    return this.translate.instant('freelancers.availableDaysPerWeekPlural', {availableDaysPerWeek: this.freelancer.availableDaysPerWeek});
  }

}
