import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { Freelancer } from '../../freelancer.model';

@Component({
  selector: 'app-freelancer-profile-sidebar',
  templateUrl: './freelancer-profile-sidebar.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FreelancerProfileSidebarComponent implements OnInit {

  @Input() freelancer: Freelancer;


  ngOnInit() {

  }

}
