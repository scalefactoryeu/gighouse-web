import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FreelancerProposedNeverReviewedComponent } from './freelancer-proposed-never-reviewed.component';

describe('FreelancerProposedNeverReviewedComponent', () => {
  let component: FreelancerProposedNeverReviewedComponent;
  let fixture: ComponentFixture<FreelancerProposedNeverReviewedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FreelancerProposedNeverReviewedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FreelancerProposedNeverReviewedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
