import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {BehaviorSubject, merge, Observable, Subject, throwError} from 'rxjs';
import {Freelancer} from '../freelancer.model';
import {FreelancerService} from '../freelancer.service';
import {catchError, map, shareReplay, switchMap} from 'rxjs/operators';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {ErrorHandlingService} from '../../../core/error-handling/error-handling.service';
import {Project} from '../../project/project.model';
import {ProjectService} from '../../project/project.service';
import {FreelancerRejectModalComponent} from "../freelancer-reject-modal/freelancer-reject-modal.component";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {SkillService} from "../../../core/services/skill.service";

@Component({
  selector: 'app-freelancer-profile',
  templateUrl: './freelancer-profile.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FreelancerProfileComponent implements OnInit {

  freelancer$: Observable<Freelancer>;
  overrideFreelancer$: Subject<Freelancer> = new Subject();
  loadingFailed$: BehaviorSubject<boolean> = new BehaviorSubject(false);
  fromUrl$: Observable<string>;

  projectMatches$: Observable<Project[]>;

  constructor(private freelancerService: FreelancerService,
              private projectService: ProjectService,
              private skillService: SkillService,
              private route: ActivatedRoute,
              private translate: TranslateService,
              private errorHandlingService: ErrorHandlingService,
              private router: Router,
              private ngbModal: NgbModal) {}

  ngOnInit(): void {
    this.setFromUrl();

    const freelancerId$: Observable<string> = this.route.paramMap
      .pipe(
        map((params) => {
          return params.get('id');
        })
      );

    const loadedFreelancer$ = freelancerId$
      .pipe(
        switchMap(freelancerId =>
          this.skillService.getAllSkills().pipe(switchMap(allSkills => {
            if (this.router.url.startsWith('/freelancers/favourites/')) {
              return this.freelancerService.getFavouriteProfile(freelancerId, allSkills);
            }
            if (this.router.url.startsWith('/freelancers/matches/')) {
              return this.freelancerService.getMatchProfile(freelancerId, allSkills);
            }
          }))),
        catchError(error => {
          this.loadingFailed$.next(true);
          throw error;
        }),
        shareReplay()
      );

    this.freelancer$ = merge(
      loadedFreelancer$,
      this.overrideFreelancer$
    );
  }

  private setFromUrl() {
    this.fromUrl$ = this.route.queryParamMap
      .pipe(
        map(params => {
          let from = params.get('from');

          if (!from) {
            from = '/freelancers';
          }

          return from;
        })
      );
  }

  back(fromUrl: string) {
    this.router.navigateByUrl(fromUrl);
  }

  availableDaysPerWeek(freelancer: Freelancer) {
    if (freelancer.availableDaysPerWeek === 1) {
      return this.translate.instant('freelancers.availableDaysPerWeekSingular', {availableDaysPerWeek: freelancer.availableDaysPerWeek});
    }
    return this.translate.instant('freelancers.availableDaysPerWeekPlural', {availableDaysPerWeek: freelancer.availableDaysPerWeek});
  }

  toggleFavourite(freelancer: Freelancer) {
    const newFavouriteValue = !freelancer.isFavourite;

    // Change state immediately on local data for UX. In case change fails on the backend, we will revert the state in GUI.
    this.toggleFavouriteOnLocalData(freelancer, true);

    this.freelancerService.setFreelancerFavourite(freelancer.contactId, newFavouriteValue)
      .pipe(
        catchError((error) => {
          let errorBody: string;
          if (newFavouriteValue) {
            errorBody = this.translate.instant('freelancers.setFavouriteFailure', {freelancerName: freelancer.name});
          } else {
            errorBody = this.translate.instant('freelancers.unsetFavouriteFailure', {freelancerName: freelancer.name});
          }

          this.errorHandlingService.openErrorModal(errorBody);
          this.toggleFavouriteOnLocalData(freelancer, false);

          return throwError(error);
        })
      )
      .subscribe(() => {
        this.toggleFavouriteOnLocalData(freelancer, false, false);
      });
  }

  private toggleFavouriteOnLocalData(freelancer: Freelancer, isFavouriteUpdating: boolean, toggleValue = true) {
    freelancer.isFavouriteUpdating = isFavouriteUpdating;

    if (toggleValue) {
      freelancer.isFavourite = !freelancer.isFavourite;
    }

    this.overrideFreelancer$.next(freelancer);
  }

  accept(freelancer: Freelancer) {
    this.freelancerService.acceptFreelancer(freelancer)
      .pipe(
        catchError((error) => {
          this.errorHandlingService.openErrorModal(this.translate.instant('freelancers.errorApprovingCandidate'));
          return throwError(error);
        })
      ).subscribe();
  }

  reject(freelancer: Freelancer) {
    const modalRef = this.ngbModal.open(FreelancerRejectModalComponent, {windowClass: 'freelancer-reject-modal', backdrop: 'static'});
    const modal = modalRef.componentInstance as FreelancerRejectModalComponent;

    modal.freelancer = freelancer;
    modalRef.result
      .then(
        () => this.onRejected(freelancer),
        () => {}
      );
  }

  private onRejected(freelancer: Freelancer) {
    freelancer.candidateStatus = 'Rejected';
    this.overrideFreelancer$.next(freelancer)
  }
}
