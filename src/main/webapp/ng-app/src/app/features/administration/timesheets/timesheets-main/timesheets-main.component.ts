import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { Moment } from 'moment';
import { cloneDeep } from 'lodash';
import { TimeSheet } from '../timesheet.model';
import { TimeSheetsService } from "../timesheets.service";
import { catchError } from "rxjs/operators";
import { throwError } from "rxjs";
import { ErrorHandlingService } from "../../../../core/error-handling/error-handling.service";
import { TranslateService } from "@ngx-translate/core";
import { ModalService } from 'src/app/modals/modal.service';

@Component({
  selector: 'app-timesheets-main',
  templateUrl: './timesheets-main.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TimesheetsMainComponent implements OnInit {

  @Input() periodFromDate: Moment;
  @Input() timeSheets: TimeSheet[];
  @Input() loading: boolean;
  @Input() loadingFailed: boolean;

  days = [
    'global.short-weekdays.1', 'global.short-weekdays.2', 'global.short-weekdays.3', 'global.short-weekdays.4',
    'global.short-weekdays.5', 'global.short-weekdays.6', 'global.short-weekdays.7'
  ];

  constructor(private timeSheetsService: TimeSheetsService, private errorHandlingService: ErrorHandlingService, private translate: TranslateService, private modalService: ModalService, private changeDetectorRef: ChangeDetectorRef) { }

  ngOnInit(): void {
  }

  showTimesheetsTable(): boolean {
    return !!this.timeSheets && this.timeSheets.length > 0 && !this.loading;
  }

  showNoTimeSheetsByCriteria(): boolean {
    return !!this.timeSheets && this.timeSheets.length === 0 && !this.loading;
  }

  /**
   * Returns date as DD/MM (e.g. 23/11) based on the day in the week (dayIndex) starting from this.startDate
   */
  getDate(dayIndex: number): string {
    return cloneDeep(this.periodFromDate).add(dayIndex, 'day').format('DD/MM');
  }

  approve(timeSheet: TimeSheet) {

    const modalRef = this.modalService.openConfirmRemoveModal(this.translate.instant('timesheets.modal.approveTitle'), this.translate.instant('timesheets.modal.approveBody'), this.translate.instant('timesheets.modal.approveConfirmButtonText'));

    modalRef.result
      .then(
        () => {

          const ids = timeSheet.timeEntries.map(entry => entry.id)
          timeSheet.status = 'Approved';

          this.timeSheetsService.approve(ids)
            .pipe(
              catchError((error) => {
                this.errorHandlingService.openErrorModal(this.translate.instant('timesheets.failedToApproveTimeSheet'));
                return throwError(error);
              })
            ).subscribe();

          this.changeDetectorRef.detectChanges();
          //this.router.navigate(['/administration/timesheets'])

        },
        () => { }
      );

  }

  reject(timeSheet: TimeSheet) {

    const modalRef = this.modalService.openConfirmRejectModal(this.translate.instant('timesheets.modal.rejectTitle'), this.translate.instant('timesheets.modal.rejectBody'),timeSheet.timeEntries.map(entry => entry.id), this.translate.instant('timesheets.modal.rejectConfirmButtonText'));

    modalRef.result
      .then(
        () => {
          timeSheet.status = 'Rejected';
          this.changeDetectorRef.detectChanges();

          //this.router.navigate(['/timesheets'])
        },
        () => { }
      );

  }

}
