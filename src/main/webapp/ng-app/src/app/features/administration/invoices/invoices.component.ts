import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { BehaviorSubject, combineLatest, merge, Observable, of, Subject } from 'rxjs';
import { Freelancer } from '../../freelancer/freelancer.model';
import { catchError, filter, map, mapTo, shareReplay, switchMap, takeUntil, tap } from 'rxjs/operators';
import { Moment } from 'moment';
import { InvoicesService } from './invoices.service';
import { Invoice } from './invoices.model';
import { InvoiceStatuses } from 'src/app/core/models/invoice-status.model';

@Component({
  selector: 'app-invoices',
  templateUrl: './invoices.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InvoicesComponent implements OnInit, OnDestroy {

  fromDate$ = new BehaviorSubject<Moment>(null);
  toDate$ = new BehaviorSubject<Moment>(null);
  selectedFreelancers$ = new BehaviorSubject<Freelancer[]>(null);
  selectedStatuses$ = new BehaviorSubject<InvoiceStatuses[]>(null);
  invoices$: Observable<Invoice[]>;
  loading$: Observable<boolean>;
  loadingFailed$: Observable<boolean>;
  invoicesTotalCount$: Observable<number>;

  private page$ = new BehaviorSubject<number>(0);
  private readonly pageSize = 20;
  private retryLoad$ = new Subject();
  private destroy$ = new Subject();


  perfectScrollbarConfig = {
    suppressScrollX: true
  };

  constructor(private invoicesService: InvoicesService) { }

  ngOnInit(): void {
    this.startLoadingInvoices();
  }

  private startLoadingInvoices() {

    const loadInvoices$ = combineLatest([this.fromDate$, this.toDate$, this.selectedFreelancers$, this.selectedStatuses$])
      .pipe(
        filter(([fromDate, toDate]) => {
          return !!fromDate && !!toDate;
        })
      );

    this.invoices$ = loadInvoices$
      .pipe(
        switchMap(([fromDate, toDate, selectedFreelancers, selectedStatuses]) => {
          return this.invoicesService.getInvoices(fromDate, toDate, selectedFreelancers, selectedStatuses)
            .pipe(
              catchError((error) => {
                // return null instead of throwing the error to keep the observable stream alive
                return of(null);
              }),
            );
        }),
        takeUntil(this.destroy$),
        shareReplay()
      );

    this.loading$ = merge(
      loadInvoices$.pipe(mapTo(true)),
      this.invoices$.pipe(mapTo(false))
    );


    this.loadingFailed$ = merge(
      loadInvoices$.pipe(mapTo(false)),
      this.invoices$.pipe(map((invoices) => !invoices || invoices.length < 1)) // loading is failed when invoices=null
    );
  }

  changeFromDate(fromDate: Moment) {
    setTimeout(() => {
      this.fromDate$.next(fromDate);
    });
  }

  changeToDate(toDate: Moment) {
    setTimeout(() => {
      this.toDate$.next(toDate);
    });
  }

  changeSelectedFreelancers(selectedFreelancers: Freelancer[]) {
    if (selectedFreelancers.length === 0) {
      this.selectedFreelancers$.next(null);
    } else {
      this.selectedFreelancers$.next(selectedFreelancers);
    }
  }

  changeSelectedStatuses(selectedStatuses: InvoiceStatuses[]) {
    if (selectedStatuses.length === 0) {
      this.selectedStatuses$.next(null);
    } else {
      this.selectedStatuses$.next(selectedStatuses);
    }
  }

  loadMoreInvoices() {
    const nextPage = this.page$.value + 1;
    this.page$.next(nextPage);
  }

  retryLoadInvoices() {
    this.retryLoad$.next();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
