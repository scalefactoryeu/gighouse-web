import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        redirectTo: 'timesheets'
      },
      {
        path: 'timesheets',
        loadChildren: () => import('./timesheets/timesheets.module').then(m => m.TimesheetsModule)
      },
      {
        path: 'invoices',
        loadChildren: () => import('./invoices/invoices.module').then(m => m.InvoicesModule)
      },
      {
        path: 'contracts',
        loadChildren: () => import('./contracts/contracts.module').then(m => m.ContractsModule)
      }
    ]),
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AdministrationRoutingModule {

}
