import {ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Freelancer} from 'src/app/features/freelancer/freelancer.model';
//import { IStatusOption, StatusOption, StatusSelection } from './invoices-filter.model';
import {cloneDeep} from 'lodash';
import * as moment from 'moment';
import {Moment} from 'moment';
import {InvoiceStatus, InvoiceStatuses} from 'src/app/core/models/invoice-status.model';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {GUI_DATE_FORMAT} from 'src/app/core/models/constants';

@Component({
  selector: 'app-invoices-filter',
  templateUrl: './invoices-filter.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InvoicesFilterComponent implements OnInit {

  @Output() changeSelectedFreelancers = new EventEmitter<Freelancer[]>();
  @Output() changeSelectedStatuses = new EventEmitter<InvoiceStatuses[]>();
  @Output() changeFromDate = new EventEmitter<Moment>();
  @Output() changeToDate = new EventEmitter<Moment>();
  form: FormGroup;
  guiDateFormat = GUI_DATE_FORMAT;

  selectedFromDate: Moment = moment().clone().startOf('month');
  selectedToDate: Moment = moment();
  selectedDateNoHighlight1: boolean = false;
  selectedDateNoHighlight2: boolean = false;

  selectedStatus = InvoiceStatuses.RELEASED;

  selectedStatuses: InvoiceStatuses[] = [];

  statusOptions: InvoiceStatus[] = [
    {
      translateLabel: 'invoices.status.RELEASED',
      icon: 'check',
      statusCssClass: 'invoice-status-paid',
      status: InvoiceStatuses.RELEASED
    },
    {
      translateLabel: 'invoices.status.CREDITED',
      icon: 'close',
      statusCssClass: 'invoice-status-not-paid',
      status: InvoiceStatuses.CREDITED
    }
  ];

  constructor(private changeDetectorRef: ChangeDetectorRef, private formBuilder: FormBuilder, private translate: TranslateService) {
  }

  ngOnInit(): void {
    //this.selectStatusOption(this.selectedStatus);
    this.changeFromDate.next(this.selectedFromDate);
    //this.form.controls['fromDate'].setValue(this.selectedFromDate);
    this.changeToDate.next(this.selectedToDate);
    //this.form.controls['toDate'].setValue(this.selectedToDate);

    const requestedFromDateControl = this.formBuilder.control(
      this.selectedFromDate,
      [Validators.required]
    );

    const requestedToDateControl = this.formBuilder.control(
      this.selectedToDate,
      [Validators.required]
    );

    this.form = this.formBuilder.group({
      ['fromDate']: requestedFromDateControl,
      ['toDate']: requestedToDateControl
    });
  }

  isStatusSelected(status: InvoiceStatuses): boolean {
    return this.selectedStatuses.some(aStatus => aStatus === status);
  }

  selectStatusOption(status: InvoiceStatuses) {
    const selectedStatuses = cloneDeep(this.selectedStatuses);

    if (this.isStatusSelected(status)) {
      const statusIndex = this.selectedStatuses.findIndex(aStatus => aStatus === status);
      selectedStatuses.splice(statusIndex, 1);
    } else {
      selectedStatuses.push(status);
    }

    setTimeout(() => {
      // Set selected users in setTimeout and trigger change detection because of a strange problem that the first time
      // checkbox doesn't change its state after being checked https://github.com/angular/angular/issues/9275
      this.selectedStatuses = selectedStatuses;
      this.changeSelectedStatuses.next(this.selectedStatuses);
      this.changeDetectorRef.detectChanges();
    });

  }

  onSelectFromDateInCalendar(fromDate: Moment) {
    setTimeout(() => {
      //console.log(this.selectedFromDate.diff(fromDate))

      if ((this.selectedFromDate != null) && (this.selectedFromDate.diff(fromDate) == 0)) {
        this.selectedFromDate = null;
        this.changeFromDate.next(null);
        this.form.controls['fromDate'].setValue(null);
        this.selectedDateNoHighlight1 = true;
      } else {
        this.selectedFromDate = fromDate;
        this.changeFromDate.next(fromDate);
        this.form.controls['fromDate'].setValue(fromDate);
        this.selectedDateNoHighlight1 = false;
      }
    });
  }

  onSelectToDateInCalendar(toDate: Moment) {
    setTimeout(() => {
      //console.log(this.selectedToDate.diff(toDate))

      if ((this.selectedToDate != null) && (this.selectedToDate.diff(toDate) == 0)) {
        this.selectedToDate = null;
        this.changeToDate.next(null);
        this.form.controls['toDate'].setValue(null);
        this.selectedDateNoHighlight2 = true;
      } else {
        this.selectedToDate = toDate;
        this.changeToDate.next(toDate);
        this.form.controls['toDate'].setValue(toDate);
        this.selectedDateNoHighlight2 = false;
      }
    });
  }


  getFromDateControl(): FormControl {
    return this.form.get('fromDate') as FormControl;
  }

  getFromDateErrorMsg(): string {
    const errors = this.getFromDateControl().errors;

    if (errors) {
      if (errors.ngbDate && !errors.required) {
        return this.translate.instant('projects.validation.startDateInvalid');
      }
    }

    return null;
  }

  getToDateControl(): FormControl {
    return this.form.get('toDate') as FormControl;
  }

  getToDateErrorMsg(): string {
    const errors = this.getToDateControl().errors;

    if (errors) {
      if (errors.ngbDate && !errors.required) {
        return this.translate.instant('projects.validation.startDateInvalid');
      }
    }

    return null;
  }

}
