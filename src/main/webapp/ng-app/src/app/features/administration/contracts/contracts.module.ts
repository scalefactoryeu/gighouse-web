import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/shared.module';
import { LayoutsModule } from '../../../layouts/layouts.module';
import { RouterModule } from '@angular/router';
import { ContractsComponent } from './contracts.component';
import { contractsRoutes } from './contracts-routes';

@NgModule({
  imports: [
    SharedModule,
    LayoutsModule,
    RouterModule.forChild(contractsRoutes)
  ],
  declarations: [
    ContractsComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ContractsModule {}
