import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import {BehaviorSubject, Observable, of, Subject} from 'rxjs';
import {catchError, debounceTime, shareReplay, takeUntil} from 'rxjs/operators';
import { Freelancer } from 'src/app/features/freelancer/freelancer.model';
import {cloneDeep} from 'lodash';
import { InvoicesService } from '../invoices.service';
import {TranslateService} from '@ngx-translate/core';
import {FormBuilder} from '@angular/forms';

@Component({
  selector: 'app-invoices-freelancers-filter',
  templateUrl: './invoices-freelancers-filter.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InvoicesFreelancersFilterComponent implements OnInit, OnDestroy {

  @Output() changeSelectedFreelancers = new EventEmitter<Freelancer[]>();
  @ViewChild('selectionContainer') selectionContainer: ElementRef;

  freelancers$: Observable<Freelancer[]>;
  loading$: Observable<boolean>;
  loadingFailed$: Observable<boolean>;
  infiniteScrollDisabled$: BehaviorSubject<boolean> = new BehaviorSubject(false);

  selectedFreelancers: Freelancer[] = [];

  freelancerFilterForm = this.formBuilder.group({
    search: ['']
  });

  private freelancerSearchFilter$: BehaviorSubject<string> = new BehaviorSubject('');
  private unsubscribe$ = new Subject();
  
  constructor(private changeDetectorRef: ChangeDetectorRef, private formBuilder: FormBuilder, private translate: TranslateService,private invoicesService: InvoicesService) { }

  ngOnInit(): void {
    this.startLoadingFreelancers();
  }

  private startLoadingFreelancers() {
    this.freelancerFilterForm.get('search').valueChanges
      .pipe(
        debounceTime(300),
        takeUntil(this.unsubscribe$)
      )
      .subscribe((val) => {
        this.infiniteScrollDisabled$.next(true);
        this.freelancerSearchFilter$.next(val);
      });

    this.freelancers$ = this.invoicesService.getFreelancersFilterList()
      .pipe(
        catchError((error) => {
          return of(null);
        }),
        shareReplay()
      );
  }

  showTopSpinner(freelancers: Freelancer[], loading: boolean): boolean {
    return !freelancers || (freelancers.length === 0 && loading);
  }

  hideFreelancerSearchInput(freelancers: Freelancer[]): boolean {
    // Hide when freelancers list is empty and search is not filled
    return freelancers.length === 0 && !this.freelancerSearchFilter$.value;
  }

  getNoFreelancersMessage(): string {
    if (this.freelancerSearchFilter$.value) {
      return this.translate.instant('invoices.noFreelancersFiltered');
    } else {
      return this.translate.instant('invoices.noFreelancers');
    }
  }

  isFreelancerSelected(freelancer: Freelancer): boolean {
    return this.selectedFreelancers.some(aFreelancer => aFreelancer.id === freelancer.id);
  }

  selectFreelancer(freelancer: Freelancer) {
    const selectedFreelancers = cloneDeep(this.selectedFreelancers);

    if (this.isFreelancerSelected(freelancer)) {
      const freelancerIndex = this.selectedFreelancers.findIndex(aFreelancer => aFreelancer.id === freelancer.id);
      selectedFreelancers.splice(freelancerIndex, 1);
    } else {
      selectedFreelancers.push(freelancer);
    }

    setTimeout(() => {
      // Set selected users in setTimeout and trigger change detection because of a strange problem that the first time
      // checkbox doesn't change its state after being checked https://github.com/angular/angular/issues/9275
      this.selectedFreelancers = selectedFreelancers;
      this.changeSelectedFreelancers.next(this.selectedFreelancers);
      this.changeDetectorRef.detectChanges();
    });

  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

}
