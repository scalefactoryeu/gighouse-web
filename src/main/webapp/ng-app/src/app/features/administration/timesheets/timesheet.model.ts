import * as moment from 'moment';
import { BACKEND_DATE_FORMAT } from '../../../core/models/constants';

export interface ITimeSheetEntry {
  id: string;
  freelancerId: string;
  projectId: string;
  start: string; // '2020-11-28T14:37:22.837Z'
  end: string; // '2020-11-28T14:37:22.837Z'
  hourlyRate: number;
  duration: number;
  status: string;
  freelancerComment: string;
}

export interface ITimeSheetFreelancer {
  id: string;
  name: string;
}

export interface IInvoicePdf {
  invoiceBlob: string;
}

export interface ITimeSheetProject {
  id: string;
  name: string;
}

export interface ITimeSheetResponse {
  dateFrom: string; // '2020-11-28'
  dateTo: string; // '2020-11-28'
  entries: ITimeSheetEntry[];
  freelancers: ITimeSheetFreelancer[];
  projects: ITimeSheetProject[];
}

export class InvoicePdf {
  invoiceBlob : string;

  constructor(invoiceBlob : IInvoicePdf) {
    this.invoiceBlob = invoiceBlob.invoiceBlob;
  }

}

export class TimeSheet {
  freelancer: ITimeSheetFreelancer;
  project: ITimeSheetProject;
  hours: number[];
  totalHours: number;
  totalCharged: number;
  timeEntries: ITimeSheetEntry[];
  status: string;

  constructor(freelancer: ITimeSheetFreelancer, project: ITimeSheetProject, hours: number[], totalCharged: number, timeEntries: ITimeSheetEntry[], status: string) {
    this.freelancer = freelancer;
    this.project = project;
    this.hours = hours;
    this.totalHours = Number(hours.reduce((accumulated, currentValue) => accumulated + currentValue, 0).toFixed(2));
    this.totalCharged = totalCharged;
    this.timeEntries = timeEntries;
    this.status = status;
  }
}

export class TimeSheetFreelancer {
  id: string;
  name: string;

  constructor(freelancerBackend: ITimeSheetFreelancer) {
    this.id = freelancerBackend.id;
    this.name = freelancerBackend.name;
  }
}

export const TimeSheetFactory = (timeSheetResponse: ITimeSheetResponse): TimeSheet[] => {
  const timeSheets: TimeSheet[] = [];

  timeSheetResponse.freelancers.forEach(freelancer => {
    const freelancerTimeEntries = timeSheetResponse.entries.filter(entry => entry.freelancerId === freelancer.id);

    timeSheetResponse.projects.forEach(project => {
      const projectTimeEntries = freelancerTimeEntries.filter(entry => entry.projectId === project.id);

      if (projectTimeEntries.length) {

        // Entries per date: {'2020-11-23T00:00:00Z': ITimeSheetEntry, '2020-11-24T00:00:00Z': ITimeSheetEntry}
        const entriesPerDate: {[isoDate: string]: ITimeSheetEntry[]} = {};

        let status = projectTimeEntries[0].status

        projectTimeEntries.forEach(entry => {
          if (entry.status != status) {
            status = 'Mixed';
          }

          const isoDate = moment.utc(entry.start).startOf('day').format();

          if (entriesPerDate[isoDate]) {
            entriesPerDate[isoDate].push(entry);
          } else {
            entriesPerDate[isoDate] = [entry];
          }
        });

        const hoursPerDay: number[] = [];
        let totalCharged = 0;

        for (let i = 0; i < 7; i++) {
          const isoDate = moment.utc(timeSheetResponse.dateFrom, BACKEND_DATE_FORMAT).add(i, 'day').format();

          if (entriesPerDate[isoDate]) {
            let totalDurationPerDate = 0;

            entriesPerDate[isoDate].forEach(entry => {
              totalDurationPerDate += entry.duration;
              totalCharged += (entry.duration * entry.hourlyRate);
            });
            let roundTotalDurationPerDate = Number(totalDurationPerDate.toFixed(2));
            hoursPerDay.push(roundTotalDurationPerDate);
          } else {
            hoursPerDay.push(0);
          }
        }
        let roundToTwoDigits =  totalCharged.toFixed(2);
        const timeSheet = new TimeSheet(freelancer, project, hoursPerDay, Number(roundToTwoDigits), projectTimeEntries, status);
        timeSheets.push(timeSheet);
      }
    });
  });

  return timeSheets;
};
