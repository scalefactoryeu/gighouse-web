import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {IInvoiceBackend, Invoice} from './invoices.model';
import {Moment} from 'moment';
import {Freelancer} from '../../freelancer/freelancer.model';
import {cloneDeep} from 'lodash';
import {BACKEND_DATE_FORMAT} from '../../../core/models/constants';
import {IInvoicePdf, InvoicePdf, ITimeSheetFreelancer, TimeSheetFreelancer} from '../timesheets/timesheet.model';
import {InvoiceStatuses} from 'src/app/core/models/invoice-status.model';

@Injectable({
  providedIn: 'root'
})
export class InvoicesService {

  constructor(private httpClient: HttpClient) {
  }

  getInvoices(fromDate: Moment, toDate: Moment, filteredFreelancers: Freelancer[], selectedStatuses: InvoiceStatuses[]): Observable<Invoice[]> {

    //const dateFromParam = fromDate.format(BACKEND_DATE_FORMAT);
    const dateFromParam = cloneDeep(fromDate).format(BACKEND_DATE_FORMAT);
    const dateToParam = cloneDeep(toDate).format(BACKEND_DATE_FORMAT);
    const freelancerIdsParam = filteredFreelancers ? filteredFreelancers.map(freelancer => freelancer.id) : [];
    const statusesParam = selectedStatuses ? selectedStatuses.map(status => status) : [];

    let params = new HttpParams();

    params = params.append('invoiceDateFrom', dateFromParam);
    params = params.append('invoiceDateTo', dateToParam);

    freelancerIdsParam.forEach((freelancerId: string) => {
      params = params.append(`freelancers`, freelancerId);
    });

    statusesParam.forEach((status: string) => {
      params = params.append(`statuses`, status);
    });

    // return of([])
    // return of(invoicesMock)
    return this.httpClient.get<IInvoiceBackend[]>(`/api/invoices/`, {params})
      .pipe(
        map((invoiceBackend: IInvoiceBackend[]) => {
          return invoiceBackend.map(invoice => new Invoice(invoice));
        })
      );
  }

  getFreelancersFilterList(): Observable<TimeSheetFreelancer[]> {
    return this.httpClient.get<ITimeSheetFreelancer[]>(`/api/timesheets/freelancers`)
      .pipe(
        map((freelancersBackend: ITimeSheetFreelancer[]) => {
          return freelancersBackend.map(freelancer => new TimeSheetFreelancer(freelancer));
        })
      );
  }


  getInvoicePdf(invoiceId : string): Observable<InvoicePdf> {
    return this.httpClient.get<IInvoicePdf>(`/api/invoices/invoicePdf/${invoiceId}`)
      .pipe(
        map((invoiceBackend: IInvoicePdf) => {
          return new InvoicePdf(invoiceBackend);
        })
      );
  }


}
