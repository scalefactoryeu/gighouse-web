import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/shared.module';
import { LayoutsModule } from '../../../layouts/layouts.module';
import { RouterModule } from '@angular/router';
import { TimesheetsComponent } from './timesheets.component';
import { TimesheetsFilterComponent } from './timesheets-filter/timesheets-filter.component';
import { TimesheetsFreelancersFilterComponent } from './timesheets-freelancers-filter/timesheets-freelancers-filter.component';
import { TimesheetsMainComponent } from './timesheets-main/timesheets-main.component';
import { timesheetsRoutes } from './timesheets-routes';
import { TimesheetsDetailComponent } from "./timesheets-detail/timesheets-detail.component";

@NgModule({
  imports: [
    SharedModule,
    LayoutsModule,
    RouterModule.forChild(timesheetsRoutes)
  ],
  declarations: [
    TimesheetsComponent,
    TimesheetsFilterComponent,
    TimesheetsFreelancersFilterComponent,
    TimesheetsMainComponent,
    TimesheetsDetailComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TimesheetsModule {}
