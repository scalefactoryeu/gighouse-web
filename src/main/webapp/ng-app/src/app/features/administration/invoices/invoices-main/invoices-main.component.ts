import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {Observable, throwError} from 'rxjs';
import {Invoice} from '../invoices.model';
import {InvoicesService} from "../invoices.service";
import {InvoicePdf} from "../../timesheets/timesheet.model";
import {catchError} from "rxjs/operators";
import {ErrorHandlingService} from "../../../../core/error-handling/error-handling.service";
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-invoices-main',
  templateUrl: './invoices-main.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InvoicesMainComponent implements OnInit {

  @Input() invoices: Invoice[];
  @Input() totalCount: number;
  @Input() loading: boolean;
  @Input() loadingFailed: boolean;

  // @Output() loadMore = new EventEmitter();
  // @Output() retry = new EventEmitter();

  //viewInitialized$: BehaviorSubject<boolean> = new BehaviorSubject(false);


  constructor(private invoicesService: InvoicesService, private errorHandlingService: ErrorHandlingService, private translate: TranslateService) {
  }

  ngOnInit() {
  }

  showInvoicesTable(): boolean {
    return !!this.invoices && this.invoices.length > 0 && !this.loading;
  }

  getInvoicePdf(invoiceId: string): void {
    this.invoicesService.getInvoicePdf(invoiceId)
      .pipe(
        catchError((error) => {
          this.errorHandlingService.openErrorModal(this.translate.instant('invoices.invoicePdfFailed'));
          return throwError(error);
        })
      ).subscribe((invoicePdf) => {
      const b64toBlob = (b64Data, contentType = '', sliceSize = 512) => {
        const byteCharacters = atob(b64Data);
        const byteArrays = [];

        for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
          const slice = byteCharacters.slice(offset, offset + sliceSize);

          const byteNumbers = new Array(slice.length);
          for (let i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
          }

          const byteArray = new Uint8Array(byteNumbers);
          byteArrays.push(byteArray);
        }

        const blob = new Blob(byteArrays, {type: contentType});
        return blob;
      }
      const blob = b64toBlob(invoicePdf.invoiceBlob, 'application/pdf');
      const url = window.URL.createObjectURL(blob);
      window.open(url);
    });
  }

  // ngAfterViewInit() {
  //   setTimeout(() => {
  //     this.viewInitialized$.next(true);
  //   });
  // }
  // scrolled() {
  //   if (this.invoices.length < this.totalCount) {
  //     this.loadMore.next();
  //   }
  // }

  /**
   * Show "Load More" button when there are more items to be loaded, but there is no scrollbar. This case happens when
   * the initially loaded items still fit in the div without the div getting a scrollbar; without scrolling the infinite scroll will
   * not trigger a new load.
   *
   * We use viewInitialized to know when the view is really initialized and to be sure that we can rely on the scrollHeight and
   * clientHeight values.
   */
  // showLoadMoreBtn(viewInitialized: boolean): boolean {
  //   const scrollableContainer = document.body;

  //   if (viewInitialized) {
  //     const scrollableContainerHasScrollbar = scrollableContainer.scrollHeight > scrollableContainer.clientHeight;
  //     return !this.loading && !this.loadingFailed && this.invoices.length < this.totalCount && !scrollableContainerHasScrollbar;
  //   }
  //   return false;
  // }
}
