import { Moment } from 'moment';
import * as moment from 'moment';
import { BACKEND_DATE_FORMAT } from 'src/app/core/models/constants';
import { InvoiceStatus, InvoiceStatuses, INVOICE_STATUSES_CONFIG } from 'src/app/core/models/invoice-status.model';


export class IInvoiceResponse {

}

// export enum InvoiceStatus {
//     PAID = "Paid",
//     NOT_PAID = "Not paid"
// }

// type InvoiceStatusConfig = {
//     translateLabel: string;
//     icon: string;
//     statusCssClass: string;
// }

// type InvoiceStatusesConfig = {
//     [P in InvoiceStatus]: InvoiceStatusConfig;
//   };

// const INVOICE_STATUSES_CONFIG: InvoiceStatusesConfig = {
//     [InvoiceStatus.PAID]: {
//         translateLabel: 'invoices.status.PAID',
//         icon: 'check',
//         statusCssClass: 'invoice-status-paid'
//     },
//     [InvoiceStatus.NOT_PAID]: {
//         translateLabel: 'invoices.status.NOT_PAID',
//         icon: 'close',
//         statusCssClass: 'invoice-status-not-paid'
//     }
// }

export interface IInvoiceFreelancer {
    id: string;
    name: string;
}

export class InvoiceFreelancer {
    id: string;
    name: string;

    constructor(invoiceFreelancer: IInvoiceFreelancer) {
        this.id = invoiceFreelancer.id;
        this.name = invoiceFreelancer.name;
    }
}

export interface IInvoiceBackend {
    id: string;
    invoiceNumber: string;
    invoiceDate: string;
    periodStartDate: string;
    periodEndDate: string;
    totalAmountWithVAT: number;
    status: InvoiceStatuses;
    freelancers: InvoiceFreelancer[];
}

export class Invoice {
    id: string;
    number: string;
    date: Moment;
    periodStartDate: Moment;
    periodEndDate: Moment;
    totalAmountWithVAT: number;
    status: InvoiceStatuses;
    statusConfig: InvoiceStatus;
    freelancers: InvoiceFreelancer[];
    freelancersConcat: string;

    constructor(invoiceBackend: IInvoiceBackend) {
        this.id = invoiceBackend.id;
        this.number = invoiceBackend.invoiceNumber === null ? null : invoiceBackend.invoiceNumber;
        this.date = invoiceBackend.invoiceDate === null ? null : moment.utc(invoiceBackend.invoiceDate, BACKEND_DATE_FORMAT);
        this.periodStartDate = invoiceBackend.periodStartDate === null ? null : moment.utc(invoiceBackend.periodStartDate, BACKEND_DATE_FORMAT);
        this.periodEndDate = invoiceBackend.periodEndDate === null ? null : moment.utc(invoiceBackend.periodEndDate, BACKEND_DATE_FORMAT);
        this.totalAmountWithVAT = invoiceBackend.totalAmountWithVAT;
        this.status = invoiceBackend.status;
        this.statusConfig = INVOICE_STATUSES_CONFIG[invoiceBackend.status];
        this.freelancers = invoiceBackend.freelancers;
        this.freelancersConcat = invoiceBackend.freelancers.map((fl) => fl.name).join();
    }
}
