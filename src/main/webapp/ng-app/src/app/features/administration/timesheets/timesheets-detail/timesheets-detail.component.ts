import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { TimeSheet } from '../timesheet.model';
import { TimeSheetsService } from "../timesheets.service";
import { catchError, map, switchMap } from "rxjs/operators";
import { BehaviorSubject, EMPTY, throwError } from "rxjs";
import { ActivatedRoute } from '@angular/router';
import { ModalService } from "../../../../modals/modal.service";
import { ErrorHandlingService } from "../../../../core/error-handling/error-handling.service";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: 'app-timesheets-detail',
  templateUrl: './timesheets-detail.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TimesheetsDetailComponent implements OnInit {

  timeSheet$: BehaviorSubject<TimeSheet> = new BehaviorSubject<TimeSheet>(null);
  loading = false;
  loadingFailed = false;

  constructor(private activatedRoute: ActivatedRoute,
              private timeSheetsService: TimeSheetsService,
              private modalService: ModalService,
              private errorHandlingService: ErrorHandlingService,
              private translate: TranslateService) {
  }

  ngOnInit(): void {
    this.loading = true;
    this.activatedRoute.params.pipe(
      map(params => ({projectId: params['projectId'], freelancerId: params['freelancerId']})),
      switchMap(params => this.timeSheetsService.getTimeSheetsForProjectAndFreelancer(params.projectId, params.freelancerId)),
      catchError(() => {
        this.loadingFailed = true
        return EMPTY;
      }),
    ).subscribe(timesheet => {
      this.timeSheet$.next(timesheet);
      this.loading = false;
    });

  }

  approve(timeSheet: TimeSheet) {

    const modalRef = this.modalService.openConfirmRemoveModal(this.translate.instant('timesheets.modal.approveTitle'), this.translate.instant('timesheets.modal.approveBody'), this.translate.instant('timesheets.modal.approveConfirmButtonText'));

    modalRef.result
      .then(
        () => {
          const ids = timeSheet.timeEntries.map(entry => entry.id)

          this.timeSheetsService.approve(ids)
            .pipe(
              catchError((error) => {
                this.errorHandlingService.openErrorModal(this.translate.instant('timesheets.failedToApproveTimeSheet'));
                return throwError(error);
              })
            ).subscribe(() => {
            this.timeSheet$.next(({
              ...timeSheet,
              timeEntries: timeSheet.timeEntries.map(timeEntry => ({...timeEntry, status: 'Approved'}))
            }))
          });
        },
        () => {
        }
      );

  }

  reject(timeSheet: TimeSheet) {

    const modalRef = this.modalService.openConfirmRejectModal(this.translate.instant('timesheets.modal.rejectTitle'), this.translate.instant('timesheets.modal.rejectBody'), timeSheet.timeEntries.map(entry => entry.id),this.translate.instant('timesheets.modal.rejectConfirmButtonText'));
  //TODO: CHANGE
    modalRef.result
      .then(
        () => {
          const ids = timeSheet.timeEntries.map(entry => entry.id)

          this.timeSheetsService.reject(ids, '')
            .pipe(
              catchError((error) => {
                this.errorHandlingService.openErrorModal(this.translate.instant('timesheets.failedToApproveTimeSheet'));
                return throwError(error);
              })
            ).subscribe(() => {
            this.timeSheet$.next(({
              ...timeSheet,
              timeEntries: timeSheet.timeEntries.map(timeEntry => ({...timeEntry, status: 'Rejected'}))
            }))
          });
        },
        () => {
        }
      );

  }
}
