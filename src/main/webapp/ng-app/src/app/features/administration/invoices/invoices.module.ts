import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/shared.module';
import { LayoutsModule } from '../../../layouts/layouts.module';
import { RouterModule } from '@angular/router';
import { InvoicesComponent } from './invoices.component';
import { invoicesRoutes } from './invoices-routes';
import { InvoicesFilterComponent } from './invoices-filter/invoices-filter.component';
import { InvoicesFreelancersFilterComponent } from './invoices-freelancers-filter/invoices-freelancers-filter.component';
import { InvoicesMainComponent } from './invoices-main/invoices-main.component';

@NgModule({
  imports: [
    SharedModule,
    LayoutsModule,
    RouterModule.forChild(invoicesRoutes)
  ],
  declarations: [
    InvoicesComponent,
    InvoicesFilterComponent,
    InvoicesFreelancersFilterComponent,
    InvoicesMainComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class InvoicesModule {}
