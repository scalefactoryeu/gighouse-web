import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import { Freelancer } from '../../../freelancer/freelancer.model';
import { cloneDeep } from 'lodash';
import { BehaviorSubject, Observable, of, Subject } from 'rxjs';
import { FormBuilder } from '@angular/forms';
import { catchError, debounceTime, shareReplay, takeUntil } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { TimeSheetsService } from "../timesheets.service";

@Component({
  selector: 'app-timesheets-freelancers-filter',
  templateUrl: './timesheets-freelancers-filter.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TimesheetsFreelancersFilterComponent implements OnInit, OnDestroy {

  @Output() changeSelectedFreelancers = new EventEmitter<Freelancer[]>();
  @ViewChild('selectionContainer') selectionContainer: ElementRef;

  freelancers$: Observable<Freelancer[]>;
  loading$: Observable<boolean>;
  loadingFailed$: Observable<boolean>;
  infiniteScrollDisabled$: BehaviorSubject<boolean> = new BehaviorSubject(false);

  selectedFreelancers: Freelancer[] = [];

  freelancerFilterForm = this.formBuilder.group({
    search: ['']
  });

  private freelancerSearchFilter$: BehaviorSubject<string> = new BehaviorSubject('');
  private unsubscribe$ = new Subject();

  constructor(private changeDetectorRef: ChangeDetectorRef, private formBuilder: FormBuilder, private translate: TranslateService,
    private timeSheetsService: TimeSheetsService) { }

  ngOnInit(): void {
    this.startLoadingFreelancers();
  }

  private startLoadingFreelancers() {
    this.freelancerFilterForm.get('search').valueChanges
      .pipe(
        debounceTime(300),
        takeUntil(this.unsubscribe$)
      )
      .subscribe((val) => {
        this.infiniteScrollDisabled$.next(true);
        this.freelancerSearchFilter$.next(val);
      });

    this.freelancers$ = this.timeSheetsService.getFreelancersFilterList()
      .pipe(
        catchError((error) => {
          return of(null);
        }),
        shareReplay()
      );
  }

  showTopSpinner(freelancers: Freelancer[], loading: boolean): boolean {
    return !freelancers || (freelancers.length === 0 && loading);
  }

  hideFreelancerSearchInput(freelancers: Freelancer[]): boolean {
    // Hide when freelancers list is empty and search is not filled
    return freelancers.length === 0 && !this.freelancerSearchFilter$.value;
  }

  getNoFreelancersMessage(): string {
    if (this.freelancerSearchFilter$.value) {
      return this.translate.instant('timesheets.noFreelancersFiltered');
    } else {
      return this.translate.instant('timesheets.noFreelancers');
    }
  }

  isFreelancerSelected(freelancer: Freelancer): boolean {
    return this.selectedFreelancers.some(aFreelancer => aFreelancer.id === freelancer.id);
  }

  selectFreelancer(freelancer: Freelancer) {
    const selectedFreelancers = cloneDeep(this.selectedFreelancers);

    if (this.isFreelancerSelected(freelancer)) {
      const freelancerIndex = this.selectedFreelancers.findIndex(aFreelancer => aFreelancer.id === freelancer.id);
      selectedFreelancers.splice(freelancerIndex, 1);
    } else {
      selectedFreelancers.push(freelancer);
    }

    setTimeout(() => {
      // Set selected users in setTimeout and trigger change detection because of a strange problem that the first time
      // checkbox doesn't change its state after being checked https://github.com/angular/angular/issues/9275
      this.selectedFreelancers = selectedFreelancers;
      this.changeSelectedFreelancers.next(this.selectedFreelancers);
      this.changeDetectorRef.detectChanges();
    });

  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

}
