import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { BehaviorSubject, combineLatest, merge, Observable, of, Subject } from 'rxjs';
import { Freelancer } from '../../freelancer/freelancer.model';
import { catchError, filter, map, mapTo, shareReplay, switchMap, takeUntil } from 'rxjs/operators';
import { Moment } from 'moment';
import { TimeSheet } from './timesheet.model';
import { TimeSheetsService } from './timesheets.service';

@Component({
  selector: 'app-timesheets',
  templateUrl: './timesheets.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TimesheetsComponent implements OnInit, OnDestroy {

  periodFromDate$ = new BehaviorSubject<Moment>(null);
  selectedFreelancers$ = new BehaviorSubject<Freelancer[]>(null);
  timeSheets$: Observable<TimeSheet[]>;
  loading$: Observable<boolean>;
  loadingFailed$: Observable<boolean>;
  filterExpanded = true;

  perfectScrollbarConfig = {
    suppressScrollX: true
  };

  private unsubscribe$ = new Subject();

  constructor(private timeSheetsService: TimeSheetsService) {}

  ngOnInit(): void {
    this.startLoadingTimeSheets();
  }

  private startLoadingTimeSheets() {
    const loadTimesheets$ = combineLatest([this.periodFromDate$, this.selectedFreelancers$])
      .pipe(
        filter(([periodFromDate, selectedFreelancers]) => {
          return !!periodFromDate;
        })
      );

    this.timeSheets$ = loadTimesheets$
      .pipe(
        switchMap(([periodFromDate, selectedFreelancers]) => {
            return this.timeSheetsService.getTimeSheetsForPeriodAndFreelancers(periodFromDate, selectedFreelancers)
              .pipe(
                catchError((error) => {
                  // return null instead of throwing the error to keep the observable stream alive
                  return of(null);
                }),
              );
        }),
        takeUntil(this.unsubscribe$),
        shareReplay()
      );

    this.loading$ = merge(
      loadTimesheets$.pipe(mapTo(true)),
      this.timeSheets$.pipe(mapTo(false))
    );

    this.loadingFailed$ = merge(
      loadTimesheets$.pipe(mapTo(false)),
      this.timeSheets$.pipe(map((timeSheets) => !timeSheets)) // loading is failed when timeSheets=null
    );
  }

  changePeriodFromDate(periodFromDate: Moment) {
    // Emit periodFromDate in the next cycle because otherwise switching between CURRENT and PREVIOUS week would not take effect until
    // the next cycle. Reason is that this component sets 'selectedPeriod' and in the same cycle child component calculates and outputs
    // 'periodFromDate'.
    setTimeout(() => {
      this.periodFromDate$.next(periodFromDate);
    });
  }

  changeSelectedFreelancers(selectedFreelancers: Freelancer[]) {
    if (selectedFreelancers.length === 0) {
      this.selectedFreelancers$.next(null);
    } else {
      this.selectedFreelancers$.next(selectedFreelancers);
    }
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

}
