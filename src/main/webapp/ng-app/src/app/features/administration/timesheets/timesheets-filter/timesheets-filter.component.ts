import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { IPeriodOption, PeriodSelection } from './timesheets-filter.model';
import { Freelancer } from '../../../freelancer/freelancer.model';
import { cloneDeep } from 'lodash';
import { Moment } from 'moment';
import * as moment from 'moment';

@Component({
  selector: 'app-timesheets-filter',
  templateUrl: './timesheets-filter.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TimesheetsFilterComponent implements OnInit {

  @Input() expanded: boolean;

  @Output() toggleExpanded = new EventEmitter();
  @Output() changePeriodFromDate = new EventEmitter<Moment>();
  @Output() changeSelectedFreelancers = new EventEmitter<Freelancer[]>();

  selectedPeriod: PeriodSelection = PeriodSelection.CURRENT_WEEK;
  selectedDate: Moment;
  rangeFromDate: Moment;
  rangeToDate: Moment;

  periodOptions: IPeriodOption[] = [
    {id: PeriodSelection.CURRENT_WEEK, label: 'timesheets.periodSelection.currentWeek'},
    {id: PeriodSelection.PREVIOUS_WEEK, label: 'timesheets.periodSelection.previousWeek'},
    {id: PeriodSelection.CUSTOM, label: 'timesheets.periodSelection.custom'}
  ];

  constructor() {}

  ngOnInit(): void {
    // Initially select a period
    this.selectPeriodOption(this.selectedPeriod);
  }

  selectPeriodOption(period: PeriodSelection) {
    this.selectedPeriod = period;

    if (this.selectedPeriod === PeriodSelection.CURRENT_WEEK) {
      this.selectedDate = moment();
    } else if (this.selectedPeriod === PeriodSelection.PREVIOUS_WEEK) {
      this.selectedDate = moment().subtract(1, 'week');
    }

    this.setWeekRange(this.selectedDate);
  }

  onSelectDateInCalendar(date: Moment) {
    this.setWeekRange(date);
    this.selectedPeriod = PeriodSelection.CUSTOM;
  }

  private setWeekRange(date: Moment) {
    this.rangeFromDate = cloneDeep(date.startOf('isoWeek'));
    this.rangeToDate = cloneDeep(date.endOf('isoWeek'));
    this.changePeriodFromDate.next(this.rangeFromDate);
  }

}
