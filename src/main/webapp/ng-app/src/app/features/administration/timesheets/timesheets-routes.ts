import { Routes } from '@angular/router';
import { TimesheetsComponent } from './timesheets.component';
import { TimesheetsDetailComponent } from "./timesheets-detail/timesheets-detail.component";

export const timesheetsRoutes: Routes = [
  {
    path: '',
    component: TimesheetsComponent,
    data: {
      authorities: [],
      pageTitle: 'timesheets.title'
    },
  },
  {
    path: 'project/:projectId/freelancer/:freelancerId',
    component: TimesheetsDetailComponent,
    data: {
      authorities: [],
      pageTitle: 'timesheets.detail.title'
    }
  },
];
