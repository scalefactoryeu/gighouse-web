import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {BehaviorSubject, combineLatest, merge, Observable, of, Subject} from 'rxjs';
import {Freelancer} from '../../freelancer/freelancer.model';
import {catchError, filter, map, mapTo, shareReplay, switchMap, takeUntil} from 'rxjs/operators';
import {Moment} from 'moment';
import {ContractsService} from './contracts.service';
import {Contract} from './contracts.model';

@Component({
  selector: 'app-invoices',
  templateUrl: './contracts.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ContractsComponent implements OnInit, OnDestroy {

  periodFromDate$ = new BehaviorSubject<Moment>(null);
  selectedFreelancers$ = new BehaviorSubject<Freelancer[]>(null);
  contracts$: Observable<Contract[]>;
  loading$: Observable<boolean>;
  loadingFailed$: Observable<boolean>;

  perfectScrollbarConfig = {
    suppressScrollX: true
  };

  private unsubscribe$ = new Subject();

  constructor(private contractsService: ContractsService) {
  }

  ngOnInit(): void {
    this.startLoadingContracts();
  }

  private startLoadingContracts() {
    const loadContracts$ = combineLatest([this.periodFromDate$, this.selectedFreelancers$])
      .pipe(
        filter(([periodFromDate, selectedFreelancers]) => {
          return !!periodFromDate;
        })
      );

    this.contracts$ = loadContracts$
      .pipe(
        switchMap(([periodFromDate, selectedFreelancers]) => {
          return this.contractsService.getContracts(periodFromDate, selectedFreelancers)
            .pipe(
              catchError((error) => {
                // return null instead of throwing the error to keep the observable stream alive
                return of(null);
              }),
            );
        }),
        takeUntil(this.unsubscribe$),
        shareReplay()
      );

    this.loading$ = merge(
      loadContracts$.pipe(mapTo(true)),
      this.contracts$.pipe(mapTo(false))
    );

    this.loadingFailed$ = merge(
      loadContracts$.pipe(mapTo(false)),
      this.contracts$.pipe(map((contracts) => !contracts)) // loading is failed when contracts=null
    );
  }

  changePeriodFromDate(periodFromDate: Moment) {
    // Emit periodFromDate in the next cycle because otherwise switching between CURRENT and PREVIOUS week would not take effect until
    // the next cycle. Reason is that this component sets 'selectedPeriod' and in the same cycle child component calculates and outputs
    // 'periodFromDate'.
    setTimeout(() => {
      this.periodFromDate$.next(periodFromDate);
    });
  }

  changeSelectedFreelancers(selectedFreelancers: Freelancer[]) {
    if (selectedFreelancers.length === 0) {
      this.selectedFreelancers$.next(null);
    } else {
      this.selectedFreelancers$.next(selectedFreelancers);
    }
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

}
