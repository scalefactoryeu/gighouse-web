import { Routes } from '@angular/router';
import { InvoicesComponent } from './invoices.component';

export const invoicesRoutes: Routes = [
  {
    path: '',
    component: InvoicesComponent,
    data: {
      authorities: [],
      pageTitle: 'invoices.title'
    }
  }
];
