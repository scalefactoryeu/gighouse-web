import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { delay, map } from 'rxjs/operators';
import { Contract, IContractResponse } from './contracts.model';
import { Freelancer } from '../../freelancer/freelancer.model';
import { Moment } from 'moment';
import { cloneDeep } from 'lodash';
import { BACKEND_DATE_FORMAT } from '../../../core/models/constants';

@Injectable({
  providedIn: 'root'
})
export class ContractsService {

  constructor(private httpClient: HttpClient) {
  }

  getContracts(periodFromDate: Moment, filteredFreelancers: Freelancer[]): Observable<Contract[]> {
    const dateFromParam = cloneDeep(periodFromDate).format(BACKEND_DATE_FORMAT);
    const dateToParam = cloneDeep(periodFromDate).add(7, 'days').format(BACKEND_DATE_FORMAT);
    const freelancerIdsParam = filteredFreelancers ? filteredFreelancers.map(freelancer => freelancer.id) : [];

    let params = new HttpParams();
    params = params.append('dateFrom', dateFromParam);
    params = params.append('dateTo', dateToParam);

    freelancerIdsParam.forEach((freelancerId: string) => {
      params = params.append(`freelancers`, freelancerId);
    });

    // return of([])
    // return of(contractsMock)
    return this.httpClient.get<IContractResponse>(`/api/contracts/`, {params})
      .pipe(
        delay(2000),
        map((contractsResponse: IContractResponse) => {
          return null;
        })
      );
  }
}
