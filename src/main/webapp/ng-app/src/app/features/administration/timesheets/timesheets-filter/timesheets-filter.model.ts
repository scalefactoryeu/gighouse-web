export enum PeriodSelection {
  CURRENT_WEEK = 'CURRENT_WEEK',
  PREVIOUS_WEEK = 'PREVIOUS_WEEK',
  CUSTOM = 'CUSTOM'
}

export interface IPeriodOption {
  id: PeriodSelection;
  label: string;
}
