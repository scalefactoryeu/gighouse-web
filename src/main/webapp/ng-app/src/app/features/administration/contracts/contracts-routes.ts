import { Routes } from '@angular/router';
import { ContractsComponent } from './contracts.component';

export const contractsRoutes: Routes = [
  {
    path: '',
    component: ContractsComponent,
    data: {
      authorities: [],
      pageTitle: 'contracts.title'
    }
  }
];
