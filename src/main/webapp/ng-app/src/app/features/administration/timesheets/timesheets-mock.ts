import { ITimeSheetResponse } from './timesheet.model';

export const timeSheetResponseMock: ITimeSheetResponse = {
  dateFrom: '2020-11-23',
  dateTo: '2020-11-29',
  entries: [
    // Kelly Vervis
    {
      freelancerId: '##001',
      start: '2020-11-23T12:00:00Z',
      end: '2020-11-23T14:00:00Z',
      hourlyRate: 10,
      projectId: '##001'
    },
    {
      freelancerId: '##001',
      start: '2020-11-25T10:00:00Z',
      end: '2020-11-25T11:00:00Z',
      hourlyRate: 10,
      projectId: '##001'
    },
    // Lieven Degrauwe
    {
      freelancerId: '##002',
      start: '2020-11-24T10:00:00Z',
      end: '2020-11-24T11:00:00Z',
      hourlyRate: 10,
      projectId: '##002'
    },
    {
      freelancerId: '##002',
      start: '2020-11-24T14:00:00Z',
      end: '2020-11-24T16:00:00Z',
      hourlyRate: 10,
      projectId: '##003'
    },
    {
      freelancerId: '##002',
      start: '2020-11-26T10:00:00Z',
      end: '2020-11-26T11:00:00Z',
      hourlyRate: 10,
      projectId: '##004'
    },
    // Barbara Dingske
    {
      freelancerId: '##003',
      start: '2020-11-24T10:00:00Z',
      end: '2020-11-24T11:00:00Z',
      hourlyRate: 10,
      projectId: '##002'
    },
    {
      freelancerId: '##003',
      start: '2020-11-24T14:00:00Z',
      end: '2020-11-24T16:00:00Z',
      hourlyRate: 10,
      projectId: '##002'
    },
    {
      freelancerId: '##003',
      start: '2020-11-24T17:00:00Z',
      end: '2020-11-24T20:00:00Z',
      hourlyRate: 10,
      projectId: '##003'
    },
    // John Doe
    {
      freelancerId: '##004',
      start: '2020-11-26T10:00:00Z',
      end: '2020-11-26T11:00:00Z',
      hourlyRate: 10,
      projectId: '##004'
    }
  ],
  freelancers: [
    {
      id: '##001',
      name: 'Kelly Vervis'
    },
    {
      id: '##002',
      name: 'Lieven Degrauwe'
    },
    {
      id: '##003',
      name: 'Barbara Dingske'
    },
    {
      id: '##004',
      name: 'John Doe'
    }
  ],
  projects: [
    {
      id: '##001',
      name: 'Project-1'
    },
    {
      id: '##002',
      name: 'Project-2'
    },
    {
      id: '##003',
      name: 'Project-3'
    },
    {
      id: '##004',
      name: 'Project-4'
    }
  ]
};
