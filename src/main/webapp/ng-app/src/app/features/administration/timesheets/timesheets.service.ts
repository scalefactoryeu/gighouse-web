import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { EMPTY, Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { ITimeSheetFreelancer, ITimeSheetResponse, TimeSheet, TimeSheetFactory, TimeSheetFreelancer } from './timesheet.model';
import { Moment } from 'moment';
import { Freelancer } from '../../freelancer/freelancer.model';
import { cloneDeep } from 'lodash';
import { BACKEND_DATE_FORMAT } from '../../../core/models/constants';

@Injectable({
  providedIn: 'root'
})
export class TimeSheetsService {

  constructor(private httpClient: HttpClient) {
  }

  getTimeSheetsForPeriodAndFreelancers(periodFromDate: Moment, filteredFreelancers: Freelancer[]): Observable<TimeSheet[]> {
    const dateFromParam = cloneDeep(periodFromDate).format(BACKEND_DATE_FORMAT);
    const dateToParam = cloneDeep(periodFromDate).add(6, 'days').format(BACKEND_DATE_FORMAT);
    const freelancerIdsParam = filteredFreelancers ? filteredFreelancers.map(freelancer => freelancer.id) : [];

    let params = new HttpParams();
    params = params.append('dateFrom', dateFromParam);
    params = params.append('dateTo', dateToParam);

    freelancerIdsParam.forEach((freelancerId: string) => {
      params = params.append(`freelancers`, freelancerId);
    });

    return this.getTimeSheetsForParams(params);
  }

  getTimeSheetsForProjectAndFreelancer(projectId: string, freelancerId: string): Observable<TimeSheet> {
    let params = new HttpParams();
    params = params.append('freelancers', freelancerId);
    params = params.append('projects', projectId);

    return this.getTimeSheetsForParams(params)
      .pipe(
        map(timesheets => {
          if (timesheets?.length != 1) {
            throwError(`No timesheets or multiple timesheets were found for project: ${projectId} and freelancerId: ${freelancerId}`)
          }
          return timesheets[0];
        }),
        catchError(error => {
          console.log(error);
          return EMPTY;
        })
      );
  }

  private getTimeSheetsForParams(params: HttpParams) {
    return this.httpClient.get<ITimeSheetResponse>(`/api/timesheets/`, {params})
      .pipe(
        map((timeSheetResponse: ITimeSheetResponse) => {
          return TimeSheetFactory(timeSheetResponse);
        })
      );
  }

  getFreelancersFilterList(): Observable<TimeSheetFreelancer[]> {
    return this.httpClient.get<ITimeSheetFreelancer[]>(`/api/timesheets/freelancers`)
      .pipe(
        map((freelancersBackend: ITimeSheetFreelancer[]) => {
          return freelancersBackend.map(freelancer => new TimeSheetFreelancer(freelancer));
        })
      );
  }

  approve(timesheetEntryIds: string[]) {
    return this.httpClient.post<any>(`/api/timesheets/approve`, {timesheetEntryIds});
  }

  reject(timesheetEntryIds: string[], customerComment: string) {
    console.log('REJECTING');
    console.log(customerComment);
    return this.httpClient.post<any>(`/api/timesheets/reject`, {timesheetEntryIds, rejectMessage: customerComment});
  }
}
