import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {IFollowUpActionsCount} from "./home.model";

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(private httpClient: HttpClient) {
  }

  getDashboardSummary(): Observable<IFollowUpActionsCount> {
    return this.httpClient.get<IFollowUpActionsCount>(`/api/dashboard/follow-up-actions`);
  }
}
