import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {AccountService} from '../../core/auth/account.service';
import {LoginService} from '../../core/login/login.service';
import {Account} from '../../core/user/account.model';
import {forkJoin, Observable, of} from 'rxjs';
import {DashboardActionItem, IFollowUpActionsCount} from './home.model';
import {HomeService} from './home.service';
import {catchError, map, shareReplay, startWith} from 'rxjs/operators';
import {TranslateService} from '@ngx-translate/core';
import {ProjectService} from '../project/project.service';
import {Project} from '../project/project.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomeComponent implements OnInit {

  account: Account | null = null;

  actionItems$: Observable<DashboardActionItem[]>;
  brokersfee: number;
  actionItemsLoadingFailed$: Observable<boolean>;
  projects$: Observable<Project[]>;
  allLoaded$: Observable<boolean>;

  constructor(private accountService: AccountService, private loginService: LoginService, private homeService: HomeService,
              private projectService: ProjectService, private translate: TranslateService) {
  }

  ngOnInit(): void {
    this.accountService.identity().subscribe(account => (this.account = account));

    const actionItemsRequest$ = this.homeService.getDashboardSummary()
      .pipe(
        catchError((error) => {
          return of(null);
        }),
        shareReplay()
      );

    this.actionItems$ = actionItemsRequest$
      .pipe(
        map((result) => {
          return this.makeActionItems(result);
        })
      );

    this.actionItemsLoadingFailed$ = actionItemsRequest$
      .pipe(map((results) => results === null));

    this.projects$ = this.projectService.getDashboardProjects()
      .pipe(
        catchError((error) => {
          return of(null);
        }),
        shareReplay()
      );

    this.allLoaded$ = forkJoin([this.actionItems$, this.projects$])
      .pipe(
        map((results) => {
          return true;
        }),
        startWith(false)
      );
  }

  isAuthenticated(): boolean {
    return this.accountService.isAuthenticated();
  }

  login(): void {
    this.loginService.login();
  }

  getActionsTotal(actionItems: DashboardActionItem[]): number {
    return actionItems
      .map(actionItem => actionItem.count)
      .reduce((acc, curr) => acc + curr, 0);
  }

  private makeActionItems(result: IFollowUpActionsCount): DashboardActionItem[] {
    this.brokersfee = result.subscriptionTypeAccount;
    return [
      {
        label: this.translate.instant('home.actionsCard.freelancers'),
        count: result ? result.rateFreelancersCount : null,
        routerLink: '/freelancers/matches'
      },
      {
        label: this.translate.instant('home.actionsCard.projects'),
        count: result ? result.projectsInDraftCount : null,
        routerLink: '/projects'
      },
      {
        label: this.translate.instant('home.actionsCard.timesheets'),
        count: result ? result.submittedTimeSheetsCount : null,
        routerLink: '/administration/timesheets'
      }
    ];
  }

}
