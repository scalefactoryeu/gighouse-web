export class DashboardActionItem {
  label: string;
  count: number;
  routerLink: string;
}

export interface IFollowUpActionsCount {
  rateFreelancersCount: number;
  projectsInDraftCount: number;
  submittedTimeSheetsCount: number;
  subscriptionTypeAccount: number;
}
