import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';

import { HomeComponent } from './home.component';
import { SharedModule } from '../../shared/shared.module';
import { LayoutsModule } from '../../layouts/layouts.module';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    SharedModule,
    LayoutsModule,
    RouterModule
  ],
  declarations: [HomeComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HomeModule {}
