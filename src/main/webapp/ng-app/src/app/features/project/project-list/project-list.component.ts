import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Project } from '../project.model';
import { ProjectService } from '../project.service';
import { BehaviorSubject, merge, Observable, of, Subject } from 'rxjs';
import {catchError, concatMap, map, mapTo, scan, shareReplay, takeUntil} from 'rxjs/operators';
import { PaginatedData } from '../../../core/models/model';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProjectListComponent implements OnInit, OnDestroy {

  isActiveProjectsPage$: BehaviorSubject<boolean> = new BehaviorSubject(false);
  projects$: Observable<Project[]>;
  projectsTotalCount$: Observable<number>;
  loading$: Observable<boolean>;
  loadingFailed$: Observable<boolean>;

  private page$ = new BehaviorSubject<number>(0);
  // Keep page size relatively big because the UX is better when user immediately sees a scrollbar.
  // With a smaller number there is no scrollbar and we have to show a "Load more" button.
  private readonly pageSize = 20;
  private retryLoad$ = new Subject();
  private destroy$ = new Subject();

  constructor(private activatedRoute: ActivatedRoute, private projectService: ProjectService) {}

  ngOnInit(): void {
    this.activatedRoute.data
      .subscribe(data => {
        this.isActiveProjectsPage$.next(!!data.isActiveProjectsPage);
        this.startLoadingProjects();
      });
  }

  private startLoadingProjects() {
    let getProjects: (page: number, pageSize: number) => Observable<Project[]>;

    if (this.isActiveProjectsPage()) {
      getProjects = this.projectService.getActiveProjects.bind(this.projectService);
    } else {
      getProjects = this.projectService.getClosedProjects.bind(this.projectService);
    }

    let lastLoadedPage: number;

    // load projects when page changes during infinite scroll and when retry button is clicked
    const loadProjects$ = merge(this.page$, this.retryLoad$);

    const newPaginatedProjects$: Observable<PaginatedData<Project>> = loadProjects$
      .pipe(
        concatMap(() => {
          return getProjects(this.page$.value, this.pageSize)
            .pipe(
              catchError(() => {
                // return null instead of throwing the error to keep the observable stream alive
                return of(null);
              })
            );
        }),
        takeUntil(this.destroy$),
        shareReplay()
      );

    this.projects$ = newPaginatedProjects$
      .pipe(
        scan((accumulatedProjects, currentData) => {
          if (currentData) {
            if (currentData.page <= lastLoadedPage) {
              // If currently loaded page is smaller than the last loaded page it means we are refreshing the data starting from a lower
              // page, meaning that we need to remove all the  existing items starting from the current page and then add newly
              // loaded items. This case happens when an item is removed from the list so we want to refresh data starting from the page
              // of that item.
              const firstItemIndexOnPage = currentData.page * this.pageSize;
              accumulatedProjects.splice(firstItemIndexOnPage);
            }

            // Add newly loaded projects to projects array.
            accumulatedProjects = accumulatedProjects.concat(currentData.items);
            lastLoadedPage = currentData.page;
          }
          return accumulatedProjects;
        }, [])
      );

    this.projectsTotalCount$ = newPaginatedProjects$
      .pipe(
        map((paginatedProjects) => paginatedProjects ? paginatedProjects.totalCount : null)
      );

    const loadingCount$ = merge(
      loadProjects$.pipe(mapTo(+1)),
      newPaginatedProjects$.pipe(mapTo(-1)),
    )
      .pipe(
        scan((accumulated, currentValue) => {
          return accumulated + currentValue;
        }, 0)
      );

    this.loading$ = loadingCount$
      .pipe(
        map(loadingCount => loadingCount > 0)
      );

    this.loadingFailed$ = merge(
      loadProjects$.pipe(mapTo(false)),
      newPaginatedProjects$.pipe(map((newPaginatedProjects) => !newPaginatedProjects)) // loading is failed when newProjects=null
    );
  }

  private isActiveProjectsPage(): boolean {
    return this.isActiveProjectsPage$.getValue();
  }

  loadMoreProjects() {
    const nextPage = this.page$.value + 1;
    this.page$.next(nextPage);
  }

  retryLoadProjects() {
    this.retryLoad$.next();
  }

  ngOnDestroy() {
    this.destroy$.next();
  }

}
