import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ProjectFormLanguage} from '../project-form.model';
import {ProjectFormService} from "../project-form.service";
import {Language} from "../../../../core/models/language";


@Component({
  selector: 'app-project-form-languages',
  templateUrl: './project-form-languages.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProjectFormLanguagesComponent implements OnInit {

  @Input() projectLanguages: ProjectFormLanguage[];
  languages: Language[];

  @Input() emptyText: string;
  @Input() emptyButtonText: string;
  @Input() emptyIcon: string;

  @Output() triggerRemove = new EventEmitter<ProjectFormLanguage>();
  @Output() openLanguagesPanel = new EventEmitter();

  constructor(public projectFormService: ProjectFormService) {

  }

  ngOnInit(): void {

  }

  isEmpty(): boolean {
    var languages = this.projectLanguages.filter(l => l.level !== 'none');
    return !(languages.length);
  }
}
