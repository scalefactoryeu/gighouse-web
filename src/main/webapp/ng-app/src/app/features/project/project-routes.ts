import { ProjectListComponent } from './project-list/project-list.component';
import { ProjectFormComponent } from './project-form/project-form.component';
import { ProjectDetailsComponent } from './project-details/project-details.component';
import { Routes } from '@angular/router';
import { ProjectFormMode } from './project.model';
import { CanDeactivateProjectFormGuard } from '../../core/guards/can-deactivate-project-form-guard.service';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'active'
  },
  {
    path: 'active',
    component: ProjectListComponent,
    data: {
      authorities: [],
      pageTitle: 'projects.activeProjectsTitle',
      isActiveProjectsPage: true
    }
  },
  {
    path: 'archive',
    component: ProjectListComponent,
    data: {
      authorities: [],
      pageTitle: 'projects.archivedProjectsTitle'
    }
  },
  {
    path: 'new',
    component: ProjectFormComponent,
    canDeactivate: [CanDeactivateProjectFormGuard],
    data: {
      authorities: [],
      pageTitle: 'projects.newProjectTitle',
      projectFormMode: ProjectFormMode.CREATE
    }
  },
  {
    path: 'edit/:projectId',
    component: ProjectFormComponent,
    canDeactivate: [CanDeactivateProjectFormGuard],
    data: {
      authorities: [],
      projectFormMode: ProjectFormMode.EDIT
    }
  },
  {
    path: 'copy/:projectId',
    component: ProjectFormComponent,
    canDeactivate: [CanDeactivateProjectFormGuard],
    data: {
      authorities: [],
      projectFormMode: ProjectFormMode.COPY
    }
  },
  {
    path: 'details/:projectId',
    component: ProjectDetailsComponent,
    data: {
      authorities: [],
      pageTitle: 'projects.projectDetailsTitle'
    }
  }
];
