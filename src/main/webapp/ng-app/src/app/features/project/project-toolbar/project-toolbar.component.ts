import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ProjectDetails, ProjectStatus } from '../project.model';

@Component({
  selector: 'app-project-toolbar',
  templateUrl: './project-toolbar.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProjectToolbarComponent implements OnInit {

  @Input() isReadOnlyProject: boolean;
  @Input() project: ProjectDetails;
  @Input() isSaving: boolean;

  @Output() archiveProject = new EventEmitter<void>();
  @Output() removeProject = new EventEmitter<void>();
  @Output() saveProject = new EventEmitter<void>();
  @Output() saveAsDraft = new EventEmitter<void>();

  constructor() {

  }

  ngOnInit(): void {

  }

  getOverviewRoute() {
    if (this.project && this.project.status === ProjectStatus.CANCELLED) {
      return 'archive';
    }

    return 'active';
  }

  saveAsConceptEnabled(): boolean {
    return !this.project || this.project.status === ProjectStatus.DRAFT;
  }

  isArchivedProject(): boolean {
    return !this.project || this.project.status === ProjectStatus.CANCELLED;
  }

}
