import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-selected-options',
  templateUrl: './selected-options.component.html',
  styleUrls: ['./selected-options.component.scss'],
})
export class SelectedOptionsComponent {
  @Input() selectedOptions: string[];
  @Input() editable: boolean;
  @Input() loading: boolean;
  @Input() maxCount: string;
  @Input() title: string;

  @Output() deleteOption = new EventEmitter<string>();

  handleDeleteOption(option: string) {
    this.deleteOption.emit(option);
  }

  get options() {
    return this.selectedOptions?.length > 0 ? this.selectedOptions : undefined;
  }
}
