import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ProjectFormSkill} from '../project-form.model';
import {ProjectFormService} from "../project-form.service";

@Component({
  selector: 'app-project-form-skills',
  templateUrl: './project-form-skills.component.html'
})
export class ProjectFormSkillsComponent implements OnInit {

  @Input() selectedSkills: string[];
  @Input() maxCount: number = 25;
  @Input() emptyText: string;
  @Input() emptyButtonText: string;
  @Input() emptyIcon: string;

  @Output() triggerRemove = new EventEmitter<ProjectFormSkill>();
  @Output() openSkillsPanel = new EventEmitter();

  constructor(public projectFormService: ProjectFormService) {

  }

  ngOnInit(): void {

  }

  isEmpty(): boolean {
    return !(this.selectedSkills.length);
  }

  handleDeleteOption(option: string) {
  }
}
