// import { IProjectDetailsBackend, ProjectStatus } from '../project.model';
// import { SkillType } from '../../../core/models/skill.model';

// export const projectDetailsMock: IProjectDetailsBackend = {
//   id: '#002',
//   sfid: '#002',
//   name: 'GH/BE/0082/20 - Social Media Expert',
//   startDate: '2020-03-02',
//   endDate: '2020-04-02',
//   sentDate: '2020-01-02',
//   closedDate: null,
//   status: ProjectStatus.FEEDBACK,
//   statusMessage: 'We selected suitable freelancers for your project',
//   budget: 550,

//   description: `Gingerbread cake lollipop. Marzipan croissant tart. Candy canes tiramisu chocolate cake powder tiramisu. Chocolate pastry soufflé. Sugar plum caramels carrot cake muffin lemon drops. Cotton candy gummi bears bear claw croissant. Halvah chocolate cake liquorice candy canes. Pie powder toffee caramels. Donut candy fruitcake powder. Dessert cake muffin cupcake macaroon candy canes toffee pie cheesecake. Caramels pie chocolate soufflé. Oat cake muffin caramels gummies halvah chocolate cake liquorice sweet roll. Biscuit cookie soufflé tiramisu oat cake sugar plum candy canes jujubes. Macaroon bonbon apple pie wafer.

// Cookie ice cream powder halvah. Cotton candy oat cake chupa chups marzipan jelly beans. Jelly-o apple pie gingerbread candy sweet jelly oat cake brownie. Sesame snaps dessert toffee caramels. Oat cake cake bonbon tootsie roll chocolate bar cupcake bear claw. Liquorice liquorice halvah marshmallow jelly ice cream carrot cake gingerbread chupa chups. Toffee gummi bears sugar plum soufflé icing. Cake topping fruitcake tart donut. Marzipan gingerbread lollipop chupa chups lemon drops. Carrot cake bonbon gummi bears muffin sesame snaps danish chocolate cake. Chocolate lollipop dragée jelly-o. Danish gummies dragée brownie cupcake caramels. Donut icing sesame snaps pie wafer topping bear claw donut carrot cake.`,

//   tags: ['Homework perfectly possible', 'Social media guru', 'Collaboration on demand'],
//   durationWeeks: 10,
//   requiredDaysPerWeek: 2,
//   dayFee: 600,
//   location: {
//     name: 'West-Vlaanderen (8890)',
//     postcode: 8890
//   },
//   workFromHomePossible: true,
//   needToHaveSkills: [
//     // Id and name come from the /skills backend
//     {
//       id: 'a0P9E000005DNPbUAO',
//       name: 'Information Security',
//       level: 3,
//       type: SkillType.SPECIALIZATION
//     },
//     {
//       id: 'a0P9E000005DNPdUAO',
//       name: 'Cloud system management',
//       level: 3,
//       type: SkillType.SPECIALIZATION
//     },
//     {
//       id: 'a0P9E000005DNPfUAO',
//       name: 'Data mining',
//       level: 4,
//       type: SkillType.SPECIALIZATION
//     },
//     {
//       id: 'a0P9E000005DNPhUAO',
//       name: 'HTML5',
//       level: 2,
//       type: SkillType.SPECIALIZATION
//     },
//     {
//       id: 'a0P9E000005DNPiUAO',
//       name: 'CSS',
//       level: 1,
//       type: SkillType.SPECIALIZATION
//     }
//   ],
//   niceToHaveSkills: [
//     // Id and name come from the /skills backend
//     {
//       id: 'a0P9E000005DNQQUA4',
//       name: 'PostgreSQL',
//       level: 1,
//       type: SkillType.SPECIALIZATION
//     },
//     {
//       id: 'a0P9E000005DNQPUA4',
//       name: 'MongoDB',
//       level: 1,
//       type: SkillType.SPECIALIZATION
//     }
//   ],
//   needToHaveTools: [
//     // Id and name come from the /skills backend
//     {
//       id: 'a0P9E000005DNQcUAO',
//       name: 'Wordpress',
//       level: 4,
//       type: SkillType.TOOLS
//     },
//     {
//       id: 'a0P9E000005DNQUUA4',
//       name: 'Nginx',
//       level: 3,
//       type: SkillType.TOOLS
//     }
//   ],
//   niceToHaveTools: [
//     // Id and name come from the /skills backend
//     {
//       id: 'a0P9E000005DNQdUAO',
//       name: 'Wix',
//       level: 1,
//       type: SkillType.TOOLS
//     }
//   ]
// };
