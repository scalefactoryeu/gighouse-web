import {Injectable} from '@angular/core';
import {HttpClient, HttpParams, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {delay, map} from 'rxjs/operators';
import {IProjectBackend, IProjectDetailsBackend, Project, ProjectDetails, ProjectSaveRequestData} from './project.model';
//import { projectDetailsMock } from './mocks/project-detail-mocked';
import {PaginatedData} from '../../core/models/model';
import {createRequestOption, Pagination} from '../../shared/util/request-util';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  constructor(private httpClient: HttpClient) {
  }

  getActiveProjects(page: number, pageSize: number): Observable<PaginatedData<Project>> {
    let params = new HttpParams();
    params = params.append('archived', "false");
    params = params.append('page', page.toString());
    params = params.append('size', pageSize.toString());

    // const mockedProjects = mockedActiveProjects.slice(page * pageSize, (page * pageSize) + pageSize);
    // const mockedResponse = new HttpResponse({
    //   body: mockedProjects,
    //   headers: new HttpHeaders({'X-Total-Count': mockedActiveProjects.length.toString()})
    // });

    // return of([])
    //return of(mockedResponse)
    return this.httpClient.get<IProjectBackend[]>(`/api/projects/`, { observe: 'response', params })
      .pipe(
        //delay(500), // ToDo: Remove delay when we start loading real backend data
        map((res: HttpResponse<IProjectBackend[]>) => {
          const projects = res.body.map(project => new Project(project));
          return new PaginatedData<Project>(res, projects, page);
          // throw new Error('error');
        })
      );
  }

  getClosedProjects(page: number, pageSize: number): Observable<PaginatedData<Project>> {
    let params = new HttpParams();
    params = params.append('archived', "true");
    params = params.append('page', page.toString());
    params = params.append('size', pageSize.toString());

    // const mockedProjects = mockedClosedProjects.slice(page * pageSize, (page * pageSize) + pageSize);
    // const mockedResponse = new HttpResponse({
    //   body: mockedProjects,
    //   headers: new HttpHeaders({ 'X-Total-Count': mockedClosedProjects.length.toString() })
    // });

    // return of([])
    //return of(mockedResponse)
    return this.httpClient.get<IProjectBackend[]>(`/api/projects/`, { observe: 'response', params })
      .pipe(
        //delay(2000), // ToDo: Remove delay when we start loading real backend data
        map((res: HttpResponse<IProjectBackend[]>) => {
          const projects = res.body.map(project => new Project(project));
          return new PaginatedData<Project>(res, projects, page);
          // throw new Error('error');
        })
        // ToDo: Remove this logic to force throwing the error on second page
        // map((res: HttpResponse<IProjectBackend[]>) => {
        //   if (page === 2) {
        //     throw new Error('error');
        //   } else {
        //     const projects = res.body.map(project => new Project(project));
        //     return new PaginatedData<Project>(res, projects);
        //   }
        // })
      );
  }

  getProjectDetails(projectId: string): Observable<ProjectDetails> {
    //return of(projectDetailsMock)
    return this.httpClient.get<IProjectDetailsBackend>(`/api/projects/${projectId}`)
      .pipe(
        // delay(2000),
        map((projectDetailsBackend: IProjectDetailsBackend) => {
          return new ProjectDetails(projectDetailsBackend);
          // throw new Error();
        })
      );
  }

  getMatchesProjects(): Observable<Project[]> {
    return this.httpClient.get<IProjectBackend[]>(`/api/freelancers/matches/projects`)
      .pipe(
        map((projectsBackend: IProjectBackend[]) => {
          return projectsBackend.map(project => new Project(project));
        })
      );
  }

  getDashboardProjects(): Observable<Project[]> {
    // return of([]);
    //return of(mockedActiveProjects)
    return this.httpClient.get<IProjectBackend[]>(`/api/projects/`)
      .pipe(
        // delay(2000),
        map((projectsBackend: IProjectBackend[]) => {
          return projectsBackend.map(project => new Project(project));
          // throw new Error('error');
        })
      );
  }

  getChatProjects(): Observable<Project[]> {
    // Make page size large for now to fetch all items at once. ToDo: Discuss if this should be paginated (infinite-scrolling)
    const req: Pagination = { page: 0, size: 1000000, sort: null };
    const params: HttpParams = createRequestOption(req);

    // return of([])
    // return of(mockedActiveProjects)
    //return this.httpClient.get<IProjectBackend[]>(`/api/projects/`, { params })
    return this.httpClient.get<IProjectBackend[]>(`/api/chat/projects`, { params })
      .pipe(
        delay(1),
        map((projectsBackend: IProjectBackend[]) => {
          return projectsBackend
            // ToDo: Filter can be removed in the future. Now it's here because backend returns some projects as null.
            .filter(project => !!project)
            .map(project => new Project(project));
          // return [];
          // throw new Error('error');
        })
      );
  }

  createProjectAndSend(projectRequestBody: ProjectSaveRequestData): Observable<ProjectDetails> {
    return this.httpClient.post<IProjectDetailsBackend>(`/api/projects/`, projectRequestBody)
      .pipe(
        // delay(2000),
        map((projectDetailsBackend: IProjectDetailsBackend) => {
          return new ProjectDetails(projectDetailsBackend);
          // throw new Error();
        })
      );
  }

  updateProjectAndSend(projectId: string, projectRequestBody: ProjectSaveRequestData): Observable<ProjectDetails> {
    return this.httpClient.put<IProjectDetailsBackend>(`/api/projects/${projectId}`, projectRequestBody)
      .pipe(
        // delay(2000),
        map((projectDetailsBackend: IProjectDetailsBackend) => {
          return new ProjectDetails(projectDetailsBackend);
          // throw new Error();
        })
      );
  }

  createProjectConcept(projectRequestBody: ProjectSaveRequestData): Observable<ProjectDetails> {
    return this.httpClient.post<IProjectDetailsBackend>(`/api/projects/`, projectRequestBody)
      .pipe(
        // delay(2000),
        map((projectDetailsBackend: IProjectDetailsBackend) => {
          return new ProjectDetails(projectDetailsBackend);
          // throw new Error();
        })
      );
  }

  updateProjectConcept(projectId: string, projectRequestBody: ProjectSaveRequestData): Observable<ProjectDetails> {
    return this.httpClient.put<IProjectDetailsBackend>(`/api/projects/${projectId}`, projectRequestBody)
      .pipe(
        map((projectDetailsBackend: IProjectDetailsBackend) => {
          return new ProjectDetails(projectDetailsBackend);
          // throw new Error();
        })
      );
  }

  removeProject(projectId: string): Observable<void> {
    return this.httpClient.delete<void>(`/api/projects/${projectId}`);
  }

  archiveProject(projectId: string): Observable<any> {
    return this.httpClient.post<void>(`/api/projects/${projectId}/archive`, {});
  }

}
