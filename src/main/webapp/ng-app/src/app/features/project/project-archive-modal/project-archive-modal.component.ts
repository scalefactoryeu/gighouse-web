import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { BehaviorSubject } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ProjectService } from '../project.service';
import { ProjectDetails } from '../project.model';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-modal-project-archive',
  templateUrl: './project-archive-modal.component.html'
})
export class ProjectArchiveModalComponent implements OnInit {

  project: ProjectDetails;

  actionRunning$: BehaviorSubject<boolean> = new BehaviorSubject(false);
  failedText$ = new BehaviorSubject<string>(null);

  constructor(public activeModal: NgbActiveModal, private projectService: ProjectService, private translate: TranslateService) {
  }

  ngOnInit() {
  }

  confirm() {
    this.actionRunning$.next(true);

    this.projectService.archiveProject(this.project.id)
      .pipe(
        catchError((error) => {
          this.actionRunning$.next(false);
          this.failedText$.next(this.translate.instant('projects.projectArchiveModal.archiveFailed'));
          throw error;
        })
      )
      .subscribe(() => {
        this.activeModal.close();
      });
  }

}
