import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SkillPanelMode} from '../project-form.model';
import {ProjectFormService} from '../project-form.service';
import {TranslateService} from '@ngx-translate/core';
import {BehaviorSubject} from "rxjs";
import {SmartTaggingRepository} from "../../smart-tagging.repository";

@Component({
  selector: 'app-project-skills-panel',
  templateUrl: './project-skills-panel.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProjectSkillsPanelComponent implements OnInit {

  @Input() mode: SkillPanelMode;

  @Output() triggerClose = new EventEmitter<void>();
  @Input() labels: string;
  @Input() maxCount: string = "25";
  @Input() options: string[];
  @Input() searchOptions: string[];
  @Input() optionsLoading: boolean;
  @Input() canAddOptions = true;
  @Input() currentJobTitle: string;
  @Input() cannotAddOptionsRedirectLocation: string;

  @Output() deleteOption = new EventEmitter<string>();

  private typesAutoComplete: string[] = ["PROPERTY"];
  private typeAutoSuggestions: string = "HARD_SKILL";

  autocompleteSuggestions$: BehaviorSubject<string[]> = this.smartTaggingRepository.autocompleteSuggestions$;
  autocompleteSuggestionsLoading$: BehaviorSubject<boolean> =
    this.smartTaggingRepository.autocompleteSuggestionsLoading$;

  suggestions$: BehaviorSubject<string[]> = this.smartTaggingRepository.suggestions$;
  suggestionsLoading$: BehaviorSubject<boolean> = this.smartTaggingRepository.suggestionsLoading$;


  constructor(private projectFormService: ProjectFormService,
              private smartTaggingRepository: SmartTaggingRepository,
              private translate: TranslateService) {
    this.searchOptions = this.searchOptions != null ? this.searchOptions : [];

  }

  ngOnInit(): void {
    this.projectFormService.formSkills$.subscribe(skills => {
      this.options = skills;
    });
    this.searchOptions = [...this.options, this.projectFormService.getExpertiseLabel(), this.projectFormService.getJobTitle()]
    this.smartTaggingRepository.getSuggestions([...this.searchOptions], this.typeAutoSuggestions);
  }

  isSkillsMode(): boolean {
    return this.mode === SkillPanelMode.SKILLS;
  }

  getApplyButtonText(): string {
    const selectedCount = this.options != null ? this.options.length : 0;

    if (selectedCount === 0) {
      return this.translate.instant('Apply');
    } else {
      if (selectedCount === 1) {
        return 'Add ' + selectedCount + ' skill';
      } else {
        return 'Add ' + selectedCount + ' skills';
      }
    }
  }


  apply() {
    // Reset the checkbox of the parent skill to be clean for the next time the panel is opened
    this.projectFormService.formSkills$.next(this.options);
    this.triggerClose.next();
  }

  get maxReached() {
    return this.options?.length >= +this.maxCount;
  }

  handleAutocomplete(searchString: string) {
    this.smartTaggingRepository.getAutocomplete(searchString, this.typesAutoComplete);
  }

  handleAutocompleteSearch(searchValue: string) {
    const keywords = [...this.searchOptions, searchValue];
    this.handleCloseAutocompleteSuggestions();
    this.handleSelectOption(searchValue);
    this.smartTaggingRepository.getSuggestions(keywords, this.typeAutoSuggestions);
  }

  handleSelectOption(selectedOption: string) {
    if (!this.maxReached && !this.options.map(op => op.toLowerCase()).includes(selectedOption.toLowerCase())) {
      if (!this.options.filter((option) => option.toLowerCase() === selectedOption.toLowerCase())) {
        this.smartTaggingRepository.getSuggestions([selectedOption, ...this.searchOptions], this.typeAutoSuggestions);
          this.options.push(selectedOption);
      } else {

        this.options.push(selectedOption);
        let uniqueSet = [...new Set([...this.searchOptions, ...this.options])];
        this.smartTaggingRepository.getSuggestions(uniqueSet, this.typeAutoSuggestions);
      }
    }
  }

  handleDeleteOption(option: string) {
    this.options = this.options.filter((o) => o !== option);
    this.searchOptions = this.searchOptions.filter((o) => o !== option);
    console.log(this.searchOptions);
    console.log(this.options);
    let uniqueSet = [...new Set([...this.searchOptions, ...this.options])];
    this.smartTaggingRepository.getSuggestions(uniqueSet, this.typeAutoSuggestions);
  }

  handleCloseAutocompleteSuggestions() {
    this.smartTaggingRepository.resetAutocompleteSuggestions();
  }

  handleClearSuggestions() {
    this.smartTaggingRepository.resetSuggestions();
  }

}
