import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';

import {SharedModule} from '../../shared/shared.module';
import {LayoutsModule} from '../../layouts/layouts.module';
import {ProjectDetailsComponent} from './project-details/project-details.component';
import {ProjectFormComponent} from './project-form/project-form.component';
import {ProjectListComponent} from './project-list/project-list.component';
import {RouterModule} from '@angular/router';
import {routes} from './project-routes';
import {ProjectTableComponent} from './project-list/project-table/project-table.component';
import {ProjectToolbarComponent} from './project-toolbar/project-toolbar.component';
import {ProjectStepsComponent} from './project-steps/project-steps.component';
import {ProjectStepComponent} from './project-steps/step.component';
import {SmartphonePreviewComponent} from './smartphone-preview/smartphone-preview.component';
import {ProjectDetailsSidebarComponent} from './project-details/project-details-sidebar/project-details-sidebar.component';
import {ProjectDetailsSummaryComponent} from './project-details/project-details-summary/project-details-summary.component';
import {ProjectDetailsSkillsComponent} from './project-details/project-details-skills/project-details-skills.component';
import {ProjectFormSummaryComponent} from './project-form/project-form-summary/project-form-summary.component';
import {ProjectFormSidebarComponent} from './project-form/project-form-sidebar/project-form-sidebar.component';
import {ProjectFormSkillsComponent} from './project-form/project-form-skills/project-form-skills.component';
import {ProjectSkillsPanelComponent} from './project-form/project-skills-panel/project-skills-panel.component';
import {ProjectArchiveModalComponent} from './project-archive-modal/project-archive-modal.component';
import {ProjectSaveSuccessModalComponent} from './project-save-success-modal/project-save-success-modal.component';
import {FreelancerSharedModule} from '../freelancer/freelancer-shared/freelancer-shared.module';
import {ProjectFormLanguagesComponent} from "./project-form/project-form-languages/project-form-languages.component";
import {ProjectLanguagesPanelComponent} from "./project-form/project-languages-panel/project-languages-panel.component";
import {ProjectDetailsLanguagesComponent} from "./project-details/project-details-languages/project-details-languages.component";
import {QuillModule} from "ngx-quill";
import {ProjectFormSuggestionsComponent} from "./project-form/project-form-suggestions/project-form-suggestions.component";
import {AutoCompleteSearchComponent} from "./project-form/autocomplete-search/autocomplete-search.component";
import {SelectorSuggestionsComponent} from "./project-form/suggestions/suggestions.component";
import {SelectedOptionsComponent} from "./project-form/selected-options/selected-options.component";

@NgModule({
  imports: [
    SharedModule,
    LayoutsModule,
    FreelancerSharedModule,
    RouterModule.forChild(routes),
    QuillModule.forRoot()
  ],
  declarations: [
    ProjectDetailsComponent,
    ProjectFormComponent,
    ProjectListComponent,
    ProjectTableComponent,
    ProjectToolbarComponent,
    ProjectStepsComponent,
    ProjectStepComponent,
    SmartphonePreviewComponent,
    ProjectDetailsSummaryComponent,
    ProjectDetailsSidebarComponent,
    ProjectDetailsSkillsComponent,
    ProjectDetailsLanguagesComponent,
    ProjectFormSummaryComponent,
    ProjectFormSidebarComponent,
    ProjectFormSkillsComponent,
    ProjectFormSuggestionsComponent,
    AutoCompleteSearchComponent,
    SelectorSuggestionsComponent,
    SelectedOptionsComponent,
    ProjectFormLanguagesComponent,
    ProjectSkillsPanelComponent,
    ProjectLanguagesPanelComponent,
    ProjectArchiveModalComponent,
    ProjectSaveSuccessModalComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GigProjectModule {
}
