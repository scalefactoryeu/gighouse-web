import {Skill, SkillType} from '../../../core/models/skill.model';
import {Language} from "../../../core/models/language";
import {LanguageLevel} from "../../../core/models/language-level";

export enum SkillPanelMode {
  SKILLS = 'SKILLS',
  TOOLS = 'TOOLS'
}

export interface SelectedSkillsResult {
  needToHave: ProjectFormSkill[];
  niceToHave: ProjectFormSkill[];
}
export interface SelectedLanguagesResult {
  languages: ProjectFormLanguage[];
}

export class ProjectFormLanguage {
  id: string;
  language: string;
  level: string;

  constructor(id: string, language: string, level: string) {
    this.id = id;
    this.language = language;
    this.level = level;
  }
}

export class ProjectFormSkill {
  id: string;
  name: string;
  type: SkillType;
  children: ProjectFormSkill[];

  isSelected: boolean;
  isNeedToHave: boolean;
  level: number;

  constructor(skill: Skill) {
    this.id = skill.id;
    this.name = skill.name;
    this.type = skill.type;
    this.children = [];
  }

  select() {
    this.isSelected = true;
  }

  unselect() {
    this.isSelected = false;
    this.isNeedToHave = false; // reset to default niceToHave
    this.level = 0; // reset to no stars
  }

}

export function createProjectFormSkillsStructure(expertiseId: string, skills: Skill[], tools: boolean): ProjectFormSkill[] {
  let projectFormSkills: ProjectFormSkill[] = [];

  const type = tools ? SkillType.TOOLS : SkillType.SPECIALIZATION;

  let firstLevel = skills.filter(_skill => expertiseId.includes(_skill.parent));
  firstLevel = firstLevel.filter(_skill => _skill.type === type)

  firstLevel.forEach(_skill => {
    const projectFormSkill = new ProjectFormSkill(_skill);
    projectFormSkills.push(projectFormSkill);
  })

  projectFormSkills.forEach(_skill => {
    let children = skills.filter(_child => _child.parent === _skill.id);
    children = children.filter(_child => _child.type === type);

    if (children.length > 0) {
      children.forEach(_child => {
        const projectFormSkill = new ProjectFormSkill(_child);
        _skill.children.push(projectFormSkill);
      })

      _skill.children = _skill.children.sort((a, b) => a.name.localeCompare(b.name))
    } else {
      let matchingSkill = skills.find(_s => _s.id === _skill.id)
      _skill.children.push(new ProjectFormSkill(matchingSkill));
    }
  })

  projectFormSkills = projectFormSkills.sort((a, b) => a.name.localeCompare(b.name))

  return projectFormSkills;
}

export function createProjectFormLanguagesStructure(languages: Language[]): ProjectFormLanguage[] {
  let projectFormLanguages: ProjectFormLanguage[] = [];
  languages.forEach(_language =>{
    projectFormLanguages.push(new ProjectFormLanguage(null, _language.label, 'none'));
  });
  return projectFormLanguages;
}


export enum CanDeactivateProjectFormReason {
  NOT_SAVED = 'NOT_SAVED', // when new project is not saved
  UNSAVED_CHANGES = 'UNSAVED_CHANGES' // when project being updated is not saved
}
