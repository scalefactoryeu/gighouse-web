import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { SkillRequirement } from 'src/app/core/models/skill.model';
import { ProjectFormSkill } from '../project-form/project-form.model';

@Component({
  selector: 'app-project-smartphone-preview',
  templateUrl: './smartphone-preview.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SmartphonePreviewComponent implements OnInit {

  @Input() projectName: string;
  @Input() projectDescription: string;
  @Input() projectBudget: string;
  @Input() projectStartDate: string;
  @Input() projectDaysPerWeek: string;
  @Input() projectDurationWeeks: string;
  @Input() projectState: string;
  @Input() projectPostcode: string;
  @Input() projectCity: string;
  @Input() projectTeasers: string[];

  //this comes from project-form
  @Input() projectSkills: ProjectFormSkill[];
  @Input() projectTools: ProjectFormSkill[];

  //this comes from project-details
  @Input() projectPositiveSkills: string[];
  // @Input() projectNiceToHaveTools: IProjectSkillBackend[];

  showFullDescription = false;
  descriptionLengthBreakpoint = 310;

  perfectScrollbarConfig = {
    suppressScrollX: true
  };

  constructor(private translate: TranslateService) { }

  ngOnInit(): void { }

  getProjectSkillsAndTools(projectSkillsOrTools: string[]): string {
    const stringArray: string[] = [];

    if (projectSkillsOrTools && projectSkillsOrTools.length > 0) {
      projectSkillsOrTools.forEach(skillOrTool => {
        stringArray.push(skillOrTool);
      })
    }

    if (stringArray && stringArray.length > 0) {
      return stringArray.join(', ');
    }
    else {
      return null;
    }
  }


  // getSkillAndTools(skillsOrTools: ProjectFormSkill[], needToHave: boolean): string {
  //   const stringArray: string[] = [];
  //
  //   if (skillsOrTools && skillsOrTools.length > 0) {
  //     skillsOrTools.forEach(skillOrTool => {
  //       skillOrTool.children.forEach(childSkillOrTool => {
  //         if (childSkillOrTool.isNeedToHave === needToHave) {
  //           stringArray.push(childSkillOrTool.name);
  //         }
  //       })
  //     })
  //
  //     if (stringArray && stringArray.length > 0) {
  //       return stringArray.join(', ');
  //     }
  //     else {
  //       return null;
  //     }
  //   }
  //   else {
  //     return null;
  //   }
  //
  // }

  getName(): string {
    return this.projectName || this.translate.instant('projects.smartphone.namePlaceholder');
  }

  getBudget(): string {
    if (this.projectBudget && this.projectBudget != null) {
      return '€' + this.projectBudget + ' / Day'
    }
    else {
      return this.translate.instant('projects.smartphone.budgetPlaceholder');
    }
  }

  getProjectPositiveSkills() {
    return this.getProjectSkillsAndTools(this.projectPositiveSkills);
  }

  // getProjectNiceToHaveSkills() {
  //   return this.getProjectSkillsAndTools(this.projectNiceToHaveSkills);
  // }
  //
  // getProjectNeedToHaveTools() {
  //   return this.getProjectSkillsAndTools(this.projectNeedToHaveTools);
  // }
  //
  // getProjectNiceToHaveTools() {
  //   return this.getProjectSkillsAndTools(this.projectNiceToHaveTools);
  // }

  // getNeedSkills(): string {
  //   return this.getSkillAndTools(this.projectSkills, true);
  // }
  //
  // getNiceSkills(): string {
  //   return this.getSkillAndTools(this.projectSkills, false);
  // }
  //
  // getNeedTools(): string {
  //   return this.getSkillAndTools(this.projectTools, true);
  //
  // }
  //
  // getNiceTools(): string {
  //   return this.getSkillAndTools(this.projectTools, false);
  // }

  getTeasers(): string {

    var filtered = this.projectTeasers.filter(function (el) {
      return (el != null) && (el != '')
    });

    if (filtered && filtered.length > 0) {
      return filtered.join(", ");
    }
    else {
      return null;
    }

  }

  getLocation(): string {
    return  this.projectCity + ' (' + this.projectPostcode + ') ' + this.projectState ;
  }

  getDaysPerWeek(): string {
    return this.projectDaysPerWeek + ' day(s) in a week' || this.translate.instant('projects.smartphone.daysPerWeekPlaceholder');
  }

  getDurationWeeks(): string {
    return this.projectDurationWeeks + ' week(s)' || this.translate.instant('projects.smartphone.durationWeeksPlaceholder');
  }

  getStartDate(): string {
    if (this.projectStartDate) {
      return 'Start Date: ' + moment(this.projectStartDate).format('DD/MM/YYYY')
    }
    else {
      return this.translate.instant('projects.smartphone.startDatePlaceholder');
    }
  }

  toggleDescription() {
    this.showFullDescription = !this.showFullDescription;
  }

  showDescriptionToggle(): boolean {
    return this.projectDescription.length > this.descriptionLengthBreakpoint;
  }

  getDescription(): string {
    if (this.projectDescription) {
      if (this.showDescriptionToggle() && !this.showFullDescription) {
        return this.projectDescription.slice(0, this.descriptionLengthBreakpoint) + '...';
      }

      return this.projectDescription;
    } else {
      return this.translate.instant('projects.smartphone.descriptionPlaceholder');
    }
  }

}
