import {ChangeDetectionStrategy, Component, Input} from '@angular/core';

@Component({
  selector: 'app-project-details-skills',
  templateUrl: './project-details-skills.component.html',
  styleUrls: ['./project-details-skills.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProjectDetailsSkillsComponent {

  @Input() selectedOptions: string[];
  @Input() loading: boolean;
  @Input() maxCount: string;
  @Input() title: string;


  get options() {
    return this.selectedOptions?.length > 0 ? this.selectedOptions : undefined;
  }

}
