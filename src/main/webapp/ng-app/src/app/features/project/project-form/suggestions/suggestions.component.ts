import { Component, EventEmitter, Input, Output } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-selector-suggestions',
  templateUrl: './suggestions.component.html',
  styleUrls: ['./suggestions.component.scss'],
})
export class SelectorSuggestionsComponent {
  @Input() loading: boolean;
  @Input() suggestions: string[];
  @Input() selectedOptions: string[];
  @Input() disabled: boolean;

  @Output() selectOption = new EventEmitter<string>();


  handleSelect(option) {
    this.selectOption.emit(option);
  }

  checkIfOptionIsSelected(option: string): boolean {
    return !!this.selectedOptions.find((suggestion) => suggestion === option);
  }
}
