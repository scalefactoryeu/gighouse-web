import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

import {AutocompleteParams, AutocompleteResponse, SuggestionsParams, SuggestionsResponse} from "./smarttagging.interface";

const SMART_TAGGING_BASE_URL = '/api/thematchbox';
const SMART_TAGGING_AUTOCOMPLETE_POSTFIX = '/autocomplete';
const SMART_TAGGING_SUGGESTIONS_POSTFIX = '/suggestions';

@Injectable({providedIn: 'root'})
export class SmartTaggingService {
  constructor(private readonly http: HttpClient) {
  }

  public getAutocomplete(text: string, types: string[], lang: string): Observable<AutocompleteResponse[]> {
    const autocompleteParams: AutocompleteParams = {
      contentTypes: types,
      lang,
      text,
    };

    return this.http.post<AutocompleteResponse[]>(
      `${SMART_TAGGING_BASE_URL}${SMART_TAGGING_AUTOCOMPLETE_POSTFIX}`,
      autocompleteParams,
      {
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json',
        },
      }
    );
  }

  public getSuggestions(keywords: string[], type: string, language: string): Observable<SuggestionsResponse[]> {
    const suggestionParams: SuggestionsParams = {
      keywords,
      type,
      language
    };

    return this.http.post<SuggestionsResponse[]>(
      `${SMART_TAGGING_BASE_URL}${SMART_TAGGING_SUGGESTIONS_POSTFIX}`,
      suggestionParams,
      {
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json',
        },
      }
    );
  }
}
