import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {IProjectLanguageBackend} from '../../project.model';

@Component({
  selector: 'app-project-details-languages',
  templateUrl: './project-details-languages.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProjectDetailsLanguagesComponent implements OnInit {

  @Input() projectLanguages: IProjectLanguageBackend[];
  @Input() showAllText: string;
  @Input() showLessText: string;

  showAll = false;

  private defaultNumLanguags = 5;

  constructor() {

  }

  ngOnInit(): void {

  }

  private getAllLanguages(): IProjectLanguageBackend[] {
    return this.projectLanguages;
  }

  showToggle(): boolean {
    return this.getAllLanguages().length > this.defaultNumLanguags;
  }

  toggleShowAll() {
    this.showAll = !this.showAll;
  }

  getLanguages(): IProjectLanguageBackend[] {
    if (this.showAll) {
      return this.getAllLanguages();
    }

    return this.getAllLanguages().slice(0, this.defaultNumLanguags);
  }

}
