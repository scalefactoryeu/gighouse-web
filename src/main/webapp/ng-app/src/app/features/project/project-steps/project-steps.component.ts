import { ChangeDetectionStrategy, Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { ProjectDetails, ProjectStatus, ProjectStatusOrderedList } from '../project.model';

@Component({
  selector: 'app-project-steps',
  templateUrl: './project-steps.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProjectStepsComponent implements OnInit {

  @Input() project: ProjectDetails;

  @Output() jumpToFreelancerMatches = new EventEmitter();

  perfectScrollbarConfig = {
    suppressScrollY: true
  };

  constructor() {

  }

  ngOnInit(): void {

  }

  getProgressBarPercentage(): string {
    const currentStepNum = ProjectStatusOrderedList.indexOf(this.project.status);
    const percentage = (currentStepNum / 6) * 100;
    return percentage + '%';
  }

  isProjectStatusFeedback(): boolean {
    return this.project.status === ProjectStatus.FEEDBACK;
  }

}
