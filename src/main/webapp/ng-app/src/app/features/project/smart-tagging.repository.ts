import {Injectable} from '@angular/core';
import {BehaviorSubject, of, Subject} from 'rxjs';
import {catchError, map, switchMap, take, tap} from 'rxjs/operators';
import {SmartTaggingService} from "./smarttagging.service";

@Injectable({providedIn: 'root'})
export class SmartTaggingRepository {
  public autocompleteSuggestions$ = new BehaviorSubject<string[]>([]);
  public autocompleteSuggestionsLoading$ = new BehaviorSubject<boolean>(false);

  public suggestions$ = new BehaviorSubject<string[]>([]);
  public suggestionsLoading$ = new BehaviorSubject<boolean>(false);

  private gettingAutocompleteSuggestions$ = new Subject<{ searchString: string; type: string[] }>();
  private gettingSuggestions$ = new Subject<{ keywords: string[]; type: string }>();

  constructor(private readonly smartTaggingService: SmartTaggingService) {
    this.smartTaggingDataListener();
  }

  getAutocomplete(searchString: string, type: string[]): void {
    this.gettingAutocompleteSuggestions$.next({searchString, type});
  }

  getSuggestions(keywords: string[], type: string) {
    this.gettingSuggestions$.next({keywords, type});
  }

  resetAutocompleteSuggestions() {
    this.autocompleteSuggestions$.next([]);
  }

  resetSuggestions() {
    // this.suggestions$.next([]);
  }

  smartTaggingDataListener() {
    this.getAutoCompleteSuggestionsListener();
    this.getSuggestionsListener();
  }

  getAutoCompleteSuggestionsListener() {
    this.gettingAutocompleteSuggestions$
      .pipe(
        tap({
          next: () => {
            this.resetAutocompleteSuggestions();
            this.autocompleteSuggestionsLoading$.next(false);
          },
        }),
        switchMap(({searchString, type}) => {
          this.autocompleteSuggestionsLoading$.next(true);
          const language = 'DUT'

          return this.smartTaggingService.getAutocomplete(searchString, type, language).pipe(
            take(1),
            map((response) => {
              const suggestions = [];

              response.map((option) => {
                suggestions.push(option.matchedTerm.text);
              });
              this.autocompleteSuggestions$.next(suggestions);
              this.autocompleteSuggestionsLoading$.next(false);
            }),
            catchError((err) => {
              this.autocompleteSuggestionsLoading$.next(false);
              return of(err);
            })
          );
        })
      )
      .subscribe();
  }

  getSuggestionsListener() {
    this.gettingSuggestions$
      .pipe(
        tap({
          next: () => {
            // this.suggestions$.next([]);
            this.suggestionsLoading$.next(false);
          },
        }),
        switchMap(({keywords, type}) => {
          this.suggestionsLoading$.next(true);
          const language: string = 'DUT';

          return this.smartTaggingService.getSuggestions(keywords, type, language).pipe(
            take(1),
            map((response) => {
              const suggestions: string[] = [];

              response.map((suggestion) => suggestions.push(suggestion.keyword));
              this.suggestions$.next(suggestions);
              this.suggestionsLoading$.next(false);
            }),
            catchError((err) => {
              this.suggestionsLoading$.next(false);
              return of(err);
            })
          );
        })
      )
      .subscribe();
  }
}
