import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-autocomplete-search',
  templateUrl: './autocomplete-search.component.html',
  styleUrls: ['./autocomplete-search.component.scss'],
})
export class AutoCompleteSearchComponent implements OnChanges {
  @Input() suggestions: string[];
  @Input() suggestionsLoading: boolean;
  @Input() addButtonText: string;
  @Input() disabled: boolean;

  @Output() autocomplete = new EventEmitter<string>();
  @Output() search = new EventEmitter<string>();
  @Output() closeSuggestions = new EventEmitter<void>();
  @Output() clearSuggestions = new EventEmitter<void>();
  @Output() addCurrentValue = new EventEmitter<string>();

  public customOptionDisabled = true;
  public searchValue: string;

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.disabled?.currentValue) {
      this.searchValue = '';
    }
  }

  handleAutocomplete(event: Event) {
    const searchEvent =  event as any;
    this.searchValue = (searchEvent as string).toLowerCase();

    if (this.searchValue.length > 1) {
      this.autocomplete.emit(this.searchValue);
    } else if (this.searchValue.length === 0) {
      this.clearSuggestions.emit();
    } else {
      this.handleCloseSuggestions();
    }
  }

  handleSearch(suggestion: string) {
    this.search.emit(suggestion);
    this.searchValue = '';
  }

  handleCloseSuggestions() {
    setTimeout(() => {
      this.closeSuggestions.emit();
    });
  }

  onEnter(searchValue: string){
    console.log("ON ENTER");
    console.log(searchValue);
    this.handleSearch(searchValue);
    this.handleCloseSuggestions();
  }
}
