import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { ProjectFormFields, ProjectFormService } from '../project-form.service';
import { GUI_DATE_FORMAT } from '../../../../core/models/constants';
import {ProjectFormLanguage, ProjectFormSkill, SelectedLanguagesResult, SelectedSkillsResult} from '../project-form.model';
import { forkJoin, Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { SkillType } from '../../../../core/models/skill.model';
import { TranslateService } from '@ngx-translate/core';
import { ModalService } from '../../../../modals/modal.service';
import { StateService } from 'src/app/core/services/state.service';
import { State } from 'src/app/core/models/states';
import {LanguageService} from "../../../../core/services/language.service";
import {Language} from "../../../../core/models/language";
import {LanguageLevel} from "../../../../core/models/language-level";

@Component({
  selector: 'app-project-form-sidebar',
  templateUrl: './project-form-sidebar.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProjectFormSidebarComponent implements OnInit, OnDestroy {

  @Output() openSkillsPanel = new EventEmitter();
  @Output() openToolsPanel = new EventEmitter();
  @Output() openLanguagesPanel = new EventEmitter();
  states$: Observable<State[]>;
  states: State[];

  selectedState: string;
  projectFormFields = ProjectFormFields;
  guiDateFormat = GUI_DATE_FORMAT;

  private destroy$ = new Subject();

  constructor(public projectFormService: ProjectFormService, private changeDetectorRef: ChangeDetectorRef,
    private translate: TranslateService, private modalService: ModalService, private stateService: StateService) {

  }

  ngOnInit(): void {

    this.states$ = this.stateService.getAllStates();

    this.projectFormService.form.valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.changeDetectorRef.detectChanges();
      });
  }

  hasSkills(selectedSkills: string[]): boolean {
    return !!(selectedSkills.length);
  }

  hasLanguages(selectedLanguages: SelectedLanguagesResult): boolean {
    var { languages } = selectedLanguages;
    languages = languages.filter(l => l.level !== 'none');
    return !!(languages.length);
  }

  removeSkill(formSkillToRemove: ProjectFormSkill) {
    // const title = this.translate.instant('projects.skillRemoveModal.title');
    // let body: string;
    // let confirmBtnText: string;
    //
    // if (formSkillToRemove.type === SkillType.TOOLS) {
    //   body = this.translate.instant('projects.skillRemoveModal.toolBody');
    //   confirmBtnText = this.translate.instant('projects.skillRemoveModal.toolConfirmBtn');
    // } else {
    //   body = this.translate.instant('projects.skillRemoveModal.skillBody');
    //   confirmBtnText = this.translate.instant('projects.skillRemoveModal.skillConfirmBtn');
    // }
    //
    // const modalRef = this.modalService.openConfirmRemoveModal(title, body, confirmBtnText);
    //
    // modalRef.result
    //   .then(
    //     () => this.projectFormService.removeSkillFromSelectedList(formSkillToRemove),
    //     () => { }
    //   );
  }
  handleSelectOption(option: string) {
    // if (this.type === SelectorType.jobs) {
    //   this.freelancerRepository.addFunction(option);
    // } else if (this.type === SelectorType.skills) {
    //   this.freelancerRepository.addSkill(option);
    // }
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
