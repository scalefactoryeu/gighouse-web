import {cloneDeep} from 'lodash';
import {IProjectBackend, ProjectStatus} from '../project.model';

const basicMockedActiveProjects: IProjectBackend[] = [
  {
      id: '#001',
      sfid: '#001',
      name: 'GH/BE/0083/24 - Web Designer',
      startDate: '2020-03-02',
      endDate: '2020-04-02',
      sentDate: '2020-01-02',
      archivedDate: '2020-04-02',
      requestedStartDate: '2020-04-02',
      status: ProjectStatus.FEEDBACK,
      statusMessage: 'Freelancers wait for your feedback',
      matchesCount: 9,
    },
  //   {
  //     id: '#002',
  //     sfid: '#002',
  //     name: 'GH/BE/0082/20 - Social Media Expert',
  //     startDate: '2020-03-02',
  //     endDate: '2020-04-02',
  //     sentDate: '2020-01-02',
  //     closedDate: null,
  //     status: ProjectStatus.SENT,
  //     statusMessage: 'We selected suitable freelancers for your project',
  //     matchesCount: 5,
  //   },
  //   {
  //     id: '#003',
  //     sfid: '#003',
  //     name: 'GH/BE/0084/25 - SEO Expert',
  //     startDate: '2020-03-02',
  //     endDate: '2020-04-02',
  //     sentDate: '2020-01-02',
  //     closedDate: null,
  //     status: ProjectStatus.DRAFT,
  //     statusMessage: 'We selected suitable freelancers for your project',
  //     matchesCount: 6,
  //   },
  //   {
  //     id: '#004',
  //     sfid: '#004',
  //     name: 'GH/BE/0085/26 - UX Designer',
  //     startDate: '2020-03-02',
  //     endDate: '2020-04-02',
  //     sentDate: '2020-01-02',
  //     closedDate: null,
  //     status: ProjectStatus.MEET_YOUR_MATCH,
  //     statusMessage: 'Chats with your matched freelancers',
  //     matchesCount: 6,
  //   }
];

const basicMockedClosedProjects: IProjectBackend[] = [
  //   {
  //     id: '#005',
  //     sfid: '#005',
  //     name: 'GH/BE/0072/10 - Social Media Expert',
  //     startDate: '2019-03-02',
  //     endDate: '2019-04-02',
  //     sentDate: '2019-01-02',
  //     closedDate: '2019-04-03',
  //     status: ProjectStatus.CLOSED,
  //     statusMessage: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
  //     matchesCount: 5,
  //   },
  //   {
  //     id: '#006',
  //     sfid: '#006',
  //     name: 'GH/BE/0073/14 - Web Designer',
  //     startDate: '2019-03-02',
  //     endDate: '2019-04-02',
  //     sentDate: '2019-01-02',
  //     closedDate: '2019-04-03',
  //     status: ProjectStatus.CLOSED,
  //     statusMessage: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
  //     matchesCount: 9,
  //   },
  //   {
  //     id: '#007',
  //     sfid: '#007',
  //     name: 'GH/BE/0074/15 - SEO Expert',
  //     startDate: '2019-03-02',
  //     endDate: '2019-04-02',
  //     sentDate: '2019-01-02',
  //     closedDate: '2019-04-03',
  //     status: ProjectStatus.CLOSED,
  //     statusMessage: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
  //     matchesCount: 6,
  //   },
  //   {
  //     id: '#008',
  //     sfid: '#008',
  //     name: 'GH/BE/0075/15 - UX Designer',
  //     startDate: '2019-03-02',
  //     endDate: '2019-04-02',
  //     sentDate: '2019-01-02',
  //     closedDate: '2019-04-03',
  //     status: ProjectStatus.CLOSED,
  //     statusMessage: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
  //     matchesCount: 6,
  //   }
];

export const mockedActiveProjects = generateMockedProjects(basicMockedActiveProjects, 0);
export const mockedClosedProjects = generateMockedProjects(basicMockedClosedProjects, 30);

export function generateMockedProjects(initialProjects: IProjectBackend[], replicateNum: number): IProjectBackend[] {
  let projects = initialProjects;

  for (let i = 0; i < replicateNum; i++) {
    const newProjects = cloneDeep(initialProjects);
    newProjects.forEach(proj => {
      const appendNumber = i + 2;
      proj.name = proj.name + ' - ' + appendNumber;
      proj.id = proj.id + '-' + appendNumber;
      proj.sfid = proj.sfid + '-' + appendNumber;
    });

    projects = projects.concat(newProjects);
  }

  return projects;
}
