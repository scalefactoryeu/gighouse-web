import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { ProjectDetails } from '../../project.model';

@Component({
  selector: 'app-project-details-sidebar',
  templateUrl: './project-details-sidebar.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProjectDetailsSidebarComponent implements OnInit {

  @Input() project: ProjectDetails;

  constructor() {

  }

  ngOnInit(): void {

  }

}
