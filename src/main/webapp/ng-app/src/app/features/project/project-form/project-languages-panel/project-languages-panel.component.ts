import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {cloneDeep} from 'lodash';
import {ProjectFormLanguage} from '../project-form.model';
import {ProjectFormService} from '../project-form.service';
import {TranslateService} from '@ngx-translate/core';
import {LanguageLevel} from "../../../../core/models/language-level";

@Component({
  selector: 'app-project-languages-panel',
  templateUrl: './project-languages-panel.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProjectLanguagesPanelComponent implements OnInit {

  @Input() mode: any;

  @Output() triggerClose = new EventEmitter<void>();

  projectLanguages: ProjectFormLanguage[];
  anyLanguagesSelectedInitially = false; // true when a skill was selected when panel was opened

  constructor(private projectFormService: ProjectFormService, private translate: TranslateService) {

  }

  ngOnInit(): void {
    this.projectLanguages = cloneDeep(this.getProjectLanguages());
  }

  private getProjectLanguages(): ProjectFormLanguage[] {
    return this.projectFormService.getFormLanguages();
  }

  private getLanguageLevels(): LanguageLevel[] {
    return this.projectFormService.getLanguageLevels()
  }


  selectedCount(): number {
    let count = 0;

    for (const projectLanguage of this.projectLanguages) {
      if (projectLanguage.level != 'none') {
        count++;
      }
    }

    return count;
  }

  getApplyButtonText(): string {
    return this.translate.instant('Apply');
  }

  apply() {
    // this.languages.forEach(parentLanguage => parentLanguage.isSelected = false);
    this.projectFormService.formLanguages$.next(this.projectLanguages);
    this.triggerClose.next();
  }

}
