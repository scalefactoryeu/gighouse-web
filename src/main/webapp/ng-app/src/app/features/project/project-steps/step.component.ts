import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { ProjectStatus, ProjectStatusOrderedList } from '../project.model';

@Component({
  selector: 'app-project-step',
  templateUrl: './step.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProjectStepComponent implements OnInit {

  @Input() stepSeqNum: number; // number in step sequence (0-6)
  @Input() projectStatus: ProjectStatus; // status of the project

  constructor() {

  }

  ngOnInit(): void {

  }

  isCurrent(): boolean {
    return this.stepSeqNum === ProjectStatusOrderedList.indexOf(this.projectStatus);
  }

  isVisited(): boolean {
    return this.stepSeqNum < ProjectStatusOrderedList.indexOf(this.projectStatus);
  }

  isUnvisited(): boolean {
    return this.stepSeqNum > ProjectStatusOrderedList.indexOf(this.projectStatus);
  }

}
