import {_TempProjectLanguage, _TempSkill} from './../project.model';
import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit, Renderer2} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ProjectService} from '../project.service';
import {BehaviorSubject, combineLatest, Subject, throwError} from 'rxjs';
import {ProjectFormService} from './project-form.service';
import {ProjectDetails, ProjectFormMode, ProjectSaveRequestData} from '../project.model';
import {Skill} from '../../../core/models/skill.model';
import {SkillService} from '../../../core/services/skill.service';
import {CanDeactivateProjectFormReason, SkillPanelMode} from './project-form.model';
import {BACKEND_DATE_FORMAT} from '../../../core/models/constants';
import {catchError, finalize, takeUntil} from 'rxjs/operators';
import {ErrorHandlingService} from '../../../core/error-handling/error-handling.service';
import {TranslateService} from '@ngx-translate/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ProjectSaveSuccessModalComponent} from '../project-save-success-modal/project-save-success-modal.component';
import {CanDeactivateResult, ComponentCanDeactivate} from '../../../shared/components/component-can-deactivate.component';
import {LanguageService} from "../../../core/services/language.service";
import {Language} from "../../../core/models/language";
import {LanguageLevel} from "../../../core/models/language-level";

@Component({
  selector: 'app-project-form',
  templateUrl: './project-form.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [ProjectFormService]
})
export class ProjectFormComponent extends ComponentCanDeactivate implements OnInit, OnDestroy {

  formMode$: BehaviorSubject<ProjectFormMode> = new BehaviorSubject(null);
  skillPanelMode: SkillPanelMode;
  languagesPanel: any;

  loadingProject$ = new BehaviorSubject<boolean>(true);
  loadingSkills$ = new BehaviorSubject<boolean>(true);
  loadingLanguages$ = new BehaviorSubject<boolean>(true);
  loadingLanguageLevels$ = new BehaviorSubject<boolean>(true);
  loadingDataFailed$ = new BehaviorSubject<boolean>(false);

  // keep a reference to a project for later
  project: ProjectDetails;

  // This needs to be a subject (not behaviourSubject) to make sure combineLatest will emit when we want it.
  private project$: Subject<ProjectDetails> = new Subject<ProjectDetails>();
  private skills$ = new Subject<Skill[]>();
  private languages$ = new Subject<Language[]>();
  private languageLevels$ = new Subject<LanguageLevel[]>();
  private initialSaveRequestData: ProjectSaveRequestData;
  private destroy$ = new Subject<void>();

  constructor(private activatedRoute: ActivatedRoute, private projectService: ProjectService, private skillService: SkillService,
              private languageService: LanguageService,
              public projectFormService: ProjectFormService, private changeDetectorRef: ChangeDetectorRef,
              private errorHandlingService: ErrorHandlingService, private translate: TranslateService,
              private ngbModal: NgbModal, private renderer: Renderer2, private router: Router) {
    super();
  }

  ngOnInit(): void {
    combineLatest([this.skills$, this.languages$, this.languageLevels$, this.project$])
      .pipe(takeUntil(this.destroy$))
      .subscribe(([skills, languages, levels, project]) => {
        this.projectFormService.init(skills,languages, levels, project, this.formMode$.value, this.destroy$);
        this.initialSaveRequestData = this.createSaveRequestData();
        this.changeDetectorRef.detectChanges();

        this.projectFormService.form.valueChanges
          .pipe(takeUntil(this.destroy$))
          .subscribe(() => {
            this.changeDetectorRef.detectChanges();
          });
      });

    this.activatedRoute.data
      .subscribe(data => {
        this.formMode$.next((data.projectFormMode as ProjectFormMode));
      });

    this.activatedRoute.paramMap
      .subscribe(paramMap => {
        if (paramMap.get('projectId')) {
          this.fetchProject(paramMap.get('projectId'));
        } else {
          this.project$.next(null);
          this.loadingProject$.next(false);
        }
      });

    this.fetchSkills();
    this.fetchLanguages();
    this.fetchLanguageLevels();
  }

  canDeactivate(): CanDeactivateResult<CanDeactivateProjectFormReason> {
    let canDeactivate = true;
    let reason: CanDeactivateProjectFormReason;

    const currentSaveRequestData = this.createSaveRequestData();

    if (JSON.stringify(this.initialSaveRequestData) !== JSON.stringify(currentSaveRequestData)) {
      canDeactivate = false;
      reason = this.isFormEditMode() ? CanDeactivateProjectFormReason.UNSAVED_CHANGES : CanDeactivateProjectFormReason.NOT_SAVED;
    }

    return {
      canDeactivate,
      reason
    };
  }

  private fetchProject(projectId: string) {
    this.projectService.getProjectDetails(projectId)
      .pipe(
        catchError((error) => {
          this.loadingDataFailed$.next(true);
          this.loadingProject$.next(false);
          return throwError(error);
        })
      )
      .subscribe((project) => {
        this.project = project;
        this.project$.next(project);
        this.loadingProject$.next(false);
      });
  }

  private fetchSkills() {
    this.skillService.getAllSkills()
      .pipe(
        catchError((error) => {
          this.loadingDataFailed$.next(true);
          this.loadingSkills$.next(false);
          return throwError(error);
        })
      )
      .subscribe(skills => {
        // skills = skills.concat(projectSkillsMock);
        this.skills$.next(skills);
        this.loadingSkills$.next(false);
      });
  }

  private fetchLanguages() {
    this.languageService.getAllLanguages()
      .pipe(
        catchError((error) => {
          this.loadingDataFailed$.next(true);
          this.loadingLanguages$.next(false);
          return throwError(error);
        })
      )
      .subscribe(languages => {
        this.languages$.next(languages);
        this.loadingLanguages$.next(false);
      });
  }

  private fetchLanguageLevels() {
    this.languageService.getAllLanguageLevels()
      .pipe(
        catchError((error) => {
          this.loadingDataFailed$.next(true);
          this.loadingLanguageLevels$.next(false);
          return throwError(error);
        })
      )
      .subscribe(languageLevel => {
        this.languageLevels$.next(languageLevel);
        this.loadingLanguageLevels$.next(false);
      });
  }

  isFormEditMode(): boolean {
    return this.formMode$.value === ProjectFormMode.EDIT;
  }

  isFormCreateMode(): boolean {
    return this.formMode$.value === ProjectFormMode.CREATE;
  }

  isFormCopyMode(): boolean {
    return this.formMode$.value === ProjectFormMode.COPY;
  }

  openSkillsPanel() {
    this.toggleBodyOverflowHidden(true);
    this.skillPanelMode = SkillPanelMode.SKILLS;
  }

  openToolsPanel() {
    this.toggleBodyOverflowHidden(true);
    this.skillPanelMode = SkillPanelMode.TOOLS;
  }

  openLanguagesPanel() {
    this.toggleBodyOverflowHidden(true);
    this.languagesPanel = 'Languages';

  }

  closeLanguagesPanel() {
    this.toggleBodyOverflowHidden(false);
    this.languagesPanel = null;
  }

  closeSkillsPanel() {
    this.toggleBodyOverflowHidden(false);
    this.skillPanelMode = null;
  }

  /* Hide scrollbar on body when skills panel is open */
  private toggleBodyOverflowHidden(overflowHidden: boolean) {
    if (overflowHidden) {
      this.renderer.setStyle(document.body, 'overflow', 'hidden');
    } else {
      this.renderer.removeStyle(document.body, 'overflow');
    }
  }

  saveProject(saveAndSend = false) {
    this.projectFormService.makeSubmitted();

    if (this.projectFormService.form.valid) {
      const requestData: ProjectSaveRequestData = this.createSaveRequestData();

      if (!saveAndSend) {
        requestData.draft = true;
      }

      this.projectFormService.toggleIsSaving(true);

      let saveMethod;

      if (this.isFormEditMode()) {
        saveMethod = saveAndSend ?
          this.projectService.updateProjectAndSend(this.project.id, requestData) :
          this.projectService.updateProjectConcept(this.project.id, requestData);
      } else {
        saveMethod = saveAndSend ?
          this.projectService.createProjectAndSend(requestData) :
          this.projectService.createProjectConcept(requestData);
      }

      saveMethod.pipe(
        catchError(error => {
          this.errorHandlingService.openErrorModal(this.translate.instant('projects.saveFailed'));
          return throwError(error);
        }),
        finalize(() => {
          this.projectFormService.toggleIsSaving(false);
        })
      )
        .subscribe((project) => {
          this.project = project;
          this.project$.next(project);
          this.loadingProject$.next(false);
          this.openSaveSuccessModal();
        });
    }
  }

  private createSaveRequestData(): ProjectSaveRequestData {
    const teasers = this.projectFormService.getTeasers();
    let selectedSkills = this.projectFormService.getFormSkills();
    let selectedLanguages = []
    let formLanguages = this.projectFormService.getFormLanguages();
    if (formLanguages != null) {
      formLanguages.forEach(s => {
         selectedLanguages.push(s);
      });
    }
    selectedLanguages = selectedLanguages.filter(l => l.level !== 'none');

    return {
      name: this.projectFormService.getJobTitle(),
      description: this.projectFormService.getJobDescription(),
      teaser1: teasers[0],
      teaser2: teasers[1],
      teaser3: teasers[2],
      daysPerWeek: this.projectFormService.getDaysPerWeek(),
      estimatedProjectDuration: this.projectFormService.getDurationWeeks(),
      startDate: this.projectFormService.getStartDate() ? this.projectFormService.getStartDate().format(BACKEND_DATE_FORMAT) : null,
      postalCode: this.projectFormService.getPostcode(),
      city: this.projectFormService.getCity(),
      homeWork: this.projectFormService.getHomeWorkingAllowed(),
      dailyFee: this.projectFormService.getBudget(),
      draft: false,
      state: this.projectFormService.getState(),
      maxBudget: this.projectFormService.getBudget(),
      expertise: this.projectFormService.getExpertise(),
      positiveSkills: selectedSkills,
      projectLanguages: selectedLanguages.map(pl => new _TempProjectLanguage(pl.id, pl.language, pl.level)),
    };
  }

  private openSaveSuccessModal() {
    const modalRef = this.ngbModal.open(ProjectSaveSuccessModalComponent, {
      windowClass: 'project-save-success-modal',
      backdrop: 'static'
    });

    modalRef.result
      .then(
        () => {
          this.router.navigate(['/projects/details', this.project.id]);
        },
        () => { }
      );
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
