import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {ProjectFormFields, ProjectFormService, QuillConfiguration} from '../project-form.service';
import {takeUntil} from 'rxjs/operators';
import {BehaviorSubject, Subject} from 'rxjs';
import {SmartTaggingRepository} from "../../smart-tagging.repository";

@Component({
  selector: 'app-project-form-summary',
  templateUrl: './project-form-summary.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProjectFormSummaryComponent implements OnInit, OnDestroy {

  quillConfiguration = QuillConfiguration;
  projectFormFields = ProjectFormFields;

  private destroy$ = new Subject();

  constructor(public projectFormService: ProjectFormService, private changeDetectorRef: ChangeDetectorRef, private smartTaggingRepository: SmartTaggingRepository) {

  }


  ngOnInit(): void {
    this.projectFormService.form.valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.changeDetectorRef.detectChanges();
      });
  }

  handleCloseAutocompleteSuggestions() {
    this.smartTaggingRepository.resetAutocompleteSuggestions();
  }


  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
