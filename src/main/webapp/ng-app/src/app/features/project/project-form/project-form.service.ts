import {Injectable} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {cloneDeep} from 'lodash';
import {ProjectDetails, ProjectFormMode} from '../project.model';
import {Moment} from 'moment';
import {TranslateService} from '@ngx-translate/core';
import {Skill, SkillType} from '../../../core/models/skill.model';
import {
  createProjectFormLanguagesStructure,
  ProjectFormLanguage,
  ProjectFormSkill,
  SelectedLanguagesResult,
  SelectedSkillsResult
} from './project-form.model';
import {BehaviorSubject, Observable, of, Subject} from 'rxjs';
import {map, skip, takeUntil} from 'rxjs/operators';
import {Language} from "../../../core/models/language";
import {LanguageLevel} from "../../../core/models/language-level";

export enum ProjectFormFields {
  JOB_TITLE = 'jobTitle',
  EXPERTISE = 'expertise',
  JOB_DESCRIPTION = 'jobDescription',
  TEASERS = 'teasers',
  DAYS_PER_WEEK = 'daysPerWeek',
  DURATION_WEEKS = 'durationWeeks',
  START_DATE = 'startDate',
  POSTCODE = 'postcode',
  CITY = 'city',
  STATE = 'state',
  HOMEWORKING_ALLOWED = 'homeworkingAllowed',
  BUDGET = 'budget'
}

export const QuillConfiguration = {
  toolbar: [
    ['bold', 'italic', 'underline', 'strike'],
    ['blockquote', 'code-block'],
    [{list: 'ordered'}, {list: 'bullet'}],
    [{header: [1, 2, 3, 4, 5, 6, false]}],
    [{color: []}, {background: []}],
    ['link'],
    ['clean'],
  ],
}

@Injectable()
export class ProjectFormService {

  form: FormGroup;

  skills: Skill[]
  languages: Language[]
  languageLevels: LanguageLevel[]
  formSkills$ = new BehaviorSubject<string[]>([]);
  formTools$ = new BehaviorSubject<ProjectFormSkill[]>(null);
  formLanguages$ = new BehaviorSubject<ProjectFormLanguage[]>(null);
  selectedSkills$: Observable<string[]>;

  selectedTools$: Observable<SelectedSkillsResult>;
  selectedLanguages$: Observable<SelectedLanguagesResult>;

  selectedState: string;

  private savingSubject$ = new BehaviorSubject<boolean>(false);
  saving$ = this.savingSubject$.asObservable();

  private submittedSubject$ = new BehaviorSubject<boolean>(false);
  submitted$ = this.submittedSubject$.asObservable();

  expertises = [];
  selectedExpertise = null;
  project: ProjectDetails;

  readonly daysPerWeekOptions = [1, 2, 3, 4, 5, 6, 7];
  readonly durationWeeksMin = 1;
  readonly budgetMin = 0;

  constructor(private formBuilder: FormBuilder, private translate: TranslateService) {

  }

  init(skills: Skill[], languages: Language[], languageLevels: LanguageLevel[], project: ProjectDetails, formMode: ProjectFormMode, destroy$: Subject<void>) {
    this.skills = skills;
    this.languages = languages;
    this.languageLevels = languageLevels;
    this.project = project;
    this.initForm(project, formMode);
    this.initProjectLanguages(languages, project);
    this.initSkillsAndTools(skills, project);

    this.saving$
      .pipe(
        skip(1),
        takeUntil(destroy$)
      )
      .subscribe(isSaving => isSaving ? this.form.disable() : this.form.enable());
  }

  private initForm(project: ProjectDetails, formMode: ProjectFormMode) {
    let projectName = project ? project.name : '';

    this.selectedState = project && project.location.name ? project.location.name : null;

    if (formMode === ProjectFormMode.COPY) {
      projectName = '[Copy] - ' + projectName;
    }

    const jobTitleControl = this.formBuilder.control(projectName, [Validators.required, Validators.maxLength(80)]);

    const expertiseControl = this.formBuilder.control(
      project ? project.expertise : null,
      [Validators.required]
    )

    const jobDescriptionControl = this.formBuilder.control(
      project ? project.description : '',
      [Validators.required, Validators.maxLength(2000)]
    );

    const teaserControl1 = this.formBuilder.control('', [Validators.maxLength(40)]);
    const teaserControl2 = cloneDeep(teaserControl1);
    const teaserControl3 = cloneDeep(teaserControl1);

    if (project && project.tags && project.tags.length) {
      teaserControl1.setValue(project.tags[0] ? project.tags[0] : '');
      teaserControl2.setValue(project.tags[1] ? project.tags[1] : '');
      teaserControl3.setValue(project.tags[2] ? project.tags[2] : '');
    }

    const teasersArray = this.formBuilder.array([teaserControl1, teaserControl2, teaserControl3]);

    const daysPerWeekControl = this.formBuilder.control(
      project ? project.requiredDaysPerWeek : 1,
      [Validators.required]
    );

    const durationWeeksControl = this.formBuilder.control(
      project ? project.durationWeeks : 1,
      [Validators.required, Validators.min(this.durationWeeksMin)]
    );

    const requestedstartDateControl = this.formBuilder.control(
      project ? project.requestedStartDate : '',
      [Validators.required]
    );

    const postcodeControl = this.formBuilder.control(
      project && project.location ? project.location.postcode : '',
      [Validators.required, Validators.maxLength(7)]
    );

    const cityControl = this.formBuilder.control(
      project && project.location ? project.location.city : '',
      [Validators.required, Validators.maxLength(80)]
    );

    const stateControl = this.formBuilder.control(
      project && project.location ? project.location.name : '',
      [Validators.required]
    );

    const homeworkingAllowedControl = this.formBuilder.control(project && project.workFromHomePossible ? project.workFromHomePossible : false);

    const budgetControl = this.formBuilder.control(
      project ? project.budget : null,
      [Validators.required, Validators.min(this.budgetMin)]
    );

    this.form = this.formBuilder.group({
      [ProjectFormFields.JOB_TITLE]: jobTitleControl,
      [ProjectFormFields.EXPERTISE]: expertiseControl,
      [ProjectFormFields.JOB_DESCRIPTION]: jobDescriptionControl,
      [ProjectFormFields.TEASERS]: teasersArray,
      [ProjectFormFields.DAYS_PER_WEEK]: daysPerWeekControl,
      [ProjectFormFields.DURATION_WEEKS]: durationWeeksControl,
      [ProjectFormFields.START_DATE]: requestedstartDateControl,
      [ProjectFormFields.POSTCODE]: postcodeControl,
      [ProjectFormFields.CITY]: cityControl,
      [ProjectFormFields.STATE]: stateControl,
      [ProjectFormFields.HOMEWORKING_ALLOWED]: homeworkingAllowedControl,
      [ProjectFormFields.BUDGET]: budgetControl
    });
  }

  private initSkillsAndTools(skills: Skill[], project: ProjectDetails) {
    this.selectedSkills$ = project != null && project.positiveSkills != null ? of(project.positiveSkills) : of(['']);
    this.expertises = skills.filter(s => s.parent == null && s.type === SkillType.FUNCTION)
      .sort((a, b) => a.name.localeCompare(b.name));
    if (project) {
      this.formSkills$.next(project.positiveSkills);
    }


  }


  private initProjectLanguages(languages: Language[], project: ProjectDetails) {
    this.selectedLanguages$ = this.formLanguages$
      .pipe(map(formLanguages => this.getSelectedLanguagesCommon(formLanguages)));
    this.formLanguages$.next(createProjectFormLanguagesStructure(languages));
    if (project) {
      this.updateFormLanguagesFromProjectDetails(this.getFormLanguages(), project);
    }
  }

  makeSubmitted() {
    this.submittedSubject$.next(true);
  }

  toggleIsSaving(isSaving: boolean) {
    this.savingSubject$.next(isSaving);
  }

  private updateFormSkillsFromProjectDetails(formSkills: ProjectFormSkill[], project: ProjectDetails) {
    formSkills.forEach(formSkill => {
      if (formSkill.children && formSkill.children.length) {
        formSkill.children.forEach(formSkillChild => {
          this.updateFormSkillFromProjectDetails(formSkillChild, project);
        });
      }
    });
  }

  private updateFormSkillFromProjectDetails(formSkill: ProjectFormSkill, project: ProjectDetails) {

    // const needToHaveList = formSkill.type === SkillType.TOOLS ? project.needToHaveTools : project.positiveSkills;
    // const niceToHaveList = formSkill.type === SkillType.TOOLS ? project.niceToHaveTools : project.niceToHaveSkills;
    //
    // let skillIsNeedToHave = false;
    // let matchingSkillInProject = needToHaveList.find(_skill => _skill.id === formSkill.id);
    //
    // if (matchingSkillInProject) {
    //   skillIsNeedToHave = true;
    // } else {
    //   matchingSkillInProject = niceToHaveList.find(_skill => _skill.id === formSkill.id);
    // }
    //
    // if (matchingSkillInProject) {
    //   formSkill.isSelected = true;
    //   formSkill.isNeedToHave = skillIsNeedToHave;
    //   formSkill.level = matchingSkillInProject.rating;
    // }
  }

  private updateFormLanguagesFromProjectDetails(formLanguages: ProjectFormLanguage[], project: ProjectDetails) {
    formLanguages.forEach(formLanguage => {
      this.updateFormLanguageFromProjectDetails(formLanguage, project);
    });
  }

  private updateFormLanguageFromProjectDetails(formLanguage: ProjectFormLanguage, project: ProjectDetails) {
    const projectLanguages = project.projectLanguages;
    let matchingLanguageInProject = projectLanguages.find(_language => _language.language === formLanguage.language);
    if (matchingLanguageInProject) {
      formLanguage.id = matchingLanguageInProject.id;
      formLanguage.language = matchingLanguageInProject.language;
      formLanguage.level = matchingLanguageInProject.level;
    }
  }


  getFormSkills(): string[] {
    return this.formSkills$.getValue();
  }

  getFormTools(): ProjectFormSkill[] {
    return this.formTools$.getValue();
  }

  getFormLanguages(): ProjectFormLanguage[] {
    return this.formLanguages$.getValue();
  }

  getLanguages(): Language[] {
    return this.languages;
  }

  getLanguageLevels(): LanguageLevel[] {
    return this.languageLevels;
  }


  removeSkillFromSelectedList(formSkillToRemove: ProjectFormSkill) {
    // const formSkills = formSkillToRemove.type === SkillType.TOOLS ? this.getFormTools() : this.getFormSkills();
    //
    // for (const parentSkill of formSkills) {
    //   for (const childSkill of parentSkill.children) {
    //     if (childSkill.id === formSkillToRemove.id) {
    //       childSkill.unselect();
    //       break;
    //     }
    //   }
    // }
    //
    // if (formSkillToRemove.type === SkillType.TOOLS) {
    //   this.formTools$.next(formSkills);
    // } else {
    //   this.formSkills$.next(formSkills);
    // }
  }

  private getSelectedSkillsCommon(formSkills: ProjectFormSkill[]): SelectedSkillsResult {
    const needToHave: ProjectFormSkill[] = [];
    const niceToHave: ProjectFormSkill[] = [];

    if (formSkills) {
      formSkills.forEach(formSkill => {
        if (formSkill.children && formSkill.children.length) {
          formSkill.children.forEach(childFormSkill => {
            if (childFormSkill.isSelected) {
              if (childFormSkill.isNeedToHave) {
                needToHave.push(childFormSkill);
              } else {
                niceToHave.push(childFormSkill);
              }
            }
          });
        }
      });
    }

    return {
      needToHave,
      niceToHave
    };
  }

  private getSelectedLanguagesCommon(formLanguages: ProjectFormLanguage[]): SelectedLanguagesResult {
    const languages: ProjectFormLanguage[] = [];
    if (formLanguages) {
      formLanguages.forEach(formLanguage => {
        languages.push(formLanguage);
      });
    }
    return {languages};
  }

  /* Control Getters */

  getJobTitleControl(): FormControl {
    return this.form.get(ProjectFormFields.JOB_TITLE) as FormControl;
  }

  getJobDescriptionControl(): FormControl {
    return this.form.get(ProjectFormFields.JOB_DESCRIPTION) as FormControl;
  }

  getTeasersFormArray() {
    return this.form.get(ProjectFormFields.TEASERS) as FormArray;
  }

  getTeasersControlByIndex(index: number): FormControl {
    return this.getTeasersFormArray().controls[index] as FormControl;
  }

  getDurationWeeksControl(): FormControl {
    return this.form.get(ProjectFormFields.DURATION_WEEKS) as FormControl;
  }

  getStartDateControl(): FormControl {
    return this.form.get(ProjectFormFields.START_DATE) as FormControl;
  }

  getPostcodeControl(): FormControl {
    return this.form.get(ProjectFormFields.POSTCODE) as FormControl;
  }

  getCityControl(): FormControl {
    return this.form.get(ProjectFormFields.CITY) as FormControl;
  }

  getStateControl(): FormControl {
    return this.form.get(ProjectFormFields.STATE) as FormControl;
  }

  getBudgetControl(): FormControl {
    return this.form.get(ProjectFormFields.BUDGET) as FormControl;
  }

  /* Value Getters */

  getJobTitle(): string {
    return this.form.get(ProjectFormFields.JOB_TITLE).value;
  }

  getJobDescription(): string {
    return this.form.get(ProjectFormFields.JOB_DESCRIPTION).value;
  }

  getTeasers(): string[] {
    return this.form.get(ProjectFormFields.TEASERS).value;
  }

  getDaysPerWeek(): number {
    return this.form.get(ProjectFormFields.DAYS_PER_WEEK).value;
  }

  getDurationWeeks(): number {
    return this.form.get(ProjectFormFields.DURATION_WEEKS).value;
  }

  getStartDate(): Moment {
    return this.form.get(ProjectFormFields.START_DATE).value;
  }

  getPostcode(): string {
    return this.form.get(ProjectFormFields.POSTCODE).value;
  }

  getCity(): string {
    return this.form.get(ProjectFormFields.CITY).value;
  }

  getState(): string {
    return this.form.get(ProjectFormFields.STATE).value;
  }

  getHomeWorkingAllowed(): boolean {
    return this.form.get(ProjectFormFields.HOMEWORKING_ALLOWED).value;
  }

  getBudget(): number {
    return this.form.get(ProjectFormFields.BUDGET).value;
  }

  getExpertise(): string {
    let split = this.form.get(ProjectFormFields.EXPERTISE).value != null ? this.form.get(ProjectFormFields.EXPERTISE).value.split(';') : null;
    if(split != null && split.length > 1){
      return split[0];
    }
    return split;
  }

  getExpertiseLabel(): string {
    let split = this.form.get(ProjectFormFields.EXPERTISE).value != null ? this.form.get(ProjectFormFields.EXPERTISE).value.split(';') : null;
    if(split != null && split.length > 1){
      return split[1];
    }
    return '';
  }

  /* Value Setters */

  setStartDate(date: Moment) {
    this.getStartDateControl().setValue(date);
  }

  setNewState(event) {
    var stateLabel = event.target.value;
    var parts = stateLabel.split(' ');
    parts.splice(0, 1);
    this.getStateControl().setValue(parts.join(" "));
    this.project.location.name = parts.join(" ");
  }

  /* Error Getters */

  getJobTitleErrorMsg(): string {
    const errors = this.getJobTitleControl().errors;

    if (errors) {
      if (errors.maxlength) {
        return this.translate.instant('projects.validation.jobTitleMaxLength', {maxLength: errors.maxlength.requiredLength});
      }
    }

    return null;
  }

  getJobDescriptionErrorMsg(): string {
    const errors = this.getJobDescriptionControl().errors;

    if (errors) {
      if (errors.maxlength) {
        return this.translate.instant('projects.validation.jobDescriptionMaxLength', {maxLength: errors.maxlength.requiredLength});
      }
    }

    return null;
  }

  getTeaserErrorMsg(index: number): string {
    const errors = this.getTeasersControlByIndex(index).errors;

    if (errors) {
      if (errors.maxlength) {
        return this.translate.instant('projects.validation.teaserMaxLength', {maxLength: errors.maxlength.requiredLength});
      }
    }

    return null;
  }

  getDurationWeeksErrorMsg(): string {
    const errors = this.getDurationWeeksControl().errors;

    if (errors) {
      if (errors.min) {
        return this.translate.instant('projects.validation.durationWeeksMin', {min: errors.min.value});
      }
    }

    return null;
  }

  getStartDateErrorMsg(): string {
    const errors = this.getStartDateControl().errors;

    if (errors) {
      if (errors.ngbDate && !errors.required) {
        return this.translate.instant('projects.validation.startDateInvalid');
      }
    }

    return null;
  }

  getPostcodeErrorMsg(): string {
    const errors = this.getPostcodeControl().errors;

    if (errors) {
      if (errors.maxlength) {
        return this.translate.instant('projects.validation.postcodeMaxLength', {maxLength: errors.maxlength.requiredLength});
      }
    }

    return null;
  }

  getCityErrorMsg(): string {
    const errors = this.getCityControl().errors;

    if (errors) {
      if (errors.maxlength) {
        return this.translate.instant('projects.validation.cityMaxLength', {maxLength: errors.maxlength.requiredLength});
      }
    }

    return null;
  }

  getBudgetErrorMsg(): string {
    const errors = this.getBudgetControl().errors;

    if (errors) {
      if (errors.min) {
        return this.translate.instant('projects.validation.budgetMin', {min: errors.min.value});
      }
    }

    return null;
  }

  expertiseSelected(event) {
    let expertiseId = event.target.value;
  }
}
