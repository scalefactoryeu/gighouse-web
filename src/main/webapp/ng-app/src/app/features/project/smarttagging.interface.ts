export class AutocompleteParams {
  contentTypes: string[];
  lang: string;
  text: string;
}

export interface AutocompleteResponse {
  id: string;
  indexId: string;
  type: string;
  matchedTerm : AutocompleteTermsResponse;
  terms : AutocompleteTermsResponse[];
  alternatives : AutocompleteTermsResponse[];
  parts : AutocompleteTermsResponse[];
  context : AutocompleteTermsResponse[];
  flags : AutocompleteTermsResponse[];
  frequency: number;
  matchedText: string;
  definition: string;
  expansions: AutocompleteTermsResponse[];

}

export interface AutocompleteTermsResponse {
  text: string;
  language: string;

}

export interface SuggestionsParams {
  keywords : string[];
  language: string;
  type: string;
}
export interface SuggestionsResponse {
  keyword: string;
  queryItemType: string;
  weight: number;
  required : boolean;
}
