import { Component, EventEmitter, Input, Output } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-project-form-suggestions',
  templateUrl: './project-form-suggestions.component.html',
  styleUrls: ['./project-form-suggestions.component.scss'],
})
export class ProjectFormSuggestionsComponent {
  @Input() loading: boolean = false;
  @Input() suggestions: string[];
  @Input() selectedOptions: string[];
  @Input() disabled: boolean;

  @Output() selectOption = new EventEmitter<string>();

  expanded$ = new BehaviorSubject<boolean>(false);

  handleSelect(option) {
    this.selectOption.emit(option);
  }

  checkIfOptionIsSelected(option: string): boolean {
    return !!this.selectedOptions.find((suggestion) => suggestion === option);
  }

  handleToggleExpandOptions() {
    this.expanded$.next(!this.expanded$.getValue());
  }
}
