import { AfterViewInit, ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Project } from '../../project.model';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-project-table',
  templateUrl: './project-table.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProjectTableComponent implements OnInit, AfterViewInit {

  @Input() projects: Project[];
  @Input() totalCount: number;
  @Input() isActiveProjectsTable: boolean;
  @Input() loading: boolean;
  @Input() loadingFailed: boolean;

  @Output() loadMore = new EventEmitter();
  @Output() retry = new EventEmitter();

  viewInitialized$: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor() {
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.viewInitialized$.next(true);
    });
  }

  scrolled() {
    if (this.projects.length < this.totalCount) {
      this.loadMore.next();
    }
  }

  /**
   * Show "Load More" button when there are more items to be loaded, but there is no scrollbar. This case happens when
   * the initially loaded items still fit in the div without the div getting a scrollbar; without scrolling the infinite scroll will
   * not trigger a new load.
   *
   * We use viewInitialized to know when the view is really initialized and to be sure that we can rely on the scrollHeight and
   * clientHeight values.
   */
  showLoadMoreBtn(viewInitialized: boolean): boolean {
    const scrollableContainer = document.body;

    if (viewInitialized) {
      const scrollableContainerHasScrollbar = scrollableContainer.scrollHeight > scrollableContainer.clientHeight;
      return !this.loading && !this.loadingFailed && this.projects.length < this.totalCount && !scrollableContainerHasScrollbar;
    }
    return false;
  }

}
