import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProjectService } from '../project.service';
import { BehaviorSubject, Observable, of, throwError } from 'rxjs';
import { ProjectDetails, ProjectStatus } from '../project.model';
import { catchError, map, shareReplay, startWith, tap } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { ModalService } from '../../../modals/modal.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ProjectArchiveModalComponent } from '../project-archive-modal/project-archive-modal.component';
import { ToastrService } from 'ngx-toastr';
import { Freelancer } from '../../freelancer/freelancer.model';
import * as moment from 'moment';

@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProjectDetailsComponent implements OnInit {

  project$: Observable<ProjectDetails>;
  loadingProject$: Observable<boolean>;
  loadingProjectFailed$: BehaviorSubject<boolean> = new BehaviorSubject(false);
  showFreelancerMatchesTitle$: BehaviorSubject<boolean> = new BehaviorSubject(true);

  private project: ProjectDetails;

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private projectService: ProjectService,
    private translate: TranslateService, private modalService: ModalService, private ngbModal: NgbModal,
    private toastr: ToastrService) {
  }

  ngOnInit(): void {
    this.activatedRoute.paramMap
      .subscribe(paramMap => {
        this.project$ = this.projectService.getProjectDetails(paramMap.get('projectId'))
          .pipe(
            tap((project) => {
              this.project = project;
            }),
            shareReplay(),
            catchError((error) => {
              this.loadingProjectFailed$.next(true);
              return throwError(error);
            })
          );
      });

    this.loadingProject$ = this.project$
      .pipe(
        map(result => {
          return false;
        }),
        catchError((error) => {
          return of(false);
        }),
        startWith(true)
      );
  }

  isArchivedProject(): boolean {
    return this.project.status === ProjectStatus.CANCELLED;
  }

  showFreelancerMatches(project: ProjectDetails): boolean {
    return project.status == ProjectStatus.FREELANCERS_VIEWING
      || project.status == ProjectStatus.SENT
      || project.status == ProjectStatus.FEEDBACK
      || project.status == ProjectStatus.MEET_YOUR_MATCH
  }

  jumpToFreelancerMatches() {
    document.getElementById('freelancer-matches-section').scrollIntoView({ behavior: 'smooth' });
  }

  archiveProject() {
    const modalRef = this.ngbModal.open(ProjectArchiveModalComponent, {
      windowClass: 'confirmation-modal project-archive-modal',
      backdrop: 'static'
    });

    const modal = modalRef.componentInstance as ProjectArchiveModalComponent;

    modal.project = this.project;

    modalRef.result
      .then(
        () => {
          this.toastr.success(this.translate.instant('projects.projectArchiveModal.archiveSuccess'));
        },
        () => {

        }
      );
  }


  removeProject() {
    const title = this.translate.instant('projects.projectRemoveModal.title');
    const confirmBtnText = this.translate.instant('projects.projectRemoveModal.confirmBtn');

    const confirmMethod: () => Observable<any> = () => {
      return this.projectService.removeProject(this.project.id)
        .pipe(
          catchError((error) => {
            return throwError(this.translate.instant('projects.projectRemoveModal.removeFailed'));
          })
        );
    };

    const modalRef = this.modalService.openConfirmRemoveModal(title, null, confirmBtnText, confirmMethod);

    const navigateToRoute = this.project.status === ProjectStatus.CANCELLED ? 'archive' : 'active';

    modalRef.result
      .then(
        () => {
          this.toastr.success(this.translate.instant('projects.projectRemoveModal.removeSuccess'));
          this.router.navigate(['/projects', navigateToRoute]);
        },
        () => { }
      );
  }

  loadedFreelancers(freelancers: Freelancer[]) {
    setTimeout(() => {
      this.showFreelancerMatchesTitle$.next(freelancers.length > 0);
    });
  }

}
