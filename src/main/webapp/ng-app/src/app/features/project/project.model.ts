import * as moment from 'moment';
import {Moment} from 'moment';
import {BACKEND_DATE_FORMAT} from '../../core/models/constants';
import {SkillRequirement, SkillType} from '../../core/models/skill.model';

export enum ProjectStatus {
  CANCELLED = 'Cancelled',
  DRAFT = 'Draft',
  FREELANCER_READY = 'Freelancer is ready!',
  FREELANCERS_VIEWING = 'Freelancers are viewing the project',
  FEEDBACK = 'Freelancers are waiting for your feedback',
  MEET_YOUR_MATCH = 'Meet your match',
  SENT = 'Sent',
  CONTRACT = 'Sign the contract',
}

export const ProjectStatusOrderedList: ProjectStatus[] = [
  ProjectStatus.DRAFT,
  ProjectStatus.SENT,
  ProjectStatus.FREELANCERS_VIEWING,
  ProjectStatus.FEEDBACK,
  ProjectStatus.MEET_YOUR_MATCH,
  ProjectStatus.CONTRACT,
  ProjectStatus.FREELANCER_READY,
  ProjectStatus.CANCELLED
];

type ProjectStatusConfig = {
  translateLabel: string;
  icon: string;
  statusCssClass: string;
};

type ProjectStatusesConfig = {
  [P in ProjectStatus]: ProjectStatusConfig;
};

const PROJECT_STATUSES_CONFIG: ProjectStatusesConfig = {
  [ProjectStatus.DRAFT]: {
    translateLabel: 'projects.status.DRAFT',
    icon: 'pencil-fill',
    statusCssClass: 'project-status-draft'
  },
  [ProjectStatus.SENT]: {
    translateLabel: 'projects.status.SENT',
    icon: 'check',
    statusCssClass: 'project-status-sent'
  },
  [ProjectStatus.FEEDBACK]: {
    translateLabel: 'projects.status.FEEDBACK',
    icon: 'question-circle',
    statusCssClass: 'project-status-feedback'
  },
  [ProjectStatus.MEET_YOUR_MATCH]: {
    translateLabel: 'projects.status.MEET_YOUR_MATCH',
    icon: 'chat',
    statusCssClass: 'project-status-meet'
  },
  [ProjectStatus.FREELANCERS_VIEWING]: {
    translateLabel: 'projects.status.FREELANCERS_VIEWING',
    icon: 'hourglass',
    statusCssClass: 'project-status-freelancers-viewing'
  },
  [ProjectStatus.CONTRACT]: {
    translateLabel: 'projects.status.CONTRACT',
    icon: 'signature',
    statusCssClass: 'project-status-contract'
  },
  [ProjectStatus.FREELANCER_READY]: {
    translateLabel: 'projects.status.READYTEST',
    icon: 'celebration',
    statusCssClass: 'project-status-ready'
  },
  [ProjectStatus.CANCELLED]: {
    translateLabel: 'projects.status.CANCELLED',
    icon: null,
    statusCssClass: 'project-status-closed'
  }
};

export interface IProjectBackend {
  id: string;
  sfid: string;
  name: string;
  startDate: string;
  endDate: string;

  // ToDo: Review this - Currently only hardcoded in GUI because backend api doesn't have this
  sentDate: string;
  //closedDate: string;
  status: ProjectStatus;
  statusMessage: string;
  requestedStartDate: string;
  archivedDate: string;

  // ToDo: Review this - Mocked in GUI for Freelancers Matches page
  matchesCount: number;
}

export class Project {
  id: string;
  sfid: string;
  name: string;
  startDate: Moment;
  endDate: Moment;
  requestedStartDate: Moment
  archivedDate: Moment;


  // ToDo: Review this - Currently only hardcoded in GUI because backend api doesn't have this
  sentDate: Moment;
  closedDate: Moment;
  status: ProjectStatus;
  statusConfig: ProjectStatusConfig;
  statusMessage: string;

  // ToDo: Review this - Mocked in GUI for Freelancers Matches page
  matchesCount: number;

  constructor(projectBackend: IProjectBackend) {
    this.id = projectBackend.id;
    this.sfid = projectBackend.sfid;
    this.name = projectBackend.name;
    this.startDate = projectBackend.startDate === null ? null : moment.utc(projectBackend.startDate, BACKEND_DATE_FORMAT);
    this.endDate = projectBackend.endDate === null ? null : moment.utc(projectBackend.endDate, BACKEND_DATE_FORMAT);
    this.sentDate = projectBackend.sentDate === null ? null : moment.utc(projectBackend.sentDate, BACKEND_DATE_FORMAT);
    this.closedDate = this.endDate;
    this.requestedStartDate = projectBackend.requestedStartDate === null ? null : moment.utc(projectBackend.requestedStartDate, BACKEND_DATE_FORMAT);
    this.archivedDate = projectBackend.archivedDate === null ? null : moment.utc(projectBackend.archivedDate, BACKEND_DATE_FORMAT);
    this.status = projectBackend.status;
    this.statusConfig = PROJECT_STATUSES_CONFIG[projectBackend.status];
    this.statusMessage = projectBackend.statusMessage;
    this.matchesCount = projectBackend.matchesCount;
  }
}

export interface IProjectLanguageBackend {
  id: string;
  language: string;
  level: string;
}


export interface IProjectLocationBackend {
  name: string;
  postcode: number;
  city: string;
}

export interface IProjectDetailsBackend {
  id: string;
  sfid: string;
  name: string;
  startDate: string;
  endDate: string;
  sentDate: string;
  status: ProjectStatus;
  statusMessage: string;
  maxBudget: number;
  expertise: string;

  description: string;
  tags: string[];
  daysPerWeek: number;
  dayFee: number;
  homeWork: boolean;
  positiveSkills: string[];
  projectLanguages: IProjectLanguageBackend[];

  teaser1: string;
  teaser2: string;
  teaser3: string;

  postalCode: string;
  country: string;
  state: string;
  city: string;
  requestedStartDate: string;
  estimatedProjectDuration: number;
  archivedDate: string;
}

export class ProjectDetails {
  id: string;
  sfid: string;
  name: string;
  startDate: Moment;
  endDate: Moment;
  sentDate: Moment;
  closedDate: Moment;
  requestedStartDate: Moment;
  archivedDate: Moment;
  status: ProjectStatus;
  statusConfig: ProjectStatusConfig;
  statusMessage: string;
  budget: number;
  expertise: string;
  description: string;
  tags: string[];
  durationWeeks: number;
  requiredDaysPerWeek: number;
  dayFee: number;
  location: IProjectLocationBackend;
  workFromHomePossible: boolean;
  positiveSkills: string[];
  // niceToHaveSkills: IProjectSkillBackend[];
  // needToHaveTools: IProjectSkillBackend[];
  // niceToHaveTools: IProjectSkillBackend[];
  projectLanguages: IProjectLanguageBackend[];

  constructor(projectDetailsBackend: IProjectDetailsBackend) {
    if (projectDetailsBackend.state != null && projectDetailsBackend.postalCode != null) {
      var _name = projectDetailsBackend.state;
      //var _name = projectDetailsBackend.state + ' (' + projectDetailsBackend.postalCode + ')';
      var _postCode = Number(projectDetailsBackend.postalCode)
    } else {
      var _name = '-';
      var _postCode = 0;
    }
    if (projectDetailsBackend.city != null) {
      var _city = projectDetailsBackend.city;
    } else {
      var _city = '';
    }

    this.id = projectDetailsBackend.id;
    this.sfid = projectDetailsBackend.sfid;
    this.name = projectDetailsBackend.name;
    this.startDate = projectDetailsBackend.startDate === null ? null : moment.utc(projectDetailsBackend.startDate, BACKEND_DATE_FORMAT);
    this.endDate = projectDetailsBackend.endDate === null ? null : moment.utc(projectDetailsBackend.endDate, BACKEND_DATE_FORMAT);
    this.sentDate = projectDetailsBackend.sentDate === null ? null : moment.utc(projectDetailsBackend.sentDate, BACKEND_DATE_FORMAT);
    this.requestedStartDate = projectDetailsBackend.requestedStartDate === null ? null : moment.utc(projectDetailsBackend.requestedStartDate, BACKEND_DATE_FORMAT);
    this.archivedDate = projectDetailsBackend.archivedDate === null ? null : moment.utc(projectDetailsBackend.archivedDate, BACKEND_DATE_FORMAT);

    this.closedDate = this.endDate;
    this.status = projectDetailsBackend.status;
    this.statusConfig = PROJECT_STATUSES_CONFIG[projectDetailsBackend.status];
    this.statusMessage = projectDetailsBackend.statusMessage;
    this.budget = projectDetailsBackend.maxBudget;

    this.description = projectDetailsBackend.description === null ? "" : projectDetailsBackend.description;
    this.tags = [projectDetailsBackend.teaser1 === null ? "" : projectDetailsBackend.teaser1, projectDetailsBackend.teaser2 === null ? "" : projectDetailsBackend.teaser2, projectDetailsBackend.teaser3 === null ? "" : projectDetailsBackend.teaser3];
    this.location = {name: _name, postcode: _postCode, city: _city};

    this.durationWeeks = projectDetailsBackend.estimatedProjectDuration === null ? 0 : projectDetailsBackend.estimatedProjectDuration;
    this.requiredDaysPerWeek = projectDetailsBackend.daysPerWeek === null ? 0 : projectDetailsBackend.daysPerWeek;
    this.dayFee = projectDetailsBackend.maxBudget === null ? 0 : projectDetailsBackend.maxBudget;
    this.workFromHomePossible = projectDetailsBackend.homeWork;

    this.expertise = projectDetailsBackend.expertise;

    // const skills = projectDetailsBackend.skills.filter(s => s.type === SkillType.SPECIALIZATION);
    // const tools = projectDetailsBackend.skills.filter(s => s.type === SkillType.TOOLS);

    this.positiveSkills = projectDetailsBackend.positiveSkills;
    // this.niceToHaveSkills = skills.filter(s => s.requirement === SkillRequirement.OPTIONAL);
    // this.needToHaveTools = tools.filter(t => t.requirement === SkillRequirement.REQUIRED);
    // this.niceToHaveTools = tools.filter(t => t.requirement === SkillRequirement.OPTIONAL);
    this.projectLanguages = projectDetailsBackend.projectLanguages;
  }

  getLocationDisplay() {
    return `${this.location.postcode} (${this.location.city})  ${this.location.name} `;
    //return `${this.location.name}`;
  }
}

export class _TempSkill {
  id: string;
  rating: number;
  requirement: string;

  constructor(id: string, rating: number, requirement: string) {
    this.id = id;
    this.rating = rating;
    this.requirement = requirement;
  }
}


export class _TempProjectLanguage {
  id: string;
  language: string;
  level: string;

  constructor(id: string, language: string, level: string) {
    this.id = id;
    this.language = this.mapLanguage(language);
    this.level = level;
  }
  private mapLanguage(language: string) : string {
    if(language === 'Nederlands') {
      language = "nl-BE";
    } else if(language === 'Frans') {
      language = "fr-FR";
    } else if(language === 'Engels') {
      language = "en-US";
    }else if(language === 'Duits') {
      language = "German";
    }else if(language === 'Spaans') {
      language = "Spanish";
    }else if(language === 'Italiaans') {
      language = "Italian";
    }
    return language;
  }
}

export interface ProjectSaveRequestData {
  name: string;
  description: string;
  teaser1: string;
  teaser2: string;
  teaser3: string;
  daysPerWeek: number;
  estimatedProjectDuration: number;
  startDate: string; // YYYY-MM-DD
  postalCode: string;
  city: string;
  homeWork: boolean;
  dailyFee: number;
  draft: boolean;
  expertise: string;
  state: string;
  maxBudget: number;
  positiveSkills: string[];
  projectLanguages: _TempProjectLanguage[];

}

export enum ProjectFormMode {
  CREATE = 'CREATE',
  EDIT = 'EDIT',
  COPY = 'COPY'
}
