import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { ProjectDetails } from '../../project.model';

@Component({
  selector: 'app-project-details-summary',
  templateUrl: './project-details-summary.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProjectDetailsSummaryComponent implements OnInit {

  @Input() project: ProjectDetails;

  showFullDescription = false;
  descriptionLengthBreakpoint = 610;

  constructor() {

  }

  ngOnInit(): void {

  }

  toggleDescription() {
    this.showFullDescription = !this.showFullDescription;
  }

  showDescriptionToggle(): boolean {
    return this.project.description.length > this.descriptionLengthBreakpoint;
  }

  getDescription(): string {
    if (this.showDescriptionToggle() && !this.showFullDescription) {
      return this.project.description.slice(0, this.descriptionLengthBreakpoint) + '...';
    }

    return this.project.description;
  }

}
