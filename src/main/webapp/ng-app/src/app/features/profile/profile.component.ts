import {Component, Input, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {ModalService} from 'src/app/modals/modal.service';
import { CompanyProfile, CoworkerProfile, UserProfile } from './profile.model';
import { ActivatedRoute } from '@angular/router';
import { ProfileService } from 'src/app/features/profile/profile.service';
import { BehaviorSubject, forkJoin, Observable, of, throwError } from 'rxjs';
import { catchError, map, shareReplay, startWith, tap } from 'rxjs/operators';
import { StateService } from 'src/app/core/services/state.service';
import { CompanyService } from 'src/app/core/services/company-format.service';
import { CompanyFormat } from 'src/app/core/models/company-format';
import { State } from 'src/app/core/models/states';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html'
})
export class ProfileComponent implements OnInit {

  userProfile: UserProfile;
  companyFormats: CompanyFormat[];
  states: State[];
  companyProfile: CompanyProfile;
  coworkerProfiles: CoworkerProfile[];
  userProfile$ = Promise.resolve(false);
  states$ = Promise.resolve(false);
  companyFormats$ = Promise.resolve(false);
  companyProfile$ = Promise.resolve(false);
  coworkerProfiles$ = Promise.resolve(false);
  loadingProfile$: Observable<boolean>;
  loadingUserProfileFailed$: BehaviorSubject<boolean> = new BehaviorSubject(false);
  loadingStatesFailed$: BehaviorSubject<boolean> = new BehaviorSubject(false);
  loadingCompanyFormatsFailed$: BehaviorSubject<boolean> = new BehaviorSubject(false);
  loadingCompanyProfileFailed$: BehaviorSubject<boolean> = new BehaviorSubject(false);
  loadingCoworkersProfileFailed$: BehaviorSubject<boolean> = new BehaviorSubject(false);
  mainMenuExpanded = false;

  perfectScrollbarConfig = {
    suppressScrollX: true
  };

  constructor(
    private activatedRoute: ActivatedRoute,
    private translate: TranslateService,
    private modalService: ModalService,
    private profileService: ProfileService,
    private stateService: StateService,
    private companyService: CompanyService) { }


  ngOnInit(): void {
    // this.activatedRoute.paramMap
    //   .subscribe(paramMap => {
    //     this.userProfile$ = this.profileService.getUserProfile()
    //       .pipe(
    //         tap((userProfile) => {
    //           this.userProfile = userProfile;
    //         }),
    //         catchError((error) => {
    //           this.loadingUserProfileFailed$.next(true);
    //           return throwError(error);
    //         })
    //       );
    //   });

    //   this.activatedRoute.paramMap
    //   .subscribe(paramMap => {
    //     this.coworkerProfiles$ = this.profileService.getCoworkersProfiles()
    //       .pipe(
    //         tap((coworkerProfiles) => {
    //           this.coworkerProfiles = [];
    //           //this.coworkerProfiles = coworkerProfiles;
    //         }),
    //         catchError((error) => {
    //           this.loadingCoworkersProfileFailed$.next(true);
    //           return throwError(error);
    //         })
    //       );
    //   });

    //   this.activatedRoute.paramMap
    //   .subscribe(paramMap => {
    //     this.companyProfile$ = this.profileService.getCompanyProfile()
    //       .pipe(
    //         tap((companyProfile) => {
    //           this.companyProfile = companyProfile;
    //         }),
    //         catchError((error) => {
    //           this.loadingCompanyProfileFailed$.next(true);
    //           return throwError(error);
    //         })
    //       );
    //   });

    //   this.activatedRoute.paramMap
    //   .subscribe(paramMap => {
    //     this.companyFormats$ = this.companyService.getAllCompanyFormats()
    //       .pipe(
    //         tap((companyFormats) => {
    //           this.companyFormats = companyFormats;
    //         }),
    //         catchError((error) => {
    //           this.loadingCompanyFormatsFailed$.next(true);
    //           return throwError(error);
    //         })
    //       );
    //   });

    //   this.activatedRoute.paramMap
    //   .subscribe(paramMap => {
    //     this.states$ = this.stateService.getAllStates()
    //       .pipe(
    //         tap((states) => {
    //           this.states = states;
    //         }),
    //         catchError((error) => {
    //           this.loadingStatesFailed$.next(true);
    //           return throwError(error);
    //         })
    //       );
    //   });

      forkJoin(
        {
          userProfile: this.profileService.getUserProfile(),
          companyFormats: this.companyService.getAllCompanyFormats(),
          states: this.stateService.getAllStates(),
          companyProfile: this.profileService.getCompanyProfile(),
          coworkerProfiles: this.profileService.getCoworkersProfiles()
        }
      )
      .subscribe(({userProfile,companyFormats, states,companyProfile,coworkerProfiles})=>{
        this.userProfile = userProfile;
        this.companyFormats = companyFormats;
        this.states = states;
        this.companyProfile = companyProfile;
        this.coworkerProfiles = coworkerProfiles;

        this.userProfile$ = Promise.resolve(true);
        this.companyFormats$ = Promise.resolve(true);
        this. states$ = Promise.resolve(true);
        this.companyProfile$ = Promise.resolve(true);
        this.coworkerProfiles$ = Promise.resolve(true);
      })


    // this.loadingProfile$ = (this.userProfile$ && this.companyProfile$ && this.coworkerProfiles$ && this.companyFormats$ && this.states$)
    //   .pipe(
    //     map(result => {
    //       return false;
    //     }),
    //     catchError((error) => {
    //       return of(false);
    //     }),
    //     startWith(true)
    //   );
  }

  toggleMenu() {
    this.mainMenuExpanded = !this.mainMenuExpanded;
  }

  editDetails(userDetails: boolean) {
    if (userDetails) {
      const userProfile = this.userProfile;
      const confirmBtnText = this.translate.instant('profiles.userProfilesModal.confirmBtnText');
      const modalRef = this.modalService.openUserDetailsModal(this.userProfile, null, this.states, confirmBtnText);
    }
    else {
      const title = this.translate.instant('profiles.companyProfilesModal.title');
      const confirmBtnText = this.translate.instant('profiles.companyProfilesModal.confirmBtnText');
      const modalRef = this.modalService.openCompanyDetailsModal(this.companyProfile, null, this.companyFormats, this.states, confirmBtnText);
    }
  }

}
