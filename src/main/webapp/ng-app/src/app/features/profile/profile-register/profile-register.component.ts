import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {AccountService} from '../../../core/auth/account.service';
import {TranslateService} from '@ngx-translate/core';
import {LoginService} from '../../../core/login/login.service';
import {Router} from '@angular/router';
import {AbstractControl, AsyncValidatorFn, FormBuilder, FormControl, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {IRegisterProfileRequest} from '../profile.model';
import {catchError, map, shareReplay} from 'rxjs/operators';
import {BehaviorSubject, Observable, throwError} from 'rxjs';
import {ProfileService} from '../profile.service';
import {ErrorHandlingService} from '../../../core/error-handling/error-handling.service';
import {AppValidators} from '../../../shared/util/validators';
import {LANGUAGES} from "../../../core/language/language.constants";
import {JhiLanguageService} from "ng-jhipster";
import {SessionStorageService} from "ngx-webstorage";

enum UserProfileRegisterFormFields {
  NAME = 'userProfileNameControl',
  SURNAME = 'userProfileSurnameControl',
  PHONE = 'userProfilePhoneControl',
  COMPANY_NAME = 'userProfileCompanyNameControl',
  VAT_NUMBER = 'userProfileVatControl',
  NEWSLETTER = 'userProfileNewsletter',
  ACCEPT_TERMS = 'userProfileAcceptTermsControl',
  ACCEPT_PRIVACY = 'userProfileAcceptPrivacyControl'
}

enum RegistrationStep {
  PROFILE = 'PROFILE',
  TERMS = 'TERMS',
  PRIVACY = 'PRIVACY'
}

@Component({
  selector: 'app-profile-register',
  templateUrl: './profile-register.component.html',
  styleUrls: ['./profile-register.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProfileRegisterComponent implements OnInit {

  languages = LANGUAGES;

  account: Account | null = null;
  form: FormGroup = this.buildForm();
  currentRegistrationStep = RegistrationStep.PROFILE;
  registrationSteps = RegistrationStep;
  privacyPolicy$: Observable<string>;
  userAgreement$: Observable<string>;
  actionRunning$: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor(public accountService: AccountService, private translate: TranslateService,
              private sessionStorage: SessionStorageService, private languageService: JhiLanguageService,
              private loginService: LoginService, private router: Router, private formBuilder: FormBuilder,
              private profileService: ProfileService, private errorHandlingService: ErrorHandlingService) {
  }

  changeLanguage(languageKey: string): void {
    this.sessionStorage.store('locale', languageKey);
    this.languageService.changeLanguage(languageKey);
  }

  ngOnInit(): void {
    this.privacyPolicy$ = this.profileService.getPrivacyPolicy().pipe(shareReplay());
    this.userAgreement$ = this.profileService.getUserAgreement().pipe(shareReplay());
  }

  logout(): void {
    this.loginService.logout();
    this.router.navigate(['']);
  }

  private buildForm(): FormGroup {
    const userProfileNameControl = this.formBuilder.control('', [Validators.required, Validators.maxLength(100)]);
    const userProfileSurnameControl = this.formBuilder.control('', [Validators.required, Validators.maxLength(100)]);
    const userProfilePhoneControl = this.formBuilder.control(
      '',
      [Validators.required, AppValidators.phoneNumberValidator(), Validators.maxLength(100)]);
    const userProfileVatControl = this.formBuilder.control('', [Validators.required, AppValidators.vatNumberValidator(), Validators.minLength(1), Validators.maxLength(100)]);
    const userProfileCompanyNameControl = this.formBuilder.control('', [Validators.required, Validators.maxLength(100)]);
    const userProfileNewsletterControl = this.formBuilder.control(false);
    const userProfileAcceptTermsControl = this.formBuilder.control(false);
    const userProfileAcceptPrivacyControl = this.formBuilder.control(false);

    return this.formBuilder.group({
      [UserProfileRegisterFormFields.NAME]: userProfileNameControl,
      [UserProfileRegisterFormFields.SURNAME]: userProfileSurnameControl,
      [UserProfileRegisterFormFields.PHONE]: userProfilePhoneControl,
      [UserProfileRegisterFormFields.COMPANY_NAME]: userProfileCompanyNameControl,
      [UserProfileRegisterFormFields.VAT_NUMBER]: userProfileVatControl,
      [UserProfileRegisterFormFields.NEWSLETTER]: userProfileNewsletterControl,
      [UserProfileRegisterFormFields.ACCEPT_TERMS]: userProfileAcceptTermsControl,
      [UserProfileRegisterFormFields.ACCEPT_PRIVACY]: userProfileAcceptPrivacyControl
    });
  }

  next() {
    if (this.currentRegistrationStep === RegistrationStep.PROFILE) {
      if (this.form.valid) {
        this.currentRegistrationStep = RegistrationStep.TERMS;
      } else {
        this.validateAllFormFields(this.form);
      }
    } else if (this.currentRegistrationStep === RegistrationStep.TERMS) {
      this.currentRegistrationStep = RegistrationStep.PRIVACY;
    } else if (this.currentRegistrationStep === RegistrationStep.PRIVACY) {
      this.registerUser();
    }
  }

  goToPreviousStep() {
    if (this.currentRegistrationStep === RegistrationStep.TERMS) {
      this.currentRegistrationStep = RegistrationStep.PROFILE;
    } else if (this.currentRegistrationStep === RegistrationStep.PRIVACY) {
      this.currentRegistrationStep = RegistrationStep.TERMS;
    }
  }

  confirmDisabled(): boolean {
    if (this.actionRunning$.value) {
      return true;
    }

    if (this.currentRegistrationStep === RegistrationStep.TERMS) {
      return !this.form.get(UserProfileRegisterFormFields.ACCEPT_TERMS).value;
    } else if (this.currentRegistrationStep === RegistrationStep.PRIVACY) {
      return !this.form.get(UserProfileRegisterFormFields.ACCEPT_PRIVACY).value;
    }

    return false;
  }

  private registerUser() {
    const request: IRegisterProfileRequest = {
      firstName: this.getUserProfileNameControl().value,
      lastName: this.getUserProfileSurNameControl().value,
      phone: this.getUserProfilePhoneControl().value,
      companyName: this.getUserProfileCompanyNameControl().value,
      vatNumber: this.getUserProfileVatControl().value,
      subscribeToNewsletter: this.form.get(UserProfileRegisterFormFields.NEWSLETTER).value,
      terms: this.form.get(UserProfileRegisterFormFields.ACCEPT_TERMS).value,
      privacy: this.form.get(UserProfileRegisterFormFields.ACCEPT_PRIVACY).value
    };

    this.actionRunning$.next(true);

    this.profileService.registerProfile(request)
      .pipe(
        catchError(error => {
          this.actionRunning$.next(false);
          this.errorHandlingService.openErrorModal(this.translate.instant('profiles.registrationFailed'));
          return throwError(error);
        })
      )
      .subscribe(() => {
        this.fetchUserAndGoToApp();
      });
  }

  private fetchUserAndGoToApp() {
    this.accountService.identity(true)
      .subscribe(() => {
        window.location.href = '/';
      });
  }

  /* Control Getters */

  getUserProfileNameControl(): FormControl {
    return this.form.get(UserProfileRegisterFormFields.NAME) as FormControl;
  }

  getUserProfileSurNameControl(): FormControl {
    return this.form.get(UserProfileRegisterFormFields.SURNAME) as FormControl;
  }

  getUserProfilePhoneControl(): FormControl {
    return this.form.get(UserProfileRegisterFormFields.PHONE) as FormControl;
  }

  getUserProfileCompanyNameControl(): FormControl {
    return this.form.get(UserProfileRegisterFormFields.COMPANY_NAME) as FormControl;
  }

  getUserProfileVatControl(): FormControl {
    return this.form.get(UserProfileRegisterFormFields.VAT_NUMBER) as FormControl;
  }

  /* Error Getters */

  getUserProfileNameErrorMsg(): string {
    const errors = this.getUserProfileNameControl().errors;

    if (errors) {
      if (errors.maxlength) {
        return this.translate.instant(
          'profiles.user-profile-form-validation.userNameMaxLength', {maxLength: errors.maxlength.requiredLength});
      }
    }

    return null;
  }

  getUserProfileSurnameErrorMsg(): string {
    const errors = this.getUserProfileSurNameControl().errors;

    if (errors) {
      if (errors.maxlength) {
        return this.translate.instant(
          'profiles.user-profile-form-validation.userSurnameMaxLength', {maxLength: errors.maxlength.requiredLength});
      }
    }

    return null;
  }

  getUserProfilePhoneErrorMsg(): string {
    const errors = this.getUserProfilePhoneControl().errors;

    if (errors && !errors['required']) {
      if (errors.maxlength) {
        return this.translate.instant(
          'profiles.user-profile-form-validation.userPhoneMaxLength', {maxLength: errors.maxlength.requiredLength});
      } else if (errors.phoneNumber) {
        return this.translate.instant(
          'profiles.user-profile-form-validation.userPhonePattern');
      }
    }

    return null;
  }

  getUserProfileCompanyNameErrorMsg(): string {
    const errors = this.getUserProfileCompanyNameControl().errors;

    if (errors) {
      if (errors.maxlength) {
        return this.translate.instant(
          'profiles.company-profile-form-validation.companyNameMaxLength', {maxLength: errors.maxlength.requiredLength});
      }
    }

    return null;
  }

  getUserProfileVatErrorMsg(): string {
    const errors = this.getUserProfileVatControl().errors;

    if (errors) {
      if (errors.maxlength) {
        return this.translate.instant(
          'profiles.company-profile-form-validation.companyVatMaxLength', {maxLength: errors.maxlength.requiredLength});
      } else if (errors.vatNumber) {
        return this.translate.instant(
          'profiles.company-profile-form-validation.companyVatFormat');
      }
    }

    return null;
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({onlySelf: true});
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  vatDuplicateValidator(): AsyncValidatorFn {
    return (control: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> => {
      if (!!control.value) {
        return this.profileService.getDuplicateVATNumber(control.value).pipe(
          map((value: any) => {
            return value ? {duplicateVat: control.value} : null
          })
        )
      }
    };
  }
}
