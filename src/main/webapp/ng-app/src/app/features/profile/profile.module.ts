import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {LayoutsModule} from 'src/app/layouts/layouts.module';
import {SharedModule} from 'src/app/shared/shared.module';
import {RouterModule} from '@angular/router';
import {routes} from './profile-routes';
import {ProfileComponent} from './profile.component';
import {ProfileRegisterComponent} from './profile-register/profile-register.component';

@NgModule({
  imports: [
    SharedModule,
    LayoutsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    ProfileComponent,
    ProfileRegisterComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GigProfileModule {
}
