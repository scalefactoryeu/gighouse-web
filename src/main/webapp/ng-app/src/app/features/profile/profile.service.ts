import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import {
  CompanyProfile,
  CoworkerProfile,
  ICompanyProfileBackend,
  ICoworkerProfileBackend,
  IRegisterProfileRequest,
  IUserProfileBackend,
  UserProfile
} from './profile.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { PaginatedData } from 'src/app/core/models/model';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(private httpClient: HttpClient) {
  }

  getUserProfile(): Observable<UserProfile> {
    return this.httpClient.get<IUserProfileBackend>(`/api/profile/user`)
      .pipe(
        map((userProfileBackend: IUserProfileBackend) => {
          return new UserProfile(userProfileBackend);
        })
      );
  }

  updateUserProfile(userProfile: UserProfile): Observable<UserProfile> {
    return this.httpClient.put<IUserProfileBackend>(`/api/profile/user`, userProfile)
      .pipe(
        // delay(2000),
        map((userProfileBackend: IUserProfileBackend) => {
          return new UserProfile(userProfileBackend);
          // throw new Error();
        })
      );
  }

  registerProfile(registerProfileRequest: IRegisterProfileRequest): Observable<void> {
    return this.httpClient.post<void>(`/api/profile/register`, registerProfileRequest);
  }

  getCompanyProfile(): Observable<CompanyProfile> {
    return this.httpClient.get<ICompanyProfileBackend>(`/api/profile/company`)
      .pipe(
        map((companyProfileBackend: ICompanyProfileBackend) => {
          return new CompanyProfile(companyProfileBackend);
        })
      );
  }

  updateCompanyProfile(companyProfile: CompanyProfile): Observable<CompanyProfile> {
    return this.httpClient.put<ICompanyProfileBackend>(`/api/profile/company`, companyProfile)
      .pipe(
        // delay(2000),
        map((companyProfileBackend: ICompanyProfileBackend) => {
          return new CompanyProfile(companyProfileBackend);
          // throw new Error();
        })
      );
  }

  // getCoworkersProfiles(): Observable<CoworkerProfile[]> {

  //   const req: Pagination = {page: 0, size: 1000, sort: null};
  //   const params: HttpParams = createRequestOption(req);

  //   return this.httpClient.get<ICoworkerProfileBackend[]>(`/api/profile/coworkers`, {params})
  //     .pipe(
  //       map((coworkersProfileBackend: ICoworkerProfileBackend[]) => {
  //         return coworkersProfileBackend.map(coworker => new CoworkerProfile(coworker));
  //       })
  //     );
  // }



  getCoworkersProfiles(): Observable<CoworkerProfile[]> {
    return this.getCoworkersPaginated(0, 1000)
      .pipe(
        map((coworkersPaginated) => {
          return coworkersPaginated.items;
        })
      );
  }

  getCoworkersPaginated(page: number, pageSize: number): Observable<PaginatedData<CoworkerProfile>> {
    let params = new HttpParams();
    params = params.append('page', page.toString());
    params = params.append('size', pageSize.toString());

    return this.httpClient.get<ICoworkerProfileBackend[]>(`/api/profile/coworkers`, {observe: 'response', params})
      .pipe(
        map((res: HttpResponse<ICoworkerProfileBackend[]>) => {
          const coworkers = res.body.map(coworker => new CoworkerProfile(coworker));
          return new PaginatedData<CoworkerProfile>(res, coworkers, page);
        })
      );
  }

  getPrivacyPolicy(): Observable<string> {
    return this.httpClient.get<{privacyPolicy: string}>(`/api/profile/privacy-policy`)
      .pipe(
        map(resp => {
          return resp.privacyPolicy;
        })
      );
  }

  getUserAgreement(): Observable<string> {
    return this.httpClient.get<{userAgreement: string}>(`/api/profile/user-agreement`)
      .pipe(
        map(resp => {
          return resp.userAgreement;
        })
      );
  }

  getDuplicateVATNumber(vatNumber : string) : Observable<boolean> {
      return this.httpClient.get<boolean>(`/api/profile/existing-vat-number/${vatNumber}`);
  }
}
