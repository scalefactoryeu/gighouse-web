

export interface IUserProfileBackend {
    firstName: string;
    lastName: string;
    street: string;
    postalCode: string;
    city: string;
    state: string;
    country: string;
    email: string;
    phone: string;
}

export class UserProfile {
    firstName: string;
    lastName: string;
    street: string;
    postalCode: string;
    city: string;
    state: string;
    country: string;
    email: string;
    phone: string;

    constructor(userProfileBackend: IUserProfileBackend){
        this.firstName = userProfileBackend.firstName === null ? null : userProfileBackend.firstName;
        this.lastName = userProfileBackend.lastName === null ? null : userProfileBackend.lastName;
        this.street = userProfileBackend.street === null ? null : userProfileBackend.street;
        this.postalCode = userProfileBackend.postalCode === null ? null : userProfileBackend.postalCode;
        this.city = userProfileBackend.city === null ? null : userProfileBackend.city;
        this.state = userProfileBackend.state === null ? null : userProfileBackend.state;
        this.country = userProfileBackend.country === null ? null : userProfileBackend.country;
        this.email = userProfileBackend.email === null ? null : userProfileBackend.email;
        this.phone = userProfileBackend.phone === null ? null : userProfileBackend.phone;
    }
}

export interface ICompanyProfileBackend{
    name: string;
    companyFormat: string;
    street: string;
    postalCode: string;
    city: string;
    state: string;
    country: string;
    invoiceEmail: string;
    phone: string;
    vatNumber: string;
}

export class CompanyProfile {
    name: string;
    companyFormat: string;
    street: string;
    postalCode: string;
    city: string;
    state: string;
    country: string;
    invoiceEmail: string;
    phone: string;
    vatNumber: string;

    constructor(companyProfileBackend: ICompanyProfileBackend){
        this.name = companyProfileBackend.name === null ? null : companyProfileBackend.name;
        this.companyFormat = companyProfileBackend.companyFormat === null ? null : companyProfileBackend.companyFormat;
        this.street = companyProfileBackend.street === null ? null : companyProfileBackend.street;
        this.postalCode = companyProfileBackend.postalCode === null ? null : companyProfileBackend.postalCode;
        this.city = companyProfileBackend.city === null ? null : companyProfileBackend.city;
        this.state = companyProfileBackend.state === null ? null : companyProfileBackend.state;
        this.country = companyProfileBackend.country === null ? null : companyProfileBackend.country;
        this.invoiceEmail = companyProfileBackend.invoiceEmail === null ? null : companyProfileBackend.invoiceEmail;
        this.phone = companyProfileBackend.phone === null ? null : companyProfileBackend.phone;
        this.vatNumber = companyProfileBackend.vatNumber === null ? null : companyProfileBackend.vatNumber;
    }
}

export interface ICoworkerProfileBackend {
    firstName: string;
    lastName: string;
    street: string;
    postalCode: string;
    city: string;
    state: string;
    email: string;
    phone: string;
}

export class CoworkerProfile {
    name: string;
    surname: string;
    street: string;
    postalCode: string;
    city: string;
    state: string;
    email: string;
    phone: string;

    constructor(coworkerProfileBackend: ICoworkerProfileBackend){
        this.name = coworkerProfileBackend.firstName === null ? null : coworkerProfileBackend.firstName;
        this.surname = coworkerProfileBackend.lastName === null ? null : coworkerProfileBackend.lastName;
        this.street = coworkerProfileBackend.street === null ? null : coworkerProfileBackend.street;
        this.postalCode = coworkerProfileBackend.postalCode === null ? null : coworkerProfileBackend.postalCode;
        this.city = coworkerProfileBackend.city === null ? null : coworkerProfileBackend.city;
        this.state = coworkerProfileBackend.state === null ? null : coworkerProfileBackend.state;
        this.email = coworkerProfileBackend.email === null ? null : coworkerProfileBackend.email;
        this.phone = coworkerProfileBackend.phone === null ? null : coworkerProfileBackend.phone;
    }
}

export interface IRegisterProfileRequest {
  firstName: string;
  lastName: string;
  phone: string;
  companyName: string;
  vatNumber: string;
  subscribeToNewsletter: boolean;
  terms: boolean;
  privacy: boolean;
}
