import {Routes} from '@angular/router';
import {ProfileComponent} from './profile.component';
import {ProfileRegisterComponent} from './profile-register/profile-register.component';
import {Authority} from '../../shared/constants/authority.constants';
import {AppLayoutComponent} from '../../layouts/app-layout/app-layout.component';
import {UserRouteAccessService} from '../../core/auth/user-route-access-service';

export const routes: Routes = [
  {
    path: '',
    component: AppLayoutComponent,
    data: {
      authorities: []
    },
    children: [
      {
        path: '',
        component: ProfileComponent,
        data: {
          authorities: [],
          pageTitle: 'profiles.myProfile'
        }
      },
    ]
  },
  {
    path: 'register',
    component: ProfileRegisterComponent,
    data: {
      // user needs to be unregistered to enter this page
      authorities: [Authority.ROLE_UNREGISTERED],
      pageTitle: 'profiles.register'
    },
    canActivate: [UserRouteAccessService]
  }
];
