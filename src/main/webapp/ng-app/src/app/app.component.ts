import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {AccountService} from './core/auth/account.service';
import {ChatService} from './core/chat/chat.service';
import {SessionStorageService} from "ngx-webstorage";
import {JhiLanguageService} from "ng-jhipster";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {

  constructor(router: Router, private accountService: AccountService, private chatService: ChatService,
              private languageService: JhiLanguageService,
              private sessionStorage: SessionStorageService) {
    router.events.subscribe((evt) => {
      window.scrollTo(0, 0);
    });
    const DEFAULT_LANGUAGE = 'nl';

    let locale = this.sessionStorage.retrieve('locale');
    if (locale) {
      this.languageService.changeLanguage(locale);
    } else {
      this.sessionStorage.store('locale', DEFAULT_LANGUAGE);
      this.languageService.changeLanguage(DEFAULT_LANGUAGE);
    }

    this.accountService.identity()
      .subscribe(account => {
        if (account) {
          this.chatService.initChatService();
        }
      });
  }
}
