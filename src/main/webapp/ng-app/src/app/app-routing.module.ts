import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ErrorComponent} from './layouts/error/error.component';
import {HomeComponent} from './features/home/home.component';
import {Authority} from './shared/constants/authority.constants';
import {UserRouteAccessService} from './core/auth/user-route-access-service';
import {AppLayoutComponent} from './layouts/app-layout/app-layout.component';
import {PendingComponent} from "./layouts/pending/pending.component";

const routes: Routes = [
  {
    path: '',
    component: AppLayoutComponent,
    data: {
      authorities: [],
    },
    canActivate: [UserRouteAccessService],
    children: [
      {
        path: '',
        component: HomeComponent,
        data: {
          authorities: [],
          pageTitle: 'home.title'
        }
      },
      {
        path: 'projects',
        loadChildren: () => import('./features/project/project.module').then(m => m.GigProjectModule),
        data: {
          authorities: [],
        }
      },
      {
        path: 'freelancers',
        loadChildren: () => import('./features/freelancer/freelancer.module').then(m => m.FreelancerModule),
        data: {
          authorities: [],
        }
      },
      {
        path: 'administration',
        loadChildren: () => import('./features/administration/administration-routing.module').then(m => m.AdministrationRoutingModule),
        data: {
          authorities: [],
        }
      }
    ]
  },
  {
    path: 'profiles',
    loadChildren: () => import('./features/profile/profile.module').then(m => m.GigProfileModule),
    data: {
      authorities: []
    },
    canActivate: [UserRouteAccessService]
  },
  // {
  //   path: 'chat',
  //   loadChildren: () => import('./features/chat/chat.module').then(m => m.ChatModule),
  //   data: {
  //     authorities: [],
  //     pageTitle: 'chat.title'
  //   },
  //   canActivate: [UserRouteAccessService]
  // },
  {
    path: 'sys-admin',
    component: AppLayoutComponent,
    data: {
      authorities: [Authority.ADMIN],
    },
    canActivate: [UserRouteAccessService],
    children: [
      {
        path: '',
        loadChildren: () => import('./features/sys-admin/sys-admin-routing.module').then(m => m.SysAdminRoutingModule),
      }
    ]
  },
  {
    path: 'error',
    component: ErrorComponent,
    data: {
      authorities: [],
      pageTitle: 'error.title',
    },
  },
  {
    path: 'accessdenied',
    component: ErrorComponent,
    data: {
      authorities: [],
      pageTitle: 'error.title',
      errorMessage: 'error.http.403',
    },
  },
  {
    path: '404',
    component: ErrorComponent,
    data: {
      authorities: [],
      pageTitle: 'error.title',
      errorMessage: 'error.http.404',
    },
  }, {
    path: 'pending',
    component: PendingComponent,
    data: {
      authorities: [],
      pageTitle: 'error.title',
      errorMessage: 'error.http.404',
    },
  },
  {
    path: '**',
    redirectTo: '/404',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
