import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../core/login/login.service';
import { JhiLanguageService } from 'ng-jhipster';
import { SessionStorageService } from 'ngx-webstorage';
import { AccountService } from '../../core/auth/account.service';
import { Router } from '@angular/router';
import {Observable} from 'rxjs';
import {ChatService} from '../../core/chat/chat.service';
import {debounceTime, map} from 'rxjs/operators';
import {LANGUAGES} from "../../core/language/language.constants";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
})
export class NavbarComponent implements OnInit {

  languages = LANGUAGES;

  unreadMessagesNum$: Observable<number>;

  constructor(
    private loginService: LoginService,
    private languageService: JhiLanguageService,
    private sessionStorage: SessionStorageService,
    private accountService: AccountService,
    private router: Router,
    public chatService: ChatService
  ) {
    this.unreadMessagesNum$ = this.chatService.subscribedChannels$
      .pipe(
        debounceTime(500), // use debounce so the unread indicator doesn't flash for user's own sent message
        map((channels) => {
          if (channels) {
            const channelsArray = Object.values(channels);
            return channelsArray.reduce((accumulated, current) => accumulated + current.unreadMessages, 0);
          }

          return 0;
        })
      );
  }

  ngOnInit() {

  }

  isAuthenticated(): boolean {
    return this.accountService.isAuthenticated();
  }

  login(): void {
    this.loginService.login();
  }

  logout(): void {
    this.loginService.logout();
    this.router.navigate(['']);
  }

  changeLanguage(languageKey: string): void {
    this.sessionStorage.store('locale', languageKey);
    this.languageService.changeLanguage(languageKey);
  }

  // getImageUrl(): string {
  //   return this.isAuthenticated() ? this.accountService.getImageUrl() : '';
  // }
}
