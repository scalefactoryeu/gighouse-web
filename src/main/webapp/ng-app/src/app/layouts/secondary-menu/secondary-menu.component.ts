import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { BaseNavItem, NavItem } from '../../navigation';

@Component({
  selector: 'app-secondary-menu',
  templateUrl: './secondary-menu.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SecondaryMenuComponent implements OnInit {

  @Input() secondaryMenuNavItems: BaseNavItem[];
  @Input() activeMainMenuNavItem: NavItem;

  perfectScrollbarConfig = {
    suppressScrollX: true
  };

  constructor() {
  }

  ngOnInit() {

  }

}
