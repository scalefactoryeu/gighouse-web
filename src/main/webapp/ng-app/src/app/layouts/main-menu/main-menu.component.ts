import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { appNavItems, sysAdminNavItems } from 'src/app/navigation';
import { AccountService } from '../../core/auth/account.service';

@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MainMenuComponent implements OnInit {

  @Input() mainMenuExpanded: boolean;
  @Output() toggleMenu = new EventEmitter();

  appNavItems = appNavItems;
  sysAdminNavItems = sysAdminNavItems;

  perfectScrollbarConfig = {
    suppressScrollX: true
  };

  constructor(private accountService: AccountService) {
  }

  ngOnInit() {

  }

  triggerToggleMenu() {
    this.toggleMenu.next();
  }

  collapseMenu() {
    if (this.mainMenuExpanded) {
      this.toggleMenu.next();
    }
  }

  isUserAdmin(): boolean {
    return this.accountService.hasAnyAuthority('ROLE_ADMIN');
  }
}
