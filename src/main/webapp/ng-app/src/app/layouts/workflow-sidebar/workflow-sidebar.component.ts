import { Observable } from 'rxjs';
import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { GigHouseContact } from 'src/app/core/models/gighouse-contact';
import { GighouseContactService } from 'src/app/core/services/gighouse-contact.service';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-workflow-sidebar',
  templateUrl: './workflow-sidebar.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WorkflowSidebarComponent implements OnInit {

  @Input() showHelpInfo = false;

  gigHouseContact$: Observable<GigHouseContact>;

  private gigHouseContact: GigHouseContact;

  constructor(private gighouseContactServise: GighouseContactService) {
  }

  ngOnInit() {
    this.gigHouseContact$ = this.gighouseContactServise.getGigHouseContact()
      .pipe(
        tap((gigHouseContact) => {
          this.gigHouseContact = gigHouseContact;
        })
      );
  }
}
