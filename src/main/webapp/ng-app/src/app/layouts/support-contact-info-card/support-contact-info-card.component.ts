import { ChangeDetectionStrategy, Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { GigHouseContact } from 'src/app/core/models/gighouse-contact';
import { GighouseContactService } from 'src/app/core/services/gighouse-contact.service';

@Component({
  selector: 'app-support-contact-info-card',
  templateUrl: './support-contact-info-card.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SupportContactInfoCardComponent implements OnInit {

  @Output() changedgigHouseContact = new EventEmitter<GigHouseContact>();

  gigHouseContact$: Observable<GigHouseContact>;
  // loadingGigHouseContact$: Observable<boolean>;
  private gigHouseContact: GigHouseContact;

  constructor(private gighouseContactServise: GighouseContactService) {
  }

  ngOnInit() {
    this.gigHouseContact$ = this.gighouseContactServise.getGigHouseContact()
      .pipe(
        tap((gigHouseContact) => {
          this.gigHouseContact = gigHouseContact;
          this.changedgigHouseContact.next(this.gigHouseContact);
        })
      );

    // this.loadingGigHouseContact$ = this.gigHouseContact$
    //   .pipe(
    //     map(result => {
    //       return false;
    //     }),
    //     catchError((error) => {
    //       return of(false);
    //     }),
    //     startWith(true)
    //   );
  }

}
