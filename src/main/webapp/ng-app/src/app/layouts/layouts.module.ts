import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { ErrorComponent } from './error/error.component';
import { RouterModule } from '@angular/router';
import { AppLayoutComponent } from './app-layout/app-layout.component';
import { MainMenuComponent } from './main-menu/main-menu.component';
import { NavbarComponent } from './navbar/navbar-menu.component';
import { SecondaryMenuComponent } from './secondary-menu/secondary-menu.component';
import { AppLayoutLargeComponent } from './app-layout-large/app-layout-large.component';
import { AppLayoutSmallComponent } from './app-layout-small/app-layout-small.component';
import { ActiveLangMenuDirective } from './navbar/active-lang-menu.directive';
import { WorkflowSidebarComponent } from './workflow-sidebar/workflow-sidebar.component';
import {SupportContactInfoCardComponent} from './support-contact-info-card/support-contact-info-card.component';
import {PendingComponent} from "./pending/pending.component";


const EXPORTED_DECLARATIONS = [
  MainMenuComponent,
  NavbarComponent,
  SupportContactInfoCardComponent,
  ErrorComponent,
  PendingComponent,
  WorkflowSidebarComponent,
  ActiveLangMenuDirective
];

@NgModule({
  imports: [
    RouterModule,
    SharedModule
  ],
  declarations: [
    AppLayoutComponent,
    AppLayoutLargeComponent,
    AppLayoutSmallComponent,
    SecondaryMenuComponent,
    ...EXPORTED_DECLARATIONS
  ],
  exports: [
    ...EXPORTED_DECLARATIONS
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class LayoutsModule {

}
