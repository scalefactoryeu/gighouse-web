import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router, RouterEvent } from '@angular/router';
import { appNavItems, BaseNavItem, NavItem } from '../../navigation';
import {cloneDeep} from 'lodash';

@Component({
  selector: 'app-layout-large',
  templateUrl: './app-layout-large.component.html'
})
export class AppLayoutLargeComponent implements OnInit {

  mainMenuExpanded = false;
  activeMainMenuNavItem: NavItem;
  secondaryMenuNavItems: BaseNavItem[];

  constructor(private router: Router) {}

  ngOnInit() {
    this.listenToRouteChanges();
  }

  toggleMenu() {
    this.mainMenuExpanded = !this.mainMenuExpanded;
  }

  private listenToRouteChanges() {
    this.findSecondaryNavItems(this.router.routerState.snapshot.url);

    this.router.events.subscribe((event: RouterEvent) => {
      if (event instanceof NavigationEnd) {
        const route = event.urlAfterRedirects ? event.urlAfterRedirects : event.url;
        this.findSecondaryNavItems(route);
      }
    });
  }

  private findSecondaryNavItems(route: string) {
    this.activeMainMenuNavItem = null;
    this.secondaryMenuNavItems = null;

    const secondaryNavItems: BaseNavItem[] = [];

    const navItems: NavItem[] = cloneDeep(appNavItems);

    const navItemRouteMatch = navItems.find(navItem => {
      if (navItem.routerLinkActiveExact) {
        return route === navItem.path;
      }
      return route.startsWith(navItem.path);
    });

    this.activeMainMenuNavItem = navItemRouteMatch;

    if (navItemRouteMatch && navItemRouteMatch.children) {
      navItemRouteMatch.children.forEach(childNavItem => {
        childNavItem.path = navItemRouteMatch.path + '/' + childNavItem.path;
        secondaryNavItems.push(childNavItem);
      });
    }

    if (secondaryNavItems.length) {
      this.secondaryMenuNavItems = secondaryNavItems;
    }
  }

}
