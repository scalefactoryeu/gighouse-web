package eu.scalefactory.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties specific to Gighouse.
 * <p>
 * Properties are configured in the {@code application.yml} file.
 * See {@link io.github.jhipster.config.JHipsterProperties} for a good example.
 */
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
@Getter
@Setter
public class ApplicationProperties {

    private Twilio twilio;
    private Domain domain;
    private Invoice invoice;

    @Getter
    @Setter
    public static class Twilio {
        private String instance;
    }

    @Getter
    @Setter
    public static class Domain {
        private Auth auth;
    }

    @Getter
    @Setter
    public static class Auth {
        private String url;
        private String provider;
    }

    @Getter
    @Setter
    public static class Invoice {
        private String url;
        private String username;
        private String password;
    }
}
