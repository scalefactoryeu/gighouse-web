package eu.scalefactory.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "gig__settinggeneral__c", schema = "salesforce")
public class SettingGeneral {

    @Id
    @Column(name = "gig__external_id__c", length = 36)
    @JsonIgnore
    private String externalId;

    @Column(name = "gig__user_agreement__c")
    private String userAgreement;

    @Column(name = "gig__privacy_policy__c")
    private String privacyPolicy;
}


