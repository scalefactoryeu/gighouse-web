package eu.scalefactory.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "gig__contact_language__c", schema = "salesforce")
public class ContactLanguage implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "sfid", length = 36)
    @JsonIgnore
    private String sfid;

    @Column(name = "gig__language__c")
    private String language;

    @Column(name = "gig__level__c")
    private String level;

    @ManyToOne
    @JoinColumn(name = "gig__contact__r__gig__external_id__c")
    @JsonIgnore
    private Contact contact;

}
