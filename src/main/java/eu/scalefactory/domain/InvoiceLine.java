package eu.scalefactory.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "gig__invoice_line__c", schema = "salesforce")
public class InvoiceLine implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "sfid", length = 18)
    private String sfid;

    @Column(name = "isdeleted")
    private boolean isDeleted;

    @Column(name = "name")
    private String name;

    @ManyToOne
    @JoinColumn(name = "gig__project__c", referencedColumnName = "sfid")
    private Project project;

    @ManyToOne
    @JoinColumn(name = "gig__project_member__c")
    private ProjectMember projectMember;

    @ManyToOne
    @JoinColumn(name = "gig__invoice__c")
    private Invoice invoice;

    public String getId() {
        return sfid;
    }
}
