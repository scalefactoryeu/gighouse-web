package eu.scalefactory.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "gig__profile__c", schema = "salesforce")
public class Profile implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "sfid", length = 36)
    @JsonIgnore
    private String sfid;

    @Column(name = "name")
    private String name;

    public String getId() {
        return sfid;
    }

}
