package eu.scalefactory.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "gig__experience__c", schema = "salesforce")
public class Referral {


    @Id
    @Column(name = "sfid", length = 18)
    private String sfid;

    @Column(name = "name")
    private String name;

    @Column(name = "gig__quote__c")
    private String quote;

    @Column(name = "gig__detail__c")
    private String detail;

    @Column(name = "gig__overal_rating__c")
    private String overallRating;

    @ManyToOne
    @JoinColumn(name = "gig__contact__r__gig__external_id__c")
    @JsonIgnore
    private Contact contact;

    @ManyToOne
    @JoinColumn(name = "gig__project__c", referencedColumnName = "sfid")
    private Project project;

    @ManyToOne
    @JoinColumn(name = "gig__profile__c")
    private Profile profile;
}
