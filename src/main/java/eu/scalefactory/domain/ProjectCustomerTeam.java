package eu.scalefactory.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "gig__project_customer_team__c", schema = "salesforce")
public class ProjectCustomerTeam implements Serializable {
    private static final long serialVersionUID = 1L;

    public ProjectCustomerTeam() {
        this.externalId = UUID.randomUUID().toString();
    }


    @Column(name = "sfid", length = 36)
    @JsonIgnore
    private String sfid;

    @Id
    @Column(name = "gig__external_id__c", length = 36)
    @JsonIgnore
    private String externalId;

    @Column(name = "gig__role__c")
    private String role;

    @ManyToOne
    @JoinColumn(name = "gig__contact__r__gig__external_id__c")
    @JsonIgnore
    private Contact contact;

    @ManyToOne
    @JoinColumn(name = "gig__project__r__gig__external_id__c")
    @JsonIgnore
    private Project project;

    @JsonIgnore
    public String getId() {
        return externalId;
    }
}
