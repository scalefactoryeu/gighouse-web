package eu.scalefactory.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "gig__invoice__c", schema = "salesforce")
public class Invoice implements Serializable {

    private static final long serialVersionUID = 1L;
    public static String STATUS_DRAFT = "Draft";
    public static String STATUS_DRAFT_END_OF_MONTH = "Draft-EOM";
    public static String STATUS_ERROR = "Error";
    public static String TYPE_SALE = "Sale";
    public static String TYPE_CREDIT_NOTE_SALE = "Credit Note - Sale";

    @Id
    @Column(name = "sfid", length = 36)
    private String sfid;

    @Column(name = "isdeleted")
    private Boolean isDeleted;

    @Column(name = "name")
    private String name;

    @Column(name = "gig__type__c")
    private String type;

    @Column(name = "gig__invoice_date__c")
    private LocalDate invoiceDate;

    @Column(name = "gig__due_date__c")
    private LocalDate dueDate;

    @Column(name = "gig__minimum_date_of_work__c")
    private LocalDate periodStartDate;

    @Column(name = "gig__maximum_date_of_work__c")
    private LocalDate periodEndDate;

    @ManyToOne
    @JoinColumn(name = "gig__account__r__gig__external_id__c")
    private Account account;

    @Column(name = "gig__total_amount_w_vat__c")
    private Double totalAmountWithVAT;

    @Column(name = "gig__payment_conditions__c")
    private String paymentConditions;

    @Column(name = "gig__status__c")
    private String status;

    @OneToMany(mappedBy = "invoice")
    private Set<InvoiceLine> lines = new HashSet<>();

    public String getId() {
        return sfid;
    }
}
