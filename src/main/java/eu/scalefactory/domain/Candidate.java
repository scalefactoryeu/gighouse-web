package eu.scalefactory.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "gig__candidate__c", schema = "salesforce")
public class Candidate implements Serializable {

    public static String STATUS_PROPOSED = "Proposed";
    public static String STATUS_CONSIDERED = "Considered";
    public static String STATUS_CONSIDERED_BY_CUSTOMER = "Considered by Customer";
    public static String STATUS_INTERVIEW_PLANNED = "Interview Planned";
    public static String STATUS_ACCEPTED = "Accepted";
    public static String STATUS_REJECTED = "Rejected";
    public static String STATUS_SET_BY = "Customer";
    public static String CONTACT_STATUS_INTERESTED= "Interested";


    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "sfid")
    @JsonIgnore
    private String sfid;

    @Column(name = "gig__status__c")
    private String status;

    @Column(name = "gig__contact_status__c")
    private String contactStatus;

    @Column(name = "gig__status_reason__c")
    private String reason;

    @Column(name = "freelancer_description__c")
    private String freelancerDescription;


    @Column(name = "gighouse_opinion__c")
    private String gighouseOpinion;

    @Column(name = "gig__status_set_by__c")
    private String statusSetBy = STATUS_SET_BY;

    @ManyToOne
    @JoinColumn(name = "gig__contact__r__gig__external_id__c")
    private Contact contact;

    @ManyToOne
    @JoinColumn(name = "gig__project__c", referencedColumnName = "sfid")
    @JsonIgnore
    private Project project;

    @ManyToOne
    @JoinColumn(name = "gig__rate__c", referencedColumnName = "sfid")
    private Rate rate;

}
