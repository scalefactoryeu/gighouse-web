package eu.scalefactory.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "gig__project_language__c", schema = "salesforce")
public class ProjectLanguage implements Serializable {

    private static final long serialVersionUID = 1L;

    public ProjectLanguage() {
        this.externalId = UUID.randomUUID().toString();
    }

    @Id
    @Column(name = "sfid", length = 36)
    @JsonIgnore
    private String sfid;

    @Id
    @Column(name = "gig__external_id__c", length = 36)
    @JsonIgnore
    private String externalId;

    @Column(name = "gig__language__c")
    private String language;

    @Column(name = "gig__level__c")
    private String level;

    @ManyToOne
    @JoinColumn(name = "gig__project__r__gig__external_id__c")
    @JsonIgnore
    private Project project;

    @JsonIgnore
    public String getId() {
        return externalId;
    }

}
