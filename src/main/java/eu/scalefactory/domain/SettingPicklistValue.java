package eu.scalefactory.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "gig__settingpicklistvalue__c", schema = "salesforce")
public class SettingPicklistValue {

    @Id
    @Column(name = "sfid")
    @JsonIgnore
    private String sfid;

    @Column(name = "isdeleted")
    private boolean isDeleted;

    @Column(name = "gig__valueext__c")
    private String valueExt;

    @Column(name = "gig__valuesf__c")
    private String valueSf;

    @Column(name = "gig__objectname__c")
    private String objectName;

    @Column(name = "gig__fieldname__c")
    private String fieldName;

}
