package eu.scalefactory.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "contract", schema = "salesforce")
public class Contract implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "gig__External_Id__c", length = 36)
    @JsonIgnore
    private String externalId;

    @Column(name = "gig__subscription_type__c")
    private Double subscriptionType;

    @ManyToOne
    @JoinColumn(name = "Account__gig__External_Id__c")
    private Account account;

    public String getId() {
        return externalId;
    }
}
