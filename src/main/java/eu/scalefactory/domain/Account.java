package eu.scalefactory.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "account", schema = "salesforce")
public class Account implements Serializable {
    public static String CUSTOMER_ACCOUNT = "Customer Account";

    private static final long serialVersionUID = 1L;

    public Account() {
        this.externalId = UUID.randomUUID().toString();
    }

    @Id
    @Column(name = "gig__external_id__c", length = 36)
    @JsonIgnore
    private String externalId;

    @ManyToOne
    @JoinColumn(name = "recordtypeid")
    @JsonIgnore
    private RecordType recordType;

    @Column(name = "name")
    private String companyName;

    @Column(name = "billingstreet")
    private String billingStreet;

    @Column(name = "billingcity")
    private String billingCity;

    @Column(name = "billingpostalcode")
    private String billingPostalCode;

    @Column(name = "billingcountry")
    private String billingCountry;

    @Column(name = "billingstate")
    private String billingState;

    @Column(name = "gig__vat_number__c")
    private String vatNumber;

    @Column(name = "gig__invoice_e_mail__c")
    private String invoiceEmail;

    @Column(name = "phone")
    private String phone;

    @Column(name = "gig__company_format__c")
    private String companyFormat;

    @Column(name = "consent__c")
    private Boolean consent;

    @Column(name = "consent_date__c")
    private LocalDate consentDate;

    @Column(name = "registration_source__c")
    private String registrationSource;

    @Column(name = "type")
    private String type;

    @Column(name = "gig__subscription_type__c")
    private Double subscriptionType;

    @ManyToOne
    @JoinColumn(name = "ownerid")
    private SalesforceUser owner;

    @OneToMany(mappedBy = "account")
    private Set<Contact> contacts = new HashSet<>();

    @OneToMany(mappedBy = "account")
    @JsonIgnore
    private Set<Project> projects = new HashSet<>();

    public String getId() {
        return externalId;
    }

}
