package eu.scalefactory.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "portfolio__c", schema = "salesforce")
public class Portfolio {
    @Id
    @Column(name = "sfid", length = 18)
    private String sfid;

    @Column(name = "name")
    private String name;

    @Column(name = "description__c")
    private String description;

    @Column(name = "link_to_source__c")
    private String linkToSource;

    @Column(name = "type__c")
    private String type;

    @ManyToOne
    @JoinColumn(name = "contact__r__gig__external_id__c")
    @JsonIgnore
    private Contact contact;
}
