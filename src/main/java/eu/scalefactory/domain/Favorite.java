package eu.scalefactory.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "gig__favourite__c", schema = "salesforce")
public class Favorite implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "gig__external_id__c", length = 36)
    @JsonIgnore
    private String externalId = UUID.randomUUID().toString();

    @ManyToOne
    @JoinColumn(name = "gig__freelancer_contact__r__gig__external_id__c")
    private Contact freelancer;

    @ManyToOne
    @JoinColumn(name = "gig__customer_contact__r__gig__external_id__c")
    private Contact customer;

}
