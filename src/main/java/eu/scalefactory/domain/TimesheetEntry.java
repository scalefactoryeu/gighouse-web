package eu.scalefactory.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Table(name = "gig__time_sheet_entry__c", schema = "salesforce")
public class TimesheetEntry implements Serializable {
    private static final Logger LOGGER = LoggerFactory.getLogger(TimesheetEntry.class);


    private static final long serialVersionUID = 1L;

    public static String STATUS_DRAFT = "Draft";
    public static String STATUS_SUBMITTED = "Submitted";
    public static String STATUS_APPROVED = "Approved";
    public static String STATUS_REJECTED = "Rejected";

    @Id
    @Column(name = "sfid", length = 36)
    @JsonIgnore
    private String sfid;

    @Column(name = "gig__start__c")
    private LocalDateTime start;

    @Column(name = "gig__end__c")
    private LocalDateTime end;

    @Column(name = "gig__status__c")
    private String status;

    @Column(name = "gig__duration__c")
    private Double duration;

    @Column(name = "gig__Freelancer_Comment__c")
    private String freelancerComment;


    @Column(name = "gig__customer_comment__c")
    private String customerComment;

    @ManyToOne
    @JoinColumn(name = "gig__project__c", referencedColumnName = "sfid")
    @JsonIgnore
    private Project project;

    @ManyToOne
    @JoinColumn(name = "gig__rate__c", referencedColumnName = "sfid")
    @JsonIgnore
    private Rate rate;

    @ManyToOne
    @JoinColumn(name = "gig__project_member__c")
    @JsonIgnore
    private ProjectMember projectMember;

    public String getId() {
        return sfid;
    }

    public String getFreelancerId() {
        return projectMember.getContact().getId();
    }

    public double getHourlyRate() {
        return this.rate.getHourlyRateWithSubscriptionType();
    }

    public String getProjectId() {
        return project.getId();
    }

}
