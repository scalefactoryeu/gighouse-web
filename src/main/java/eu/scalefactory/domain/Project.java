package eu.scalefactory.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "gig__project__c", schema = "salesforce")
public class Project implements Serializable {

    public static String STATUS_DRAFT = "Draft";
    public static String STATUS_SENT = "Sent";
    public static String STATUS_SIGN_CONTRACT = "Sign Contract";
    public static String STATUS_ONGOING = "Ongoing";
    public static String STATUS_CLOSED = "Closed";
    public static String STATUS_CANCELLED = "Cancelled";


    private static final long serialVersionUID = 1L;

    public Project() {
        this.externalId = UUID.randomUUID().toString();
    }

    @Id
    @Column(name = "gig__external_id__c", length = 36)
    @JsonIgnore
    private String externalId;

    @Column(name = "sfid")
    @JsonIgnore
    private String sfid;

    @Column(name = "name")
    private String name;

    @Column(name = "gig__full_description__c")
    private String description;

    @Column(name = "gig__start_date__c")
    private LocalDate startDate;

    @Column(name = "gig__end_date__c")
    private LocalDate endDate;

    @Column(name = "gig__send_date__c")
    private LocalDate sentDate;

    @Column(name = "gig__requested_start_date__c")
    private LocalDate requestedStartDate;

    @Column(name = "gig__postal_code__c")
    private String postalCode;

    @Column(name = "gig__city__c")
    private String city;

    @Column(name = "gig__country__c")
    private String country;

    @Column(name = "gig__state__c")
    private String state;

    @Column(name = "gig__project_status__c")
    private String status;

    @Column(name = "gig__teaser__c")
    private String teaser1;

    @Column(name = "gig__teaser_2__c")
    private String teaser2;

    @Column(name = "gig__teaser_3__c")
    private String teaser3;

    @Column(name = "gig__maximum_budget__c")
    private Double maxBudget;

    @Column(name = "gig__days_per_week__c")
    private Double daysPerWeek;

    @Column(name = "gig__home_work__c")
    private Boolean homeWork;

    @Column(name = "gig__estimated_project_duration__c")
    private Double estimatedProjectDuration;

    @Column(name = "gig__cancelled_date__c")
    private LocalDate archivedDate;

    @ManyToOne
    @JoinColumn(name = "gig__niche__c")
    @JsonIgnore
    private Skill expertise;

    @ManyToOne
    @JoinColumn(name = "gig__account__r__gig__external_id__c")
    @JsonIgnore
    private Account account;

    @ManyToOne
    @JoinColumn(name = "ownerid", referencedColumnName = "sfid")
    private SalesforceUser owner;

    @ManyToOne
    @JoinColumn(name = "created_by_contact__r__gig__external_id__c")
    @JsonIgnore
    private Contact createdByContact;

    @Column(name = "smartpositiveskills__c")
    private String positiveSkills;

    @Column(name = "smartnegativeskills__c")
    private String negativeSkills;

    @Column(name = "smartpositivefunctions__c")
    private String positiveFunctions;

    @Column(name = "smartnegativefunctions__c")
    private String negativeFunctions;

    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Set<ProjectSkill> projectSkills = new HashSet<>();

    @OneToMany(mappedBy = "project")
    private Set<Candidate> candidates = new HashSet<>();

    @OneToMany(mappedBy = "project")
    private Set<ProjectMember> members = new HashSet<>();

    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Set<ProjectLanguage> projectLanguages = new HashSet<>();

    public String getId() {
        return externalId;
    }


}
