package eu.scalefactory.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "gig__project_member__c", schema = "salesforce")
public class ProjectMember implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "sfid", length = 36)
    @JsonIgnore
    private String sfid;

    @ManyToOne
    @JoinColumn(name = "gig__contact__r__gig__external_id__c")
    @JsonIgnore
    private Contact contact;

    @ManyToOne
    @JoinColumn(name = "gig__project__c", referencedColumnName = "sfid")
    @JsonIgnore
    private Project project;

    @ManyToOne
    @JoinColumn(name = "gig__account__r__gig__external_id__c")
    @JsonIgnore
    private Account account;

    public String getId() {
        return sfid;
    }

}
