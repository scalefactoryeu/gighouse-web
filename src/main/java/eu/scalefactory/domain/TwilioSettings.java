package eu.scalefactory.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "gig__settingtwilio__c", schema = "salesforce")
public class TwilioSettings implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "sfid", length = 36)
    @JsonIgnore
    private String sfid;

    @Column(name = "gig__account_sid__c")
    private String accountSid;

    @Column(name = "gig__auth_token__c")
    private String authToken;

    @Column(name = "gig__api_key_sid__c")
    private String apiKeySid;

    @Column(name = "gig__api_key_secret__c")
    private String apiKeySecret;

    @Column(name = "gig__message_service_id__c")
    private String messageServiceId;

    @Column(name = "gig__chat_service_id__c")
    private String chatServiceId;

    @Column(name = "gig__instance__c")
    private String instance;

}
