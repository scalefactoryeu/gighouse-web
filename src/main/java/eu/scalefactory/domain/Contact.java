package eu.scalefactory.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.StringUtils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@EqualsAndHashCode(of = {"externalId"})
@Entity
@Table(name = "contact", schema = "salesforce")
public class Contact implements Serializable {
    public static String CUSTOMER_CONTACT = "Customer Contact";

    private static final long serialVersionUID = 1L;

    public Contact() {
        this.externalId = UUID.randomUUID().toString();
    }

    @Id
    @Column(name = "gig__external_id__c", length = 36)
    @JsonIgnore
    private String externalId;

    @Column(name = "name")
    private String name;

    @Column(name = "firstname")
    private String firstName;

    @Column(name = "lastname")
    private String lastName;

    @Column(name = "phone")
    private String phone;

    @Column(name = "description")
    private String description;

    @Column(name = "opinion__c")
    private String gighouseOpinion;

    @Column(name = "gig__rating__c")
    private String rating;

    @Column(name = "mailingcity")
    private String mailingCity;

    @Column(name = "mailingpostalcode")
    private String mailingPostalCode;

    @Column(name = "mailingstate")
    private String mailingState;

    @Column(name = "mailingstreet")
    private String mailingStreet;

    @Column(name = "mailingcountry")
    private String mailingCountry;

    @Column(name = "mobilephone")
    private String mobilePhone;

    @Column(name = "email")
    private String email;

    @Column(name = "is_registered_on_website__c")
    private Boolean registeredOnWebsite;

    @Column(name = "gig__registration_source__c")
    private String registationSource;

    @Column(name = "gig__Earliest_Available_Date__c")
    private LocalDate availabilityDate;

    @Column(name = "gig__days_week__c")
    private Double availableDaysAWeek;

    @Column(name = "gig__does_weekend_work__c")
    private Boolean doesWeekendWork;

    @Column(name = "gig__does_evening_work__c")
    private Boolean doesEveningWork;

    @Column(name = "gig__minimum_rate__c")
    private Double dayFee;

    @Column(name = "gig__has_own_car__c")
    private Boolean ownCar;

    @Column(name = "gig__is_available_for_teleworking__c")
    private Boolean teleworking;

    @Column(name = "gig__consent__c")
    private Boolean consent;

    @Column(name = "gig__consent_date__c")
    private LocalDate consentDate;


    @Column(name = "privacy_policy_accepted__c")
    private Boolean privacyPolicy;

    @Column(name = "privacy_policy_signature_date__c")
    private LocalDateTime privacyPolicySignature;

    @ManyToOne
    @JoinColumn(name = "recordtypeid")
    @JsonIgnore
    private RecordType recordType;

    @Column(name = "gig__is_web_allowed__c")
    private Boolean isWebAllowed;

    @ManyToOne
    @JoinColumn(name = "account__gig__external_id__c")
    private Account account;

    @OneToMany(mappedBy = "contact", fetch = FetchType.EAGER)
    private Set<ContactSkill> skills = new HashSet<>();

    @OneToMany(mappedBy = "contact", fetch = FetchType.EAGER)
    @JsonIgnore
    private Set<ContactProfile> profiles = new HashSet<>();

    @OneToMany(mappedBy = "contact", fetch = FetchType.EAGER)
    private Set<ContactLanguage> contactLanguages = new HashSet<>();

    @OneToMany(mappedBy = "contact", fetch = FetchType.EAGER)
    @JsonIgnore
    private Set<Portfolio> portfolios = new HashSet<>();

    @OneToMany(mappedBy = "contact", fetch = FetchType.EAGER)
    @JsonIgnore
    private Set<Referral> referrals = new HashSet<>();

    @OneToMany(mappedBy = "contact")
    private Set<Candidate> candidates = new HashSet<>();

    public String getId() {
        return externalId;
    }

    public String getProfileName() {
        String name = null;
        Profile achievedProfile = getCurrentAchievedProfile();
        if (achievedProfile != null) {
            name = achievedProfile.getName();
        }

        return name;
    }

    @JsonIgnore
    public Profile getCurrentAchievedProfile() {
        for (ContactProfile profile : profiles) {
            if (profile.getCurrentAchieved() != null && profile.getCurrentAchieved()) {
                return profile.getProfile();
            }
        }

        return null;
    }

    public String getShortName() {
        StringBuilder name = new StringBuilder(this.firstName);
        if (!StringUtils.isEmpty(this.lastName)) {
            String[] lastNameSplit = this.lastName.split("\\s+");
            for(String last : lastNameSplit){
                name.append(" ").append(last.charAt(0)).append(".");
            }
        }

        return name.toString();
    }

}
