package eu.scalefactory.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "gig__skill__c", schema = "salesforce")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@NoArgsConstructor
public class Skill implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "sfid", length = 36)
    @JsonIgnore
    private String sfid;

    @Column(name = "name")
    private String name;

    @Column(name = "gig__type__c")
    private String type;

    @Column(name = "gig__visible_in_mobile_app__c")
    private Boolean visibleInMobileApp;

    @ManyToOne
    @JoinColumn(name = "gig__parent_skill__c")
    private Skill parent;

    @OneToMany(mappedBy="parent")
    @JsonIgnore
    private Set<Skill> children;

    public Skill(String sfid) {
        this.sfid = sfid;
    }

    public String getId() {
        return sfid;
    }

    public String getParentId() {
        if (parent == null) {
            return null;
        }

        return parent.getId();
    }

}
