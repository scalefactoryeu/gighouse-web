package eu.scalefactory.domain;

import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "gig__project_skill__c", schema = "salesforce")
public class ProjectSkill implements Serializable {

    private static final long serialVersionUID = 1L;

    public ProjectSkill() {
        this.externalId = UUID.randomUUID().toString();
    }

    @Id
    @Column(name = "sfid", length = 36)
    @JsonIgnore
    private String sfid;

    @Id
    @Column(name = "gig__external_id__c", length = 36)
    @JsonIgnore
    private String externalId;

    @Column(name = "gig__rating__c")
    private Double rating;

    @Column(name = "gig__requirement__c")
    private String requirement;

    @ManyToOne
    @JoinColumn(name = "gig__project__r__gig__external_id__c")
    @JsonIgnore
    private Project project;

    @ManyToOne
    @JoinColumn(name = "gig__skill__c")
    @JsonIdentityReference(alwaysAsId = true)
    private Skill skill;

    @JsonIgnore
    public String getId() {
        return externalId;
    }

}
