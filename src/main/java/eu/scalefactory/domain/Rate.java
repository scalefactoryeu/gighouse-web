package eu.scalefactory.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Logger;

import static java.util.Optional.ofNullable;

@Getter
@Setter
@Entity
@Table(name = "gig__rate__c", schema = "salesforce")
public class Rate implements Serializable {
    private static final Logger LOGGER = Logger.getLogger(Rate.class.getName());

    private static final long serialVersionUID = 1L;
    private static final String RATE_UNIT_DAYS = "Days";
    private static final String RATE_UNIT_HOURS = "Hours";
    private static final String RATE_UNIT_MINUTES = "Minutes";

    private static final List<String> RATE_UNITS = List.of(RATE_UNIT_DAYS, RATE_UNIT_HOURS, RATE_UNIT_MINUTES);

    @Id
    @Column(name = "sfid")
    @JsonIgnore
    private String sfid;

    @Column(name = "gig__rate_requested_by_freelancer__c")
    private Double rateRequestedByFreelancer;

    @Column(name = "gig__rate_unit_del__c")
    private String rateUnit;

    @ManyToOne
    @JoinColumn(name = "gig__contract__r__gig__external_id__c")
    @JsonIgnore
    private Contract contract;

    @Override
    public String toString() {
        return "Rate{" +
            "sfid='" + sfid + '\'' +
            ", rateRequestedByFreelancer=" + rateRequestedByFreelancer +
            ", rateProposedToCustomer=" + getRateProposedToCustomer() +
            ", hourlyRateWithSubscriptionType=" + getHourlyRateWithSubscriptionType() +
            '}';
    }

    public Double getRateProposedToCustomer() {
        if (rateRequestedByFreelancer == null) {
            return 0d;
        }

        return ofNullable(contract)
            .map(Contract::getSubscriptionType)
            .map(rate -> rateRequestedByFreelancer + rateRequestedByFreelancer * rate)
            .orElse(0d);
    }

    public Double getHourlyRateWithSubscriptionType() {
        if (RATE_UNIT_MINUTES.equals(rateUnit)) {
            return getRateProposedToCustomer() * 60;
        } else if (RATE_UNIT_DAYS.equals(rateUnit)) {
            return getRateProposedToCustomer() / 8;
        }

        return getRateProposedToCustomer();
    }
}
