package eu.scalefactory.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "user", schema = "salesforce")
public class SalesforceUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "sfid")
    @JsonIgnore
    private String sfid;

    @Column(name = "name")
    private String name;

    @Column(name = "phone")
    private String phone;

    @Column(name = "email")
    private String email;


}
