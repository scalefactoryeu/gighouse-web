package eu.scalefactory.service.util;

import org.owasp.html.HtmlPolicyBuilder;
import org.owasp.html.PolicyFactory;

public class CustomHtmlSanitizer {
    public static String sanitizeHTML(String untrustedHTML) {
        PolicyFactory policy = new HtmlPolicyBuilder()
            .allowAttributes("src").onElements("img")
            .allowAttributes("href", "rel", "noreferrer", "target").onElements("a")
            .allowAttributes("class", "spellcheck").onElements("pre")
            .allowAttributes("style", "background-color").onElements("span")
            .allowStandardUrlProtocols()
            .allowElements(
                "a", "img", "p", "br",
                "ul", "ol", "li",
                "strong", "em", "s", "u",
                "blockquote", "pre",
                "h1","h2","h3","h4","h5","h6",
                "span"
            ).toFactory();

        return policy.sanitize(untrustedHTML);
    }
}

