package eu.scalefactory.service;

import eu.scalefactory.domain.*;
import eu.scalefactory.repository.CandidateRepository;
import eu.scalefactory.repository.ProjectRepository;
import eu.scalefactory.repository.TimesheetEntryRepository;
import eu.scalefactory.service.dto.FollowUpActionsDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.temporal.WeekFields;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;

@Service
@Transactional
public class DashboardService {

    private final CandidateRepository candidateRepository;
    private final ContactHelper contactHelper;
    private final ProjectRepository projectRepository;
    private final TimesheetEntryRepository timesheetEntryRepository;

    public DashboardService(CandidateRepository candidateRepository, ContactHelper contactHelper, ProjectRepository projectRepository, TimesheetEntryRepository timesheetEntryRepository) {
        this.candidateRepository = candidateRepository;
        this.contactHelper = contactHelper;
        this.projectRepository = projectRepository;
        this.timesheetEntryRepository = timesheetEntryRepository;
    }

    public FollowUpActionsDto getFollowUpActions() {

        Contact currentUserContact = contactHelper.getCurrentUserContact();
        Account account = currentUserContact.getAccount();
        Double subscriptionType = account.getSubscriptionType() != null ? account.getSubscriptionType() * 100 : 0;
        int countCandidatesProposed;
        int countProjectStatusDraft;
        int countTimeSheets;
        List<ProjectCustomerTeam> approverProjectsForContact = contactHelper.getApproverProjectsForContact(currentUserContact);
        if (approverProjectsForContact.size() > 0) {
            List<String> projectIds = approverProjectsForContact.stream().map(projectCustomerTeam -> projectCustomerTeam.getProject().getExternalId()).collect(toList());
            countCandidatesProposed = candidateRepository.findAllByProjectExternalIdInAndStatusInAndProjectStatusNotIn(projectIds, singletonList(Candidate.STATUS_PROPOSED), Arrays.asList(Project.STATUS_CANCELLED, Project.STATUS_CLOSED, Project.STATUS_ONGOING)).size();
            countProjectStatusDraft = projectRepository.findAllByExternalIdInAndStatus(projectIds, Project.STATUS_DRAFT).size();
            WeekFields weekFields = WeekFields.of(Locale.getDefault());
            countTimeSheets = (int) timesheetEntryRepository.findAllByProjectExternalIdInAndStatus(projectIds, TimesheetEntry.STATUS_SUBMITTED)
                .stream()
                .map(TimesheetEntry::getStart)
                .map(date -> date.get(weekFields.weekOfWeekBasedYear()))
                .distinct()
                .count();

        } else {

            countCandidatesProposed = candidateRepository.findAllByProjectAccountAndStatusInAndProjectStatusNotIn(currentUserContact.getAccount(), singletonList(Candidate.STATUS_PROPOSED), Arrays.asList(Project.STATUS_CANCELLED, Project.STATUS_CLOSED, Project.STATUS_ONGOING)).size();
            countProjectStatusDraft = projectRepository.findAllByAccountAndStatus(currentUserContact.getAccount(), Project.STATUS_DRAFT).size();
            WeekFields weekFields = WeekFields.of(Locale.getDefault());
            countTimeSheets = (int) timesheetEntryRepository.findAllByProjectAccountAndStatus(currentUserContact.getAccount(), TimesheetEntry.STATUS_SUBMITTED)
                .stream()
                .map(TimesheetEntry::getStart)
                .map(date -> date.get(weekFields.weekOfWeekBasedYear()))
                .distinct()
                .count();
        }

        return new FollowUpActionsDto(countCandidatesProposed, countProjectStatusDraft, countTimeSheets, subscriptionType);
    }
}
