package eu.scalefactory.service;

import eu.scalefactory.domain.*;
import eu.scalefactory.repository.ProjectRepository;
import eu.scalefactory.repository.TimesheetEntryRepository;
import eu.scalefactory.repository.TimesheetEntrySpec;
import eu.scalefactory.service.dto.TimesheetDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

@Service
@Transactional
public class TimesheetService {

    private final TimesheetEntryRepository timesheetEntryRepository;
    private final ProjectRepository projectRepository;
    private final ContactHelper contactHelper;

    public TimesheetService(TimesheetEntryRepository timesheetEntryRepository, ProjectRepository projectRepository, ContactHelper contactHelper) {
        this.timesheetEntryRepository = timesheetEntryRepository;
        this.projectRepository = projectRepository;
        this.contactHelper = contactHelper;
    }

    public TimesheetDto getTimesheets(TimesheetEntrySpec.TimesheetFilter filter) {
        Contact currentUserContact = contactHelper.getCurrentUserContact();
        List<ProjectCustomerTeam> approverProjectsForContact = contactHelper.getApproverProjectsForContact(currentUserContact);
        if (approverProjectsForContact.size() > 0) {
            approverProjectsForContact.addAll(contactHelper.getDecisionMakerForContact(currentUserContact));
            List<String> projects = approverProjectsForContact.stream().map(projectCustomerTeam -> projectCustomerTeam.getProject().getExternalId()).collect(toList());
            filter.setProjects(projects);

        } else {
            filter.setProjectAccount(currentUserContact.getAccount());
        }

        List<TimesheetEntry> entries = timesheetEntryRepository.findAll(
                new TimesheetEntrySpec(filter)).stream()
            .sorted(Comparator.comparing(TimesheetEntry::getStart).reversed())
            .collect(toList());


        TimesheetDto timesheet = new TimesheetDto();
        timesheet.setEntries(entries);
        timesheet.setDateFrom(filter.getDateFrom());
        timesheet.setDateTo(filter.getDateTo());
        timesheet.setFreelancers(entries.stream().map(TimesheetDto.Freelancer::new).collect(toSet()));
        timesheet.setProjects(entries.stream().map(TimesheetDto.Project::new).collect(toSet()));

        return timesheet;
    }

    public List<TimesheetDto.Freelancer> getFreelancers() {
        Contact currentUserContact = contactHelper.getCurrentUserContact();
        List<ProjectCustomerTeam> approverProjectsForContact = contactHelper.getApproverProjectsForContact(currentUserContact);
        List<Project> projects;
        if (approverProjectsForContact.size() > 0) {
            approverProjectsForContact.addAll(contactHelper.getDecisionMakerForContact(currentUserContact));
            List<String> projectsList = approverProjectsForContact.stream().map(projectCustomerTeam -> projectCustomerTeam.getProject().getExternalId()).collect(toList());
            projects = projectRepository.findAllByExternalIdInAndStatusNotIn(projectsList, asList(Project.STATUS_DRAFT, Project.STATUS_CLOSED, Project.STATUS_CANCELLED));
        } else {
            projects = projectRepository.findAllByAccountAndStatusNotIn(currentUserContact.getAccount(), asList(Project.STATUS_DRAFT, Project.STATUS_CLOSED, Project.STATUS_CANCELLED));
        }

        return projects.stream()
            .map(Project::getMembers)
            .filter(Objects::nonNull)
            .flatMap(Collection::stream)
            .map(ProjectMember::getContact)
            .distinct()
            .map(c -> new TimesheetDto.Freelancer(c.getId(), c.getName()))
            .collect(toList());
    }

    public void changeEntryStatus(List<String> ids, String newStatus, String rejectMessage) {
        Contact currentUserContact = contactHelper.getCurrentUserContact();

        for (String id : ids) {
            TimesheetEntry timesheetEntry = timesheetEntryRepository.findById(id).orElseThrow();
            if (!timesheetEntry.getProject().getAccount().equals(currentUserContact.getAccount())) {
                throw new RuntimeException("Not allowed");
            }

            timesheetEntry.setStatus(newStatus);
            timesheetEntry.setCustomerComment(rejectMessage);
        }
    }

}
