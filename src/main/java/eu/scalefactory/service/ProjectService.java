package eu.scalefactory.service;

import eu.scalefactory.domain.*;
import eu.scalefactory.repository.ProjectRepository;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

import static eu.scalefactory.service.util.CustomHtmlSanitizer.sanitizeHTML;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toList;

@Service
@Transactional
public class ProjectService {

    private final ProjectRepository projectRepository;
    private final PicklistService picklistService;
    private final ContactHelper contactHelper;

    public ProjectService(ProjectRepository projectRepository, PicklistService picklistService, ContactHelper contactHelper) {
        this.projectRepository = projectRepository;
        this.picklistService = picklistService;
        this.contactHelper = contactHelper;
    }

    public Page<Project> getProjects(Pageable pageable, Boolean archived) {
        Contact currentUserContact = contactHelper.getCurrentUserContact();
        List<ProjectCustomerTeam> approverProjectsForContact = contactHelper.getApproverProjectsForContact(currentUserContact);

        Page<Project> page;
        //CHECK IF APPROVER IN PROJECT CUSTOMER TEAM
        if (archived != null && archived) {
            if (approverProjectsForContact.size() > 0) {
                approverProjectsForContact.addAll(contactHelper.getDecisionMakerForContact(currentUserContact));
                List<String> projectIds = approverProjectsForContact.stream().map(projectCustomerTeam -> projectCustomerTeam.getProject().getExternalId()).collect(toList());
                page = projectRepository.findAllByExternalIdInAndStatusIn(projectIds, asList(Project.STATUS_CLOSED, Project.STATUS_CANCELLED), pageable);
            } else {
                page = projectRepository.findAllByAccountAndStatusIn(currentUserContact.getAccount(), asList(Project.STATUS_CLOSED, Project.STATUS_CANCELLED), pageable);
            }
        } else {
            if (approverProjectsForContact.size() > 0) {
                approverProjectsForContact.addAll(contactHelper.getDecisionMakerForContact(currentUserContact));
                List<String> projectIds = approverProjectsForContact.stream().map(projectCustomerTeam -> projectCustomerTeam.getProject().getExternalId()).collect(toList());
                page = projectRepository.findAllByExternalIdInAndStatusNotIn(projectIds, asList(Project.STATUS_CLOSED, Project.STATUS_CANCELLED), pageable);
            } else {
                page = projectRepository.findAllByAccountAndStatusNotIn(currentUserContact.getAccount(), asList(Project.STATUS_CLOSED, Project.STATUS_CANCELLED), pageable);
            }
        }


        return page;
    }

    public Project getProject(String id) {
        Contact currentUserContact = contactHelper.getCurrentUserContact();
        Project project = projectRepository.findById(id).orElseThrow();
        checkUserAuthorizationForProject(currentUserContact, project);

        return project;
    }

    public List<String> getProjectPositiveSkills(Project project) {
        return project.getPositiveSkills() != null && !project.getPositiveSkills().isEmpty() ? Arrays.stream(project.getPositiveSkills().split(";")).map(String::trim).collect(toList()) : emptyList();
    }

    public List<ProjectLanguageData> getProjectLanguages(String id) {
        Contact currentUserContact = contactHelper.getCurrentUserContact();
        Project project = projectRepository.findById(id).orElseThrow();
        checkUserAuthorizationForProject(currentUserContact, project);

        return project.getProjectLanguages().stream()
            .map(pl -> new ProjectLanguageData(pl.getId(), pl.getLanguage(), pl.getLevel()))
            .collect(toList());
    }

    public Project createProject(ProjectData data) {
        Contact currentUserContact = contactHelper.getCurrentUserContact();

        data.validate();

        Project project = new Project();
        toProject(data, project);
        project.setCountry("BE");
        project.setSentDate(LocalDate.now());
        project.setCreatedByContact(currentUserContact);
        project.setAccount(currentUserContact.getAccount());

        return projectRepository.save(project);
    }

    public Project updateProject(String id, ProjectData data) {
        Contact currentUserContact = contactHelper.getCurrentUserContact();
        Project project = projectRepository.findById(id).orElseThrow();
        checkUserAuthorizationForProject(currentUserContact, project);

        data.validate();
        toProject(data, project);

        return project;
    }

    public void archiveProject(String id) {
        Contact currentUserContact = contactHelper.getCurrentUserContact();
        Project project = projectRepository.findById(id).orElseThrow();
        checkUserAuthorizationForProject(currentUserContact, project);

        project.setArchivedDate(LocalDate.now());
        project.setStatus(Project.STATUS_CANCELLED);
    }

    public void deleteProject(String id) {
        Contact currentUserContact = contactHelper.getCurrentUserContact();
        Project project = projectRepository.findById(id).orElseThrow();
        checkUserAuthorizationForProject(currentUserContact, project);

        if (!Project.STATUS_DRAFT.equals(project.getStatus())) {
            throw new RuntimeException("Only draft projects can be deleted");
        }

        projectRepository.deleteById(id);
    }

    private void toProject(ProjectData data, Project project) {
        project.setName(data.getName());
        project.setDescription(data.getDescription());
        project.setTeaser1(data.getTeaser1());
        project.setTeaser2(data.getTeaser2());
        project.setTeaser3(data.getTeaser3());
        project.setDaysPerWeek(data.getDaysPerWeek().doubleValue());
        project.setEstimatedProjectDuration(data.getEstimatedProjectDuration().doubleValue());
        project.setRequestedStartDate(data.getStartDate());
        project.setPostalCode(data.getPostalCode());
        project.setCity(data.getCity());
        project.setState(data.getState());
        project.setMaxBudget(data.getMaxBudget());
        project.setExpertise(new Skill(data.getExpertise()));

        if (data.getDraft() == null || !data.getDraft()) {
            project.setStatus(Project.STATUS_SENT);
        } else {
            project.setStatus(Project.STATUS_DRAFT);
        }

        project.getProjectSkills().clear();
        project.setPositiveSkills(data.getPositiveSkills() != null ?  String.join("; ", data.getPositiveSkills()) : null);
//        for (ProjectSkillData s : data.getSkills()) {
//            ProjectSkill projectSkill = new ProjectSkill();
//            Skill skill = new Skill();
//            skill.setSfid(s.getId());
//            projectSkill.setSkill(skill);
//            projectSkill.setRating(s.getRating());
//            projectSkill.setRequirement(s.getRequirement());
//            projectSkill.setProject(project);
//
//            project.getProjectSkills().add(projectSkill);
//        }
        project.getProjectLanguages().clear();
        for (ProjectLanguageData s : data.getProjectLanguages()) {
            ProjectLanguage projectLanguage = new ProjectLanguage();
            projectLanguage.setLanguage(s.getLanguage());
            projectLanguage.setLevel(s.getLevel());
            projectLanguage.setProject(project);
            project.getProjectLanguages().add(projectLanguage);
        }
    }

    public Map<String, String> getStatusesMap() {
        List<SettingPicklistValue> spvs = picklistService.getPicklist(PicklistService.Picklist.PROJECT_STATUSES);
        Map<String, String> statuses = new HashMap<>();
        for (SettingPicklistValue spv : spvs) {
            statuses.put(spv.getValueSf(), spv.getValueExt());
        }

        return statuses;
    }

    private void checkUserAuthorizationForProject(Contact currentUserContact, Project project) {
        String accountId = currentUserContact.getAccount().getId();

        if (!accountId.equals(project.getAccount().getId())) {
            throw new AccessDeniedException("User does not have permission to access project.");
        }


    }

    @Getter
    @Setter
    public static class ProjectData {
        private String name;
        private String description;
        private String teaser1;
        private String teaser2;
        private String teaser3;
        private Integer daysPerWeek;
        private Integer estimatedProjectDuration;
        private LocalDate startDate;
        private String postalCode;
        private String city;
        private String state;
        private Boolean homeWork;
        private Double maxBudget;
        private Boolean draft;
        private String expertise;
        private List<String> positiveSkills = new ArrayList<>();
        private List<ProjectLanguageData> projectLanguages = new ArrayList<>();

        public void validate() {
            this.description = sanitizeHTML(this.description);

            if (name.length() > 80) {
                throw new RuntimeException("Project name more than 80 characters");
            }
            if (postalCode.length() > 7) {
                throw new RuntimeException("Postal code has at most 7 digits");
            }
            if (city.length() > 80) {
                throw new RuntimeException("City name more than 80 characters");
            }
            if (teaser1.length() > 40) {
                throw new RuntimeException("Teaser length more than 40 characters");
            }
            if (teaser2.length() > 40) {
                throw new RuntimeException("Teaser length more than 40 characters");
            }
            if (teaser3.length() > 40) {
                throw new RuntimeException("Teaser length more than 40 characters");
            }
        }
    }

    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    public static class ProjectSkillData {
        private String id;
        private String name;
        private String requirement;
        private Double rating;
        private String type;
    }

    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    public static class ProjectLanguageData {
        private String id;
        private String language;
        private String level;
    }


}
