package eu.scalefactory.service.mapper;

import eu.scalefactory.domain.Account;
import eu.scalefactory.domain.Contact;
import eu.scalefactory.service.ProfileService;
import eu.scalefactory.service.dto.CompanyProfileDto;
import eu.scalefactory.service.dto.GighouseContactDto;
import eu.scalefactory.service.dto.UserProfileDto;
import org.springframework.stereotype.Component;

@Component
public class ProfileMapper {
    public UserProfileDto contactToUserProfileDTO(Contact contact) {
        UserProfileDto userProfileDTO = new UserProfileDto();

        userProfileDTO.setFirstName(contact.getFirstName());
        userProfileDTO.setLastName(contact.getLastName());
        userProfileDTO.setStreet(contact.getMailingStreet());
        userProfileDTO.setPostalCode(contact.getMailingPostalCode());
        userProfileDTO.setCity(contact.getMailingCity());
        userProfileDTO.setState(contact.getMailingState());
        userProfileDTO.setCountry(contact.getMailingCountry());
        userProfileDTO.setEmail(contact.getEmail());
        userProfileDTO.setPhone(contact.getPhone());

        return userProfileDTO;
    }

    public ProfileService.UserProfileData userProfileDTOtoUserProfileData(UserProfileDto userProfileDTO) {
        ProfileService.UserProfileData data = new ProfileService.UserProfileData();

        data.setFirstName(userProfileDTO.getFirstName());
        data.setLastName(userProfileDTO.getLastName());
        data.setMailingStreet(userProfileDTO.getStreet());
        data.setMailingPostalCode(userProfileDTO.getPostalCode());
        data.setMailingCity(userProfileDTO.getCity());
        data.setMailingState(userProfileDTO.getState());
        data.setEmail(userProfileDTO.getEmail());
        data.setPhone(userProfileDTO.getPhone());

        return data;
    }

    public CompanyProfileDto accountToCompanyProfileDTO(Account account) {
        CompanyProfileDto companyProfileDTO = new CompanyProfileDto();

        companyProfileDTO.setName(account.getCompanyName());
        companyProfileDTO.setCompanyFormat(account.getCompanyFormat());
        companyProfileDTO.setStreet(account.getBillingStreet());
        companyProfileDTO.setPostalCode(account.getBillingPostalCode());
        companyProfileDTO.setCity(account.getBillingCity());
        companyProfileDTO.setState(account.getBillingState());
        companyProfileDTO.setCountry(account.getBillingCountry());
        companyProfileDTO.setInvoiceEmail(account.getInvoiceEmail());
        companyProfileDTO.setPhone(account.getPhone());
        companyProfileDTO.setVatNumber(account.getVatNumber());

        return companyProfileDTO;
    }

    public ProfileService.CompanyProfileData companyProfileDTOtoCompanyProfileData(CompanyProfileDto companyProfileDTO) {
        ProfileService.CompanyProfileData data = new ProfileService.CompanyProfileData();

        data.setCompanyName(companyProfileDTO.getName());
        data.setCompanyFormat(companyProfileDTO.getCompanyFormat());
        data.setBillingStreet(companyProfileDTO.getStreet());
        data.setBillingPostalCode(companyProfileDTO.getPostalCode());
        data.setBillingCity(companyProfileDTO.getCity());
        data.setBillingState(companyProfileDTO.getState());
        data.setInvoiceEmail(companyProfileDTO.getInvoiceEmail());
        data.setPhone(companyProfileDTO.getPhone());
        data.setVatNumber(companyProfileDTO.getVatNumber());

        return data;
    }

    public GighouseContactDto toGighouseContactDTO(Contact contact) {
        GighouseContactDto dto = new GighouseContactDto();

        dto.setName(contact.getName());
        dto.setPhone(contact.getPhone());
        dto.setEmail(contact.getEmail());

        return dto;
    }
}
