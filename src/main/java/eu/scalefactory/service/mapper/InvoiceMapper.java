package eu.scalefactory.service.mapper;

import eu.scalefactory.domain.Contact;
import eu.scalefactory.domain.Invoice;
import eu.scalefactory.domain.InvoiceLine;
import eu.scalefactory.domain.ProjectMember;
import eu.scalefactory.domain.SettingPicklistValue;
import eu.scalefactory.service.PicklistService;
import eu.scalefactory.service.dto.InvoiceDto;
import eu.scalefactory.service.dto.InvoiceFreelancerDto;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class InvoiceMapper {

    private final PicklistService picklistService;

    public InvoiceMapper(PicklistService picklistService) {
        this.picklistService = picklistService;
    }

    public InvoiceDto invoiceToInvoiceDTO(Invoice invoice) {
        InvoiceDto invoiceDTO = new InvoiceDto();

        invoiceDTO.setId(invoice.getSfid());
        invoiceDTO.setInvoiceNumber(invoice.getName());
        invoiceDTO.setInvoiceDate(invoice.getInvoiceDate());
        invoiceDTO.setPeriodStartDate(invoice.getPeriodStartDate());
        invoiceDTO.setPeriodEndDate(invoice.getPeriodEndDate());
        invoiceDTO.setTotalAmountWithVAT(invoice.getTotalAmountWithVAT());
        invoiceDTO.setStatus(getStatusesMap().get(invoice.getStatus()));
        invoiceDTO.setFreelancers(invoiceToInvoiceFreelancerDTOList(invoice));

        return invoiceDTO;
    }

    public InvoiceFreelancerDto contactToInvoiceFreelancerDTO(Contact contact) {
        InvoiceFreelancerDto invoiceFreelancerDTO = new InvoiceFreelancerDto();

        invoiceFreelancerDTO.setId(contact.getId());
        invoiceFreelancerDTO.setName(contact.getName());

        return invoiceFreelancerDTO;
    }

    private List<InvoiceFreelancerDto> invoiceToInvoiceFreelancerDTOList(Invoice invoice) {
        return invoice.getLines().stream()
            .map(InvoiceLine::getProjectMember)
            .map(ProjectMember::getContact)
            .distinct()
            .map(this::contactToInvoiceFreelancerDTO)
            .collect(Collectors.toList());
    }

    public Map<String, String> getStatusesMap() {
        List<SettingPicklistValue> spvs = picklistService.getPicklist(PicklistService.Picklist.INVOICE_STATUSES);
        Map<String, String> statuses = new HashMap<>();
        for (SettingPicklistValue spv : spvs) {
            statuses.put(spv.getValueSf(), spv.getValueExt());
        }

        return statuses;
    }
}
