package eu.scalefactory.service;

import eu.scalefactory.domain.*;
import eu.scalefactory.repository.*;
import eu.scalefactory.service.dto.ChatParticipantDto;
import eu.scalefactory.service.dto.FreelancerDto;
import eu.scalefactory.service.dto.FreelancerMatchingProjectDto;
import eu.scalefactory.service.dto.MatchesProjectDto;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;

@Service
@Transactional
public class FreelancerService {

    private final CandidateRepository candidateRepository;
    private final FavoriteRepository favoriteRepository;
    private final ContactRepository contactRepository;
    private final ProjectRepository projectRepository;
    private final ProjectMemberRepository projectMemberRepository;
    private final PicklistService picklistService;
    private final ContactHelper contactHelper;

    public FreelancerService(CandidateRepository candidateRepository, FavoriteRepository favoriteRepository, ContactRepository contactRepository, ProjectRepository projectRepository, ProjectMemberRepository projectMemberRepository, PicklistService picklistService, ContactHelper contactHelper) {
        this.candidateRepository = candidateRepository;
        this.favoriteRepository = favoriteRepository;
        this.contactRepository = contactRepository;
        this.projectRepository = projectRepository;
        this.projectMemberRepository = projectMemberRepository;
        this.picklistService = picklistService;
        this.contactHelper = contactHelper;
    }

    public List<FreelancerDto> getMatches(String projectId, boolean all) {
        Contact myContact = contactHelper.getCurrentUserContact();

        List<Candidate> candidates;
        if (projectId != null) {
            Project project = projectRepository.findById(projectId).orElseThrow();
            checkUserAuthorizationForProject(myContact, project);

            if (all) {
                candidates = candidateRepository.findAllByProjectExternalIdAndStatusInAndContactStatusIn(projectId,
                    asList(Candidate.STATUS_PROPOSED, Candidate.STATUS_CONSIDERED_BY_CUSTOMER, Candidate.STATUS_INTERVIEW_PLANNED, Candidate.STATUS_ACCEPTED, Candidate.STATUS_REJECTED),
                    Collections.singletonList(Candidate.CONTACT_STATUS_INTERESTED));
            } else {
                candidates = candidateRepository.findAllByProjectExternalIdAndStatus(projectId, Candidate.STATUS_PROPOSED);
            }
        } else {

            List<ProjectCustomerTeam> approverProjectsForContact = contactHelper.getApproverProjectsForContact(myContact);
            if (approverProjectsForContact.size() > 0) {
                approverProjectsForContact.addAll(contactHelper.getDecisionMakerForContact(myContact));
                List<String> projectIds = approverProjectsForContact.stream().map(projectCustomerTeam -> projectCustomerTeam.getProject().getExternalId()).collect(toList());
                candidates = candidateRepository.findAllByProjectExternalIdInAndStatusAndContactStatusIn(projectIds, Candidate.STATUS_PROPOSED, Collections.singletonList(Candidate.CONTACT_STATUS_INTERESTED));
            } else {
                candidates = candidateRepository.findAllByProjectAccountAndStatusAndContactStatusIn(myContact.getAccount(), Candidate.STATUS_PROPOSED, Collections.singletonList(Candidate.CONTACT_STATUS_INTERESTED));
            }
        }

        List<Favorite> myFavorites = favoriteRepository.findAllByCustomer(myContact);

        return candidates.stream()
            .map(candidate -> FreelancerDto.fromCandidate(candidate, myFavorites, null, getLanguagesMap()))
            .collect(toList());
    }

    public List<FreelancerDto> getAcceptedForProjects(String projectId, boolean all) {
        Contact myContact = contactHelper.getCurrentUserContact();

        List<Candidate> candidates;
        if (projectId != null) {
            Project project = projectRepository.findById(projectId).orElseThrow();
            checkUserAuthorizationForProject(myContact, project);
            candidates = candidateRepository.findAllByProjectExternalIdAndStatusInAndContactStatusIn(projectId, asList(Candidate.STATUS_CONSIDERED_BY_CUSTOMER, Candidate.STATUS_INTERVIEW_PLANNED, Candidate.STATUS_ACCEPTED), Collections.singletonList(Candidate.CONTACT_STATUS_INTERESTED));
        } else {

            List<ProjectCustomerTeam> approverProjectsForContact = contactHelper.getApproverProjectsForContact(myContact);
            if (approverProjectsForContact.size() > 0) {
                approverProjectsForContact.addAll(contactHelper.getDecisionMakerForContact(myContact));
                List<String> projectIds = approverProjectsForContact.stream().map(projectCustomerTeam -> projectCustomerTeam.getProject().getExternalId()).collect(toList());
                candidates = candidateRepository.findAllByProjectExternalIdInAndStatusAndContactStatusIn(projectIds, Candidate.STATUS_CONSIDERED_BY_CUSTOMER, Collections.singletonList(Candidate.CONTACT_STATUS_INTERESTED));
            } else {
                candidates = candidateRepository.findAllByProjectAccountAndStatusAndContactStatusIn(myContact.getAccount(), Candidate.STATUS_CONSIDERED_BY_CUSTOMER, Collections.singletonList(Candidate.CONTACT_STATUS_INTERESTED));
            }

        }

        List<Favorite> myFavorites = favoriteRepository.findAllByCustomer(myContact);

        return candidates.stream()
            .map(candidate -> FreelancerDto.fromCandidate(candidate, myFavorites, null, getLanguagesMap()))
            .collect(toList());
    }

    public FreelancerDto getCandidate(String id) {
        Contact myContact = contactHelper.getCurrentUserContact();

        List<Favorite> myFavorites = favoriteRepository.findAllByCustomer(myContact);
        Candidate candidate = candidateRepository.findById(id).orElseThrow();
        checkUserAuthorizationForCandidate(myContact, candidate);

        List<FreelancerMatchingProjectDto> matchingProjects = getMatchingProjects(candidate.getContact().getId());
        return FreelancerDto.fromCandidate(candidate, myFavorites, matchingProjects, getLanguagesMap());
    }

    public void acceptCandidate(String candidateId) {
        Contact myContact = contactHelper.getCurrentUserContact();
        Candidate candidate = candidateRepository.findById(candidateId).orElseThrow();
        checkUserAuthorizationForCandidate(myContact, candidate);
        candidate.setStatus(Candidate.STATUS_CONSIDERED_BY_CUSTOMER);
        candidate.setStatusSetBy(Candidate.STATUS_SET_BY);
    }

    public void rejectCandidate(String candidateId, String reason) {
        Contact myContact = contactHelper.getCurrentUserContact();
        Candidate candidate = candidateRepository.findById(candidateId).orElseThrow();
        checkUserAuthorizationForCandidate(myContact, candidate);
        candidate.setStatus(Candidate.STATUS_REJECTED);
        candidate.setStatusSetBy(Candidate.STATUS_SET_BY);
        candidate.setReason(reason);
    }

    public List<FreelancerDto> getFavorites() {
        Contact myContact = contactHelper.getCurrentUserContact();
        List<Favorite> favorites = favoriteRepository.findAllByCustomer(myContact);
        return favorites.stream()
            .map((Favorite favorite) -> FreelancerDto.fromFavorite(favorite, favorites, null, getLanguagesMap()))
            .collect(toList());
    }

    public FreelancerDto getFavorite(String id) {
        Contact myContact = contactHelper.getCurrentUserContact();
        Favorite favorite = favoriteRepository.findById(id).orElseThrow();
        checkUserAuthorizationForFavorite(myContact, favorite);

        List<Favorite> favorites = favoriteRepository.findAllByCustomer(myContact);
        List<FreelancerMatchingProjectDto> matchingProjects = getMatchingProjects(favorite.getFreelancer().getId());
        return FreelancerDto.fromFavorite(favorite, favorites, matchingProjects, getLanguagesMap());
    }

    public void createFavorite(String freelancerContactId) {
        Contact freelancer = contactRepository.findById(freelancerContactId).orElseThrow();
        Contact myContact = contactHelper.getCurrentUserContact();

        Optional<Favorite> existingFavorite = favoriteRepository.findByCustomerAndFreelancer(myContact, freelancer);
        if (existingFavorite.isPresent()) {
            // one is enough
            return;
        }

        Favorite favorite = new Favorite();
        favorite.setFreelancer(freelancer);
        favorite.setCustomer(myContact);
        favoriteRepository.save(favorite);
    }

    public void deleteFavorite(String freelancerContactId) {
        Contact myContact = contactHelper.getCurrentUserContact();
        Contact freelancer = contactRepository.findById(freelancerContactId).orElseThrow();
        favoriteRepository.findByCustomerAndFreelancer(myContact, freelancer).orElseThrow();

        favoriteRepository.deleteByCustomerAndFreelancerExternalId(myContact, freelancerContactId);
    }

    public List<FreelancerDto> getAccepted() {
        Contact myContact = contactHelper.getCurrentUserContact();
        List<Candidate> candidates;
        List<ProjectCustomerTeam> approverProjectsForContact = contactHelper.getApproverProjectsForContact(myContact);
        if (approverProjectsForContact.size() > 0) {
            approverProjectsForContact.addAll(contactHelper.getDecisionMakerForContact(myContact));
            List<String> projectIds = approverProjectsForContact.stream().map(projectCustomerTeam -> projectCustomerTeam.getProject().getExternalId()).collect(toList());
            candidates = candidateRepository.findAllByProjectExternalIdInAndStatusInAndContactStatusIn(projectIds, asList(Candidate.STATUS_CONSIDERED_BY_CUSTOMER, Candidate.STATUS_INTERVIEW_PLANNED, Candidate.STATUS_ACCEPTED), Collections.singletonList(Candidate.CONTACT_STATUS_INTERESTED));
        } else {
            candidates = candidateRepository.findAllByProjectAccountAndStatusInAndContactStatusIn(
                myContact.getAccount(),
                asList(Candidate.STATUS_CONSIDERED_BY_CUSTOMER, Candidate.STATUS_INTERVIEW_PLANNED, Candidate.STATUS_ACCEPTED), Collections.singletonList(Candidate.CONTACT_STATUS_INTERESTED)
            );
        }


        List<Favorite> favorites = favoriteRepository.findAllByCustomer(myContact);

        return candidates.stream()
            .map(c -> FreelancerDto.fromCandidate(c, favorites, null, getLanguagesMap()))
            .collect(toList());
    }

    public List<FreelancerDto> getProposedNeverReviewed() {
        Contact myContact = contactHelper.getCurrentUserContact();

        List<Candidate> candidates;
        List<ProjectCustomerTeam> approverProjectsForContact = contactHelper.getApproverProjectsForContact(myContact);
        if (approverProjectsForContact.size() > 0) {
            approverProjectsForContact.addAll(contactHelper.getDecisionMakerForContact(myContact));
            List<String> projectIds = approverProjectsForContact.stream().map(projectCustomerTeam -> projectCustomerTeam.getProject().getExternalId()).collect(toList());
            candidates = candidateRepository.findAllByProjectExternalIdInAndProjectStatusInAndStatusAndContactStatusIn(projectIds,Arrays.asList(Project.STATUS_ONGOING, Project.STATUS_CLOSED, Project.STATUS_CANCELLED), Candidate.STATUS_PROPOSED, Collections.singletonList(Candidate.CONTACT_STATUS_INTERESTED));
        } else {

            candidates = candidateRepository.findAllByProjectAccountAndProjectStatusInAndStatusAndContactStatusIn(myContact.getAccount(), Arrays.asList(Project.STATUS_ONGOING, Project.STATUS_CLOSED, Project.STATUS_CANCELLED), Candidate.STATUS_PROPOSED, Collections.singletonList(Candidate.CONTACT_STATUS_INTERESTED));
        }


        List<Favorite> favorites = favoriteRepository.findAllByCustomer(myContact);
        return candidates.stream()
            .map(c -> FreelancerDto.fromCandidate(c, favorites, null, getLanguagesMap()))
            .collect(toList());
    }

    public List<FreelancerDto> getRejected() {
        Contact myContact = contactHelper.getCurrentUserContact();

        List<Candidate> candidates;
        List<ProjectCustomerTeam> approverProjectsForContact = contactHelper.getApproverProjectsForContact(myContact);
        if (approverProjectsForContact.size() > 0) {
            approverProjectsForContact.addAll(contactHelper.getDecisionMakerForContact(myContact));
            List<String> projectIds = approverProjectsForContact.stream().map(projectCustomerTeam -> projectCustomerTeam.getProject().getExternalId()).collect(toList());
            candidates = candidateRepository.findAllByProjectExternalIdInAndStatusAndContactStatusIn(projectIds,Candidate.STATUS_REJECTED, Collections.singletonList(Candidate.CONTACT_STATUS_INTERESTED));
        } else {

            candidates = candidateRepository.findAllByProjectAccountAndStatusAndContactStatusIn(myContact.getAccount(), Candidate.STATUS_REJECTED, Collections.singletonList(Candidate.CONTACT_STATUS_INTERESTED));
        }



        List<Favorite> favorites = favoriteRepository.findAllByCustomer(myContact);
        return candidates.stream()
            .map(c -> FreelancerDto.fromCandidate(c, favorites, null, getLanguagesMap()))
            .collect(toList());
    }

    public List<MatchesProjectDto> getAllProjectsWithMatches() {
        Contact currentUserContact = contactHelper.getCurrentUserContact();

        List<Project> projects;
        List<ProjectCustomerTeam> approverProjectsForContact = contactHelper.getApproverProjectsForContact(currentUserContact);
        if (approverProjectsForContact.size() > 0) {
            approverProjectsForContact.addAll(contactHelper.getDecisionMakerForContact(currentUserContact));
            List<String> projectIds = approverProjectsForContact.stream().map(projectCustomerTeam -> projectCustomerTeam.getProject().getExternalId()).collect(toList());
            projects = projectRepository.findAllByExternalIdInAndStatusNotIn(projectIds, asList(Project.STATUS_DRAFT, Project.STATUS_SIGN_CONTRACT, Project.STATUS_ONGOING, Project.STATUS_CLOSED, Project.STATUS_CANCELLED));
        } else {

            projects = projectRepository.findAllByAccountAndStatusNotIn(currentUserContact.getAccount(), asList(Project.STATUS_DRAFT, Project.STATUS_SIGN_CONTRACT, Project.STATUS_ONGOING, Project.STATUS_CLOSED, Project.STATUS_CANCELLED));
        }


        return projects.stream()
            .distinct()
            .map(p -> new MatchesProjectDto(p.getId(), p.getName()))
            .collect(toList());
    }

    public List<ChatParticipantDto> getChatFreelancers(String projectId) {
        Contact myContact = contactHelper.getCurrentUserContact();
        Project project = projectRepository.findById(projectId).orElseThrow();
        checkUserAuthorizationForProject(myContact, project);

        List<Candidate> candidates = candidateRepository.findAllByProjectExternalIdAndStatusInAndContactStatusIn(projectId, asList(Candidate.STATUS_PROPOSED, Candidate.STATUS_ACCEPTED, Candidate.STATUS_CONSIDERED, Candidate.STATUS_CONSIDERED_BY_CUSTOMER), Collections.singletonList(Candidate.CONTACT_STATUS_INTERESTED));
        List<ProjectMember> projectMembers = projectMemberRepository.findAllByProjectExternalId(projectId);

        List<ChatParticipantDto> freelancers = new ArrayList<>();

        freelancers.addAll(candidates.stream()
            .map(c -> new ChatParticipantDto(c.getContact().getId(), c.getContact().getName(), c.getContact().getRating(), ChatParticipantDto.Type.FREELANCER))
            .collect(Collectors.toList()));

        freelancers.addAll(projectMembers.stream()
            .map(pm -> new ChatParticipantDto(pm.getContact().getId(), pm.getContact().getName(), pm.getContact().getName(), ChatParticipantDto.Type.FREELANCER))
            .collect(Collectors.toList()));

        return freelancers.stream().distinct().collect(toList());
    }

    private List<FreelancerMatchingProjectDto> getMatchingProjects(String freelancerId) {
        Contact currentUserContact = contactHelper.getCurrentUserContact();


        return candidateRepository.findAllByContactExternalId(freelancerId)
            .stream()
            .filter(c -> c.getProject().getAccount().equals(currentUserContact.getAccount()))
            .map(c -> new FreelancerMatchingProjectDto(c.getProject().getId(), c.getProject().getName(), c.getSfid()))
            .distinct()
            .collect(toList());
    }

    // TODO: figure out if needed
    public Map<String, String> getLanguagesMap() {
        List<SettingPicklistValue> languages = picklistService.getPicklist(PicklistService.Picklist.CONTACT_LANGUAGES);
        Map<String, String> languagesMap = new HashMap<>();
        for (SettingPicklistValue spv : languages) {
            languagesMap.put(spv.getValueSf(), spv.getValueExt());
        }

        return languagesMap;
    }

    private void checkUserAuthorizationForCandidate(Contact currentUserContact, Candidate candidate) {
        String accountId = currentUserContact.getAccount().getId();

        if (!accountId.equals(candidate.getProject().getAccount().getId())) {
            throw new AccessDeniedException("User does not have permission to access candidate.");
        }
    }

    private void checkUserAuthorizationForFavorite(Contact currentUserContact, Favorite favorite) {
        if (!favorite.getCustomer().getExternalId().equals(currentUserContact.getExternalId())) {
            throw new AccessDeniedException("User does not have permission to access favorite.");
        }
    }

    private void checkUserAuthorizationForProject(Contact currentUserContact, Project project) {
        String accountId = currentUserContact.getAccount().getId();

        if (!accountId.equals(project.getAccount().getId())) {
            throw new AccessDeniedException("User does not have permission to access project.");
        }
    }

}
