package eu.scalefactory.service.dto;

import eu.scalefactory.domain.TimesheetEntry;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Setter
@Getter
public class TimesheetDto {

    private LocalDate dateFrom;
    private LocalDate dateTo;
    private Set<Freelancer> freelancers = new HashSet<>();
    private Set<Project> projects = new HashSet<>();
    private List<TimesheetEntry> entries = new ArrayList<>();


    @Getter
    @Setter
    @EqualsAndHashCode(of = "id")
    public static class Freelancer {
        private String id;
        private String name;

        public Freelancer(TimesheetEntry entry) {
            this.id = entry.getProjectMember().getContact().getId();
            this.name = entry.getProjectMember().getContact().getName();
        }

        public Freelancer(String id, String name) {
            this.id = id;
            this.name = name;
        }
    }

    @Getter
    @Setter
    @EqualsAndHashCode(of = "id")
    public static class Project {
        private String id;
        private String name;

        public Project(TimesheetEntry entry) {
            this.id = entry.getProject().getId();
            this.name = entry.getProject().getName();
        }
    }

}
