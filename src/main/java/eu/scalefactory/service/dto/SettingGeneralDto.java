package eu.scalefactory.service.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class SettingGeneralDto {
    private String privacyPolicy;
    private String userAgreement;
}
