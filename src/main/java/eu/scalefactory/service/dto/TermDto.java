package eu.scalefactory.service.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor

public class TermDto {
    private String text;
    private String language;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TermDto termDto = (TermDto) o;

        return Objects.equals(text.toLowerCase(), termDto.text.toLowerCase());
    }

    @Override
    public int hashCode() {
        return text != null ? text.hashCode() : 0;
    }
}
