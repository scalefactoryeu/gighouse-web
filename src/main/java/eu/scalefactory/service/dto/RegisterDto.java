package eu.scalefactory.service.dto;

import lombok.Data;

@Data
public class RegisterDto {

    private String firstName;
    private String lastName;
    private String phone;
    private String companyName;
    private String vatNumber;
    private Boolean subscribeToNewsletter;
    private Boolean terms;
    private Boolean privacy;

}
