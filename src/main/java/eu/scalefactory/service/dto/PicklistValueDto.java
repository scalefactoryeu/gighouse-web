package eu.scalefactory.service.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class PicklistValueDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private String value;
    private String label;
}
