package eu.scalefactory.service.dto;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = {"id"})
public class ChatParticipantDto {
    private String id;
    private String name;
    private String rating;
    private Type type = Type.FREELANCER;

    public enum Type {
        FREELANCER, CUSTOMER
    }
}
