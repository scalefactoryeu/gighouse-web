package eu.scalefactory.service.dto;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Data
@Getter
@Setter
public class InvoiceFreelancerDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;
    private String name;
}
