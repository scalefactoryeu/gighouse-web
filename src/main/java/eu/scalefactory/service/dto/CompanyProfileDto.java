package eu.scalefactory.service.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class CompanyProfileDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private String name;
    private String companyFormat;
    private String street;
    private String postalCode;
    private String city;
    private String state;
    private String country;
    private String invoiceEmail;
    private String phone;
    private String vatNumber;
}
