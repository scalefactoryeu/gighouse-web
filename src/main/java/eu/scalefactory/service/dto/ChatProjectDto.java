package eu.scalefactory.service.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ChatProjectDto {

    private String id;
    private String name;

}
