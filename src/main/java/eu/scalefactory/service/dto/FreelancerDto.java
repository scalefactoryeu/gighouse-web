package eu.scalefactory.service.dto;

import eu.scalefactory.domain.Candidate;
import eu.scalefactory.domain.Contact;
import eu.scalefactory.domain.ContactLanguage;
import eu.scalefactory.domain.Favorite;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.StringUtils;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

@Getter
@Setter
public class FreelancerDto {

    private String candidateId;
    private String contactId;
    private String favouriteId;
    private Type type;
    private String name;
    private String profileName;
    private String description;
    private String rating;
    private String state;
    private Double rateRequestedByFreelancer;
    private Double rateProposedToCustomer;
    private LocalDate availabilityDate;
    private Double availableDaysPerWeek;
    private Boolean hasOwnCar;
    private Boolean availableForTeleworking;
    private Boolean doesWeekendWork;
    private Boolean doesEveningWork;
    private Boolean isFavourite = false;
    private String candidateStatus;
    private String candidateProjectName;
    private String gighouseOpinion;
    private List<LanguageDto> contactLanguages = new ArrayList<>();
    private List<FreelancerMatchingProjectDto> matchingProjects = new ArrayList<>();
    private List<ContactSkillDto> skills = new ArrayList<>();
    private List<ContactPortfolioDto> portfolios = new ArrayList<>();
    private List<ContactReferralDto> referrals = new ArrayList<>();

    private enum Type {
        MATCH, FAVORITE, REJECTED
    }

    public static FreelancerDto fromContact(Contact contact, List<Favorite> myFavorites, List<FreelancerMatchingProjectDto> matchingProjects, Map<String, String> languages) {
        FreelancerDto freelancer = new FreelancerDto();

        freelancer.setContactId(contact.getId());
        freelancer.setName(contact.getShortName());
        freelancer.setProfileName(contact.getProfileName());
        freelancer.setDescription(contact.getDescription());
        freelancer.setGighouseOpinion(contact.getGighouseOpinion());
        freelancer.setRating(contact.getRating());
        freelancer.setState(contact.getMailingState());
        freelancer.setAvailabilityDate(contact.getAvailabilityDate());
        freelancer.setAvailableDaysPerWeek(contact.getAvailableDaysAWeek());
        freelancer.setHasOwnCar(contact.getOwnCar());
        freelancer.setAvailableForTeleworking(contact.getTeleworking());
        freelancer.setDoesWeekendWork(contact.getDoesWeekendWork());
        freelancer.setDoesEveningWork(contact.getDoesEveningWork());
        freelancer.setRateRequestedByFreelancer(contact.getDayFee());
        freelancer.setMatchingProjects(matchingProjects);

        Favorite fav = getMatchingFavorite(freelancer.getContactId(), myFavorites);
        if (fav != null) {
            freelancer.setFavouriteId(fav.getExternalId());
            freelancer.setIsFavourite(true);
        }

        for (ContactLanguage contactLanguage : contact.getContactLanguages()) {
            String languageName = languages.get(contactLanguage.getLanguage());
            if (languageName == null) {
                languageName = contactLanguage.getLanguage();
            }
            freelancer.getContactLanguages().add(new LanguageDto(languageName, contactLanguage.getLevel()));
        }

        freelancer.setContactLanguages(contact.getContactLanguages().stream().map(l -> new LanguageDto(l.getLanguage(), l.getLevel())).collect(toList()));
        freelancer.setSkills(contact.getSkills().stream().map(ContactSkillDto::new).collect(toList()));
        freelancer.setPortfolios(contact.getPortfolios().stream().map(ContactPortfolioDto::new).collect(toList()));
        freelancer.setReferrals(contact.getReferrals().stream().map(ContactReferralDto::new).collect(toList()));


        return freelancer;
    }

    public static FreelancerDto fromCandidate(Candidate candidate, List<Favorite> myFavorites, List<FreelancerMatchingProjectDto> matchingProjects, Map<String, String> languages) {
        FreelancerDto resp = fromContact(candidate.getContact(), myFavorites, matchingProjects, languages);
        resp.setCandidateId(candidate.getSfid());
        if (candidate.getRate() != null) {
            resp.setRateRequestedByFreelancer(candidate.getRate().getRateRequestedByFreelancer());
            resp.setRateProposedToCustomer(candidate.getRate().getRateProposedToCustomer());
        }
        resp.setType(Type.MATCH);
        resp.setCandidateStatus(candidate.getStatus());
        resp.setCandidateProjectName(candidate.getProject().getName());
        if (!StringUtils.isEmpty(candidate.getGighouseOpinion())) {
            resp.setGighouseOpinion(candidate.getGighouseOpinion());
        }

        if (!StringUtils.isEmpty(candidate.getFreelancerDescription())) {
            resp.setDescription(candidate.getFreelancerDescription());
        }

        return resp;
    }

    public static FreelancerDto fromFavorite(Favorite favorite, List<Favorite> myFavorites, List<FreelancerMatchingProjectDto> matchingProjects, Map<String, String> languages) {
        FreelancerDto resp = fromContact(favorite.getFreelancer(), myFavorites, matchingProjects, languages);
        resp.setFavouriteId(favorite.getExternalId());
        resp.setIsFavourite(true);
        resp.setType(Type.FAVORITE);
        return resp;
    }

    private static Favorite getMatchingFavorite(String contactId, List<Favorite> favorites) {
        for (Favorite f : favorites) {
            if (f.getFreelancer().getId().equals(contactId)) {
                return f;
            }
        }

        return null;
    }

}
