package eu.scalefactory.service.dto;

import eu.scalefactory.domain.Portfolio;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ContactPortfolioDto {
    private String id;
    private String name;
    private String description;
    private String linkToSource;
    private String type;

    public ContactPortfolioDto(Portfolio portfolio) {
        this.id = portfolio.getSfid();
        this.name = portfolio.getName();
        this.description = portfolio.getDescription();
        if (portfolio.getLinkToSource().contains("http")) {
            this.linkToSource = portfolio.getLinkToSource();
        } else {
            this.linkToSource = "https://" + portfolio.getLinkToSource();
        }

        this.type = portfolio.getType();
    }
}
