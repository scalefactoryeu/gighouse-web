package eu.scalefactory.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LanguageDto {

    private String language;
    private String level;

}
