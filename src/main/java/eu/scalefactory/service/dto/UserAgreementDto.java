package eu.scalefactory.service.dto;

import lombok.Data;

@Data
public class UserAgreementDto {

    private String userAgreement;

}
