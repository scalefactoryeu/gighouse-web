package eu.scalefactory.service.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class FollowUpActionsDto {
    private int rateFreelancersCount;
    private int projectsInDraftCount;
    private int submittedTimeSheetsCount;
    private Double subscriptionTypeAccount;

}
