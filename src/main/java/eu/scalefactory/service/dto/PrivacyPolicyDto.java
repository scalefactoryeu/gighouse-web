package eu.scalefactory.service.dto;

import lombok.Data;

@Data
public class PrivacyPolicyDto {

    private String privacyPolicy;

}
