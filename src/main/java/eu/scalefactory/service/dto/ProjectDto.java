package eu.scalefactory.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import eu.scalefactory.service.ProjectService;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
public class ProjectDto {

    private String id;
    private String name;
    private String description;
    private LocalDate startDate;
    private LocalDate endDate;
    private LocalDate sentDate;
    private LocalDate requestedStartDate;
    private String postalCode;
    private String country;
    private String state;
    private String status;
    private String teaser1;
    private String teaser2;
    private String teaser3;
    private Double maxBudget;
    private Double daysPerWeek;
    private Boolean homeWork;
    private Double estimatedProjectDuration;
    private LocalDate archivedDate;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<ProjectService.ProjectSkillData> skills = new ArrayList<>();

}
