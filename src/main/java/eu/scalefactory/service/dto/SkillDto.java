package eu.scalefactory.service.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class SkillDto {

    private String id;
    private String name;
    private String type;
    private String parent;

}
