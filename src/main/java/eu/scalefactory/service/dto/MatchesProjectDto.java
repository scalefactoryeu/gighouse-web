package eu.scalefactory.service.dto;

import lombok.Data;

@Data
public class MatchesProjectDto {

    private String id;
    private String name;

    public MatchesProjectDto(String id, String name) {
        String[] splittedName = name.split(" - ", 2);
        name = splittedName.length > 1 ? splittedName[1] : splittedName[0];

        this.id = id;
        this.name = name;
    }
}
