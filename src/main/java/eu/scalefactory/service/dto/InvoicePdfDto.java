package eu.scalefactory.service.dto;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Data
@Getter
@Setter
public class InvoicePdfDto implements Serializable {
    private String invoiceBlob;

}
