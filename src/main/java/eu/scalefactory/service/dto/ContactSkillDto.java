package eu.scalefactory.service.dto;

import eu.scalefactory.domain.ContactSkill;
import eu.scalefactory.domain.Skill;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ContactSkillDto {

    private String id;
    private Double rating;
    private SkillDto skill;

    public ContactSkillDto(ContactSkill contactSkill) {
        this.id = contactSkill.getSfid();
        this.rating = contactSkill.getRating();

        Skill skill = contactSkill.getSkill();
        this.skill = new SkillDto(skill.getId(), skill.getName(), skill.getType(), skill.getParentId());
    }

}
