package eu.scalefactory.service.dto;

import lombok.Data;

@Data
public class GighouseContactDto {

    private String name;
    private String phone;
    private String email;

}
