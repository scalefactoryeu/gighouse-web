package eu.scalefactory.service.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class GetInvoicePdfDto implements Serializable {
    private String invoice;
}
