package eu.scalefactory.service.dto;

import eu.scalefactory.domain.Referral;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ContactReferralDto {
    private String id;
    private String name;
    private String rating;
    private String quote;
    private String detail;
    private String profile;

    public ContactReferralDto(Referral referral) {
        this.id = referral.getSfid();
        this.name = referral.getName();
        this.rating = referral.getOverallRating();
        this.quote = referral.getQuote();
        this.detail = referral.getDetail();
        this.profile = referral.getProfile().getName();
    }
}
