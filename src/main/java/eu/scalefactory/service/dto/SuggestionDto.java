package eu.scalefactory.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SuggestionDto {
    @JsonProperty
    private String keyword;
    @JsonProperty
    private String queryItemType;
    @JsonProperty
    private double weight;
    @JsonProperty
    private boolean required;

}
