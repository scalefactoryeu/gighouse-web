package eu.scalefactory.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class FreelancerMatchingProjectDto {

    private String id;
    private String name;
    private String candidateId;

}
