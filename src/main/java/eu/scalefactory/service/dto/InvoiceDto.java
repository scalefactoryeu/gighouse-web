package eu.scalefactory.service.dto;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
public class InvoiceDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;
    private String invoiceNumber;
    private LocalDate invoiceDate;
    private LocalDate periodStartDate;
    private LocalDate periodEndDate;
    private Double totalAmountWithVAT;
    private String status;
    private List<InvoiceFreelancerDto> freelancers = new ArrayList<>();
}
