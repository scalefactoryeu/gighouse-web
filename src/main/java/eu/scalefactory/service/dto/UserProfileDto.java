package eu.scalefactory.service.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserProfileDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private String firstName;
    private String lastName;
    private String street;
    private String postalCode;
    private String city;
    private String state;
    private String country;
    private String email;
    private String phone;
}
