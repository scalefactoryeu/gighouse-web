package eu.scalefactory.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import java.util.List;
import java.util.Objects;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class AutocompleteDto {
    private String id;
    private String indexId;
    private String type;
    private TermDto matchedTerm;
    private List<TermDto> terms;
    private List<TermDto> alternatives;
    private List<TermDto> parts;
    private List<TermDto> context;
    private List<TermDto> flags;
    private int frequency;
    private String matchedText;
    private String definition;
    private List<TermDto> expansions;



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AutocompleteDto that = (AutocompleteDto) o;

        return Objects.equals(matchedTerm, that.matchedTerm);
    }

    @Override
    public int hashCode() {
        return matchedTerm != null ? matchedTerm.hashCode() : 0;
    }
}
