package eu.scalefactory.service;

import eu.scalefactory.domain.Skill;
import eu.scalefactory.repository.SkillRepository;
import eu.scalefactory.service.dto.SkillDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class SkillService {

    private final SkillRepository skillRepository;

    public SkillService(SkillRepository skillRepository) {
        this.skillRepository = skillRepository;
    }

    @Transactional(readOnly = true)
    public List<SkillDto> getAllSkills() {
        List<Skill> skills = skillRepository.findAllByVisibleInMobileAppTrue();
        return skills.stream().map(s -> new SkillDto(s.getSfid(), s.getName(), s.getType(), s.getParentId())).collect(Collectors.toList());
    }

}
