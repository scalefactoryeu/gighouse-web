package eu.scalefactory.service;

import eu.scalefactory.domain.*;
import eu.scalefactory.repository.*;
import eu.scalefactory.security.SecurityUtils;
import eu.scalefactory.service.dto.GighouseContactDto;
import eu.scalefactory.service.dto.RegisterDto;
import eu.scalefactory.service.mapper.ProfileMapper;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ProfileService {

    private final ContactRepository contactRepository;
    private final ContactUserSettingRepository contactUserSettingRepository;
    private final AccountRepository accountRepository;
    private final UserRepository userRepository;
    private final PicklistService picklistService;
    private final ProfileMapper profileMapper;
    private final ContactHelper contactHelper;
    private final RecordTypeRepository recordTypeRepository;

    public ProfileService(ContactRepository contactRepository, ContactUserSettingRepository contactUserSettingRepository, AccountRepository accountRepository, UserRepository userRepository, PicklistService picklistService, ProfileMapper profileMapper, ContactHelper contactHelper, RecordTypeRepository recordTypeRepository) {
        this.contactRepository = contactRepository;
        this.contactUserSettingRepository = contactUserSettingRepository;
        this.accountRepository = accountRepository;
        this.userRepository = userRepository;
        this.picklistService = picklistService;
        this.profileMapper = profileMapper;
        this.contactHelper = contactHelper;
        this.recordTypeRepository = recordTypeRepository;
    }

    public Contact getUserProfile() {
        return contactHelper.getCurrentUserContact();
    }

    public Contact updateUserProfile(UserProfileData data) {
        data.validate(picklistService.getPicklist(PicklistService.Picklist.STATES));

        Contact contact = contactHelper.getCurrentUserContact();
        toContact(data, contact);

        return contact;
    }

    private void toContact(UserProfileData data, Contact contact) {
        contact.setFirstName(data.getFirstName());
        contact.setLastName(data.getLastName());
        contact.setMailingStreet(data.getMailingStreet());
        contact.setMailingPostalCode(data.getMailingPostalCode());
        contact.setMailingCity(data.getMailingCity());
        contact.setMailingState(data.getMailingState());
        contact.setEmail(data.getEmail());
        contact.setPhone(data.getPhone());
    }

    @Getter
    @Setter
    public static class UserProfileData {
        private String firstName;
        private String lastName;
        private String mailingStreet;
        private String mailingPostalCode;
        private String mailingCity;
        private String mailingState;
        private String email;
        private String phone;

        public void validate(List<SettingPicklistValue> validStates) {
            if (firstName.length() > 40) {
                throw new RuntimeException("First name more than 40 characters");
            }
            if (lastName.length() > 80) {
                throw new RuntimeException("Last name more than 80 characters");
            }
            if (mailingStreet.length() > 255) {
                throw new RuntimeException("Mailing street more than 255 characters");
            }
            if (mailingPostalCode.length() > 20) {
                throw new RuntimeException("Mailing postal code more than 20 characters");
            }
            if (mailingCity.length() > 40) {
                throw new RuntimeException("Mailing city more than 40 characters");
            }
            if (mailingState.length() > 80) {
                throw new RuntimeException("Mailing state more than 80 characters");
            }
            if (validStates.stream().noneMatch(s -> s.getValueSf().equals(mailingState))) {
                throw new RuntimeException("Mailing state not in picklist");
            }
            if (email.length() > 80) {
                throw new RuntimeException("Email more than 80 characters");
            }
            if (!EmailValidator.getInstance().isValid(email)) {
                throw new RuntimeException("Email is not valid");
            }
            if (phone.length() > 40) {
                throw new RuntimeException("Phone number more than 40 characters");
            }
        }
    }

    public Account getCompanyProfile() {
        return contactHelper.getCurrentUserContact().getAccount();
    }

    public Account updateCompanyProfile(CompanyProfileData data) {
        data.validate(picklistService.getPicklist(PicklistService.Picklist.STATES));

        Account account = contactHelper.getCurrentUserContact().getAccount();
        toAccount(data, account);

        return account;
    }

    private void toAccount(CompanyProfileData data, Account account) {
        account.setCompanyName(data.getCompanyName());
        account.setCompanyFormat(data.getCompanyFormat());
        account.setBillingStreet(data.getBillingStreet());
        account.setBillingPostalCode(data.getBillingPostalCode());
        account.setBillingCity(data.getBillingCity());
        account.setBillingState(data.getBillingState());
        account.setInvoiceEmail(data.getInvoiceEmail());
        account.setPhone(data.getPhone());
        account.setVatNumber(data.getVatNumber());
    }

    @Getter
    @Setter
    public static class CompanyProfileData {
        private String companyName;
        private String companyFormat;
        private String billingStreet;
        private String billingPostalCode;
        private String billingCity;
        private String billingState;
        private String invoiceEmail;
        private String phone;
        private String vatNumber;

        public void validate(List<SettingPicklistValue> validStates) {
            if (companyName.length() > 255) {
                throw new RuntimeException("Company name more than 255 characters");
            }
            if (companyFormat.length() > 255) {
                throw new RuntimeException("Company format more than 255 characters");
            }
            if (billingStreet.length() > 255) {
                throw new RuntimeException("Billing street more than 255 characters");
            }
            if (billingPostalCode.length() > 20) {
                throw new RuntimeException("Billing postal code more than 20 characters");
            }
            if (billingCity.length() > 40) {
                throw new RuntimeException("Billing city more than 40 characters");
            }
            if (billingState.length() > 80) {
                throw new RuntimeException("Billing state more than 80 characters");
            }
            if (validStates.stream().noneMatch(s -> s.getValueSf().equals(billingState))) {
                throw new RuntimeException("Billing state not in picklist");
            }
            if (invoiceEmail.length() > 80) {
                throw new RuntimeException("Invoice email more than 80 characters");
            }
            if (!EmailValidator.getInstance().isValid(invoiceEmail)) {
                throw new RuntimeException("Invoice email is not valid");
            }
            if (phone.length() > 40) {
                throw new RuntimeException("Phone number more than 40 characters");
            }
            if (vatNumber.length() > 15) {
                throw new RuntimeException("VAT number more than 15 characters");
            }
        }
    }

    public Page<Contact> getCoworkers(Pageable pageable) {
        Contact userContact = contactHelper.getCurrentUserContact();

        Page<Contact> page = contactRepository.findAllByAccountAndExternalIdNotOrderByExternalId(
            userContact.getAccount(),
            userContact.getId(),
            pageable);

        return page;
    }

    public GighouseContactDto getGighouseContact() {
        Contact userContact = contactHelper.getCurrentUserContact();
        Contact adminContact = contactUserSettingRepository.findByUser(userContact.getAccount().getOwner()).orElseThrow().getContact();
        return profileMapper.toGighouseContactDTO(adminContact);
    }

    public void register(RegisterDto registerData) {
        String currentUserLogin = SecurityUtils.getCurrentUserLogin().orElseThrow();
        User user = userRepository.findOneByLogin(currentUserLogin).orElseThrow();

        Optional<Contact> existingContact = contactRepository.findByEmailAndRecordTypeName(user.getEmail(), Contact.CUSTOMER_CONTACT);
        if (existingContact.isPresent()) {
            throw new RuntimeException("already registered");
        }

        String vatNumber = registerData.getVatNumber().replace(" ", "").replace(".", " ");

        Optional<List<Account>> accountOptional = accountRepository.findByVatNumberAndRecordTypeName(vatNumber, Account.CUSTOMER_ACCOUNT);
        Optional<RecordType> accountRecordType = recordTypeRepository.findByName(Account.CUSTOMER_ACCOUNT);
        Optional<RecordType> contactRecordType = recordTypeRepository.findByName(Contact.CUSTOMER_CONTACT);
        boolean isWebAllowed = accountOptional.isPresent() ? false : true;
        Account account;
        if (accountOptional.isPresent() && accountOptional.get().size() > 0) {
            account = accountOptional.get().get(0);
        } else {
            account = new Account();
            account.setInvoiceEmail(user.getEmail());
            account.setCompanyName(registerData.getCompanyName());
            account.setVatNumber(vatNumber);
            if (registerData.getPrivacy() && registerData.getTerms()) {
                account.setConsent(true);
                account.setConsentDate(LocalDate.now());
            }
            account.setRegistrationSource("Webapplication");
            account.setType("Registered");
            accountRecordType.ifPresent(account::setRecordType);
        }


        Contact contact = new Contact();
        contact.setEmail(user.getEmail());
        contact.setAccount(account);
        contact.setFirstName(registerData.getFirstName());
        contact.setLastName(registerData.getLastName());
        contact.setPhone(registerData.getPhone());
        contact.setRegistationSource("Webapplication");
        contact.setRegisteredOnWebsite(true);
        contact.setIsWebAllowed(isWebAllowed);
        if (registerData.getTerms()) {
            contact.setConsent(true);
            contact.setConsentDate(LocalDate.now());
        }
        if (registerData.getPrivacy()) {
            contact.setPrivacyPolicy(true);
            contact.setPrivacyPolicySignature(LocalDateTime.now());
        }
        contactRecordType.ifPresent(contact::setRecordType);

        accountRepository.save(account);
        contactRepository.save(contact);
    }

    public boolean duplicateVATNumber(String vatNumber) {
        return accountRepository.findByVatNumberAndRecordTypeName(vatNumber, Account.CUSTOMER_ACCOUNT).isPresent();
    }
}
