package eu.scalefactory.service;

import eu.scalefactory.domain.Contact;
import eu.scalefactory.domain.ProjectCustomerTeam;
import eu.scalefactory.domain.User;
import eu.scalefactory.repository.ContactRepository;
import eu.scalefactory.repository.ProjectCustomerTeamRepository;
import eu.scalefactory.repository.UserRepository;
import eu.scalefactory.security.SecurityUtils;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ContactHelper {

    public static final String APPROVER = "APPROVER";
    public static final String DECISION_MAKER = "DECISION MAKER";
    private final UserRepository userRepository;
    private final ContactRepository contactRepository;
    private final ProjectCustomerTeamRepository projectCustomerTeamRepository;

    public ContactHelper(UserRepository userRepository, ContactRepository contactRepository, ProjectCustomerTeamRepository projectCustomerTeamRepository) {
        this.userRepository = userRepository;
        this.contactRepository = contactRepository;
        this.projectCustomerTeamRepository = projectCustomerTeamRepository;
    }

    public Contact getCurrentUserContact() {
        String currentUserLogin = SecurityUtils.getCurrentUserLogin()
            .orElseThrow(() -> new RuntimeException("Failed to get current user login"));
        User user = userRepository.findOneByLogin(currentUserLogin).orElseThrow();

        Contact myContact;
//        if (currentUserLogin.equals("admin")) {
//            // for dev only
//            myContact = contactRepository.findAllByRecordTypeName(Pageable.unpaged(), "Customer Contact").getContent().get(0);
//        } else {
        myContact = contactRepository.findByEmailAndRecordTypeName(user.getEmail(), Contact.CUSTOMER_CONTACT).orElse(null);
//        }

        // todo
//        myContact = contactRepository.findAllByRecordTypeName(Pageable.unpaged(), "Customer Contact").getContent().get(0);

        return myContact;
    }


    public List<ProjectCustomerTeam> getApproverProjectsForContact(Contact contact) {
        List<ProjectCustomerTeam> allByContact = projectCustomerTeamRepository.findAllByContact(contact);
        //filter where he's approver
        return allByContact.stream().filter(projectCustomerTeam -> projectCustomerTeam.getRole().equalsIgnoreCase(APPROVER)).collect(Collectors.toList());
    }

    public List<ProjectCustomerTeam> getDecisionMakerForContact(Contact contact) {
        List<ProjectCustomerTeam> allByContact = projectCustomerTeamRepository.findAllByContact(contact);
        //filter where he's approver
        return allByContact.stream().filter(projectCustomerTeam -> projectCustomerTeam.getRole().equalsIgnoreCase(DECISION_MAKER)).collect(Collectors.toList());
    }



    public static void checkUserIsCustomer(Contact user) {
        if (!"Customer Contact".equals(user.getRecordType().getName())) {
            throw new AccessDeniedException("User is not a customer.");
        }
    }
}
