package eu.scalefactory.service;

import eu.scalefactory.domain.SettingGeneral;
import eu.scalefactory.repository.SettingGeneralRepository;
import eu.scalefactory.service.dto.SettingGeneralDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class SettingGeneralService {
    private final SettingGeneralRepository settingGeneralRepository;

    public SettingGeneralService(SettingGeneralRepository settingGeneralRepository) {
        this.settingGeneralRepository = settingGeneralRepository;
    }

    @Transactional(readOnly = true)
    public SettingGeneralDto getGeneralSetting() {
        SettingGeneral settingGeneral = settingGeneralRepository.findAll().stream().findFirst().orElseThrow(() -> new RuntimeException("no general settings"));
        return new SettingGeneralDto(settingGeneral.getPrivacyPolicy(), settingGeneral.getUserAgreement());
    }
}
