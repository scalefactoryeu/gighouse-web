package eu.scalefactory.service;

import eu.scalefactory.config.ApplicationProperties;
import eu.scalefactory.domain.Contact;
import eu.scalefactory.domain.Invoice;
import eu.scalefactory.domain.ProjectCustomerTeam;
import eu.scalefactory.repository.InvoiceRepository;
import eu.scalefactory.repository.InvoiceSpec;
import eu.scalefactory.service.dto.GetInvoicePdfDto;
import eu.scalefactory.service.dto.InvoiceDto;
import eu.scalefactory.service.mapper.InvoiceMapper;
import eu.scalefactory.web.rest.InvoiceResource;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

@Service
@Transactional
public class InvoiceService {
    private static final Logger LOG = LoggerFactory.getLogger(InvoiceResource.class);
//    public static final String ACCOUNT_ID_NOWJOBS_1 = "0011t000003aiSdAAI";
//    public static final String ACCOUNT_ID_NOWJOBS_2 = "0016N00000KgiNqQAJ";
//    public static final String ACCOUNT_ID_NOWJOBS_3 = "0011t00000vKUTuAAO";
//    public static final String ACCOUNT_ID_HOUSE_OF_INVEST = "0016N000007KmvsQAC";
//    public static final String ACCOUNT_ID_HOUSE_OF_HR_1 = "0011t000003aiS9AAI";
//    public static final String ACCOUNT_ID_HOUSE_OF_HR_2 = "0016N00000RniwlQAB";
//    public static final String ACCOUNT_ID_HOUSE_OF_HR_3 = "0016N00000F0e80QAB";
//    public static final String ACCOUNT_ID_HOUSE_OF_HR_4 = "0016N00000CHK4zQAH";

    private final InvoiceRepository invoiceRepository;
    private final InvoiceMapper invoiceMapper;
    private final ContactHelper contactHelper;
    private final ApplicationProperties props;

    public InvoiceService(ApplicationProperties props, InvoiceRepository invoiceRepository, InvoiceMapper invoiceMapper, ContactHelper contactHelper) {
        this.props = props;
        this.invoiceRepository = invoiceRepository;
        this.invoiceMapper = invoiceMapper;
        this.contactHelper = contactHelper;
    }

    public List<InvoiceDto> getInvoices(InvoiceSpec.InvoiceFilter filter) {
        Contact currentUserContact = contactHelper.getCurrentUserContact();
        List<ProjectCustomerTeam> approverProjectsForContact = contactHelper.getApproverProjectsForContact(currentUserContact);
        if (approverProjectsForContact.size() > 0) {
            approverProjectsForContact.addAll(contactHelper.getDecisionMakerForContact(currentUserContact));
            List<String> projects = approverProjectsForContact.stream().map(projectCustomerTeam -> projectCustomerTeam.getProject().getExternalId()).collect(toList());
            filter.setProjects(projects);

        } else {
            filter.setAccountId(contactHelper.getCurrentUserContact().getAccount().getId());
        }

        List<Invoice> invoices;
//        if (filterAccountNotPermittedToSeeInvoices(filter)) {
            invoices = invoiceRepository.findAll(new InvoiceSpec(filter));
            return invoices.stream().map(invoiceMapper::invoiceToInvoiceDTO).collect(Collectors.toList());
//        } else {
//            return Collections.emptyList();
//        }
    }

    public String geInvoice(String invoiceNumber) {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity httpEntity = new HttpEntity<>(createHeaders(props.getInvoice().getUsername(), props.getInvoice().getPassword()));
        String url = props.getInvoice().getUrl() + "/private/invoice/" + invoiceNumber;
        ResponseEntity<GetInvoicePdfDto> entity = restTemplate.exchange(url, HttpMethod.GET, httpEntity, GetInvoicePdfDto.class);
        if (entity.getStatusCodeValue() == 200 && entity.getBody() != null) {
            return entity.getBody().getInvoice();
        }
        return "";
    }

    private HttpHeaders createHeaders(String username, String password) {
        return new HttpHeaders() {{
            String auth = username + ":" + password;
            byte[] encodedAuth = Base64.encodeBase64(
                auth.getBytes(Charset.forName("US-ASCII")));
            String authHeader = "Basic " + new String(encodedAuth);
            set("Authorization", authHeader);
        }};
    }

//    private boolean filterAccountNotPermittedToSeeInvoices(InvoiceSpec.InvoiceFilter filter) {
//        return !filter.getAccountId().equals(ACCOUNT_ID_NOWJOBS_1) &&
//            !filter.getAccountId().equals(ACCOUNT_ID_NOWJOBS_2) &&
//            !filter.getAccountId().equals(ACCOUNT_ID_NOWJOBS_3) &&
//            !filter.getAccountId().equals(ACCOUNT_ID_HOUSE_OF_INVEST) &&
//            !filter.getAccountId().equals(ACCOUNT_ID_HOUSE_OF_HR_1) &&
//            !filter.getAccountId().equals(ACCOUNT_ID_HOUSE_OF_HR_2) &&
//            !filter.getAccountId().equals(ACCOUNT_ID_HOUSE_OF_HR_3) &&
//            !filter.getAccountId().equals(ACCOUNT_ID_HOUSE_OF_HR_4);
//    }
}
