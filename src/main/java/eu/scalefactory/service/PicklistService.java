package eu.scalefactory.service;

import eu.scalefactory.domain.SettingPicklistValue;
import eu.scalefactory.repository.SettingPicklistValueRepository;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class PicklistService {

    private final SettingPicklistValueRepository settingPicklistValueRepository;

    public PicklistService(SettingPicklistValueRepository settingPicklistValueRepository) {
        this.settingPicklistValueRepository = settingPicklistValueRepository;
    }

    @Cacheable(value = "picklists", key="#picklist")
    public List<SettingPicklistValue> getPicklist(Picklist picklist) {
        return settingPicklistValueRepository.findAllByObjectNameAndFieldNameAndIsDeletedFalse(picklist.getObject(), picklist.getField());
    }

    public enum Picklist {
        STATES("gig__Project__c", "gig__State__c"),
        COMPANY_FORMATS("Account", "gig__Company_Format__c"),
        CONTACT_LANGUAGES("gig__Contact_Language__c", "gig__Language__c"),
        PROJECT_LANGUAGES("gig__Project_Language__c", "gig__Language__c"),
        PROJECT_LANGUAGE_LEVELS("gig__Project_Language__c", "gig__Level__c"),
        PROJECT_STATUSES("gig__Project__c", "gig__Project_Status__c"),
        INVOICE_STATUSES("gig__Invoice__c", "gig__Status__c");

        private final String object;
        private final String field;

        Picklist(String object, String field) {
            this.object = object;
            this.field = field;
        }

        public String getObject() {
            return this.object;
        }

        public String getField() {
            return this.field;
        }
    }
}
