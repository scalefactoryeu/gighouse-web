package eu.scalefactory.service.thematchbox;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Value;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
public class GetSuggestionsJsonRequest {
    @JsonProperty
    private List<String> keywords;
    @JsonProperty
    private  String language;
    @JsonProperty
    private  String type;
    public GetSuggestionsJsonRequest() {
        //NEEDED FOR Jackson
    }

}
