package eu.scalefactory.service.thematchbox;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder
@AllArgsConstructor
public class TMBAutocompleteRequest {
    @JsonProperty
    public String text;
    @JsonProperty
    public String lang;
    @JsonProperty
    public List<String> conceptTypes;
}
