
package eu.scalefactory.service.thematchbox;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
public class GetAutocompleteJsonRequest {
    @JsonProperty
    private String text;
    @JsonProperty
    private String lang;
    @JsonProperty
    private List<String> conceptTypes;

    public GetAutocompleteJsonRequest(){
        //NEEDED FOR Jackson
    }
}
