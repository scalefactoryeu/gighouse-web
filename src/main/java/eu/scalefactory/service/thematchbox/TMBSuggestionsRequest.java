package eu.scalefactory.service.thematchbox;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder
@AllArgsConstructor
public class TMBSuggestionsRequest {
    @JsonProperty
    public List<String> keywords;
    @JsonProperty
    public String language;
    @JsonProperty
    public List<String> queryItemTypes;

}
