package eu.scalefactory.service.thematchbox;

import eu.scalefactory.service.TheMatchboxService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TmbConfig {

    @Value("${tmb.authenticationUrl}")
    private String authenticationUrl;

    @Value("${tmb.baseUrl}")
    private String tmbBaseUrl;

    @Value("${tmb.client.id}")
    private String tmbClientId;

    @Value("${tmb.client.secret}")
    private String tmbClientSecret;

    @Value("${tmb.audience}")
    private String tmbAudience;

    @Value("${tmb.grant.type}")
    private String tmbGrantType;


    @Bean(name = "theMatchboxService")
    public TheMatchboxService theMatchboxService() {
        return new TheMatchboxService(tmbTokenProvider(), tmbBaseUrl, tmbAudience);
    }

    public TmbTokenProvider tmbTokenProvider() {
        return new TmbTokenProvider(authenticationUrl, tmbClientId, tmbClientSecret, tmbGrantType, tmbAudience);
    }

}
