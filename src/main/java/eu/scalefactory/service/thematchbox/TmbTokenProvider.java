package eu.scalefactory.service.thematchbox;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.LinkedHashMap;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

public class TmbTokenProvider {

    private static final Logger LOGGER = LoggerFactory.getLogger(TmbTokenProvider.class);

    private final String tmbAuthenticationURL;
    private final String clientId;
    private final String secret;
    private final String grantType;
    private final String audience;
    private String currentAccessToken = null;


    public TmbTokenProvider(String tmbAuthenticationURL, String clientId, String secret, String grantType, String audience) {
        this.tmbAuthenticationURL = tmbAuthenticationURL;
        this.clientId = clientId;
        this.secret = secret;
        this.grantType = grantType;
        this.audience = audience;
    }

    public String getAccessToken() {
        loadAccessToken();
        return currentAccessToken;
    }

    public void loadAccessToken() {

        LOGGER.info("Getting Management AccessToken from TMB");
        LOGGER.info("tmbAuthenticationURL:" + tmbAuthenticationURL);
        LOGGER.info("clientId:" + clientId);
        LOGGER.info("secret:" + secret);
        LOGGER.info("grantType:" + grantType);
        LOGGER.info("audience:" + audience);

        MultiValueMap<String, String> body = new LinkedMultiValueMap();
        body.add("client_id", clientId);
        body.add("client_secret", secret);
        body.add("grant_type", grantType);
        body.add("audience", audience);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", APPLICATION_JSON_VALUE);

        HttpEntity<?> entity = new HttpEntity<Object>(body, headers);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<LinkedHashMap> response = restTemplate.exchange(
            tmbAuthenticationURL, HttpMethod.POST, entity, LinkedHashMap.class);

        LinkedHashMap responseBody = response.getBody();
        LOGGER.info("response:" + responseBody);
        LOGGER.info("access token:" + responseBody.get("access_token"));

        currentAccessToken = (String) responseBody.get("access_token");

    }

    @Scheduled(cron = "0 0 0 * * *")
    public void refreshAccessToken() {
        currentAccessToken = null;
        loadAccessToken();
    }
}
