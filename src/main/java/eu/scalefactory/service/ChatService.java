package eu.scalefactory.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.twilio.Twilio;
import com.twilio.base.ResourceSet;
import com.twilio.jwt.accesstoken.AccessToken;
import com.twilio.jwt.accesstoken.ChatGrant;
import com.twilio.rest.conversations.v1.Conversation;
import com.twilio.rest.conversations.v1.conversation.Participant;
import eu.scalefactory.config.ApplicationProperties;
import eu.scalefactory.domain.*;
import eu.scalefactory.repository.*;
import eu.scalefactory.security.SecurityUtils;
import eu.scalefactory.service.dto.ChatParticipantDto;
import eu.scalefactory.service.dto.ChatProjectDto;
import eu.scalefactory.service.dto.ConversationDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;

@Service
@Transactional
public class ChatService {
    private static final Logger LOG = LoggerFactory.getLogger(ChatService.class);


    private final ContactRepository contactRepository;
    private final ContactUserSettingRepository contactUserSettingRepository;
    private final TwilioSettingsRepository twilioSettingsRepository;
    private final ProjectRepository projectRepository;
    private final AccountRepository accountRepository;
    private final CandidateRepository candidateRepository;
    private final ProjectMemberRepository projectMemberRepository;
    private final ObjectMapper objectMapper;
    private final ContactHelper contactHelper;

    private String twilioSettingsInstance;
    private TwilioSettings twilioSettings;
    private boolean isInitialized = false;

    public ChatService(ApplicationProperties props, ContactRepository contactRepository, ContactUserSettingRepository contactUserSettingRepository, TwilioSettingsRepository twilioSettingsRepository, ProjectRepository projectRepository, AccountRepository accountRepository, CandidateRepository candidateRepository, ProjectMemberRepository projectMemberRepository, ObjectMapper objectMapper, ContactHelper contactHelper) {
        this.contactRepository = contactRepository;
        this.contactUserSettingRepository = contactUserSettingRepository;
        this.twilioSettingsRepository = twilioSettingsRepository;
        this.projectRepository = projectRepository;
        this.accountRepository = accountRepository;
        this.candidateRepository = candidateRepository;
        this.projectMemberRepository = projectMemberRepository;
        this.objectMapper = objectMapper;
        this.twilioSettingsInstance = props.getTwilio().getInstance();
        this.contactHelper = contactHelper;
    }

    public ConversationDto getConversation(String projectId, String freelancerId, boolean admin) throws JsonProcessingException {
        initializeTwilio();

        String currentUserLogin = SecurityUtils.getCurrentUserLogin()
            .orElseThrow(() -> new RuntimeException("Failed to get current user login"));

        Contact myContact = contactHelper.getCurrentUserContact();
        Optional<ContactUserSetting> contactUserSettings = contactUserSettingRepository.findByContact(myContact);

        ConversatonAttributes attributes = new ConversatonAttributes();

        if (admin) {
            attributes.setProjectId(projectId);
            attributes.getParticipants().add(freelancerId);
            attributes.getParticipants().add(myContact.getId());
        } else {
            attributes.setProjectId(projectId);
            attributes.getParticipants().add(myContact.getId());
            attributes.getParticipants().add(freelancerId);
        }


        String attributesJson = objectMapper.writeValueAsString(attributes);

        ResourceSet<Conversation> conversations = Conversation.reader().read();
        Conversation conversation = null;

        for (Conversation c : conversations) {
            ConversatonAttributes a = objectMapper.readValue(c.getAttributes(), ConversatonAttributes.class);

            if (attributes.equals(a)) {
                conversation = c;
                break;
            }
        }

        if (conversation == null) {
            String conversationName;
            if (admin) {
                conversationName = projectId + " / " + freelancerId + "-" + myContact.getId();
            } else {
                conversationName = projectId + " / " + myContact.getId() + "-" + freelancerId;
            }


            conversation = Conversation.creator()
                .setMessagingServiceSid(twilioSettings.getMessageServiceId())
                .setUniqueName(conversationName)
                .setFriendlyName(conversationName)
                .setAttributes(attributesJson)
                .create();

            Participant.creator(conversation.getSid())
                .setIdentity(myContact.getId())
                .create();

            Participant.creator(conversation.getSid())
                .setIdentity(freelancerId)
                .create();
        }

        return new ConversationDto(conversation.getSid());
    }

    public AccessTokenResponse getAccessToken(String identity) {
        initializeTwilio();

        if (identity == null) {
            Contact myContact = contactHelper.getCurrentUserContact();
            identity = myContact.getId();
        }

        ChatGrant grant = new ChatGrant();
        grant.setServiceSid(twilioSettings.getChatServiceId());

        AccessToken token = new AccessToken.Builder(twilioSettings.getAccountSid(), twilioSettings.getApiKeySid(), twilioSettings.getApiKeySecret())
            .identity(identity)
            .grant(grant)
            .build();

        return new AccessTokenResponse(identity, token.toJwt());
    }

    public ConversationDto getAdminConversation(String projectId) throws JsonProcessingException {
        initializeTwilio();

        Contact currentUserContact = contactHelper.getCurrentUserContact();
        SalesforceUser owner = projectRepository.findById(projectId).orElseThrow(() -> new RuntimeException("project not found")).getOwner();
        ContactUserSetting cus = contactUserSettingRepository.findByUser(owner).orElseThrow();

        return getConversation(projectId, cus.getContact().getId(), true);
    }

    public List<ChatProjectDto> getProjects() {
        Contact currentUserContact = contactHelper.getCurrentUserContact();
        Optional<ContactUserSetting> contactUserSettings = contactUserSettingRepository.findByContact(currentUserContact);
//        SalesforceUser user = contactUserSettingRepository.findAll().get(0).getUser();
        List<ChatProjectDto> chatProjects = new ArrayList<>();

        LOG.info(contactUserSettings.toString());

        // gighouse admin
        if (contactUserSettings.isPresent()) {
            List<Project> projects = projectRepository.findAllByOwnerAndArchivedDateIsNullOrStatusIsNotIn(contactUserSettings.get().getUser(), asList(Project.STATUS_CANCELLED, Project.STATUS_CLOSED));
            chatProjects = projects.stream()
                .filter(p -> p.getArchivedDate() == null)
                .map(this::mapProjectToChatProjectDto)
                .collect(Collectors.toList());

            // customer
        } else {
            chatProjects = projectRepository.findAllByAccountAndStatusNotIn(currentUserContact.getAccount(), asList(Project.STATUS_DRAFT, Project.STATUS_CLOSED, Project.STATUS_CANCELLED))
                .stream()
                .map(this::mapProjectToChatProjectDto)
                .collect(Collectors.toList());
        }

        return chatProjects;
    }

    private ChatProjectDto mapProjectToChatProjectDto(Project project) {
        String clearProjectName = project.getName();
        if (clearProjectName != null) {
            String[] splittedString = clearProjectName.split("-");
            if (splittedString.length > 1) {
                clearProjectName = splittedString[1];
            }

        }
        return new ChatProjectDto(project.getId(), clearProjectName);
    }

    public List<ChatParticipantDto> getChatParticipants(String projectId) {
        Contact currentUserContact = contactHelper.getCurrentUserContact();
        Optional<ContactUserSetting> contactUserSettings = contactUserSettingRepository.findByContact(currentUserContact);

        List<ChatParticipantDto> chatParticipants = new ArrayList<>();

        // gighouse admin
        if (contactUserSettings.isPresent()) {
            Project project = projectRepository.findById(projectId).orElseThrow();
            List<Contact> candidates = project.getCandidates().stream().map(Candidate::getContact).collect(Collectors.toList());
            List<Contact> members = project.getMembers().stream().map(ProjectMember::getContact).collect(Collectors.toList());
            Set<Contact> customers = project.getAccount().getContacts();

            chatParticipants = Stream.of(candidates, members)
                .flatMap(Collection::stream)
                .map(c -> new ChatParticipantDto(c.getId(), c.getName(), c.getRating(), ChatParticipantDto.Type.FREELANCER))
                .sorted(Comparator.comparing(ChatParticipantDto::getName))
                .collect(Collectors.toList());

            chatParticipants.addAll(customers.stream()
                .map(c -> new ChatParticipantDto(c.getId(), c.getName(), null, ChatParticipantDto.Type.CUSTOMER))
                .sorted(Comparator.comparing(ChatParticipantDto::getName))
                .collect(Collectors.toList()));

            // customer
        } else {
            List<Candidate> candidates = candidateRepository.findAllByProjectExternalIdAndStatusInAndContactStatusIn(projectId,
                asList(Candidate.STATUS_CONSIDERED_BY_CUSTOMER, Candidate.STATUS_INTERVIEW_PLANNED, Candidate.STATUS_ACCEPTED),
                singletonList(Candidate.CONTACT_STATUS_INTERESTED));

            List<ProjectMember> projectMembers = projectMemberRepository.findAllByProjectExternalId(projectId);

            List<ChatParticipantDto> freelancers = new ArrayList<>();

            freelancers.addAll(candidates.stream()
                .map(c -> new ChatParticipantDto(c.getContact().getId(), c.getContact().getName(), c.getContact().getRating(), ChatParticipantDto.Type.FREELANCER))
                .collect(Collectors.toList()));

            freelancers.addAll(projectMembers.stream()
                .map(pm -> new ChatParticipantDto(pm.getContact().getId(), pm.getContact().getName(), pm.getContact().getName(), ChatParticipantDto.Type.FREELANCER))
                .collect(Collectors.toList()));

            chatParticipants = freelancers.stream().distinct().collect(toList());
        }

        return chatParticipants;
    }

    private synchronized TwilioSettings getTwilioSettings() {
        if (twilioSettings == null) {
            this.twilioSettings = twilioSettingsRepository.findByInstance(twilioSettingsInstance).orElseThrow();
        }

        return this.twilioSettings;
    }

    private synchronized void initializeTwilio() {
        if (!isInitialized) {
            Twilio.init(getTwilioSettings().getAccountSid(), getTwilioSettings().getAuthToken());
            this.isInitialized = true;
        }
    }

    @Getter
    @Setter
    @AllArgsConstructor
    public static class AccessTokenResponse {
        private String identity;
        private String token;
    }

    @Data
    public static class ConversatonAttributes {
        private String projectId;
        private List<String> participants = new ArrayList<>();
    }
}
