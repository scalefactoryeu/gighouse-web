package eu.scalefactory.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.scalefactory.service.dto.AutocompleteDto;
import eu.scalefactory.service.dto.SuggestionDto;
import eu.scalefactory.service.thematchbox.TMBAutocompleteRequest;
import eu.scalefactory.service.thematchbox.TMBSuggestionsRequest;
import eu.scalefactory.service.thematchbox.TmbTokenProvider;
import eu.scalefactory.service.thematchbox.GetAutocompleteJsonRequest;
import eu.scalefactory.service.thematchbox.GetSuggestionsJsonRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.List;

import static java.util.Arrays.asList;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Service
@Transactional
public class TheMatchboxService {
    private Logger LOGGER = LoggerFactory.getLogger(TheMatchboxService.class);


    private final TmbTokenProvider accessTokenProvider;
    private final String baseUrl;
    private final String apiAudience;

    public TheMatchboxService(TmbTokenProvider tmbTokenProvider, String baseUrl, String apiAudience) {
        this.accessTokenProvider = tmbTokenProvider;
        this.baseUrl = baseUrl;
        this.apiAudience = apiAudience;
    }

    public List<SuggestionDto> getSuggestions(GetSuggestionsJsonRequest jsonRequest) {
        TMBSuggestionsRequest tmbSuggestionsRequest = TMBSuggestionsRequest.builder()
            .keywords(jsonRequest.getKeywords())
            .language(getLanguageMapping(jsonRequest.getLanguage()))
            .queryItemTypes(Collections.singletonList(jsonRequest.getType()))
            .build();

        HttpEntity<TMBSuggestionsRequest> request = new HttpEntity<>(tmbSuggestionsRequest, createAuthorizedHeaders());

        try {
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<String> response = restTemplate.exchange(baseUrl + "/api/hrmatching/suggest/suggestions", HttpMethod.POST,
                request,
                String.class);
            if (response.getStatusCode().equals(HttpStatus.OK)) {
                try {
                    ObjectMapper objectMapper = new ObjectMapper();
                    SuggestionDto[] suggestionResponses = objectMapper.readValue(response.getBody(), SuggestionDto[].class);
                    if (suggestionResponses != null) {
                        return asList(suggestionResponses);
                    }
                } catch (Exception e) {
                    LOGGER.error("Error in parsing JSON: " + response.getBody());
                }
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }

        return Collections.emptyList();

    }

    public List<AutocompleteDto> getAutocomplete(GetAutocompleteJsonRequest jsonRequest) {
        TMBAutocompleteRequest tmbAutocompleteRequest = TMBAutocompleteRequest.builder()
            .text(jsonRequest.getText())
            .lang(getLanguageMapping(jsonRequest.getLang()))
            .conceptTypes(jsonRequest.getConceptTypes())
            .build();

        HttpEntity<TMBAutocompleteRequest> request = new HttpEntity<>(tmbAutocompleteRequest, createAuthorizedHeaders());

        try {
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<String> response = restTemplate.exchange(baseUrl + "/api/hrmatching/findPrefix", HttpMethod.POST,
                request,
                String.class);
            if (response.getStatusCode().equals(HttpStatus.OK)) {
                try {
                    ObjectMapper objectMapper = new ObjectMapper();
                    AutocompleteDto[] autocompleteResponses = objectMapper.readValue(response.getBody(), AutocompleteDto[].class);
                    if (autocompleteResponses != null) {
                        return asList(autocompleteResponses);
                    }
                } catch (Exception e) {
                    LOGGER.error("Error in parsing JSON: " + response.getBody());
                }
            }


        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }

        return Collections.emptyList();
    }

    private HttpHeaders createAuthorizedHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", APPLICATION_JSON_VALUE);
        headers.add(AUTHORIZATION, "Bearer " + accessTokenProvider.getAccessToken());
        return headers;
    }

    private String getLanguageMapping(String language) {
        if (language.equals("nl")) {
            return "DUT";
        } else if (language.equals("en")) {
            return "ENG";
        } else if (language.equals("fr")) {
            return "FRE";
        }

        return language;
    }

}
