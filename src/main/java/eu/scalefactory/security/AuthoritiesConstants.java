package eu.scalefactory.security;

/**
 * Constants for Spring Security authorities.
 */
public final class AuthoritiesConstants {

    public static final String ADMIN = "ROLE_ADMIN";

    public static final String GIG_ADMIN = "ROLE_GIG_ADMIN";

    public static final String USER = "ROLE_USER";

    public static final String ANONYMOUS = "ROLE_ANONYMOUS";

    public static final String UNREGISTERED = "ROLE_UNREGISTERED";

    public static final String PENDING_USER = "PENDING_USER";

    private AuthoritiesConstants() {
    }
}
