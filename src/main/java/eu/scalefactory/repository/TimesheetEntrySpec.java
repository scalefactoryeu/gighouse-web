package eu.scalefactory.repository;

import eu.scalefactory.domain.Account;
import eu.scalefactory.domain.Contact_;
import eu.scalefactory.domain.ProjectMember_;
import eu.scalefactory.domain.Project_;
import eu.scalefactory.domain.TimesheetEntry;
import eu.scalefactory.domain.TimesheetEntry_;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TimesheetEntrySpec implements Specification<TimesheetEntry> {

    private final TimesheetFilter filter;

    public TimesheetEntrySpec(TimesheetFilter filter) {
        this.filter = filter;
    }

    @Override
    public Predicate toPredicate(Root<TimesheetEntry> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        Predicate predicate = cb.conjunction();

        predicate.getExpressions().add(
            root.get(TimesheetEntry_.STATUS).in(Arrays.asList(TimesheetEntry.STATUS_SUBMITTED, TimesheetEntry.STATUS_APPROVED, TimesheetEntry.STATUS_REJECTED))
        );

        if (filter.dateFrom != null && filter.dateTo != null) {
            predicate.getExpressions().add(
                cb.between(root.get(TimesheetEntry_.START), filter.dateFrom.atStartOfDay(), filter.dateTo.atTime(23, 59, 59))
            );
        }

        if (filter.freelancers.size() > 0) {
            predicate.getExpressions().add(
                root.get(TimesheetEntry_.PROJECT_MEMBER).get(ProjectMember_.CONTACT).get(Contact_.EXTERNAL_ID).in(filter.freelancers)
            );
        }

        if (filter.projects.size() > 0) {
            predicate.getExpressions().add(
                root.get(TimesheetEntry_.PROJECT).get(Project_.EXTERNAL_ID).in(filter.projects)
            );
        }

        //TODO REWRITE THIS
        if (filter.getProjectAccount() != null) {
            predicate.getExpressions().add(
                cb.equal(root.get(TimesheetEntry_.PROJECT).get(Project_.ACCOUNT), filter.getProjectAccount())
            );
        }

        return predicate;
    }

    @Getter
    @Setter
    public static class TimesheetFilter {
        private LocalDate dateFrom;
        private LocalDate dateTo;
        private List<String> freelancers = new ArrayList<>();
        private List<String> projects = new ArrayList<>();
        private Account projectAccount;
    }

}
