package eu.scalefactory.repository;

import eu.scalefactory.domain.Account;
import eu.scalefactory.domain.Contact;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

public interface ContactRepository extends JpaRepository<Contact, String>, JpaSpecificationExecutor<Contact> {

    @EntityGraph(attributePaths = { "account", "skills", "profiles", "profiles.profile", "languages" })
    Page<Contact> findAllByRecordTypeName(Pageable pageable, String recordTypeName);

    Optional<Contact> findByEmailAndRecordTypeName(String email, String recordTypeName);

    Page<Contact> findAllByAccountAndExternalIdNotOrderByExternalId(Account account, String sfid, Pageable pageable);
}
