package eu.scalefactory.repository;

import eu.scalefactory.domain.SettingPicklistValue;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SettingPicklistValueRepository extends JpaRepository<SettingPicklistValue, String> {

    List<SettingPicklistValue> findAllByObjectNameAndFieldName(String objectName, String fieldName);

    List<SettingPicklistValue> findAllByObjectNameAndFieldNameAndIsDeletedFalse(String objectName, String fieldName);
}
