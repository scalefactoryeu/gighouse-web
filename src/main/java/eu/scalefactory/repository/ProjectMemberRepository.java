package eu.scalefactory.repository;

import eu.scalefactory.domain.ProjectMember;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProjectMemberRepository extends JpaRepository<ProjectMember, String> {

    @EntityGraph(attributePaths = { "contact", "project", "account" })
    List<ProjectMember> findAllByProjectExternalId(String projectId);

}
