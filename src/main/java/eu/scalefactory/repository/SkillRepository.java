package eu.scalefactory.repository;

import eu.scalefactory.domain.Skill;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SkillRepository extends JpaRepository<Skill, String> {

    List<Skill> findAllByVisibleInMobileAppTrue();

}
