package eu.scalefactory.repository;

import eu.scalefactory.domain.SettingGeneral;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SettingGeneralRepository extends JpaRepository<SettingGeneral, String> {

}
