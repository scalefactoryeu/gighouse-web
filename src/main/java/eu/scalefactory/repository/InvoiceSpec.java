package eu.scalefactory.repository;

import eu.scalefactory.domain.*;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class InvoiceSpec implements Specification<Invoice> {

    private final InvoiceFilter filter;

    public InvoiceSpec(InvoiceFilter filter) {
        this.filter = filter;
    }

    @Override
    public Predicate toPredicate(Root<Invoice> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        List<String> types = Arrays.asList(Invoice.TYPE_SALE,Invoice.TYPE_CREDIT_NOTE_SALE);
        Predicate predicate = cb.conjunction();
        predicate.getExpressions().add(
            cb.notEqual(root.get(Invoice_.STATUS), Invoice.STATUS_DRAFT)
        );
        predicate.getExpressions().add(
            cb.notEqual(root.get(Invoice_.STATUS), Invoice.STATUS_DRAFT_END_OF_MONTH)
        );
        predicate.getExpressions().add(
            cb.notEqual(root.get(Invoice_.STATUS), Invoice.STATUS_ERROR)
        );

        predicate.getExpressions().add(
            cb.equal(root.get(Invoice_.TYPE), Invoice.TYPE_SALE)
        );

        predicate.getExpressions().add(
            root.get(Invoice_.TYPE).in(types)
        );


        if (filter.accountId != null) {
            predicate.getExpressions().add(
                cb.equal(root.get(Invoice_.ACCOUNT).get(Account_.EXTERNAL_ID), filter.accountId)
            );
        }

        if (filter.invoiceDateFrom != null) {
            predicate.getExpressions().add(
                cb.greaterThanOrEqualTo(root.get(Invoice_.INVOICE_DATE), filter.invoiceDateFrom)
            );
        }

        if (filter.invoiceDateTo != null) {
            predicate.getExpressions().add(
                cb.lessThanOrEqualTo(root.get(Invoice_.INVOICE_DATE), filter.invoiceDateTo)
            );
        }

        if (filter.statuses.size() > 0) {
            predicate.getExpressions().add(
                root.get(Invoice_.STATUS).in(filter.statuses)
            );
        }

        if (filter.freelancers.size() > 0) {
            predicate.getExpressions().add(
                root.join(Invoice_.LINES).get(InvoiceLine_.PROJECT_MEMBER).get(ProjectMember_.CONTACT).get(Contact_.EXTERNAL_ID).in(filter.freelancers)
            );
        }

        if (filter.projects.size() > 0) {
            predicate.getExpressions().add(
                root.join(Invoice_.LINES).get(InvoiceLine_.PROJECT).get(Project_.EXTERNAL_ID).in(filter.projects)
            );
        }

        return predicate;
    }

    @Getter
    @Setter
    public static class InvoiceFilter {
        private String accountId;
        private LocalDate invoiceDateFrom;
        private LocalDate invoiceDateTo;
        private List<String> statuses = new ArrayList<>();
        private List<String> freelancers = new ArrayList<>();
        private List<String> projects = new ArrayList<>();
    }
}
