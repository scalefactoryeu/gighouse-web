package eu.scalefactory.repository;

import eu.scalefactory.domain.Account;
import eu.scalefactory.domain.TimesheetEntry;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface TimesheetEntryRepository extends JpaRepository<TimesheetEntry, String>, JpaSpecificationExecutor<TimesheetEntry> {

    @Override
//    @EntityGraph(attributePaths = {"rate"})
    List<TimesheetEntry> findAll(Specification<TimesheetEntry> spec);

    List<TimesheetEntry> findAllByProjectAccountAndStatus(Account account, String status);

    List<TimesheetEntry> findAllByProjectExternalIdInAndStatus(List<String> projectIds, String status);

}
