package eu.scalefactory.repository;

import eu.scalefactory.domain.Invoice;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface InvoiceRepository extends JpaRepository<Invoice, String>, JpaSpecificationExecutor<Invoice> {

    @Override
    @EntityGraph(attributePaths = { "lines", "lines.projectMember", "lines.projectMember.contact" })
    List<Invoice> findAll(Specification<Invoice> spec);
}
