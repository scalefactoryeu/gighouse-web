package eu.scalefactory.repository;

import eu.scalefactory.domain.Account;
import eu.scalefactory.domain.Contact;
import eu.scalefactory.domain.Favorite;
import eu.scalefactory.domain.ProjectCustomerTeam;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;
import java.util.Optional;

public interface ProjectCustomerTeamRepository extends JpaRepository<ProjectCustomerTeam, String>, JpaSpecificationExecutor<ProjectCustomerTeam> {

    List<ProjectCustomerTeam> findAllByContact(Contact customer);

}
