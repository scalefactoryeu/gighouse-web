package eu.scalefactory.repository;

import eu.scalefactory.domain.Account;
import eu.scalefactory.domain.Contact;
import eu.scalefactory.domain.RecordType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

public interface RecordTypeRepository  extends JpaRepository<RecordType, String>, JpaSpecificationExecutor<RecordType> {

    Optional<RecordType> findByName(String recordTypeName);

}
