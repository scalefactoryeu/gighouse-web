package eu.scalefactory.repository;

import eu.scalefactory.domain.Account;
import eu.scalefactory.domain.SalesforceUser;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, String> {

    @EntityGraph(attributePaths = { "projects" })
    List<Account> findByOwner(SalesforceUser user);

    //TODO: CHECK IF REPLACES WORK
    @Query(value = "select a from Account a where REPLACE(REPLACE(a.vatNumber,' ', ''),'.','') = ?1 and a.recordType.name = ?2")
    Optional<List<Account>> findByVatNumberAndRecordTypeName(String vatNumber, String recordTypeName);

}
