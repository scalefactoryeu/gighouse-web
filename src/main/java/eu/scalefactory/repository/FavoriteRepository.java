package eu.scalefactory.repository;

import eu.scalefactory.domain.Contact;
import eu.scalefactory.domain.Favorite;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface FavoriteRepository extends JpaRepository<Favorite, String> {

    @EntityGraph(attributePaths = { "freelancer", "customer" })
    List<Favorite> findAllByCustomer(Contact customer);

    Optional<Favorite> findByCustomerAndFreelancer(Contact customer, Contact freelancer);

    void deleteByCustomerAndFreelancerExternalId(Contact customer, String freelancerSfid);

}
