package eu.scalefactory.repository;

import eu.scalefactory.domain.TwilioSettings;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TwilioSettingsRepository extends JpaRepository<TwilioSettings, String> {

    Optional<TwilioSettings> findByInstance(String instance);

}
