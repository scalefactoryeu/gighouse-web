package eu.scalefactory.repository;

import eu.scalefactory.domain.Contact;
import eu.scalefactory.domain.ContactUserSetting;
import eu.scalefactory.domain.SalesforceUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ContactUserSettingRepository extends JpaRepository<ContactUserSetting, String> {

    Optional<ContactUserSetting> findByUser(SalesforceUser user);
    Optional<ContactUserSetting> findByContact(Contact contact);

}
