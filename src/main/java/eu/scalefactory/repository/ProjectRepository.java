package eu.scalefactory.repository;

import eu.scalefactory.domain.Account;
import eu.scalefactory.domain.Project;
import eu.scalefactory.domain.SalesforceUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ProjectRepository extends JpaRepository<Project, String> {

    @Override
//    @EntityGraph(attributePaths = { "projectSkills", "projectSkills.skill", "members", "members.contact", "candidates", "candidates.contact", "account", "account.contacts" })
    Optional<Project> findById(String projectId);

    Page<Project> findAllByAccountAndArchivedDateIsNull(Account account, Pageable pageable);

    Page<Project> findAllByAccountAndArchivedDateIsNotNull(Account account, Pageable pageable);

    Page<Project> findAllByAccountAndStatusNotIn(Account account, List<String> status, Pageable pageable);

    Page<Project> findAllByAccountAndStatusIn(Account account, List<String> status, Pageable pageable);

    Page<Project> findAllByExternalIdInAndStatusNotIn(List<String> projectIds, List<String> status, Pageable pageable);

    Page<Project> findAllByExternalIdInAndStatusIn(List<String> projectIds, List<String> status, Pageable pageable);

    List<Project> findAllByAccountAndStatus(Account account, String status);

    List<Project> findAllByExternalIdInAndStatus(List<String> projectIds, String status);

    @EntityGraph(attributePaths = {"members", "members.contact"})
    List<Project> findAllByAccountAndArchivedDateIsNull(Account account);

    List<Project> findAllByAccountAndStatusNotIn(Account account, List<String> statuses);

    List<Project> findAllByExternalIdInAndStatusNotIn(List<String> projects, List<String> statuses);


    @EntityGraph(attributePaths = {"members", "members.contact"})
    List<Project> findAllByOwnerAndArchivedDateIsNullOrStatusIsNotIn(SalesforceUser salesforceUser, List<String> statuses);

}
