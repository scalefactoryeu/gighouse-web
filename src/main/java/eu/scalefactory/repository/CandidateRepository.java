package eu.scalefactory.repository;

import eu.scalefactory.domain.Account;
import eu.scalefactory.domain.Candidate;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface CandidateRepository extends JpaRepository<Candidate, String>, JpaSpecificationExecutor<Candidate> {

    @EntityGraph(attributePaths = {"project", "contact", "rate"})
    List<Candidate> findAllByProjectExternalIdAndStatus(String projectId, String status);

    @EntityGraph(attributePaths = {"project", "contact", "rate"})
    List<Candidate> findAllByProjectExternalIdInAndStatus(List<String> projectIds, String status);

    @EntityGraph(attributePaths = {"project", "contact", "rate"})
    List<Candidate> findAllByProjectExternalIdAndStatusInAndContactStatusIn(String projectId, List<String> statuses, List<String> contactStatus);

    @EntityGraph(attributePaths = {"project", "contact", "rate"})
    List<Candidate> findAllByProjectExternalIdInAndStatusInAndContactStatusIn(List<String> projectIds, List<String> statuses, List<String> contactStatus);
    @EntityGraph(attributePaths = {"project", "contact"})
    List<Candidate> findAllByProjectAccountAndStatusAndContactStatusIn(Account account, String status, List<String> contactStatus);

    @EntityGraph(attributePaths = {"project", "contact"})
    List<Candidate> findAllByProjectExternalIdInAndStatusAndContactStatusIn(List<String> projectIds, String status, List<String> contactStatus);

    @EntityGraph(attributePaths = {"project", "contact"})
    List<Candidate> findAllByProjectAccountAndProjectStatusInAndStatusAndContactStatusIn(Account account,List<String> projectStatus, String status, List<String> contactStatus);

    @EntityGraph(attributePaths = {"project", "contact"})
    List<Candidate> findAllByProjectExternalIdInAndProjectStatusInAndStatusAndContactStatusIn(List<String> projectIds,List<String> projectStatus, String status, List<String> contactStatus);

    @EntityGraph(attributePaths = {"project", "contact"})
    List<Candidate> findAllByProjectAccountAndStatusInAndContactStatusIn(Account account, List<String> statuses, List<String> contactStatus);

    @EntityGraph(attributePaths = {"project", "contact"})
    List<Candidate> findAllByProjectAccountAndStatusInAndProjectStatusNotIn(Account account, List<String> statuses,List<String> projectStatuses);
    List<Candidate> findAllByProjectExternalIdInAndStatusInAndProjectStatusNotIn(List<String> projectIds, List<String> statuses,List<String> projectStatuses);


    @EntityGraph(attributePaths = {"project", "contact"})
    List<Candidate> findAllByContactExternalId(String freelancerId);

}
