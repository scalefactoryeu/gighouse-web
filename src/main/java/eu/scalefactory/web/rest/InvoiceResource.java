package eu.scalefactory.web.rest;

import eu.scalefactory.repository.InvoiceSpec;
import eu.scalefactory.service.InvoiceService;
import eu.scalefactory.service.TimesheetService;
import eu.scalefactory.service.dto.InvoiceDto;
import eu.scalefactory.service.dto.InvoicePdfDto;
import eu.scalefactory.service.dto.TimesheetDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/api/invoices")
public class InvoiceResource {


    private final InvoiceService invoiceService;
    private final TimesheetService timesheetService;

    public InvoiceResource(InvoiceService invoiceService, TimesheetService timesheetService) {
        this.invoiceService = invoiceService;
        this.timesheetService = timesheetService;
    }

    @GetMapping("/")
    public ResponseEntity<List<InvoiceDto>> getInvoices(InvoiceSpec.InvoiceFilter filter) {
        List<InvoiceDto> invoices = invoiceService.getInvoices(filter);
        return new ResponseEntity<>(invoices, HttpStatus.OK);
    }


    @GetMapping("/invoicePdf/{id}")
    public ResponseEntity<InvoicePdfDto> getInvoicePdf(@PathVariable String id) {
        String blob = invoiceService.geInvoice(id);
        InvoicePdfDto invoicePdfDto = new InvoicePdfDto();
        invoicePdfDto.setInvoiceBlob(blob);
        return new ResponseEntity<>(invoicePdfDto, HttpStatus.OK);
    }

    @GetMapping("/freelancers")
    public List<TimesheetDto.Freelancer> getFreelancers() {
        return timesheetService.getFreelancers();
    }



}
