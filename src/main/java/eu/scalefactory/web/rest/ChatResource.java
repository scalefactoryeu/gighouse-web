package eu.scalefactory.web.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import eu.scalefactory.service.ChatService;
import eu.scalefactory.service.dto.ChatParticipantDto;
import eu.scalefactory.service.dto.ChatProjectDto;
import eu.scalefactory.service.dto.ConversationDto;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/chat")
public class ChatResource {

    private final ChatService chatService;

    public ChatResource(ChatService chatService) {
        this.chatService = chatService;
    }


    @PostMapping("/conversation")
    @ResponseStatus(HttpStatus.OK)
    public ConversationDto getConversation(@RequestBody InitChatRequest request) throws JsonProcessingException {
        return chatService.getConversation(request.getProjectId(), request.getFreelancerId(), false);
    }

    @PostMapping("/access-token")
    @ResponseStatus(HttpStatus.OK)
    public ChatService.AccessTokenResponse getAccessToken(@RequestBody AccessTokenRequest request) {
        return chatService.getAccessToken(request.getIdentity());
    }

    @PostMapping("/conversation/admin")
    public ConversationDto getAdminConversation(@RequestBody InitAdminChatRequest request) throws JsonProcessingException {
        return chatService.getAdminConversation(request.getProjectId());
    }

    @GetMapping("/projects")
    public List<ChatProjectDto> getProjects() {
        return chatService.getProjects();
    }

    @GetMapping("/participants")
    public List<ChatParticipantDto> getParticipants(@RequestParam(name = "projectId") String projectId) {
        return chatService.getChatParticipants(projectId);
    }

    @Getter
    @Setter
    public static class InitChatRequest {
        private String projectId;
        private String freelancerId;
    }

    @Getter
    @Setter
    public static class InitAdminChatRequest {
        private String projectId;
    }


    @Getter
    @Setter
    public static class AccessTokenRequest {
        private String identity;
    }


}
