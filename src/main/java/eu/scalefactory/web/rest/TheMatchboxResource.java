package eu.scalefactory.web.rest;

import eu.scalefactory.service.TheMatchboxService;
import eu.scalefactory.service.dto.AutocompleteDto;
import eu.scalefactory.service.dto.SuggestionDto;
import eu.scalefactory.service.thematchbox.GetAutocompleteJsonRequest;
import eu.scalefactory.service.thematchbox.GetSuggestionsJsonRequest;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/thematchbox")
public class TheMatchboxResource {
    public static final int MAX_AUTOCOMPLETE = 20;

    private final TheMatchboxService theMatchboxService;

    public TheMatchboxResource(TheMatchboxService theMatchboxService) {
        this.theMatchboxService = theMatchboxService;
    }

    @PostMapping("/autocomplete")
    public List<AutocompleteDto> getAutocomplete(@RequestBody GetAutocompleteJsonRequest jsonRequest) {
        List<AutocompleteDto> autocompletes = theMatchboxService.getAutocomplete(jsonRequest);
        return autocompletes.stream().filter(autocompleteDto -> !autocompleteDto.getMatchedTerm().getText().equalsIgnoreCase(jsonRequest.getText())).distinct().limit(MAX_AUTOCOMPLETE).collect(Collectors.toList());

    }

    @PostMapping("/suggestions")
    public List<SuggestionDto> getSuggestions(@RequestBody GetSuggestionsJsonRequest jsonRequest) {
        return theMatchboxService.getSuggestions(jsonRequest);
    }

    public static <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor) {
        Map<Object, Boolean> map = new ConcurrentHashMap<>();
        return t -> map.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }
}


