package eu.scalefactory.web.rest;

import eu.scalefactory.service.PicklistService;
import eu.scalefactory.service.dto.PicklistValueDto;
import eu.scalefactory.web.rest.mapper.PicklistMapper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/lists")
public class PicklistResource {

    private final PicklistService picklistService;
    private final PicklistMapper picklistMapper;

    public PicklistResource(PicklistService picklistService, PicklistMapper picklistMapper) {
        this.picklistService = picklistService;
        this.picklistMapper = picklistMapper;
    }

    @GetMapping("/states")
    public List<PicklistValueDto> getStates() {
        return picklistMapper.settingPicklistValueListToPicklistValueDtoList(
            picklistService.getPicklist(PicklistService.Picklist.STATES));
    }

    @GetMapping("/company-formats")
    public List<PicklistValueDto> getCompanyFormats() {
        return picklistMapper.settingPicklistValueListToPicklistValueDtoList(
            picklistService.getPicklist(PicklistService.Picklist.COMPANY_FORMATS));
    }

    @GetMapping("/project-languages")
    public List<PicklistValueDto> getAllLanguages() {
        return picklistMapper.settingPicklistValueListToPicklistValueDtoList(
            picklistService.getPicklist(PicklistService.Picklist.PROJECT_LANGUAGES));
    }

    @GetMapping("/project-language-levels")
    public List<PicklistValueDto> getAllLevels() {
        return picklistMapper.settingPicklistValueListToPicklistValueDtoList(
            picklistService.getPicklist(PicklistService.Picklist.PROJECT_LANGUAGE_LEVELS));
    }
}
