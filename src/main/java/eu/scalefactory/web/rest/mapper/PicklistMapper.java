package eu.scalefactory.web.rest.mapper;

import eu.scalefactory.domain.SettingPicklistValue;
import eu.scalefactory.service.dto.PicklistValueDto;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class PicklistMapper {
    public PicklistValueDto settingPicklistValueToPicklistValueDTO(SettingPicklistValue picklistValue) {
        PicklistValueDto picklistDTO = new PicklistValueDto();

        picklistDTO.setValue(picklistValue.getValueSf());
        picklistDTO.setLabel(picklistValue.getValueExt());

        return picklistDTO;
    }

    public List<PicklistValueDto> settingPicklistValueListToPicklistValueDtoList(List<SettingPicklistValue> settingPicklistValues) {
        return settingPicklistValues
            .stream()
            .map(this::settingPicklistValueToPicklistValueDTO)
            .collect(Collectors.toList());
    }
}
