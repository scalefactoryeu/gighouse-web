package eu.scalefactory.web.rest;

import eu.scalefactory.domain.Contact;
import eu.scalefactory.service.ProfileService;
import eu.scalefactory.service.SettingGeneralService;
import eu.scalefactory.service.dto.*;
import eu.scalefactory.service.mapper.ProfileMapper;
import io.github.jhipster.web.util.PaginationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.List;
import java.util.logging.Logger;

@RestController
@RequestMapping("/api/profile")
public class ProfileResource {
    private static final Logger LOGGER = Logger.getLogger(ProfileResource.class.getName());

    private final ProfileService profileService;
    private final ProfileMapper profileMapper;
    private final SettingGeneralService settingGeneralService;

    public ProfileResource(ProfileService profileService, ProfileMapper profileMapper, SettingGeneralService settingGeneralService) {
        this.profileService = profileService;
        this.profileMapper = profileMapper;
        this.settingGeneralService = settingGeneralService;
    }

    @GetMapping("/user")
    public UserProfileDto getUserProfile() {
        return profileMapper.contactToUserProfileDTO(
            profileService.getUserProfile()
        );
    }

    @PutMapping("/user")
    public UserProfileDto updateUserProfile(@RequestBody UserProfileDto profile) {
        return profileMapper.contactToUserProfileDTO(
            profileService.updateUserProfile(
                profileMapper.userProfileDTOtoUserProfileData(profile)
            )
        );
    }

    @GetMapping("/company")
    public CompanyProfileDto getCompanyProfile() {
        return profileMapper.accountToCompanyProfileDTO(
            profileService.getCompanyProfile()
        );
    }

    @PutMapping("/company")
    public CompanyProfileDto updateCompanyProfile(@RequestBody CompanyProfileDto profile) {
        return profileMapper.accountToCompanyProfileDTO(
            profileService.updateCompanyProfile(
                profileMapper.companyProfileDTOtoCompanyProfileData(profile)
            )
        );
    }

    @GetMapping("/coworkers")
    public ResponseEntity<List<UserProfileDto>> getCoworkers(Pageable pageable) {
        Page<Contact> coworkerContacts = profileService.getCoworkers(pageable);

        Page<UserProfileDto> page = coworkerContacts.map(profileMapper::contactToUserProfileDTO);

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/gighouse-contact")
    public GighouseContactDto getGighouseContact() {
        return profileService.getGighouseContact();
    }

    @PostMapping("/register")
    public void register(@RequestBody RegisterDto registerData) {
        profileService.register(registerData);
    }

    @GetMapping("/user-agreement")
    public UserAgreementDto getUserAgreement() {
        String userAgreement = settingGeneralService.getGeneralSetting().getUserAgreement();
        UserAgreementDto dto = new UserAgreementDto();
        dto.setUserAgreement(userAgreement);
        return dto;
    }

    @GetMapping("/privacy-policy")
    public PrivacyPolicyDto getPrivacyPolicy() {
        String privacyPolicy = settingGeneralService.getGeneralSetting().getPrivacyPolicy();
        PrivacyPolicyDto dto = new PrivacyPolicyDto();
        dto.setPrivacyPolicy(privacyPolicy);
        return dto;
    }

    @GetMapping("/existing-vat-number/{vatNumber}")
    public boolean getExistingVATNumber(@PathVariable(value = "vatNumber") String vatNumber) {
        return profileService.duplicateVATNumber(vatNumber);
    }
}
