package eu.scalefactory.web.rest;

import com.fasterxml.jackson.annotation.JsonInclude;
import eu.scalefactory.domain.Project;
import eu.scalefactory.repository.ProjectRepository;
import eu.scalefactory.service.ProjectService;
import io.github.jhipster.web.util.PaginationUtil;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/projects")
public class ProjectResource {

    private final ProjectRepository projectRepository;
    private final ProjectService projectService;

    public ProjectResource(ProjectRepository projectRepository, ProjectService projectService) {
        this.projectRepository = projectRepository;
        this.projectService = projectService;
    }

    @GetMapping("/")
    public ResponseEntity<List<ProjectResponse>> getProjects(Pageable pageable, Boolean archived) {
        Map<String, String> statusesMap = projectService.getStatusesMap();
        Page<ProjectResponse> page = projectService.getProjects(pageable, archived).map(p -> toProjectResponse(p, statusesMap, null, null));

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ProjectResponse getProject(@PathVariable("id") String id) {
        Project project = projectService.getProject(id);
        return toProjectResponse(project, projectService.getStatusesMap(), projectService.getProjectPositiveSkills(project), projectService.getProjectLanguages(id));
    }

    @PostMapping("/")
    public ProjectResponse createProject(@RequestBody ProjectService.ProjectData data) {

        return toProjectResponse(projectService.createProject(data), projectService.getStatusesMap(), data.getPositiveSkills(), data.getProjectLanguages());
    }

    @PutMapping("/{id}")
    public ProjectResponse updateProject(@PathVariable("id") String id, @RequestBody ProjectService.ProjectData data) {
        return toProjectResponse(projectService.updateProject(id, data), projectService.getStatusesMap(), data.getPositiveSkills(), data.getProjectLanguages());
    }

    @PostMapping("/{id}/archive")
    public void archiveProject(@PathVariable("id") String id) {
        projectService.archiveProject(id);
    }

    @DeleteMapping("/{id}")
    public void deleteProject(@PathVariable("id") String id) {
        projectService.deleteProject(id);
    }

    private ProjectResponse toProjectResponse(Project project, Map<String, String> statusesMap, List<String> positiveSkills, List<ProjectService.ProjectLanguageData> projectLanguages) {
        ProjectResponse response = new ProjectResponse();
        String[] splittedName = project.getName().split(" - ", 2);
        response.setName(splittedName.length > 1 ? splittedName[1] : splittedName[0]);
        response.setId(project.getId());
        response.setDescription(project.getDescription());
        response.setStartDate(project.getStartDate());
        response.setEndDate(project.getEndDate());
        response.setSentDate(project.getSentDate());
        response.setRequestedStartDate(project.getRequestedStartDate());
        response.setPostalCode(project.getPostalCode());
        response.setCity(project.getCity());
        response.setCountry(project.getCountry());
        response.setState(project.getState());
        response.setTeaser1(project.getTeaser1());
        response.setTeaser2(project.getTeaser2());
        response.setTeaser3(project.getTeaser3());
        response.setMaxBudget(project.getMaxBudget());
        response.setDaysPerWeek(project.getDaysPerWeek());
        response.setHomeWork(project.getHomeWork());
        response.setEstimatedProjectDuration(project.getEstimatedProjectDuration());
        response.setArchivedDate(project.getArchivedDate());
        response.setPositiveSkills(positiveSkills);
        response.setProjectLanguages(projectLanguages);
        response.setExpertise(project.getExpertise() != null ? project.getExpertise().getId() : null);
        response.setStatus(statusesMap.get(project.getStatus()));

        return response;
    }

    @Setter
    @Getter
    private static class ProjectResponse {
        private String id;
        private String name;
        private String description;
        private LocalDate startDate;
        private LocalDate endDate;
        private LocalDate sentDate;
        private LocalDate requestedStartDate;
        private String postalCode;
        private String city;
        private String country;
        private String state;
        private String status;
        private String teaser1;
        private String teaser2;
        private String teaser3;
        private Double maxBudget;
        private Double daysPerWeek;
        private Boolean homeWork;
        private Double estimatedProjectDuration;
        private LocalDate archivedDate;
        private String expertise;

        @JsonInclude(JsonInclude.Include.NON_NULL)
        private List<String> positiveSkills = new ArrayList<>();

        @JsonInclude(JsonInclude.Include.NON_NULL)
        private List<ProjectService.ProjectLanguageData> projectLanguages = new ArrayList<>();
    }
}
