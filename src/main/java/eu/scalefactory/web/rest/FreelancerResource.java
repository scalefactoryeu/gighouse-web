package eu.scalefactory.web.rest;

import eu.scalefactory.service.FreelancerService;
import eu.scalefactory.service.dto.ChatParticipantDto;
import eu.scalefactory.service.dto.FreelancerDto;
import eu.scalefactory.service.dto.MatchesProjectDto;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/freelancers")
public class FreelancerResource {

    private final FreelancerService freelancerService;

    public FreelancerResource(FreelancerService freelancerService) {
        this.freelancerService = freelancerService;
    }

    @GetMapping("/matches")
    public List<FreelancerDto> getMatches(@RequestParam(value = "projectId") String projectId, @RequestParam(value = "all", required = false) Boolean all) {
        if (all == null) {
            all = false;
        }
        return freelancerService.getMatches(projectId, all);
    }

    @GetMapping("/accepted-for-project")
    public List<FreelancerDto> getAcceptedForProjects(@RequestParam(value = "projectId") String projectId, @RequestParam(value = "all", required = false) Boolean all) {
        if (all == null) {
            all = false;
        }
        return freelancerService.getAcceptedForProjects(projectId, all);
    }

    @GetMapping("/matches/projects")
    public List<MatchesProjectDto> getProjectsForMatches() {
        return freelancerService.getAllProjectsWithMatches();
    }

    @GetMapping("/matches/{id}")
    public FreelancerDto getCandidate(@PathVariable("id") String id) {
        return freelancerService.getCandidate(id);
    }

    @PostMapping("/matches/{id}/accept")
    public void acceptCandidate(@PathVariable("id") String id) {
        freelancerService.acceptCandidate(id);
    }

    @PostMapping("/matches/{id}/reject")
    public void rejectCandidate(@PathVariable("id") String id, @RequestBody RejectCandidateRequest body) {
        freelancerService.rejectCandidate(id, body.getReason());
    }

    @GetMapping("/favorites")
    public List<FreelancerDto> getFavorites() {
        return freelancerService.getFavorites();
    }

    @GetMapping("/favorites/{favoriteId}")
    public FreelancerDto getFavorite(@PathVariable("favoriteId") String favoriteId) {
        return freelancerService.getFavorite(favoriteId);
    }

    @PostMapping("/favorites")
    public void createFavorite(@RequestBody CreateFavoriteRequest request) {
        freelancerService.createFavorite(request.getContactId());
    }

    @DeleteMapping("/favorites/{freelancerContactId}")
    public void deleteFavorite(@PathVariable("freelancerContactId") String freelancerContactId) {
        freelancerService.deleteFavorite(freelancerContactId);
    }

    @GetMapping("/accepted")
    public List<FreelancerDto> getAccepted() {
        return freelancerService.getAccepted();
    }

    @GetMapping("/rejected")
    public List<FreelancerDto> getRejected() {
        return freelancerService.getRejected();
    }

    @GetMapping("/proposed-never-reviewed")
    public List<FreelancerDto> getProposedNeverReviewed() {
        return freelancerService.getProposedNeverReviewed();
    }

    @Deprecated
    @GetMapping("/chat-freelancers")
    public List<ChatParticipantDto> getChatFreelancers(@RequestParam(value = "projectId") String projectId) {
        return freelancerService.getChatFreelancers(projectId);
    }

    @Getter
    @Setter
    private static class CreateFavoriteRequest {
        private String contactId;
    }

    @Getter
    @Setter
    private static class RejectCandidateRequest {
        private String reason;
    }

}
