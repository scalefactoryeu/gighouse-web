package eu.scalefactory.web.rest;

import eu.scalefactory.service.DashboardService;
import eu.scalefactory.service.dto.FollowUpActionsDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/dashboard")
public class DashboardResource {
    private final DashboardService dashboardService;

    public DashboardResource(DashboardService dashboardService) {
        this.dashboardService = dashboardService;
    }

    @GetMapping("/follow-up-actions")
    public FollowUpActionsDto getFollowUpActions() {
        return dashboardService.getFollowUpActions();
    }

}
