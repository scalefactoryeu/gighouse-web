package eu.scalefactory.web.rest;

import eu.scalefactory.service.SkillService;
import eu.scalefactory.service.dto.SkillDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/skills")
public class SkillResource {

    private final SkillService skillService;

    public SkillResource(SkillService skillService) {
        this.skillService = skillService;
    }

    @GetMapping("/")
    public List<SkillDto> getAllSkills() {
        return skillService.getAllSkills();
    }
}


