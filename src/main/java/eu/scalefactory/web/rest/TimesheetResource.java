package eu.scalefactory.web.rest;

import eu.scalefactory.domain.TimesheetEntry;
import eu.scalefactory.repository.TimesheetEntrySpec;
import eu.scalefactory.service.TimesheetService;
import eu.scalefactory.service.UserService;
import eu.scalefactory.service.dto.TimesheetDto;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/timesheets")
public class TimesheetResource {
    private final Logger log = LoggerFactory.getLogger(UserService.class);


    private final TimesheetService timesheetService;

    public TimesheetResource(TimesheetService timesheetService) {
        this.timesheetService = timesheetService;
    }

    @GetMapping("/")
    public ResponseEntity<TimesheetDto> getTimesheets(TimesheetEntrySpec.TimesheetFilter filter) {
        return new ResponseEntity<>(timesheetService.getTimesheets(filter), HttpStatus.OK);
    }

    @GetMapping("/freelancers")
    public List<TimesheetDto.Freelancer> getFreelancers() {
        return timesheetService.getFreelancers();
    }

    @PostMapping("/approve")
    public void approve(@RequestBody ApproveRejectRequest request) {
        timesheetService.changeEntryStatus(request.getTimesheetEntryIds(), TimesheetEntry.STATUS_APPROVED, "");
    }

    @PostMapping("/reject")
    public void reject(@RequestBody ApproveRejectRequest request) {
        log.info("Rejectmessage {}",request.rejectMessage);
        timesheetService.changeEntryStatus(request.getTimesheetEntryIds(), TimesheetEntry.STATUS_REJECTED, request.getRejectMessage());
    }


    @Getter
    @Setter
    public static class ApproveRejectRequest {
        private List<String> timesheetEntryIds = new ArrayList<>();
        private String rejectMessage;
    }
}
